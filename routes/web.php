<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test/Adomicile', 'Adomicile@index');
Route::get('/test/Check', 'Check@index');
Route::get('/test/Transfer', 'Transfer@index');


Route::get('/valide/{id}', 'Adomicile@update');
Route::post('/test/send', 'EmailController@send')->name('Admin.mail');
Route::get('admin/sendbox', 'EmailController@show')->name('Admin.send.mail');
Route::get('admin/sendbox/{Email}', 'EmailController@showmail');
Route::get('admin/trash', 'EmailController@showtrash')->name('Admin.trash');
Route::get('admin/destroy/{Email}', 'EmailController@destroy');
Route::get('admin/chat/', 'EmailController@chat')->name('chat.index');



Route::post('/test/payer', 'ProductsController@payer')->name('payer');

Route::get('/test/Strip', 'Strip@index');
Route::post('/charge','strip@charge');

Route::get('/test/Paypal', 'Paypal@paypal');
Route::get('/status', 'Paypal@status')->name('status');
Route::get('/canceled', 'Paypal@canceled')->name('canceled');

Route::get('/test', 'ProductsController@index');

Route::get('show/{id}','ProductsController@show')->name('store.show');
Route::get('cart', 'ProductsController@showCart')->name('cart');
Route::get('add-to-cart/{Product}', 'ProductsController@addToCart')->name('cart.add');;
Route::patch('update-cart', 'ProductsController@update');

Route::delete('remove-from-cart', 'ProductsController@remove');



Route::get('fr','web\fr\IndexController@index')->name('web.fr.index');
Route::get('de','web\de\IndexController@index')->name('web.de.index');
Route::get('en','web\en\IndexController@index')->name('web.en.index');

Route::get('products/{id}','ProductsController@products_seo')->name('products.seo');

Route::get('animation_caroucel', 'Animation@animation_caroucel');
Route::get('story', 'Animation@story');

Route::get('animation', 'Animation@index');
Route::get('animation_slider', 'Animation@slider');
Route::get('animation2', 'Animation@index2');
Route::get('animation3', 'Animation@index3');
Route::get('animation4', 'Animation@index4');
Route::get('animation5', 'Animation@index5');
Route::get('animation6', 'Animation@index6');
Route::get('animation7', 'Animation@index7');

Route::get('testcart', 'ProductsController@testcart');
Route::get('testcartlist', 'ProductsController@testcartlist')->name('testcartlist');

Route::get('change/{id}', 'ProductsController@listproduitdollar')->name('home.store.dolar');
Route::get('/', 'ProductsController@listproduit')->name('index.store');
Route::get('home', 'ProductsController@listproduit')->name('home.store');
Route::get('electronics', 'ProductsController@listproduit')->name('store.electronics');
Route::get('women', 'ProductsController@listproduit')->name('store.women');
Route::get('men', 'ProductsController@listproduit')->name('store.men');
Route::get('shop', 'ProductsController@listproduit')->name('store.shop.all');



Route::get('checkbox', 'MemberController@index')->name('checkbox.index');
Route::get('createposts', 'MemberController@create');
Route::post('store', 'MemberController@store');
Route::resource('checkbox', 'MemberController');

Route::get('checkboxtable','FrontpageController@home');
Route::post('/orders','OrderController@store');
Route::get('/orders','OrderController@index');
Route::post('store', 'OrderController@store')->name('order.store');
Route::get('/items/{id}','OrderController@items');

Route::get('/cart/details','user\CartdetailsController@index')->name('cart.details.user');
Route::post('/cart/details','user\CartdetailsController@edit')->name('cart.details.user.save');

Route::get('/cart/delivery/{id}','user\CartdeliveryController@index')->name('cart.delivery.user');
Route::post('/cart/delivery/{id}','user\CartdeliveryController@edit')->name('cart.delivery.user.save');



Route::get('/cart/payment/{id}','user\PaymentController@index')->name('cart.payment.user');
Route::post('/cart/payment/{id}','user\PaymentController@edit')->name('cart.payment.user.save');


Route::get('cart/login', 'UserLoginStoreController@index')->name('login.store');
Route::post('cart/login', 'UserLoginStoreController@postLogin')->name('user.login.store.save'); 

Route::get('login', 'UserLoginController@index')->name('login');
Route::post('login', 'UserLoginController@postLogin'); 

//registration routes
Route::get('registration', 'UserLoginController@registration')->name('user.registration');
Route::post('registration', 'UserLoginController@store')->name('user.registration.save'); 
Route::get('registrationedite', 'UserLoginController@registrationedite');
Route::post('registrationedite', 'UserLoginController@postregistrationedite'); 

//dashboard and logout route
Route::middleware('auth')->group(function(){
    Route::get('user', 'user\UserController@dashboard'); 
    Route::get('user/dashboard', 'user\UserController@dashboard')->name('user.dashboard'); 
    Route::get('user/profile', 'UserLoginController@profile')->name('user.profile');
    Route::get('user/logout', 'UserLoginController@logout');
    });
    
Route::get('user/request','user\OrderController@index')->name('user.request');
Route::get('user/request/show/{id}','user\OrderController@showrequest')->name('user.request.show');
Route::get('user/address', 'user\UserController@address')->name('user.address');

Route::get('user/edit','user\UserController@show')->name('user.edit');
Route::post('user/edit','user\UserController@edit')->name('user.edit.save');
Route::get('user/setting', 'user\UserController@setting')->name('user.setting'); 
Route::get('user/setting/change-password', 'user\ChangePasswordController@index')->name('change.password.view');
Route::post('user/setting/change-password', 'user\ChangePasswordController@store')->name('change.password');	
	
Route::get('user/setting/pic','user\PhotoController@show')->name('change.pic.view');
Route::post('user/setting/pic','user\PhotoController@edit')->name('change.pic');
	
Route::get('admin/stape1', 'AdminLoginController@stape1')->name('login.admin.stape1');

Route::get('admin', 'AdminLoginController@index')->name('login.admin');
Route::post('admin/varification', 'AdminLoginController@stape2')->name('login.admin.stape2');
	
Route::get('admin', 'AdminLoginController@index')->name('login.admin');
Route::post('admin', 'AdminLoginController@postLogin')->name('saveadminlogin'); 


Route::middleware('auth')->group(function(){
Route::get('admin/home', 'admin\AdminController@dashboard')->name('home'); 
Route::get('admin/homes', 'admin\AdminController@dashboards')->name('homes'); 


Route::post('admin/logout', 'AdminLoginController@logout')->name('logout'); 
});
//Admin profile
Route::get('admin/profile', 'admin\ProfileController@profile')->name('admin.profile');
Route::get('admin/profile/edit','admin\ProfileController@show')->name('admin.edit.profile');
Route::post('admin/profile/edit','admin\ProfileController@edit')->name('admin.edit');
Route::get('admin/profile/edit/pic','admin\PhotoController@show')->name('change.pic.adminview');
Route::post('admin/profile/edit/pic','admin\PhotoController@edit')->name('change.pic.admin');
Route::get('admin/profile/change-password', 'admin\ChangePasswordController@index')->name('change.password.adminview');
Route::post('admin/profile/change-password', 'admin\ChangePasswordController@store')->name('change.password.admin');	

Route::get('admin/manage-category', 'admin\ManageCategoryController@index')->name('manage-category.index'); 
Route::resource('admin/manage-category', 'admin\ManageCategoryController');

Route::get('admin/settings', 'admin\SettingController@index')->name('settings.index'); 
Route::resource('admin/settings', 'admin\SettingController');

Route::get('admin/manage-statistics', 'admin\ManageStatisticsController@index')->name('manage-statistics.index'); 
Route::resource('admin/manage-statistics', 'admin\ManageStatisticsController');



Route::get('admin/statistics-clients', 'admin\StatisticsClientsController@index')->name('statistics-clients.index'); 
Route::resource('admin/statistics-clients', 'admin\StatisticsClientsController');
Route::get('admin/statistics-clients/{id}', 'admin\StatisticsClientsController@show ')->name('statistics-clients.index.filter'); 
Route::resource('admin/statistics-clients', 'admin\StatisticsClientsController');


Route::get('admin/statistics-booking', 'admin\StatisticsBookingController@index')->name('statistics-booking.index'); 
Route::resource('admin/statistics-booking', 'admin\StatisticsBookingController');

Route::get('admin/statistics-sale', 'admin\StatisticsSaleController@index')->name('statistics-sale.index'); 
Route::resource('admin/statistics-sale', 'admin\StatisticsSaleController');




Route::get('admin/subcategory', 'admin\SubCategoryController@index')->name('subcategory.index'); 
Route::get('admin/subcategory/card', 'admin\SubCategoryController@index_card')->name('subcategory.card'); 
Route::post('admin/subcategory/check', 'admin\SubCategoryController@check')->name('subcategory.check');
Route::get('createposts', 'admin\SubCategoryController@create');
Route::post('store', 'admin\SubCategoryController@store');
Route::post('destroy', 'admin\SubCategoryController@destroy');
Route::get('show', 'admin\SubCategoryController@show');
Route::get('admin/subcategory/{id}/translate', 'admin\SubCategoryController@translate')->name('subcategory.translate'); 
Route::resource('admin/subcategory', 'admin\SubCategoryController');


Route::get('admin/product', 'admin\ProductController@index')->name('product.index');
Route::get('admin/product/card', 'admin\ProductController@index_card')->name('product.card'); 
Route::post('admin/product/check', 'admin\ProductController@check')->name('product.check');
Route::get('create', 'admin\ProductController@create');
Route::post('store', 'admin\ProductController@store');
Route::post('destroy', 'admin\ProductController@destroy');
Route::get('show', 'admin\ProductController@show');
Route::resource('admin/product', 'admin\ProductController');
Route::post('admin/product/store_category/{id}', 'admin\ProductController@store_category')->name('product.store_category');
Route::post('admin/product/destroy_category/{id}', 'admin\ProductController@destroy_category')->name('product.destroy_category');


Route::patch('admin/product/description', 'admin\ProductDescriptionController@update')->name('product.description'); 
Route::resource('admin/product/description', 'admin\ProductDescriptionController');

Route::patch('admin/product/shortdescription', 'admin\ProductShortDescriptionController@update')->name('product.shortdescription'); 
Route::resource('admin/product/shortdescription', 'admin\ProductShortDescriptionController');

Route::patch('admin/product/photoprincipal', 'admin\ProductPhotoPrincipalController@update')->name('product.photoprincipal'); 
Route::resource('admin/product/photoprincipal', 'admin\ProductPhotoPrincipalController');


Route::get('admin/product/photo/{id}', 'admin\ProductPhotoController@index')->name('product.photo.index'); 
Route::get('create', 'admin\ProductPhotoController@create');
Route::post('store', 'admin\ProductPhotoController@store');
Route::post('destroy', 'admin\ProductPhotoController@destroy');
Route::get('show', 'admin\ProductPhotoController@show');
Route::post('subcat', 'admin\ProductPhotoController@subCat')->name('subcat');
Route::resource('admin/product/photo', 'admin\ProductPhotoController');



Route::get('admin/user', 'admin\UserController@index')->name('user.index'); 
Route::get('createposts', 'admin\UserController@create');
Route::post('store', 'admin\UserController@store');
Route::post('destroy', 'admin\UserController@destroy');
Route::get('show', 'admin\UserController@show');
Route::resource('admin/user', 'admin\UserController');

Route::get('admin/booking', 'admin\bookingController@index')->name('booking.index'); 
Route::get('createposts', 'admin\bookingController@create');
Route::post('store', 'admin\bookingController@store');
Route::post('destroy', 'admin\bookingController@destroy');
Route::get('show', 'admin\bookingController@show');
Route::resource('admin/booking', 'admin\bookingController');
Route::get('admin/booking/payment/{id}', 'admin\bookingController@showpayment')->name('booking.payment');



Route::get('admin/stock', 'admin\StockController@index')->name('stock.index'); 
Route::get('create', 'admin\StockController@create');
Route::post('store', 'admin\StockController@store');
Route::post('destroy', 'admin\StockController@destroy');
Route::get('show', 'admin\StockController@show');
Route::resource('admin/stock', 'admin\StockController');



Route::get('admin/method-delivery', 'admin\MethoddeliveryController@index')->name('method-delivery.index'); 
Route::get('createposts', 'admin\MethoddeliveryController@create');
Route::post('store', 'admin\MethoddeliveryController@store');
Route::post('destroy', 'admin\MethoddeliveryController@destroy');
Route::get('show', 'admin\MethoddeliveryController@show');
Route::resource('admin/method-delivery', 'admin\MethoddeliveryController');



Route::get('admin/settings-currency', 'admin\CurrencyController@index')->name('settings-currency.index'); 
Route::get('createposts', 'admin\CurrencyController@create');
Route::post('store', 'admin\CurrencyController@store');
Route::post('destroy', 'admin\CurrencyController@destroy');
Route::get('show', 'admin\CurrencyController@show');
Route::resource('admin/settings-currency', 'admin\CurrencyController');




Route::get('admin/settings-languages', 'admin\LanguagesController@index')->name('settings-languages.index'); 
Route::get('createposts', 'admin\LanguagesController@create');
Route::post('store', 'admin\LanguagesController@store');
Route::post('destroy', 'admin\LanguagesController@destroy');
Route::get('show', 'admin\LanguagesController@show');
Route::resource('admin/settings-languages', 'admin\LanguagesController');



Route::get('admin/transport_costs', 'admin\setting\TransportCosts@index')->name('transport_costs'); 
Route::get('admin/transport_costs/create', 'admin\setting\TransportCosts@create')->name('transport_costs.create'); 
Route::post('admin/transport_costs/store', 'admin\setting\TransportCosts@store')->name('transport_costs.store'); 
Route::post('admin/transport_costs/destroy', 'admin\setting\TransportCosts@destroy')->name('transport_costs.destroy');





Route::get('admin/manage-admin', 'admin\ManageAdminController@index')->name('manage-admin.index'); 
Route::post('admin/manage-admin/check', 'admin\ManageAdminController@check')->name('admin.check');
Route::get('createposts', 'admin\ManageAdminController@create');
Route::post('store', 'admin\ManageAdminController@store');
Route::post('destroy', 'admin\ManageAdminController@destroy');
Route::get('show', 'admin\ManageAdminController@show');
Route::resource('admin/manage-admin', 'admin\ManageAdminController');



Route::get('admin/notification', 'admin\NotificationController@index')->name('notification');
Route::patch('admin/notification/update', 'admin\NotificationController@update');
Route::resource('admin/notification', 'admin\NotificationController');

Route::get('admin/mysettings', 'admin\Account_settings\HomeController@index')->name('mysettings.index'); 


Route::get('admin/category', 'admin\CategoryController@index')->name('category.index'); 
Route::get('admin/category/card', 'admin\CategoryController@index_card')->name('category.card'); 
Route::post('admin/category/check', 'admin\CategoryController@check')->name('category.check');
Route::get('createposts', 'admin\CategoryController@create');
Route::post('store', 'admin\CategoryController@store');
Route::post('destroy', 'admin\CategoryController@destroy');
Route::get('show', 'admin\CategoryController@show')->name('category.show'); 
Route::get('category/{id}', 'admin\CategoryController@view')->name('category.view'); 
Route::get('admin/category/{id}/translate', 'admin\CategoryController@translate')->name('category.translate'); 
Route::resource('admin/category', 'admin\CategoryController');
Route::patch('admin/category/update_photo/{id}', 'admin\CategoryController@update_photo')->name('category.update_photo');
Route::post('admin/category/destroy_product/{id}', 'admin\CategoryController@destroy_product')->name('category.destroy_product');
Route::post('admin/category/store_product', 'admin\CategoryController@store_product')->name('category.store_product');

Route::get('admin/category/fr/{id}', 'admin\CategoryController@show_fr')->name('category_fr.view'); 
Route::get('admin/category/de/{id}', 'admin\CategoryController@show_de')->name('category_de.view'); 
Route::patch('admin/category/{id}/seo_en', 'admin\CategoryController@seo_en')->name('category.seo_en'); 
Route::patch('admin/category/{id}/seo_de', 'admin\CategoryController@seo_de')->name('category.seo_de'); 
Route::patch('admin/category/{id}/seo_fr', 'admin\CategoryController@seo_fr')->name('category.seo_fr');

Route::get('admin/blog', 'admin\blog\HomeController@home')->name('blog.index'); 

Route::get('admin/news', 'admin\NewsController@index')->name('news.index'); 
Route::get('admin/news/card', 'admin\NewsController@index_card')->name('news.card'); 
Route::get('admin/news/create', 'admin\NewsController@create')->name('news.create'); 
Route::post('admin/news/store', 'admin\NewsController@store')->name('news.store'); 
Route::post('admin/news/destroy', 'admin\NewsController@destroy');
Route::patch('admin/news/update/{id}', 'admin\NewsController@update')->name('news.update'); 
Route::patch('admin/news/updateslider/{id}', 'admin\NewsController@updateslider')->name('news.updateslider'); 
Route::get('admin/news/{id}/edit', 'admin\NewsController@edit')->name('news.edit'); 
Route::get('admin/news/{id}', 'admin\NewsController@show')->name('news.show');
Route::get('admin/news/{id}/translate', 'admin\NewsController@translate')->name('news.translate'); 
Route::patch('admin/news/translate/{id}', 'admin\NewsController@translate_update')->name('news.translate_update'); 
Route::get('admin/news/translate/{id}', 'admin\NewsController@show_translate')->name('news.show_translate');
Route::post('admin/news/destroy_category/{id}', 'admin\NewsController@destroy_category')->name('news.destroy_category');
Route::post('admin/news/store_category/{id}', 'admin\NewsController@store_category')->name('news.store_category');


Route::get('admin/blog/category', 'admin\blog\HomeController@index')->name('blog.category'); 
Route::get('admin/blog/card', 'admin\blog\HomeController@index_card')->name('blog.category.card'); 
Route::post('admin/blog/category/store', 'admin\blog\HomeController@store')->name('blog.category.store'); 
Route::get('admin/blog/category/{id}', 'admin\blog\HomeController@show')->name('blog.category.show'); 
Route::get('admin/blog/category/{id}/edit', 'admin\blog\HomeController@edit')->name('blog.category.edit'); 
Route::patch('admin/blog/category/update/{id}', 'admin\blog\HomeController@update')->name('blog.category.update'); 


Route::get('admin/cms', 'admin\ManageCmsController@index')->name('cms.index'); 
Route::get('admin/cms/home', 'admin\cms\HomeController@index')->name('cms.home'); 
Route::get('admin/cms/home/slider', 'admin\cms\slider\HomeController@index')->name('cms.home.slider'); 
Route::get('admin/cms/home/slider/{id}', 'admin\cms\slider\HomeController@show'); 
Route::patch('admin/cms/home/slider/update/{id}', 'admin\cms\slider\HomeController@update')->name('cms.home.slider.update');
Route::get('admin/cms/home/slider/{x}/{y}', 'admin\cms\slider\HomeController@create')->name('cms.home.slider.create');
Route::post('admin/cms/home/slider/store', 'admin\cms\slider\HomeController@store')->name('cms.home.slider.store');



Route::get('admin/cms/home/about', 'admin\cms\about\HomeController@index')->name('cms.about'); 
Route::patch('admin/cms/home/about/update/{id}', 'admin\cms\about\HomeController@update')->name('cms.about.update'); 
Route::patch('admin/cms/home/about/updatephoto/{id}', 'admin\cms\about\HomeController@updatephoto')->name('cms.about.updatephoto'); 



Route::get('admin/cms/home/catalogue', 'admin\cms\catalogue\HomeController@index')->name('cms.catalogue'); 
Route::get('admin/cms/home/catalogue/create', 'admin\cms\catalogue\HomeController@create')->name('catalogue.create'); 
Route::post('admin/cms/home/catalogue/store', 'admin\cms\catalogue\HomeController@store')->name('catalogue.store'); 
Route::get('admin/cms/home/catalogue/{id}', 'admin\cms\catalogue\HomeController@show')->name('catalogue.show'); 
Route::get('admin/cms/home/catalogue/{id}/edit', 'admin\cms\catalogue\HomeController@edit')->name('catalogue.edit'); 
Route::post('admin/cms/home/catalogue/destroy/{id}', 'admin\cms\catalogue\HomeController@destroy')->name('catalogue.destroy'); 
Route::patch('admin/cms/home/catalogue/updatephotoprin/{id}', 'admin\cms\catalogue\HomeController@updatephotoprin')->name('catalogue.updatephotoprin'); 
Route::post('admin/cms/home/catalogue/addupdate/{id}', 'admin\cms\catalogue\HomeController@addupdate')->name('catalogue.addupdate'); 
Route::get('admin/cms/home/catalogue/product/{id}', 'admin\cms\catalogue\HomeController@showproduct')->name('catalogue.show.product'); 
Route::patch('admin/cms/home/catalogue/product/updateproduct/{id}', 'admin\cms\catalogue\HomeController@updateproduct')->name('catalogue.updateproduct'); 
Route::post('admin/cms/home/catalogue/product/productdelete/{id}', 'admin\cms\catalogue\HomeController@productdelete')->name('catalogue.productdelete'); 
Route::get('admin/cms/home/catalogue/{id}/{x}/{y}', 'admin\cms\catalogue\HomeController@add_product')->name('catalogue.add_product'); 
Route::post('admin/cms/home/catalogue/store_product', 'admin\cms\catalogue\HomeController@store_product')->name('catalogue.store_product'); 
Route::post('admin/cms/home/catalogue/store_product2', 'admin\cms\catalogue\HomeController@store_product2')->name('catalogue.store_product2'); 




Route::get('admin/cms/home/our_article', 'admin\cms\our_article\HomeController@index')->name('cms.our_article'); 
Route::patch('admin/cms/home/our_article/update/{id}', 'admin\cms\our_article\HomeController@update')->name('cms.our_article.update'); 
Route::patch('admin/cms/home/our_article/updatephoto/{id}', 'admin\cms\our_article\HomeController@updatephoto')->name('cms.our_article.updatephoto'); 
Route::patch('admin/cms/home/our_article/updatedescription/{id}', 'admin\cms\our_article\HomeController@updatedescription')->name('cms.our_article.updatedescription'); 
Route::patch('admin/cms/home/our_article/update_product_p/{id}', 'admin\cms\our_article\HomeController@update_product_p')->name('cms.our_article.update_product_p'); 
Route::patch('admin/cms/home/our_article/update_product_s1/{id}', 'admin\cms\our_article\HomeController@update_product_s1')->name('cms.our_article.update_product_s1'); 
Route::patch('admin/cms/home/our_article/update_product_s2/{id}', 'admin\cms\our_article\HomeController@update_product_s2')->name('cms.our_article.update_product_s2'); 
Route::patch('admin/cms/home/our_article/update_product_s3/{id}', 'admin\cms\our_article\HomeController@update_product_s3')->name('cms.our_article.update_product_s3'); 
Route::patch('admin/cms/home/our_article/update_product_s4/{id}', 'admin\cms\our_article\HomeController@update_product_s4')->name('cms.our_article.update_product_s4'); 

Route::get('admin/cms/image-favicon', 'admin\cms\image_favicon\HomeController@index')->name('cms.image-favicon'); 
Route::get('admin/cms/image-favicon/create', 'admin\cms\image_favicon\HomeController@create')->name('cms.image-favicon.create'); 
Route::post('admin/cms/image-favicon/store', 'admin\cms\image_favicon\HomeController@store')->name('cms.image-favicon.store'); 
Route::post('admin/cms/image-favicon/destroy', 'admin\cms\image_favicon\HomeController@destroy')->name('cms.image-favicon.destroy');
Route::get('admin/cms/image-favicon/{id}/edit', 'admin\cms\image_favicon\HomeController@edit')->name('cms.image-favicon.edit');
Route::patch('admin/cms/image-favicon/update/{id}', 'admin\cms\image_favicon\HomeController@update')->name('cms.image-favicon.update');



Route::get('admin/cms/book-reader', 'admin\cms\book\HomeController@index')->name('cms.book'); 
Route::get('admin/country', 'admin\setting\CountryController@index')->name('country.index'); 
Route::get('admin/country/create', 'admin\setting\CountryController@create')->name('country.create'); 
Route::post('admin/country/check', 'admin\setting\CountryController@check')->name('country.check');
Route::post('admin/country/store', 'admin\setting\CountryController@store')->name('country.store'); 
Route::get('admin/country/{id}', 'admin\setting\CountryController@show')->name('country.show');
Route::get('admin/country/{id}/edit', 'admin\setting\CountryController@edit')->name('country.edit'); 
Route::patch('admin/country/update/{id}', 'admin\setting\CountryController@update')->name('country.update'); 
Route::post('admin/country/store_city', 'admin\setting\CountryController@store_city')->name('city.store');
Route::patch('admin/country/update_logo/{id}', 'admin\setting\CountryController@update_logo')->name('country.update_logo');



Route::get('admin/location', 'admin\location\HomeController@index')->name('location'); 
Route::get('admin/location/list', 'admin\location\HomeController@list')->name('location.list'); 
Route::get('admin/location/list/create', 'admin\location\HomeController@create')->name('location.create'); 
Route::post('admin/location/list/store', 'admin\location\HomeController@store')->name('location.store'); 
Route::get('admin/location/list/{id}', 'admin\location\HomeController@show')->name('location.show'); 
Route::get('admin/location/list/{id}/edit', 'admin\location\HomeController@edit')->name('location.edit'); 
Route::patch('admin/location/list/update/{id}', 'admin\location\HomeController@update')->name('location.update'); 
Route::patch('admin/location/list/destroy/{id}', 'admin\location\HomeController@destroy')->name('location.destroy'); 


Route::get('admin/contact', 'admin\contact\HomeController@index')->name('contact.index'); 
Route::get('admin/contact/{id}', 'admin\contact\HomeController@show')->name('contact.show'); 

Route::get('admin/newsletter', 'admin\newsletter\HomeController@index')->name('newsletter.index'); 
Route::get('admin/inbox', 'admin\newsletter\HomeController@inbox')->name('newsletter.inbox'); 

Route::get('admin/newsletter/{id}', 'admin\newsletter\HomeController@show')->name('newsletter.show'); 



Route::get('admin/product/{id}/translate','admin\ProductControllerTranslate@translate')->name('product.translate');
Route::patch('admin/product/{id}/translate','admin\ProductControllerTranslate@update')->name('product.translate.save');
Route::get('admin/product/translate/{id}','admin\ProductControllerTranslate@show')->name('product.translate.view');
Route::patch('admin/product/translate/{id}/update_long_description','admin\ProductControllerTranslate@update_long_description')->name('translate.description.save');
Route::patch('admin/product/translate/{id}/update_short_description','admin\ProductControllerTranslate@update_short_description')->name('translate.shortdescription.save');

Route::get('admin/product/{id}/offer','admin\ProductControllerOffer@translate')->name('product.offer');
Route::get('admin/product/offer/{id}','admin\ProductControllerOffer@show')->name('product.offer.view');
Route::patch('admin/product/{id}/offer','admin\ProductControllerOffer@update')->name('product.offer.save');


Route::get('admin/product/pic','admin\ProductControllerImage@show')->name('change.pic.product');
Route::post('admin/product/pic','admin\ProductControllerImage@edit')->name('change.pic');
Route::get('admin/story','admin\ProductController@story')->name('product.story');




Route::get('/categorytest','admin\ShopController@index')->name('category');
Route::get('admin/notificationtest', 'NotificationController@index')->name('admin.markNotification.get');
Route::post('admin/notificationtest', 'NotificationController@markNotification')->name('admin.markNotification');


Route::get('admin/users_filter', 'UserControllerFilter@index')->name('users.index');
Route::get('admin/image_map', 'ImagemapperController@index');
Route::get('admin/image_map_test', 'ImagemapperController@indextest');

Route::get('admin/seo', 'admin\seo\HomeController@index')->name('seo');
Route::get('admin/seo/settings', 'admin\seo\HomeController@settings')->name('seo.settings');
Route::get('admin/seo/settings/url', 'admin\seo\HomeController@url')->name('seo.settings.url');
Route::get('admin/seo/settings/url/create', 'admin\seo\HomeController@create')->name('seo.settings.url.create');
Route::post('admin/seo/settings/url/create/store', 'admin\seo\HomeController@store')->name('seo.settings.url.store');
Route::get('admin/seo/settings/url/{id}', 'admin\seo\HomeController@show')->name('seo.settings.url.show');
Route::get('admin/seo/settings/url/{id}/edit', 'admin\seo\HomeController@edit')->name('seo.settings.url.edit');
Route::patch('admin/seo/settings/url/{id}/update', 'admin\seo\HomeController@update')->name('seo.settings.url.update');
Route::get('admin/seo/settings/url/{id}/update_pub_active', 'admin\seo\HomeController@update_pub_active')->name('seo.settings.url.update_pub_active');
Route::get('admin/seo/settings/url/{id}/update_pub_blocked', 'admin\seo\HomeController@update_pub_blocked')->name('seo.settings.url.update_pub_blocked');

Route::get('admin/seo/settings/url/category', 'admin\seo\HomeController@category')->name('seo.settings.url.category');


Route::get('admin/crm/pages', 'admin\cms\pages\HomeController@index')->name('cms.pages'); 
Route::get('admin/crm/pages/create', 'admin\cms\pages\HomeController@create')->name('cms.pages.create'); 
Route::post('admin/crm/pages/store', 'admin\cms\pages\HomeController@store')->name('cms.pages.store'); 
Route::get('admin/crm/pages/{id}', 'admin\cms\pages\HomeController@show')->name('cms.pages.show'); 



Route::patch('admin/update_layouts','admin\ProfileController@update_layouts')->name('admin.update_layouts');




Route::get('simple-qr-code','GenerateQrCodeController@simpleQrCode');
Route::get('color-qr-code','GenerateQrCodeController@colorQrCode');
Route::get('image-qr-code','GenerateQrCodeController@imageQrCode');



Route::get('admin/filter/', 'admin\FilterTest@index')->name('month.test');
Route::get('admin/filter/{id}', 'admin\FilterTest@filter')->name('month.filter');


