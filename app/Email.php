<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
 protected $fillable=['frommail','tomail','sub','body'];

}
