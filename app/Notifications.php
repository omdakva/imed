<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
       protected $table = 'notifications';
        protected $fillable = ['notifiable_type','id_user','notifiable','status','read_at'];
		
	public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
	
 public function products()
    {
        return $this->hasMany('App\Product', 'category_id');
    }
	

	public function subcategory(){

        return $this->hasMany('App\Category', 'parent_id');

    }
		

}
