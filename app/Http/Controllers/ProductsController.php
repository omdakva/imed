<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\OrderController;

use Session;
Use App\User;
use App\Product;
use App\Orders;
use App\Items;
use App\Ordre;

use App\Post;
use App\Category;
use App\Cart;

use App\Purchase;

class ProductsController extends Controller
{
	public function Payer(Request $request)
    {



if (session()->has('cart')) {
if (Ordre::where('orders_number',session()->get('cart')->ordre)->exists())
{
    Ordre::where('orders_number', '=', session()->get('cart')->ordre)->update(['total_amount' =>session()->get('cart')->totalPrice,'items' =>session()->get('cart')->items ]);

} else {
    $store=new OrderController;
$store->store();


}


        $s=$request->input("payer");
        return redirect('/test/'.$s);



}else
return Redirect('/');

    }
	public function testcart()
    {
        
		
		$products = Product::all();
		
		$products_electro = Product::all()
	    ->where('category_id', '=', "1");
		
		$products_women = Product::all()
	    ->where('category_id', '=', "2");
		
		$products_men = Product::all()
	    ->where('category_id', '=', "3");
        return view('testcart', compact('products_women','products'));
    }
	
		public function testcartlist()
    {
        $products = Product::all();
        return view('testcartlist', compact('products'));
    }

public function index()
    {
        $products = Product::all();
        return view('products', compact('products'));
    }	
	

	
	
	
	
	public function listproduit()
    {
		
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
        $products = Product::all();
		

        return view('home',$data, compact('products'));
    }
	
	
	
	public function listproduitdollar($id)
    {
		
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
        $products = Product::all();
		

        return view('home',$data, compact('products'));
    }
	
	
	
	
	
	
	
	
	

    public function cart()
    {
	   if(Auth::check())
	   {
		 $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
		$users = DB::table('users')
	    ->where('id_user', '=', "".ucfirst(Auth()->user()->id_user)."");
		return view('cart',$data, compact('users'));
       }
	   $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		return view('cart',$data);

    }
	
	
	
	
	
	    public function store(Request $request)
    {
        $data=$request->all();
        $lastid=Purchase::create($data)->id;
        if(count($request->list_product) > 0)
        {
        foreach($request->list_product as $item=>$v){
            $data2=array(
                'list_product'=>$request->list_product[$item],
                'id_user'=>$request->id_user[$item],
                'active'=>$request->active[$item],
                'total'=>$request->total[$item],
                'statut'=>$request->statut[$item],
            );
        Purchase::insert($data2);
      }
        }
        return redirect()->back()->with('success','data insert successfully');
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function show($id)
    {
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
        $products = Product::find($id);

        return view('show',$data, compact('products'));
    }
	
	
    
    


    public function products_seo($id)
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Product = DB::table('products')
		->where('products.product_name', '=', $id)
	    ->get();
		
		$Category_product = DB::table('products')
		->join('category_products', 'products.id', '=', 'category_products.id_product')
		->join('category', 'category_products.id_category', '=', 'category.id')
		->select('category.id', 'category.name', 'category.parent_id')
		->where('products.product_name', '=', $id)
	    ->get();
	 
	      $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
		
		
        $Category = Category::where('parent_id',0)->get();

        return view('web.products.show',$data, compact('Product', 'notifications', 'Category_product'));
		}
	    
		return redirect::to("admin");

    }
	
    public function addToCart($id)
    {

        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
        } else {
            $cart = new Cart();
        }
        $product=Product::find($id);
        $cart->add($product);
        session()->put('cart', $cart);

return redirect()->back()->with('success', 'Product added to cart successfully!');
//return $cart->totalQty;
    }
	
    public function showCart()
    {

        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
        } else {
            $cart = null;
        }
        $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
      return view('cart',$data, compact('cart'));
 
 //  return response()->json([$cart]);



    }



	
  
	
	
	public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["quantity"] = $request->quantity;

            session()->put('cart', $cart);

            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('success', 'Product removed successfully');
        }
    }
	
	
	
	
	function save_comment(Request $request)
	{
        $data=new \App\Comment;
        $data->post_id=$request->post;
        $data->comment_text=$request->comment;
        $data->save();
        return response()->json([
            'bool'=>true
        ]);
    }
}