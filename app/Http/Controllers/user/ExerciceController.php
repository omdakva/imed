<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\exercise;


class ExerciceController extends Controller 
{
   public function index() 
   {
	  
      if(Auth::check()){
	    
		$exercise = DB::table('exercises')
	    ->where('section', '=', "".ucfirst(Auth()->user()->section)."")

       ->join('sections', 'exercises.section', '=', 'sections.id')
       ->select('exercises.exercise_name', 'exercises.id', 'exercises.description', 'exercises.created_at', 'exercises.fichier', 'sections.title')
       ->get();
	   
		
		$exercisecount = DB::table('exercises')
			    ->where('section', '=', "".ucfirst(Auth()->user()->section)."")

	    ->count('id');
		
        return view('userexercice.index', compact('exercise', 'exercisecount'));
		
		
      }
      
   }


}