<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use DB;
use Symfony\Component\HttpFoundation\Response;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
Use App\User;
use App\Ordre;
use App\Items;
use App\Company;
use App\Category;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::check())
	   {
		 $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
        $orders = DB::select('select * from ordres where customer_id="'.ucfirst(Auth()->user()->id).'"');
        return view('user.order',$data, compact('orders'));
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $lastid=Orders::create($data)->id;
        if(count($request->product_name) > 0)
        {
        foreach($request->product_name as $item=>$v){
            $data2=array(
                'orders_id'=>$lastid,
                'product_name'=>$request->product_name[$item],
                'brand'=>$request->brand[$item],
                'quantity'=>$request->quantity[$item],
                'budget'=>$request->budget[$item],
                'amount'=>$request->amount[$item]
            );
        Items::insert($data2);
      }
        }
        return redirect()->back()->with('success','data insert successfully');
    }

   

    public function items($id)
    {
        $items=Items::where('orders_id','=',$id)->get();
        return view('user.ordershow',compact('items'));
    }
	
	
    public function showrequest($id)
    {
		
		$orders=Ordre::find($id);

        return view('user.ordershow',compact('orders'));
    }
}