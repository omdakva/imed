<?php
namespace App\Http\Controllers\user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator,Redirect,Response;
use DB;
use App\Product;
use Cart;
use App\Category;
use App\Orders;
use App\Items;
use Session;


class CartdetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    public function index()
    {
	   
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
        } else {
            $cart = null;
        }

        return view('user.cart_details.index',compact('cart'));
		 }




 public function edit(Request $request) 
   {
      $id_order = $request->input('id_order');
      $firstname = $request->input('firstname');
      $lastname = $request->input('lastname');
      $tel = $request->input('tel');
      $mobile = $request->input('mobile');
      $country = $request->input('country');
      $city = $request->input('city');
      $address = $request->input('address');
      $postcode = $request->input('postcode');
      $updated_at = $request->input('updated_at');
      DB::update('update users set firstname = ?, lastname = ?, tel = ?, mobile = ?, country = ?, city = ?, address = ?, postcode = ?, updated_at = ? where id = ?',[$firstname,$lastname,$tel,$mobile,$country,$city,$address,$postcode,$updated_at,ucfirst(Auth()->user()->id)]);
      return redirect('cart/delivery/'.$request->input('id_order').'')->with('success', 'Update with success!');

   }
    
}
