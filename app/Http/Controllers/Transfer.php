<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use Illuminate\Support\Facades\Mail;
use DB;

class Transfer extends Controller
{
    public function index()
    {
        if (session()->has('cart')) {

            Mail::to('Dulucsshop@gmail.com')
                ->send(new \App\Mail\Mail());
            $cart = new Cart(session()->get('cart'));
            //$ordre  =new OrdreController;
           // $ordre ->store(0);
            $regulation  =new RegulationController;
            $regulation ->store("In Progress","Transfer",0,$cart->ordre);
            session()->forget('cart');
            return redirect("/cart")->with('echec', "Votre Commande est enregistrer,veuillez confirmer");

        }else
        {
            return redirect("/cart")->with('echec', "Panier Vide");


        }
    }
    public function update($id)
    {
        DB::table('ordres')
        	->join('users', 'users.id', '=', 'ordres.customer_id')

            ->where('users.email', $id)
            ->update(['statut' => 1]);
            return redirect("/cart")->with('echec', "Commande confirmer");



    }
}
