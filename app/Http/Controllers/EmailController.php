<?php

namespace App\Http\Controllers;
use DB;

use App\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;



class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function show()

    {

	if(Auth::check())
		{
		
//$mails=Email::all()->sortByDesc('created_at');
$mails= Email::where('active','=',1)->orderBy('created_at', 'desc')->Paginate(5);
  $nb_trash = Email::where('active','=',0)->count();    
  $nb_outbox = Email::where('active','=',1)->count();    


		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Subscribe_newsletter = DB::table('subscribe_newsletter')
        ->get();
	
	
        return view('admin.newsletter.sendbox', compact('Subscribe_newsletter', 'notifications','mails','nb_trash','nb_outbox'));
		
		
		}
	    
		return redirect::to("admin");   
         }

public function showtrash()

    {

	if(Auth::check())
		{
		
//$mails=Email::all()->sortByDesc('created_at');
$mails= Email::where('active','=',0)->orderBy('created_at', 'desc')->paginate(5);
 $nb_trash = Email::where('active','=',0)->count();    
  $nb_outbox = Email::where('active','=',1)->count();    


		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Subscribe_newsletter = DB::table('subscribe_newsletter')
        ->get();
	
	
        return view('admin.newsletter.trash', compact('Subscribe_newsletter', 'notifications','mails','nb_trash','nb_outbox'));
		
		
		}
	    
		return redirect::to("admin");  
        
        
          }

public function chat()

    {

	if(Auth::check())
		{
		
//$mails=Email::all()->sortByDesc('created_at');
$mails= Email::where('active','=',0)->orderBy('created_at', 'desc')->paginate(5);
 $nb_trash = Email::where('active','=',0)->count();    
  $nb_outbox = Email::where('active','=',1)->count();    


		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Subscribe_newsletter = DB::table('subscribe_newsletter')
        ->get();
	
	
        return view('admin.newsletter.chat', compact('Subscribe_newsletter', 'notifications','mails','nb_trash','nb_outbox'));
		
		
		}
	    
		return redirect::to("admin");  
        
        
          }














        public function showmail($email)

    {

	if(Auth::check())
		{
		
$mails= Email::find($email);
 $count = Email::count();    

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Subscribe_newsletter = DB::table('subscribe_newsletter')
        ->get();
	
	
        return view('admin.newsletter.sendboxmail', compact('Subscribe_newsletter', 'notifications','mails','count'));
		
		
		}
	    
		return redirect::to("admin");    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function edit(Email $email)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Email $email)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function destroy($email)
    {
                 
	if(Auth::check())
		{
		 DB::table('emails')
              ->where('id', $email)
              ->update(['active' => 0]);


	
            return redirect('/admin/trash');
		
		
		}
	    
		return redirect::to("admin");  
    }

         public function send(Request $request)
    {
        

            Mail::to('Dulucsshop@gmail.com')
                ->send(new \App\Mail\Adminmail($request));
                Email::create([
                'frommail' =>'Dulucsshop@gmail.com',
                'tomail' =>$request->input("email"),
                'sub' =>$request->input("subject"),
                'body' =>$request->input("message"),
               

            ]);
            
            return redirect()->back()->with('echec', "email envoyer avec success");

       
    }

}
