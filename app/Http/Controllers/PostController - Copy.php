<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Cart;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
	public function index(){
        $posts=\App\Post::all();
        return view('posts',['posts'=>$posts]);
    }
    // Post Detail
    public function detail(Request $request,$id){
        $post=\App\Post::find($id);
        return view('postsdetail',['post'=>$post]);
    }
	// Save Comment
    function save_comment(Request $request)
	{
        $data=new \App\Comment;
        $data->post_id=$request->post;
        $data->comment_text=$request->comment;
        $data->save();
        return response()->json([
            'bool'=>true
        ]);
    }
}
