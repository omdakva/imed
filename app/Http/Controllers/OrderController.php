<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
Use App\User;
use App\Ordre;
use App\Items;
use App\Purchase;
use App\Cart;



class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
          
       

        
            return view('user.cart_details.index',compact('cart'));
        }    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
            $item =json_encode($cart->items, JSON_FORCE_OBJECT);
            

            Ordre::create([
                'orders_number' =>$cart->ordre,
                'customer_id' =>  Auth::user()->id,
                'totalQty' =>  $cart->totalQty,
                'total_amount' => $cart->totalPrice,
                'currency' =>'$',
                'items' => $item,
                'cost' => $cart->cost,
                'localisation_etats'=>1,
                'active' => 1,
                'statut' => 0,

            ]);
       


        
      return view('user.cart_details.index',compact('cart'));
        }
    }

   

    public function items($id)
    {
        $items=Items::where('orders_id','=',$id)->get();
        return view('front_page.items',compact('items'));
    }

    

}