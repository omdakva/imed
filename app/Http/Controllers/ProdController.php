<?php

namespace App\Http\Controllers;

use App\Prod;
use Illuminate\Http\Request;

class ProdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prod  $prod
     * @return \Illuminate\Http\Response
     */
    public function show(Prod $prod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prod  $prod
     * @return \Illuminate\Http\Response
     */
    public function edit(Prod $prod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prod  $prod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prod $prod)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prod  $prod
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prod $prod)
    {
        //
    }

    public function addToCart(Prod $product)
    {
echo $product;
      /*  if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
        } else {
            $cart = new Cart();
        }
        $cart->add($product);
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');*/
    }
	
}
