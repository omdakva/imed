<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
  public function index()
  {
    return view('ajaxPostForm');
  }

  public function store(Request $request)
  {
      $validator = \Validator::make($request->all(), [
            'title' => 'required',
        ]);
     
        if ($validator->passes()) {

          Post::create($request->all());
            return response()->json(['success'=>'Added new records.']);
        }
     
        return response()->json(['error'=>$validator->errors()->all()]);
  }
}