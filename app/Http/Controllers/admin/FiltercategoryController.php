<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;


class FiltercategoryController extends Controller
{


    /**
     * Show the application layout
     *
     * @return \Illuminate\Http\Response
     */
    public function myform()
    {
        $states = DB::table("category")->pluck("name","id");
        return view('admin.product.myform',compact('states'));
    }


    /**
     * Get Ajax Request and restun Data
     *
     * @return \Illuminate\Http\Response
     */
    public function myformAjax($id)
    {
        $cities = DB::table("products")
                    ->where("category_id",$id)
                    ->pluck("product_name","id");
        return json_encode($cities);
    }


}