<?php

namespace App\Http\Controllers\admin\contact;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Contact;
use Response;

class HomeController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Contact = DB::table('contact')
        ->get();
	
	
        return view('admin.contact.index', compact('Contact', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
	
	


	
	

	

	
	
	public function show($id)
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Contact = Contact::find($id);

	 

        return view('admin.contact.show', compact('Contact', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	
	
}