<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Illuminate\Validation\Rule;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Redirect;
use Session;

use App\User;
use App\Notifications;

use Response;

class ManageAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
		if(Auth::check()){
				 
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		  
		$data['Users'] = DB::table('users')
		->where('role', '!=', "0")
	    ->get();

	   	
        $userscount = DB::table('users')
        ->count('id');
		
        return view('admin.manage-admin.index',$data, compact('userscount', 'notifications'));
		}
	    
		return redirect::to("admin");

    }

	
	
	
	
	
	function check(Request $request)
   	{
   		if($request->get('email'))
   		{
   			$email = $request->get('email');
   			$data = DB::table("users")
   				->where('email', $email)
   				->count();
   			if($data > 0)
   			{
   				echo 'not_unique';
   			}
   			else
   			{
   				echo 'unique';
   			}
   		}
   	}
	
	
	
 
     public function create()
    {
        if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Users'] = DB::table('users')
		->where('role', '=', "1")
	    ->get();
		
		

        return view('admin.manage-admin.create',$data, compact('notifications'));
		}
		return redirect::to("admin");
    }
	
		
	public function store(Request $request)
    {
        $this->validate($request,[
          'status' => 'required',
          'role' => 'required',
          'birthday' => 'required',
          'gender' => 'required',
          'firstname' => 'required',
          'lastname' => 'required',
          'image' => 'required',
          'country' => 'required',
          'city' => 'required',
          'address' => 'required',
          'postcode' => 'required',
          'tel' => 'required',
          'mobile' => 'required',
          'email' => 'required|email|unique:users',
		  'password' => 'required',
          'block_description' => 'required',
          'month_number' => 'required',
          'year' => 'required'
		  ]);

      $User= new User([
        'status' => $request->get('status'),
        'role' => $request->get('role'),
        'birthday' => $request->get('birthday'),
        'gender' => $request->get('gender'),
        'firstname' => $request->get('firstname'),
        'lastname' => $request->get('lastname'),
        'image' => $request->get('image'),
        'country' => $request->get('country'),
        'city' => $request->get('city'),
        'address' => $request->get('address'),
        'postcode' => $request->get('postcode'),
        'tel' => $request->get('tel'),
        'mobile' => $request->get('mobile'),
        'email' => $request->get('email'),
        'password' => Hash::make($request['password']),
        'block_description' => $request->get('block_description'),
        'month_number' => $request->get('month_number'),
        'year' => $request->get('year'),
        ]);
		
      $User->save();
	  


      return redirect::route('manage-admin.index')->with('success', 'Create with success!');
  
    }
	
	
	
	
	public function store2(Request $request)
    {
		
		$data=$request->all();

		  request()->validate([
          'status' => 'required',
          'role' => 'required',
          'birthday' => 'required',
          'gender' => 'required',
          'firstname' => 'required',
          'lastname' => 'required',
          'image' => 'required',
          'country' => 'required',
          'city' => 'required',
          'email' => 'required|email|unique:users',
		  'password' => 'required',
          'block_description' => 'required',
          'year' => 'required',


        ]);
		
		
		
		
        $lastid=User::create($data)->id;
		
		$data2=array(
                'id_user'=>$lastid,
                'notifiable_type' => 'Register',
                'notifiable' => 'Create new adminddd',
                'status' => '0',    
				'read_at' => '0'



            );
        Notifications::insert($data2);
       

	  
      return redirect::route('manage-admin.index')->with('success', 'Create with success!');
  
    }
	
	
	
	public function edit($id)
    {
        if(Auth::check())
		{
       $Category = Category::find($id);

 
        return view('admin.category.edit', compact('Category'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'status'=> 'required|max:2055',
			'image' =>'required',
			]);

       
		
        $User = User::find($id);
        $User->status = $request->get('status');
        $User->save();

        return redirect::to('admin/manage-admin/'.$id.'')->with('success', 'Update with success!');
    }

	


	
	public function show($id)
    {
        
		if(Auth::check()){
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();

		
		$User = User::find($id);

	 

        return view('admin.manage-admin.show', compact('User', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 public function destroy($id)
    {
        //
        $post = Category::where('id',$id)->delete();
   
        return Response::json($post);
    }
}