<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Purchase;
use App\Orders;
use App\Items;
use App\Notifications;
use App\Ordre;
use App\Regulation;

use Response;

class bookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        
		if(Auth::check()){
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		

		$ordres1 = DB::table('ordres')
		->join('users', 'ordres.customer_id', '=', 'users.id')
		->select('*')
	    ->get();
$ordres = DB::table('ordres')
		->join('users', 'ordres.customer_id', '=', 'users.id')
        ->join('regulations', 'ordres.orders_number', '=', 'regulations.id_ordre')

		->select('*')
	    ->get();
	   	
        $orderscount = DB::table('ordres')
        ->count('id');
       return view('admin.booking.index', compact('orderscount', 'notifications','ordres'));
		}
	    
		return redirect::to("admin");

    }



	
	public function update(Request $request, $id)
    {
        $request->validate([
            'localisation_etats'=> 'required|max:2055',
			]);

        $orders = Orders::find($id);
        $orders->orders_number = $request->get('orders_number');
        $orders->localisation_etats = $request->get('localisation_etats');
        $orders->save();
		
		$data2=array(
                'id_user'=>''.ucfirst(Auth()->user()->id).'',
                'notifiable_type' => 'Edit localisation',
                'notifiable' => 'Confirmed localisation of '.$request->get('user_name').'',
                'status' => '0',    
				'read_at' => '0'



            );
        Notifications::insert($data2);

        return redirect::to('admin/booking/'.$request->get('orders_number').'')->with('success', 'Update with success!');
    }


	
	
	
	
	
	public function show($orders_number)
    {
        
		if(Auth::check()){				 
       $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		$Orders = DB::table('ordres')
		->where('orders_number', '=', "".$orders_number."")
		->join('users', 'ordres.customer_id', '=', 'users.id')
		->select('*')
	    ->get();
		

        return view('admin.booking.show', compact('Orders','notifications','orders_number'));
		}
	    
		return redirect::to("admin");

    }
    public function showpayment($orders_number)
    {
        
		if(Auth::check()){				 
       $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Orders = DB::table('ordres')
		->where('orders_number', '=',$orders_number)
		->join('regulations','ordres.orders_number','=','regulations.id_ordre')
		->select('*')
	    ->get();
		

 return view('admin.booking.showpayment', compact('Orders','notifications','orders_number'));
		}
	    
		return redirect::to("admin");

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 public function destroy($id)
    {
        $post = Category::where('id',$id)->delete();
   
        return Response::json($post);
    }
}