<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;

use Response;

class SubCategoryController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		

		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Category'] = Category::where('parent_id', '!=', "0")
		->where('parent_id', '!=', "0")
        ->get();
		
		$Categorylist = DB::table('category')
        ->where('parent_id', '=', "0")
        ->get();

	     $Product = DB::table('products')
         ->get();

		$Categorycount = DB::table('category')
		->where('parent_id', '!=', "0")
        ->count('id');
		
	    $Categorycount2 = DB::table('category')
		->join('category_products', 'category.id', '=', 'category_products.id_category');


	
        return view('admin.subcategory.index',$data, compact('Categorycount', 'Product', 'Categorylist', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }


    
    
    public function index_card()
    {
        
		if(Auth::check())
		{
		

		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Category'] = Category::where('parent_id', '!=', "0")
		->where('parent_id', '!=', "0")
        ->get();
		
		$Categorylist = DB::table('category')
        ->where('parent_id', '=', "0")
        ->get();

	     $Product = DB::table('products')
         ->get();

		$Categorycount = DB::table('category')
		->where('parent_id', '!=', "0")
        ->count('id');
		
	    $Categorycount2 = DB::table('category')
		->join('category_products', 'category.id', '=', 'category_products.id_category');


	
        return view('admin.subcategory.index_card',$data, compact('Categorycount', 'Product', 'Categorylist', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }


	
	
    public function index2()
    {
        //
		if(Auth::check())
		{
		  
		$Categorylist = DB::table('category')
        ->where('parent_id', '=', "0")
        ->get();
		
		$data['Category'] = DB::table('category')
        ->where('parent_id', '!=', "0")
        ->get();
		
		 $Product = DB::table('products')
         ->get();
		
	   	$subcategorycount = DB::table('category')
        ->where('parent_id', '!=', "0")
		->join('category_products', 'category.id', '=', 'category_products.id_category')
         ->count('name');


        return view('admin.subcategory.index',$data, compact('subcategorycount', 'Categorylist'));
		
		
		}
	    
		return redirect::to("admin");

    }
	
	
	    public function create()
    {
		 if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();

		$category = DB::table('category')
		  ->where('parent_id', '=', "0")
          ->get();

        return view('admin.subcategory.create', compact('notifications'), [
            'category'   => [],
            'category' => Category::with('children')->where('parent_id', '0')->get(),
            ], compact('category'));
			}
								return redirect::to("admin");

    }
	
	
	
	
	function check(Request $request)
   	{
   		if($request->get('name'))
   		{
   			$name = $request->get('name');
   			$data = DB::table("category")
   				->where('name', $name)
   				->count();
   			if($data > 0)
   			{
   				echo 'not_unique';
   			}
   			else
   			{
   				echo 'unique';
   			}
   		}
   	}
	
	
	    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required|max:255',
			'name_de'=> 'required|max:2055',
            'parent_id'=> 'required|max:255',
            'photo'=> 'required|max:255',
            'active'=> 'required|max:255'
		  ]);

      $Category= new Category([
            'name' => $request->get('name'),
            'name_de' => $request->get('name_de'),
            'parent_id' => $request->get('parent_id'),
            'photo' => $request->get('photo'),
            'active' => $request->get('active'),
        ]);
		
      $Category->save();

      return redirect::route('subcategory.index')->with('success', 'Create with success!');
  
    }
	
	public function edit($id)
    {
        if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Category = Category::find($id);

	   $Categorylist = DB::table('category')
        ->where('parent_id', '=', "0")
        ->get();
 
        return view('admin.subcategory.edit', compact('Category', 'Categorylist', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'parent_id'=> 'required|max:2055',
            'name'=> 'required|max:2055',
            'name_de'=> 'required|max:2055',			
            'active'=> 'required|max:2055',
			]);

        $Category = Category::find($id);
        $Category->parent_id = $request->get('parent_id');
        $Category->name = $request->get('name');
		$Category->name_de = $request->get('name_de');
        $Category->active = $request->get('active');
        $Category->save();

        return redirect::route('subcategory.index')->with('success', 'Update with success!');
    }


	
	
	
	
		public function translate($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Category = Category::find($id);

 
        return view('admin.subcategory.translate', compact('Category', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	
	public function show($id)
    {
        
		if(Auth::check()){
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Category = Category::find($id);

	 

        return view('admin.subcategory.show', compact('Category', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 public function destroy($id)
    {
        //
        $post = Category::where('id',$id)->delete();
   
        return Response::json($post);
    }
}