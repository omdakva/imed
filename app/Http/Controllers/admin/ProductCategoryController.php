<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;
use App\Product2;
use App\Category_product;
use App\Orders;
use App\Items;
use Response;

class ProductCategoryController extends Controller
{
    
	 
	
    public function index()
    {
        
		if(Auth::check())
		{
		
				 
	   $data['category_products'] = DB::table('category_products')
       ->get();
	   
		 

        return view('admin.product.index',$data);
		}
	    
		return redirect::to("admin");

    } 

	public function destroy($id)
    {
        $post = Category_product::where('id',$id)->delete();
   
        return Response::json($post);
    }
}