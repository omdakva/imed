<?php
namespace App\Http\Controllers\admin\blog;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Blog_category;
use App\Blog_category_list;

use Response;

class HomeController extends Controller
{
    
    
    public function home()
    {
        if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Blog_category'] = DB::table('blog_category')
        ->get();

	    

		$blog_categorycount = DB::table('blog_category')
        ->count('id');
	
	
        return view('admin.blog.index',$data, compact('blog_categorycount', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }



    public function index()
    {
        if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Blog_category'] = DB::table('blog_category')
        ->get();

	    

		$blog_categorycount = DB::table('blog_category')
        ->count('id');
	
	
        return view('admin.news.category.index',$data, compact('blog_categorycount', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
    
    





    public function index_card()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Blog_category'] = DB::table('blog_category')
        ->get();

	    

		$blog_categorycount = DB::table('blog_category')
        ->count('id');
	
	
        return view('admin.news.category.index_card',$data, compact('blog_categorycount', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
    

	



















	
	
	public function store(Request $request)
    {
		    $this->validate($request,[
            'name'=> 'required|max:1255',
            'name_fr'=> 'required|max:255',
            'name_de'=> 'required|max:255',
            'active'=> 'required|max:255'
		  ]);

        $blog_category= new Blog_category([
        'name' => $request->get('name'),
        'name_fr' => $request->get('name_fr'),
        'name_de' => $request->get('name_de'),
        'active' => $request->get('active'),
        ]);
	
        $blog_category->save();
        return redirect::route('blog.category')->with('success', 'Create with success!');
  
    }
	
	
	
	
	public function edit($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Blog_category = Blog_category::find($id);

 
        return view('admin.news.category.edit', compact('Blog_category', 'notifications'));
		}
		return redirect::to("admin");
    }
	
	
	
	
	public function translate($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $News = News::find($id);

 
        return view('admin.news.translate', compact('News', 'notifications'));
		}
		return redirect::to("admin");
    }
	
	
	
	
	
	
	public function translate_update(Request $request, $id) 
   {
	   $this->validate($request,[
		  'name_de'=> 'required|max:22055',
		  'description_de'=> 'required|max:22055'
      ]);
        
	    $name_de = $request->input('name_de');
	    $description_de = $request->input('description_de');

        DB::update('update news set name_de = ?, description_de = ? where id = ?',[$name_de,$description_de,$id]);
	    return redirect()->to('admin/news/'.$id.'')->with('success', 'Create with success!');

   }
	
	
	public function update(Request $request, $id) 
   {
	   $this->validate($request,[
		  'name'=> 'required|max:22055',
		  'name_fr'=> 'required|max:22055',
		  'name_de'=> 'required|max:22055',
		  'active'=> 'required|max:22055'
      ]);
        
	    $name = $request->input('name');
	    $name_fr = $request->input('name_fr');
	    $name_de = $request->input('name_de');
	    $active = $request->input('active');

        DB::update('update blog_category set name = ?, name_fr = ?, name_de = ?, active = ? where id = ?',[$name,$name_fr,$name_de,$active,$id]);
	    return redirect()->to('admin/blog/category/'.$id.'')->with('success', 'Create with success!');

   }


    
   


   public function updateslider(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
          'photo' =>'required',
          'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
        
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/blog_image'),
		$photo->getClientOriginalName());

	  

	  
      DB::update('update news set photo = ? where id = ?',[$inputValue['photo'],$id]);
	    return redirect()->to('admin/news/'.$id.'')->with('success', 'Create with success!');

   }
	
	
	
	
	public function show($id)
    {
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Blog_category = Blog_category::find($id);

	 

        return view('admin.news.category.show', compact('Blog_category', 'notifications'));
		}
	    
		return redirect::to("admin");
    }
	
	
	
	
	
	public function show_translate($id)
    {
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $News = News::find($id);

	 

        return view('admin.news.show_translate', compact('News', 'notifications'));
		}
	    
		return redirect::to("admin");
    }
	

	
	
	
	
		public function view($id)
    {
        $data['Category'] = Category::find($id)
		->where('parent_id', '=', "0")
        ->get();
		

      $Category2 = DB::select('select * from category where id="'.$id.'"');
	  $subCategory = DB::select('select * from category where parent_id="'.$id.'"');

      $products = DB::select('select * from products where category_id="'.$id.'"');
      
	  $productscount = DB::table('products')
	  ->where('category_id', '=', "".$id."")
      ->count('id');




        return view('web.category',$data, compact('products' ,'Category2', 'productscount', 'subCategory'));
	
	    

    }
    
	
	
	 
	 public function destroy($id)
    {
        //
        $post = Category::where('id',$id)->delete();
   
   
        return Response::json($post);
    }
}