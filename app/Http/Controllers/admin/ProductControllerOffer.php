<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;
use App\Product2;
use App\Category_product;
use App\Orders;
use App\Items;
use App\Currency;

use Response;

class ProductControllerOffer extends Controller
{


  	public function translate($id)
    {
        if(Auth::check())
		{		
        $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Product = Product::find($id);


        return view('admin.product.offer', compact('Product', 'notifications'));
		}
		
		return redirect::to("admin");

    }  
    
	
	public function update(Request $request, $id) 
   {
	   $this->validate($request,[
	      'remise'=> 'required|max:2055',
      ]);
        
		
	    $products_id = $request->input('products_id');
	    $remise = $request->input('remise');

	  
      DB::update('update products set remise = ? where id = ?',[$remise,$id]);
	    return redirect()->route('product.index')->with('success', 'Create offer with success!');

   }
   
   
	public function show($id)
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Product = DB::table('products')
		->where('products.id', '=', $id)
	    ->get();
		
	
	 

        return view('admin.product.translateview', compact('Product', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
}