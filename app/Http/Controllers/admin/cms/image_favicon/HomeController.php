<?php

namespace App\Http\Controllers\admin\cms\image_favicon;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Image_favicon;
use Response;

class HomeController extends Controller
{
    

	
	public function index()
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		
		
		$Image_favicon = DB::table('image_favicon')
	    ->get();
		
	

        return view('admin.cms.image_favicon.index', compact('Image_favicon', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	


	public function create()
    {
		if(Auth::check())
		{
        
         $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();

        return view('admin.cms.image_favicon.create', compact('notifications'));
		}
	     return redirect::to("admin");

    }

	




		
	public function store(Request $request)
    {
        $this->validate($request,[
            'active'=> 'required|max:255',
			'photo' =>'required',
            'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
		  ]);

		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/image_favicon'),
		$photo->getClientOriginalName());
		
		
      $image_favicon= new Image_favicon([
        'active' => $request->get('active'),
	    'photo' => $inputValue['photo'],
        ]);
		
      $image_favicon->save();

      return redirect::route('cms.image-favicon')->with('success', 'Create with success!');
	}
	




	public function edit($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Image_favicon = Image_favicon::find($id);

 
        return view('admin.cms.image_favicon.edit', compact('Image_favicon', 'notifications'));
		}
		return redirect::to("admin");

    }

	
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'active'=> 'required|max:2055',
			]);

        $image_favicon = Image_favicon::find($id);
        $image_favicon->active = $request->get('active');
        $image_favicon->save();

	    return redirect()->route('cms.image-favicon')->with('success', 'Update with success!');
	}
	



   public function update2(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
		  'about'=> 'required|max:22055',
		  'about_de'=> 'required|max:22055'
      ]);
        

	  
	    $about = $request->input('about');
	    $about_de = $request->input('about_de');

	  
      DB::update('update crm_about set about = ?, about_de = ?  where id = ?',[$about,$about_de,$id]);
	    return redirect()->to('admin/cms/home/about')->with('success', 'Create with success!');

   }

   
   
	
public function updatephoto(Request $request, $id) 
   {
	   $this->validate($request,[
	     'photo' =>'required',
          'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:7048'
      ]);
        
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/about'),
		$photo->getClientOriginalName());
	  
	  

	  
      DB::update('update crm_about set photo = ? where id = ?',[$inputValue['photo'],$id]);
	    return redirect()->to('admin/cms/home/about')->with('success', 'Create with success!');

   }


   public function destroy($id)
   {
	   $post = Image_favicon::where('id',$id)->delete();
	   return Response::json($post);
   }

}