<?php

namespace App\Http\Controllers\admin\cms;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Slider_image;
use App\Product;
use Response;

class HomeController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Slider_image = DB::table('slider_image')
		->join('products', 'slider_image.id_product', '=', 'products.id')
		->select('products.product_name', 'products.id', 'slider_image.id_slider', 'slider_image.coordinate_x', 'slider_image.coordinate_y', 'products.product_price', 'products.product_currency')
	    ->get();
	
	
        return view('admin.cms.home.index', compact('Slider_image', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
	
	
	
	
    public function index_test()
    {
        
		if(Auth::check())
		{
		
	
        return view('admin.news.index_test');
		
		
		}
	    
		return redirect::to("admin");

    }
	
	



	
		    public function create()
    {
		if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
	
        return view('admin.news.create', compact('notifications'));
		}
	 return redirect::to("admin");

    }
	
	

	
	
	public function store(Request $request)
    {
		

        $this->validate($request,[
            'name'=> 'required|max:1255',
            'name_de'=> 'required|max:255',
            'description'=> 'required|max:24255',
            'photo'=> 'required|max:24255',
            'active'=> 'required|max:255'
		  ]);

      $News= new News([
        'name' => $request->get('name'),
        'name_de' => $request->get('name_de'),
        'description' => $request->get('description'),
        'photo' => $request->get('photo'),
        'active' => $request->get('active'),
        ]);
	
      $News->save();

		
return redirect::route('news.index')->with('success', 'Create with success!');
  
}
	
	public function edit($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Category = Category::find($id);

 
        return view('admin.category.edit', compact('Category', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
		  'description'=> 'required|max:22055',
          'photo' =>'required',
          'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
        
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/blog_image'),
		$photo->getClientOriginalName());
	    $description = $request->input('description');

	  
	    $products_id = $request->input('products_id');

	  
      DB::update('update news set photo = ?, description = ? where id = ?',[$inputValue['photo'],$description,$id]);
	    return redirect()->to('admin/news/'.$id.'')->with('success', 'Create with success!');

   }


	
	
	
	
	
	public function show($id)
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $News = News::find($id);

	 

        return view('admin.news.show', compact('News', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	
	
	public function translate($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Category = Category::find($id);

 
        return view('admin.category.translate', compact('Category', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	
	
	public function view($id)
    {
        $data['Category'] = Category::find($id)
		->where('parent_id', '=', "0")
        ->get();
		

      $Category2 = DB::select('select * from category where id="'.$id.'"');
	  $subCategory = DB::select('select * from category where parent_id="'.$id.'"');

      $products = DB::select('select * from products where category_id="'.$id.'"');
      
	  $productscount = DB::table('products')
	  ->where('category_id', '=', "".$id."")
      ->count('id');




        return view('web.category',$data, compact('products' ,'Category2', 'productscount', 'subCategory'));
	
	    

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 public function destroy($id)
    {
        //
        $post = Category::where('id',$id)->delete();
   
   
        return Response::json($post);
    }
}