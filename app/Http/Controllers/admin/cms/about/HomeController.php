<?php

namespace App\Http\Controllers\admin\cms\about;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Crm_about;
use Response;

class HomeController extends Controller
{
    

	
	public function index()
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		
		
		$Crm_about = DB::table('crm_about')
	    ->get();
		
	

        return view('admin.cms.home.about.index', compact('Crm_about', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	


	
	
	
	public function update(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
		  'about'=> 'required|max:22055',
		  'about_de'=> 'required|max:22055'
      ]);
        

	  
	    $about = $request->input('about');
	    $about_de = $request->input('about_de');

	  
      DB::update('update crm_about set about = ?, about_de = ?  where id = ?',[$about,$about_de,$id]);
	    return redirect()->to('admin/cms/home/about')->with('success', 'Create with success!');

   }

   
   
	
	public function updatephoto(Request $request, $id) 
   {
	   $this->validate($request,[
	     'photo' =>'required',
          'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:7048'
      ]);
        
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/about'),
		$photo->getClientOriginalName());
	  
	  

	  
      DB::update('update crm_about set photo = ? where id = ?',[$inputValue['photo'],$id]);
	    return redirect()->to('admin/cms/home/about')->with('success', 'Create with success!');

   }

}