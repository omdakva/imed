<?php
namespace App\Http\Controllers\admin\cms\pages;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Pages;
use Response;

class HomeController extends Controller
{
    


	public function index()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
	

	    $data['Pages'] = DB::table('Pages')
		 ->get();
		 
		 $Product = DB::table('products')
         ->get();

		$Cataloguecount = DB::table('Pages')
        ->count('id');
		
	
        return view('admin.cms.pages.index',$data, compact('Cataloguecount', 'Product', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

	}
	








	public function create()
    {
		if(Auth::check())
		{
	     $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
	
        return view('admin.cms.pages.create', compact('notifications'));
	    }
	    
		return redirect::to("admin");
    }
	
	
	
	
	
	public function store(Request $request)
    {
		 $inputValue= $request->validate([
            'name'=> 'required|max:255',
			'name_de'=> 'required|max:255',
			'name_fr'=> 'required|max:255',
			'front_page'=> 'required|max:255',
			'active'=> 'required|max:255'


		  ]);
		 
        $pages= new Pages([
            'name' => $request->get('name'),
            'name_de' => $request->get('name_de'),
			'name_fr' => $request->get('name_fr'),
			'front_page' => $request->get('front_page'),
			'active' => $request->get('active'),

        ]);
		
		$pages->save();
	    return redirect()->route('cms.pages')->with('success', 'Create with success!');
	}
	


		
	
	public function show($id)
    {
        if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		
		
		$Pages = Pages::find($id);

		
        return view('admin.cms.pages.show', compact('Pages', 'notifications'));
		}
	    
		return redirect::to("admin");
    }
	





	public function store_product2(Request $request)
    {
		 $inputValue= $request->validate([
            'id_product'=> 'required|max:255',
            'parent_id'=> 'required|max:255',
            'photo'=> 'required|max:255',
			'active'=> 'required|max:255',
			'coordinate_x'=> 'required|max:255',
			'coordinate_y'=> 'required|max:255'
		  ]);
		  $Catalogue= new Catalogue([
            'id_product' => $request->get('id_product'),
            'parent_id' => $request->get('parent_id'),
			'active' => $request->get('active'),
			'coordinate_x' => $request->get('coordinate_x'),
            'coordinate_y' => $request->get('coordinate_y'),
            'photo' => $request->get('photo'),

        ]);	
		$Catalogue->save();
	    return redirect()->to('admin/cms/home/catalogue/'.$request->get('parent_id').'')->with('success', 'Create with success!');
	}



	public function add_product($id, $x, $y)
    {
       if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Catalogue = DB::table('catalogue')
		->where('catalogue.id_catalogue', '=', $id)
		->get();
		
		
		$Productlist = DB::table('products')
		->get();

		
		
        return view('admin.cms.home.catalogue.new_product', compact('Catalogue', 'Productlist', 'notifications'));
		}
	    
		return redirect::to("admin");
	}
	








	public function store_product(Request $request)
    {
		 $inputValue= $request->validate([
            'id_product'=> 'required|max:255',
            'parent_id'=> 'required|max:255',
			'photo'=> 'required|max:255',
			'coordinate_x'=> 'required|max:255',
			'coordinate_y'=> 'required|max:255',
			'active'=> 'required|max:255',
			'updated_at'=> 'required|max:255',
			'created_at'=> 'required|max:255'


		  ]);
	

        $Catalogue= new Catalogue([
            'id_product' => $request->get('id_product'),
            'parent_id' => $request->get('parent_id'),
			'photo' => $request->get('photo'),
			'coordinate_x' => $request->get('coordinate_x'),
            'coordinate_y' => $request->get('coordinate_y'),
			'active' => $request->get('active'),
			'updated_at' => $request->get('updated_at'),
			'created_at' => $request->get('created_at'),

        ]);
		
		$Catalogue->save();
	    return redirect()->route('cms.catalogue')->with('success', 'Create with success!');
	}







	public function showproduct($id)
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Catalogue = DB::table('catalogue')
		->where('catalogue.id_catalogue', '=', $id)
		->get();
		
		$Catalogueproduct = DB::table('catalogue')
		->where('catalogue.parent_id', '=', $id)
	    ->get();
	
		$Productlist = DB::table('products')
		->get();

		 $Productlistview = DB::table('products')
         ->join('catalogue', 'products.id', '=', 'catalogue.id_product')
		 ->select('products.id', 'products.photo', 'products.product_name', 'products.long_description', 'products.short_description', 'products.photo', 'products.stock', 'products.remise', 'products.product_price', 'products.product_currency', 'products.created_at', 'products.active', 'catalogue.id_catalogue', 'catalogue.id_product', 'catalogue.parent_id', 'catalogue.coordinate_x', 'catalogue.coordinate_y')
		 ->where('catalogue.id_catalogue', '=', $id)
         ->get();
		
        return view('admin.cms.home.catalogue.showproduct', compact('Catalogue', 'Catalogueproduct', 'Productlistview', 'Productlist', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	


	
	public function updatephotoprin(Request $request, $id) 
	{
		$this->validate($request,[
			'photo' =>'required',
			'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
	   ]);
		 
		 $photo = $request->file('photo');
		 $inputValue['photo'] = $photo->getClientOriginalName();
		 $photo->move(public_path('photo/cataloge'),
		 $photo->getClientOriginalName());
	   
	   
	     DB::update('update catalogue set photo = ? where id_catalogue = ?',[$inputValue['photo'],$id]);
		 return redirect()->to('admin/cms/home/catalogue/'.$id.'')->with('success', 'Create with success!');
 
	}









		
	public function addupdate(Request $request)
    {
		
		    $inputValue= $request->validate([
            'id_product'=> 'required|max:255',
            'parent_id'=> 'required|max:255',
            'photo'=> 'required|max:255',
			'active'=> 'required|max:255',
			'coordinate_x'=> 'required|max:255',
			'coordinate_y'=> 'required|max:255'


		  ]);
		  
		
		

        $Catalogue= new Catalogue([
            'id_product' => $request->get('id_product'),
            'parent_id' => $request->get('parent_id'),
            'photo' => $request->get('photo'),
            'active' => $request->get('active'),
            'coordinate_x' => $request->get('coordinate_x'),
            'coordinate_y' => $request->get('coordinate_y'),

        ]);
		
		$Catalogue->save();
	    return redirect()->to('admin/cms/home/catalogue/'.$request->get('parent_id').'')->with('success', 'Create with success!');
	}





	public function updateproduct(Request $request, $id) 
   {
	   $this->validate($request,[
		  'id_product'=> 'required|max:22055'
      ]);
        
   $id_product = $request->input('id_product');

	DB::update('update catalogue set id_product = ? where id_catalogue = ?',[$id_product,$id]);
    return redirect()->to('admin/cms/home/catalogue/product/'.$id.'')->with('success', 'Create with success!');

   }





	
	public function update(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
		  'id_product'=> 'required|max:22055'
      ]);
        

	  
	    $id_product = $request->input('id_product');

	  
      DB::update('update slider_image set id_product = ? where id_slider = ?',[$id_product,$id]);
	    return redirect()->to('admin/cms/home/slider/'.$id.'')->with('success', 'Create with success!');

   }




   public function productdelete($id)
   {
	   //
	   $post = Catalogue::where('id_catalogue',$id)->delete();

  	   return redirect()->back()->with('success', 'Create with success!');
	   
   }


   public function destroy($id)
    {
        //
        $post = Catalogue::where('id_catalogue',$id)->delete();
   
   
        return Response::json($post);
    }


}