<?php

namespace App\Http\Controllers\admin\cms\our_article;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Our_article;
use App\Product;

use Response;

class HomeController extends Controller
{
    

	
	public function index()
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		
	
		$Our_article = DB::table('our_article')
	    ->get();
		
		$Productlist = DB::table('products')
		->get();

		 $productview = DB::table('our_article')
         ->join('products', 'our_article.id_product_p', '=', 'products.id')
		 ->select('products.id', 'products.photo', 'products.product_name', 'products.long_description', 'products.short_description', 'products.photo', 'products.stock', 'products.remise', 'products.product_price', 'products.product_currency', 'products.created_at', 'products.active', 'our_article.id_article', 'our_article.id_product_p')
		 ->get();
		 

		 $productview1 = DB::table('our_article')
         ->join('products', 'our_article.id_product_s1', '=', 'products.id')
		 ->select('products.id', 'products.photo', 'products.product_name', 'products.long_description', 'products.short_description', 'products.photo', 'products.stock', 'products.remise', 'products.product_price', 'products.product_currency', 'products.created_at', 'products.active', 'our_article.id_article', 'our_article.id_product_s1')
		 ->get();
		 
		 $productview2 = DB::table('our_article')
         ->join('products', 'our_article.id_product_s2', '=', 'products.id')
		 ->select('products.id', 'products.photo', 'products.product_name', 'products.long_description', 'products.short_description', 'products.photo', 'products.stock', 'products.remise', 'products.product_price', 'products.product_currency', 'products.created_at', 'products.active', 'our_article.id_article', 'our_article.id_product_s2')
		 ->get();
		 

		 $productview3 = DB::table('our_article')
         ->join('products', 'our_article.id_product_s3', '=', 'products.id')
		 ->select('products.id', 'products.photo', 'products.product_name', 'products.long_description', 'products.short_description', 'products.photo', 'products.stock', 'products.remise', 'products.product_price', 'products.product_currency', 'products.created_at', 'products.active', 'our_article.id_article', 'our_article.id_product_s3')
		 ->get();
		 
		 $productview4 = DB::table('our_article')
         ->join('products', 'our_article.id_product_s4', '=', 'products.id')
		 ->select('products.id', 'products.photo', 'products.product_name', 'products.long_description', 'products.short_description', 'products.photo', 'products.stock', 'products.remise', 'products.product_price', 'products.product_currency', 'products.created_at', 'products.active', 'our_article.id_article', 'our_article.id_product_s4')
         ->get();
	

        return view('admin.cms.home.our_article.index', compact('Our_article', 'Productlist', 'productview', 'productview1', 'productview2', 'productview3', 'productview4', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	









	public function updatedescription(Request $request, $id) 
	{
		$this->validate($request,[
		   'description'=> 'required|max:22055'
	   ]);
		 
 
	   
		 $description = $request->input('description');
 
	   
	   DB::update('update our_article set description = ?  where id_article = ?',[$description,$id]);
	   return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
 
	}



	




   public function update_product_p(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_p'=> 'required|max:22055'
	]);
	  
    $id_product_p = $request->input('id_product_p');
    DB::update('update our_article set id_product_p = ?  where id_article = ?',[$id_product_p,$id]);
	return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }





   public function update_product_s1(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_s1'=> 'required|max:22055'
	]);
      $id_product_s1 = $request->input('id_product_s1');
      DB::update('update our_article set id_product_s1 = ?  where id_article = ?',[$id_product_s1,$id]);
	  return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }






   


   
   public function update_product_s2(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_s2'=> 'required|max:22055'
	]);
      $id_product_s2 = $request->input('id_product_s2');
      DB::update('update our_article set id_product_s2 = ?  where id_article = ?',[$id_product_s2,$id]);
	  return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }



   public function update_product_s3(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_s3'=> 'required|max:22055'
	]);
      $id_product_s3 = $request->input('id_product_s3');
      DB::update('update our_article set id_product_s3 = ?  where id_article = ?',[$id_product_s3,$id]);
	  return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }


   

   public function update_product_s4(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_s4'=> 'required|max:22055'
	]);
      $id_product_s4 = $request->input('id_product_s4');
      DB::update('update our_article set id_product_s4 = ?  where id_article = ?',[$id_product_s4,$id]);
	  return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }



}







