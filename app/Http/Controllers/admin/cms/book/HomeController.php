<?php

namespace App\Http\Controllers\admin\cms\book;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Category_product;
use App\Product;

use Response;

class HomeController extends Controller
{
	
	public function index()
    {
       if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();

	     $Product = DB::table('products')
         ->get();

		$Categorycount = DB::table('category')
		->where('parent_id', '=', "0")
		->count('id');
		
		$products_count = DB::table('category')
		->join('category_products', 'category.id', '=', 'category_products.id_category')
		->join('products', 'category_products.id_product', '=', 'products.id')
		->select('category.id', 'category.name', 'products.product_name', 'category.parent_id', 'category_products.id_product')
	    ->get();

        return view('admin.cms.book.index',$data, compact('products_count','Product','Categorycount','notifications'));
		}
	    
		return redirect::to("admin");
    }
	
	

	public function updatedescription(Request $request, $id) 
	{
		$this->validate($request,[
		   'description'=> 'required|max:22055'
	   ]);
		 
 
	   
		 $description = $request->input('description');
 
	   
	   DB::update('update our_article set description = ?  where id_article = ?',[$description,$id]);
	   return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
	}





   public function update_product_p(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_p'=> 'required|max:22055'
	]);
	  
    $id_product_p = $request->input('id_product_p');
    DB::update('update our_article set id_product_p = ?  where id_article = ?',[$id_product_p,$id]);
	return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }





   public function update_product_s1(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_s1'=> 'required|max:22055'
	]);
      $id_product_s1 = $request->input('id_product_s1');
      DB::update('update our_article set id_product_s1 = ?  where id_article = ?',[$id_product_s1,$id]);
	  return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }



   
   public function update_product_s2(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_s2'=> 'required|max:22055'
	]);
      $id_product_s2 = $request->input('id_product_s2');
      DB::update('update our_article set id_product_s2 = ?  where id_article = ?',[$id_product_s2,$id]);
	  return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }



   public function update_product_s3(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_s3'=> 'required|max:22055'
	]);
      $id_product_s3 = $request->input('id_product_s3');
      DB::update('update our_article set id_product_s3 = ?  where id_article = ?',[$id_product_s3,$id]);
	  return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }


   

   public function update_product_s4(Request $request, $id) 
   {
	$this->validate($request,[
		'id_product_s4'=> 'required|max:22055'
	]);
      $id_product_s4 = $request->input('id_product_s4');
      DB::update('update our_article set id_product_s4 = ?  where id_article = ?',[$id_product_s4,$id]);
	  return redirect()->to('admin/cms/home/our_article')->with('success', 'Create with success!');
   }



}







