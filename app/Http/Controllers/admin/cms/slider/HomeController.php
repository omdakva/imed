<?php

namespace App\Http\Controllers\admin\cms\slider;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Slider_image;
use App\Product;
use Response;

class HomeController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Slider_image = DB::table('slider_image')
		->join('products', 'slider_image.id_product', '=', 'products.id')
		->select('products.product_name', 'products.id', 'slider_image.id_slider', 'slider_image.coordinate_x', 'slider_image.coordinate_y', 'products.product_price', 'products.product_currency')
	    ->get();
	
	
        return view('admin.cms.home.slider.index', compact('Slider_image', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
	
	
	




	
	
		
	
	public function show($id)
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Product1 = DB::table('products')
		->where('products.id', '=', $id)
	    ->get();
		
		$Productlist = DB::table('products')
	    ->get();
		
		$Category = DB::table('products')
		->join('category_products', 'products.id', '=', 'category_products.id_product')
		->join('category', 'category_products.id_category', '=', 'category.id')
		->select('category.id', 'category.name', 'category.parent_id')
		->where('products.id', '=', $id)
	    ->get();
		
	   	 $Product = DB::table('slider_image')
         ->join('products', 'slider_image.id_product', '=', 'products.id')
		 ->select('products.*', 'slider_image.id_slider', 'slider_image.id_product')
	     ->where('slider_image.id_slider', '=', $id)
         ->get();

        return view('admin.cms.home.slider.show', compact('Product', 'Productlist', 'notifications', 'Category'));
		}
	    
		return redirect::to("admin");

    }
	
	


	
	
	


	public function update(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
		  'id_product'=> 'required|max:22055'
      ]);
        

	  
	    $id_product = $request->input('id_product');

	  
      DB::update('update slider_image set id_product = ? where id_slider = ?',[$id_product,$id]);
	    return redirect()->to('admin/cms/home/slider/'.$id.'')->with('success', 'Create with success!');

   }






   


   public function create($x, $y)
   {
	  if(Auth::check()){
				
		$notifications = DB::table('notifications')
	   ->where('status', '=', "0")
	   ->get();
	  
	   $Productlist = DB::table('products')
	   ->get();

	   
	   
	   return view('admin.cms.home.slider.create', compact('Productlist', 'notifications'));
	   }
	   
	   return redirect::to("admin");
   }





	public function store(Request $request)
    {
		 $inputValue= $request->validate([
            'id_product'=> 'required|max:255',
			'coordinate_x'=> 'required|max:255',
			'coordinate_y'=> 'required|max:255',
			'active'=> 'required|max:255'


		  ]);
	

        $slider_image= new Slider_image([
            'id_product' => $request->get('id_product'),
			'coordinate_x' => $request->get('coordinate_x'),
            'coordinate_y' => $request->get('coordinate_y'),
			'active' => $request->get('active'),

        ]);
		
		$slider_image->save();
	    return redirect()->route('cms.home.slider')->with('success', 'Create with success!');
	}


}