<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Transport_Costs;
use Response;

class TransportCosts extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$TransportCosts = DB::table('transport_costs')
		->get();
	
	
        return view('admin.setting.transport_costs.index', compact('TransportCosts', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
	
	



	
	public function create()
    {
		if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
        
        
        $currency = DB::table('currency')
		->where('active', '=', "1")
       ->get();
	
        return view('admin.setting.transport_costs.create', compact('currency', 'notifications'));
		}
	 return redirect::to("admin");

    }
	
	

	
	
	public function store(Request $request)
    {
		

        $this->validate($request,[
            'type'=> 'required|max:1255',
            'kilometre'=> 'required|max:255',
            'quantity'=> 'required|max:255',
            'price'=> 'required|max:24255',
            'currency'=> 'required|max:24255',
            'active'=> 'required|max:24255'
		  ]);

      $Transport_Costs= new Transport_Costs([
        'type' => $request->get('type'),
        'kilometre' => $request->get('kilometre'),
        'quantity' => $request->get('quantity'),
        'price' => $request->get('price'),
        'currency' => $request->get('currency'),
        'active' => $request->get('active'),
        ]);
	
      $Transport_Costs->save();

		
return redirect::route('transport_costs')->with('success', 'Create with success!');
  
}
	
	public function edit($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Location = Location::find($id);

 
        return view('admin.location.edit', compact('Location', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
            'country'=> 'required|max:2055',
            'city'=> 'required|max:2055',
            'address'=> 'required|max:2055',
            'postcode'=> 'required|max:2055',
            'email'=> 'required|max:2055',
            'tel'=> 'required|max:2055',
            'mobile'=> 'required|max:2055',
            'active'=> 'required|max:2055'
      ]);
        
		
	    $country = $request->input('country');
        $city = $request->input('city');
        $address = $request->input('address');
        $postcode = $request->input('postcode');
        $email = $request->input('email');
        $tel = $request->input('tel');
        $mobile = $request->input('mobile');
        $active = $request->input('active');

	  
      DB::update('update location set country = ?, city = ?, address = ?, postcode = ?, email = ?, tel = ?, mobile = ?, active = ? where id = ?',[$country,$city,$address,$postcode,$email,$tel,$mobile,$active,$id]);
	    return redirect()->to('admin/location/list/'.$id.'')->with('success', 'Create with success!');

   }


	
	
	
	
	
	public function show($id)
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Location = Location::find($id);

	 

        return view('admin.location.show', compact('Location', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 


    public function destroy($id)
    {
        //
        $post = Transport_Costs::where('id',$id)->delete();
   
   
        return Response::json($post);
    }
}