<?php
namespace App\Http\Controllers\admin\setting;
use Illuminate\Http\Request;
use DB;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Country;
use Response;

class CountryController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		
		
		$data['Country'] = Country::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
	
	
        return view('admin.setting.country.index', $data, compact('notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
	
	



	
	public function create()
    {
		if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
         return view('admin.setting.country.create', compact('notifications'));
		}
	 return redirect::to("admin");

    }
	
	
	function check(Request $request)
   	{
   		if($request->get('country'))
   		{
   			$country = $request->get('country');
   			$data = DB::table("country")
   				->where('country', $country)
   				->count();
   			if($data > 0)
   			{
   				echo 'not_unique';
   			}
   			else
   			{
   				echo 'unique';
   			}
   		}
   	}
	
	
	public function store(Request $request)
    {
		

        $this->validate($request,[
            'country'=> 'required|max:255',
            'symbole'=> 'required|max:255',
            'code'=> 'required|max:255',
            'parent_id'=> 'required|max:255',
            'active'=> 'required|max:24255',
            'photo' =>'required',
            'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
          ]);
          
          $photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/country_logo'),
		$photo->getClientOriginalName());
		

      $Country= new Country([
        'country' => $request->get('country'),
        'symbole' => $request->get('symbole'),
        'code' => $request->get('code'),
        'parent_id' => $request->get('parent_id'),
        'active' => $request->get('active'),
        'photo' => $inputValue['photo'],
        ]);
	
      $Country->save();
return redirect::route('country.index')->with('success', 'Create with success!');
}







	public function store_city(Request $request)
    {
		    $this->validate($request,[
            'country'=> 'required|max:255',
            'symbole'=> 'required|max:255',
            'code'=> 'required|max:255',
            'parent_id'=> 'required|max:255',
            'photo'=> 'required|max:255',
            'active'=> 'required|max:24255'
		  ]);

      $Country= new Country([
        'country' => $request->get('country'),
        'symbole' => $request->get('symbole'),
        'code' => $request->get('code'),
        'parent_id' => $request->get('parent_id'),
        'photo' => $request->get('photo'),
        'active' => $request->get('active'),
        ]);
	
      $Country->save();
      return redirect::to('admin/country/'.$request->get('parent_id').'')->with('success', 'Create with success!');
     }
	 
	 
     
     
     public function update_logo(Request $request, $id) 
     {
         $this->validate($request,[
            'photo' =>'required',
            'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
          
          $photo = $request->file('photo');
          $inputValue['photo'] = $photo->getClientOriginalName();
          $photo->move(public_path('photo/country_logo'),
          $photo->getClientOriginalName());
        
         
  
        
        DB::update('update country set photo = ? where id = ?',[$inputValue['photo'],$id]);
          return redirect()->to('admin/country/'.$id.'')->with('success', 'Create with success!');
     }
    


     
	public function edit($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Country = Country::find($id);

 
        return view('admin.setting.country.edit', compact('Country', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
            'country'=> 'required|max:2055',
            'symbole'=> 'required|max:2055',
            'code'=> 'required|max:2055',
            'active'=> 'required|max:2055'
      ]);
        
		
	    $country = $request->input('country');
        $symbole = $request->input('symbole');
        $code = $request->input('code');
        $active = $request->input('active');

	  
      DB::update('update country set country = ?, symbole = ?, code = ?, active = ? where id = ?',[$country,$symbole,$code,$active,$id]);
	    return redirect()->to('admin/country/'.$id.'')->with('success', 'Create with success!');

   }


	
	
	
	
	
	public function show($id)
    {
		if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Country = Country::find($id);
		 
		 
		 $City = DB::table('country')
		->where('parent_id', '=', "".$id."")
        ->get();
		 
         return view('admin.setting.country.show', compact('Country', 'City', 'notifications'));
		}
		return redirect::to("admin");
    }
	
	
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 


    public function destroy($id)
    {
        //
        $post = Transport_Costs::where('id',$id)->delete();
   
   
        return Response::json($post);
    }
}