<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;

use Response;

class StatisticsClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
		if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		  
		$data['Category'] = DB::table('category')
	    ->get();

	   
		
		$Userscount = DB::table('users')
		->where('year', '=', "".date('Y')."")
	    ->get();
		
		$Userscountmale01 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "01".date('Y')."")
	    ->get();
		
		$Userscountmale02 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "02".date('Y')."")
	    ->get();
		
		$Userscountmale03 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "03".date('Y')."")
	    ->get();
		
		$Userscountmale04 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "04".date('Y')."")
	    ->get();


        $Userscountmale05 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "05".date('Y')."")
	    ->get();
		
		$Userscountmale06 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "06".date('Y')."")
	    ->get();
		
		$Userscountmale07 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "07".date('Y')."")
	    ->get();
		
		$Userscountmale08 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "08".date('Y')."")
	    ->get();
		
		
		$Userscountmale09 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "09".date('Y')."")
	    ->get();


        $Userscountmale10 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "10".date('Y')."")
	    ->get();
		
		$Userscountmale11 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "11".date('Y')."")
	    ->get();
		
		
		$Userscountmale12 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "12".date('Y')."")
		->get();
		





		$Userscountfemale01 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "01".date('Y')."")
	    ->get();
		
		$Userscountfemale02 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "02".date('Y')."")
	    ->get();
		
		$Userscountfemale03 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "03".date('Y')."")
	    ->get();
		
		$Userscountfemale04 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "04".date('Y')."")
		->get();

		$Userscountfemale05 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "05".date('Y')."")
		->get();

		$Userscountfemale06 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "06".date('Y')."")
		->get();

		$Userscountfemale07 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "07".date('Y')."")
		->get();

		$Userscountfemale08 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "08".date('Y')."")
		->get();

		$Userscountfemale09 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "09".date('Y')."")
		->get();

		$Userscountfemale10 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "10".date('Y')."")
		->get();

		$Userscountfemale11 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "11".date('Y')."")
		->get();

		$Userscountfemale12 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "12".date('Y')."")
		->get();


        $yearslist = collect(range(0, 5))->map(function ($item) {
        return (string) date('Y') - $item;
        });
		
        return view('admin.statisticsclient.index',$data, compact('Userscount', 'yearslist', 'Userscountmale01', 'Userscountmale02', 'Userscountmale03', 'Userscountmale04', 'Userscountmale05', 'Userscountmale06', 'Userscountmale07', 'Userscountmale08', 'Userscountmale09', 'Userscountmale10', 'Userscountmale11', 'Userscountmale12', 'Userscountfemale01', 'Userscountfemale02', 'Userscountfemale03', 'Userscountfemale04', 'Userscountfemale05', 'Userscountfemale06', 'Userscountfemale07', 'Userscountfemale08', 'Userscountfemale09', 'Userscountfemale10', 'Userscountfemale11', 'Userscountfemale12', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	 public function show ($id)
    {
        //
		if(Auth::check())
		{
		  $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Category'] = DB::table('category')
	    ->get();

	   
		
		$Userscount = DB::table('users')
		->where('year', '=', "".$id."")
	    ->get();
		
		$Userscountmale01 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "01".$id."")
	    ->get();
		
		$Userscountmale02 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "02".$id."")
	    ->get();
		
		$Userscountmale03 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "03".$id."")
	    ->get();
		
		$Userscountmale04 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "04".$id."")
	    ->get();


        $Userscountmale05 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "05".$id."")
	    ->get();
		
		$Userscountmale06 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "06".$id."")
	    ->get();
		
		$Userscountmale07 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "07".$id."")
	    ->get();
		
		$Userscountmale08 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "08".$id."")
	    ->get();
		
		
		$Userscountmale09 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "09".$id."")
	    ->get();


        $Userscountmale10 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "10".$id."")
	    ->get();
		
		$Userscountmale11 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "11".$id."")
	    ->get();
		
		
		$Userscountmale12 = DB::table('users')
		->where('gender', '=', "1")
		->where('month_number', '=', "12".$id."")
		->get();
		



		$Userscountfemale01 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "01".$id."")
	    ->get();
		
		$Userscountfemale02 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "02".$id."")
	    ->get();
		
		$Userscountfemale03 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "03".$id."")
	    ->get();
		
		$Userscountfemale04 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "04".$id."")
		->get();

		$Userscountfemale05 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "05".$id."")
		->get();

		$Userscountfemale06 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "06".$id."")
		->get();

		$Userscountfemale07 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "07".$id."")
		->get();

		$Userscountfemale08 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "08".$id."")
		->get();

		$Userscountfemale09 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "09".$id."")
		->get();

		$Userscountfemale10 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "10".$id."")
		->get();

		$Userscountfemale11 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "11".$id."")
		->get();

		$Userscountfemale12 = DB::table('users')
		->where('gender', '=', "2")
		->where('month_number', '=', "12".$id."")
		->get();



        $yearslist = collect(range(0, 5))->map(function ($item) {
        return (string) date('Y') - $item;
        });
		
        return view('admin.statisticsclient.filter',$data, compact('Userscount', 'yearslist', 'Userscountmale01', 'Userscountmale02', 'Userscountmale03', 'Userscountmale04', 'Userscountmale05', 'Userscountmale06', 'Userscountmale07', 'Userscountmale08', 'Userscountmale09', 'Userscountmale10', 'Userscountmale11', 'Userscountmale12', 'Userscountfemale01', 'Userscountfemale02', 'Userscountfemale03', 'Userscountfemale04', 'Userscountfemale05', 'Userscountfemale06', 'Userscountfemale07', 'Userscountfemale08', 'Userscountfemale09', 'Userscountfemale10', 'Userscountfemale11', 'Userscountfemale12', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

	}
	
    }

  