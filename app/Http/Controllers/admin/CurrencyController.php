<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Currency;

use Response;

class CurrencyController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		
        $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();

	     $data['currency'] = DB::table('currency')
       ->get();

	
        return view('admin.setting.currency.index',$data, compact('notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }



	
	
	
	    public function create()
    {
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
         $data['currency'] = DB::table('currency')
       ->get();

	
        return view('admin.setting.currency.create',$data, compact('notifications'));
    }
	
	
	    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required|max:255',
            'symbole'=> 'required|max:255',
            'active'=> 'required|max:255'
		  ]);

      $Currency= new Currency([
            'name' => $request->get('name'),
            'symbole' => $request->get('symbole'),
            'active' => $request->get('active'),
        ]);
		
      $Currency->save();

      return redirect::route('settings-currency.index')->with('success', 'Create with success!');
  
    }
	
	public function edit($id)
    {
        if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Currency = Currency::find($id);

 
        return view('admin.setting.currency.edit', compact('Currency', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'name'=> 'required|max:2055',
            'symbole'=> 'required|max:2055',
            'active'=> 'required|max:2055',
            ]);
            

         
                $Currency = Currency::find($id);
                $Currency->name = $request->get('name');
                $Currency->symbole = $request->get('symbole');
                $Currency->active = $request->get('active');
            
            $Currency->save();

                        return redirect::route('settings-currency.index')->with('success', 'Update with success!');

    
     }


	
	
	
	
	
	public function show($id)
    {
        
		if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Currency = Currency::find($id);

	 

        return view('admin.setting.currency.show', compact('Currency', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	
	 
	 public function destroy($id)
    {
        //
        $post = Currency::where('id',$id)->delete();
   
   
        return Response::json($post);
    }
}