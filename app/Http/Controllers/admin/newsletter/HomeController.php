<?php

namespace App\Http\Controllers\admin\newsletter;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Subscribe_newsletter;
use Response;

class HomeController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Subscribe_newsletter = DB::table('subscribe_newsletter')
        ->get();
	
	
        return view('admin.newsletter.index', compact('Subscribe_newsletter', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
	public function inbox()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Subscribe_newsletter = DB::table('subscribe_newsletter')
        ->get();
	
	
        return view('admin.newsletter.inbox', compact('Subscribe_newsletter', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
	


	
	

	

	
	
	public function show($id)
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Subscribe_newsletter = Subscribe_newsletter::find($id);

	 

        return view('admin.newsletter.show', compact('Subscribe_newsletter', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	

}