<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\News;
use App\Blog_category;
use App\Blog_category_list;
use Response;

class NewsController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['news'] = DB::table('news')
        ->get();

	    $blog_category = DB::table('blog_category')
		->get();

		$newscount = DB::table('news')
        ->count('id');
	
	
        return view('admin.news.index',$data, compact('blog_category', 'newscount', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
    
    






    public function index_card()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['news'] = DB::table('news')
        ->get();

	    

		$newscount = DB::table('news')
        ->count('id');
	
	
        return view('admin.news.index_card',$data, compact('newscount', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }

    

	
	public function create()
    {
		if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
        
       
		
		$data['news'] = DB::table('news')
        ->get();

	    $blog_category = DB::table('blog_category')
		->get();

		$newscount = DB::table('news')
        ->count('id');

	
        return view('admin.news.create',$data, compact('blog_category', 'newscount', 'notifications'));

		}
	 return redirect::to("admin");

    }
    
    






    public function store(Request $request)
    {
		    $inputValue= $request->validate([
                'name'=> 'required|max:1255',
            'name_de'=> 'required|max:255',
            'description'=> 'required|max:24255',
            'description_de'=> 'required|max:24255',
            'photo'=> 'required|max:24255',
            'active'=> 'required|max:255'

		  ]);
		  
	
        $News= new News([
            'name' => $request->get('name'),
            'name_de' => $request->get('name_de'),
            'description' => $request->get('description'),
            'description_de' => $request->get('description_de'),
            'photo' => $request->get('photo'),
            'active' => $request->get('active'),

        ]);
		
		
		$lastid=News::create($inputValue)->id;

		
        if(count($request->id_category) > 0)
        {
        foreach($request->id_category as $item=>$v){
            $data2=array(
                'id_blog'=>$lastid,
                'id_category'=>$request->id_category[$item]
            );
            Blog_category_list::insert($data2);
        }
        }

      return redirect::route('news.index')->with('success', 'Create with success!');
    }




	
    
    



    public function store_category(Request $request, $id)
    {
		if(count($request->category_id) > 0)
        {
            foreach($request->category_id as $item=>$v){
            $data2=array(
                'id_blog'=>$id,
                'id_category'=>$request->category_id[$item]
            );
            Blog_category_list::insert($data2);
        }
        }

        elseif(count($request->category_id) == 0)
        {
        return redirect()->to('admin/news/'.$id.'')->with('success', 'Create with success!');
        }
	return redirect()->to('admin/news/'.$id.'')->with('success', 'Create with success!');
    }




	
	
	public function edit($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $News = News::find($id);

 
        return view('admin.news.edit', compact('News', 'notifications'));
		}
		return redirect::to("admin");
    }
	
	
	
	
	public function translate($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $News = News::find($id);

 
        return view('admin.news.translate', compact('News', 'notifications'));
		}
		return redirect::to("admin");
    }
	
	
	
	
	
	
	public function translate_update(Request $request, $id) 
   {
	   $this->validate($request,[
		  'name_de'=> 'required|max:22055',
		  'description_de'=> 'required|max:22055'
      ]);
        
	    $name_de = $request->input('name_de');
	    $description_de = $request->input('description_de');

        DB::update('update news set name_de = ?, description_de = ? where id = ?',[$name_de,$description_de,$id]);
	    return redirect()->to('admin/news/'.$id.'')->with('success', 'Create with success!');

   }
	
	
	public function update(Request $request, $id) 
   {
	   $this->validate($request,[
		  'name'=> 'required|max:22055',
		  'description'=> 'required|max:22055',
		  'active'=> 'required|max:22055'
      ]);
        
	    $name = $request->input('name');
	    $description = $request->input('description');
	    $active = $request->input('active');

        DB::update('update news set name = ?, description = ?, active = ? where id = ?',[$name,$description,$active,$id]);
	    return redirect()->to('admin/news/'.$id.'')->with('success', 'Create with success!');

   }


    
   


   public function updateslider(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
          'photo' =>'required',
          'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
        
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/blog_image'),
		$photo->getClientOriginalName());

	  

	  
      DB::update('update news set photo = ? where id = ?',[$inputValue['photo'],$id]);
	    return redirect()->to('admin/news/'.$id.'')->with('success', 'Create with success!');

   }
	
	
	
	
	public function show($id)
    {
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $News = News::find($id);

         $Category = DB::table('blog_category')
		->where('active', '!=', "0")
        ->get();

         $Category_blog = DB::table('news')
         ->join('blog_category_list', 'news.id', '=', 'blog_category_list.id_blog')
         ->join('blog_category', 'blog_category_list.id_category', '=', 'blog_category.id')
         ->select('blog_category.id', 'blog_category.name', 'blog_category_list.created_at')
         ->where('news.id', '=', $id)
         ->get();

        return view('admin.news.show', compact('News', 'Category', 'Category_blog', 'notifications'));
		}
	    
		return redirect::to("admin");
    }
	
	
	
	
	
	public function show_translate($id)
    {
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $News = News::find($id);

	 

        return view('admin.news.show_translate', compact('News', 'notifications'));
		}
	    
		return redirect::to("admin");
    }
	

	
	
	
	
		public function view($id)
    {
        $data['Category'] = Category::find($id)
		->where('parent_id', '=', "0")
        ->get();
		

      $Category2 = DB::select('select * from category where id="'.$id.'"');
	  $subCategory = DB::select('select * from category where parent_id="'.$id.'"');

      $products = DB::select('select * from products where category_id="'.$id.'"');
      
	  $productscount = DB::table('products')
	  ->where('category_id', '=', "".$id."")
      ->count('id');




        return view('web.category',$data, compact('products' ,'Category2', 'productscount', 'subCategory'));
	}
    
	
    
    
    public function destroy_category($id)
    {
    $post = Blog_category_list::where('id_category',$id)->delete();
	return redirect()->back()->with('message', 'Delete!');

    }


	 
	 public function destroy($id)
    {
     $post = Category::where('id',$id)->delete();
     return Response::json($post);
    }
}