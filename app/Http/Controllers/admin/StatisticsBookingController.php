<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;

use Response;

class StatisticsBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
		if(Auth::check())
		{
		   $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		$data['Category'] = DB::table('category')
	    ->get();

	   
		
		$Categorycount = DB::table('category')
		->count('id');

        $yearslist = collect(range(0, 5))->map(function ($item) {
        return (string) date('Y') - $item;
        });
		
        return view('admin.statisticsbooking.index',$data, compact('Categorycount', 'yearslist', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
    }

  