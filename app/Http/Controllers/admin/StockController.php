<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;
use App\Notifications;

use Response;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        
		if(Auth::check())
		{
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Category = DB::table('category')
		->where('parent_id', '=', "0")
		->get();


				 
	   $data['Product'] = DB::table('products')
       ->get();
	   
	   
		 

        return view('admin.stock.index',$data, compact('Category', 'notifications'));
		}
	    
		return redirect::to("admin");

    }


	
	public function edit($id)
    {
        if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		
       $Product = Product::find($id);


        return view('admin.stock.edit', compact('Product', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id)
    {
            $request->validate([
            'stock'=> 'required|max:2055',
			]);

        $Product = Product::find($id);
        $Product->product_name = $request->get('product_name');
        $Product->stock = $request->get('stock');
        $Product->save();
		
		$data2=array(
                'id_user'=>''.ucfirst(Auth()->user()->id).'',
                'notifiable_type' => 'Edit stock',
                'notifiable' => 'Edit stock number of '.$request->get('product_name').' to '.$request->get('stock').'',
                'status' => '0',    
				'read_at' => '0'



            );
        Notifications::insert($data2);

        return redirect::route('stock.index')->with('success', 'Update with success!');
    }


	
	
	
	
	
	public function show($id)
    {
        
		if(Auth::check()){
				 
		$Product = Product::find($id);

	 

        return view('admin.product.show', compact('Product'));
		}
	    
		return redirect::to("admin");

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 public function destroy($id)
    {
        //
        $post = Category::where('id',$id)->delete();
   
        return Response::json($post);
    }
}