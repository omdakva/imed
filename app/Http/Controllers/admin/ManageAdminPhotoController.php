<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;
use App\Product_photo;
use App\Category_product;
use App\Orders;
use App\User;
use Response;

class ManageAdminPhotoController extends Controller
{
    

	public function update(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
          'photo' =>'required',
          'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
        
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('profile_pic/admin'),
		$photo->getClientOriginalName());
	  
	    $products_id = $request->input('products_id');

	  
      DB::update('update users set photo = ? where id = ?',[$inputValue['photo'],$id]);
	    return redirect()->to('admin/manage-admin/'.$id.'')->with('success', 'Create with success!');

   }

}