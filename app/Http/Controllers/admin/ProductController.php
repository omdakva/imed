<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;
use App\Product2;
use App\Category_product;
use App\Orders;
use App\Items;
use App\Currency;

use Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Category = DB::table('category')
		->where('parent_id', '=', "0")
		->get();


				 
	   $data['Product'] = DB::table('products')
       ->get();

	   
		 

        return view('admin.product.index',$data, compact('Category', 'notifications'));
		}
	    
		return redirect::to("admin");

    }   



 



    public function index_card()
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Category = DB::table('category')
		->where('parent_id', '=', "0")
		->get();


				 
	   $data['Product'] = DB::table('products')
       ->get();

	   
		 

        return view('admin.product.index_card',$data, compact('Category', 'notifications'));
		}
	    
		return redirect::to("admin");

    } 
    
    

    public function subCat(Request $request)
    {
         
        $parent_id = $request->cat_id;
         
        $subcategory = Category::where('id',$parent_id)
                              ->with('subcategory')
                              ->get();
        return response()->json([
            'subcategory' => $subcategory
        ]);
    }
	
	

    public function create()
    {
        if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$currency = DB::table('currency')
		->where('active', '=', "1")
       ->get();
	   
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
		
		
        $Category = Category::where('parent_id',0)->get();
		

        return view('admin.product.create',$data, compact('Category', 'notifications', 'currency'));
		}
		return redirect::to("admin");
    }
	
	
	
	
	
	
		
	function check(Request $request)
   	{
   		if($request->get('product_name'))
   		{
   			$product_name = $request->get('product_name');
   			$data = DB::table("products")
   				->where('product_name', $product_name)
   				->count();
   			if($data > 0)
   			{
   			echo 'not_unique';
   			}
   			else
   			{
   			echo 'unique';
   			}
   		}
   	}
	
	
	
	
	public function store(Request $request)
    {
		    $inputValue= $request->validate([
            'product_name'=> 'required|max:255',
            'product_name_de'=> 'required|max:255',
            'product_name_fr'=> 'required|max:255',
            'short_description'=> 'required|max:22055',
            'short_description_de'=> 'required|max:22055',
            'short_description_fr'=> 'required|max:22055',
            'product_price'=> 'required|max:255',
            'product_currency'=> 'required|max:255',
            'active'=> 'required|max:255',
            'long_description'=> 'required|max:22055',
            'long_description_de'=> 'required|max:22055',
            'long_description_fr'=> 'required|max:22055',
            'statut'=> 'required|max:255',
            'remise'=> 'required|max:255',
			'photo' =>'required',
            'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
			'stock'=> 'required|max:2048',
			'offer'=> 'required|max:2048'

		  ]);
		  
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/products_logo'),
		$photo->getClientOriginalName());
		

        $Product= new Product2([
            'product_name' => $request->get('product_name'),
            'product_name_de' => $request->get('product_name_de'),
            'product_name_fr' => $request->get('product_name_fr'),
            'short_description' => $request->get('short_description'),
            'short_description_de' => $request->get('short_description_de'),
            'short_description_fr' => $request->get('short_description_fr'),
            'product_price' => $request->get('product_price'),
            'product_currency' => $request->get('product_currency'),
            'active' => $request->get('active'),
            'long_description' => $request->get('long_description'),
            'long_description_de' => $request->get('long_description_de'),
            'long_description_fr' => $request->get('long_description_fr'),
            'statut' => $request->get('statut'),
            'remise' => $request->get('remise'),
            'stock' => $request->get('stock'),
            'offer' => $request->get('offer'),
		    'photo' => $inputValue['photo'],

        ]);
		
		
		$lastid=Product::create($inputValue)->id;

		
        if(count($request->category_id) > 0)
        {
        foreach($request->category_id as $item=>$v){
            $data2=array(
                'id_product'=>$lastid,
                'id_category'=>$request->category_id[$item]
            );
       Category_product::insert($data2);
      }
      }
	return redirect()->to('admin/product/'.$lastid.'/translate')->with('success', 'Create with success!');
    }
	
	
	
	
	
	
	
	
	
	public function store_category(Request $request, $id)
    {
        
        if(empty($request->category_id))
        {
        return redirect()->to('admin/product/'.$id.'')->with('success', 'Create with success!');
        }


        if(!empty($request->category_id))
        {
        if(count($request->category_id) > 0)
        {
            foreach($request->category_id as $item=>$v){
            $data2=array(
                'id_product'=>$id,
                'id_category'=>$request->category_id[$item]
            );
       Category_product::insert($data2);
        }
        }
    }

        elseif(count($request->category_id) < 0)
        {
        return redirect()->to('admin/product/'.$id.'')->with('success', 'Create with success!');
        }
	return redirect()->to('admin/product/'.$id.'')->with('success', 'Create with success!');
    }
    
    


    
    
	
	public function update(Request $request, $id) 
   {
	   $this->validate($request,[
	      'product_name_de'=> 'required|max:2055',
	      'long_description_de'=> 'required|max:2055',
	      'short_description_de'=> 'required|max:2055',
          'photo' =>'required',
          'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
        
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/products_logo'),
		$photo->getClientOriginalName());
	  
	    $products_id = $request->input('products_id');
	    $product_name_de = $request->input('product_name_de');
	    $long_description_de = $request->input('long_description_de');
	    $short_description_de = $request->input('short_description_de');

	  
      DB::update('update products set photo = ?, product_name_de = ?, long_description_de = ?, short_description_de = ? where id = ?',[$inputValue['photo'],$product_name_de,$long_description_de,$short_description_de,$id]);
	    return redirect()->to('admin/product/'.$request->input('products_id').'')->with('success', 'Create with success!');

   }
   
   
   
	
	public function translate($id)
    {
        if(Auth::check())
		{		
        $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Product = Product::find($id);


        return view('admin.product.translate', compact('Product', 'notifications'));
		}
		
		return redirect::to("admin");

    }
	

	
	public function show($id)
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Product = DB::table('products')
		->where('products.id', '=', $id)
	    ->get();
		
		$Category_product = DB::table('products')
		->join('category_products', 'products.id', '=', 'category_products.id_product')
		->join('category', 'category_products.id_category', '=', 'category.id')
		->select('category.id', 'category.name', 'category.parent_id')
		->where('products.id', '=', $id)
	    ->get();
	 
	    $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
		
		
        $Category = Category::where('parent_id',0)->get();

        return view('admin.product.show',$data, compact('Product', 'notifications', 'Category_product'));
		}
	    
		return redirect::to("admin");

    }







    public function story()
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Category = DB::table('category')
		->where('parent_id', '=', "0")
		->get();


				 
	   $data['Product'] = DB::table('products')
       ->get();

	   
		 

        return view('admin.product.story',$data, compact('Category', 'notifications'));
		}
	    
		return redirect::to("admin");

    } 








   
   
   	public function destroy_category($id)
    {
        $post = Category_product::where('id_category',$id)->delete();
		return redirect()->back()->with('message', 'Delete!');

    }
   
   
	 
	 public function destroy($id)
    {
        //
        $post = Product::where('id',$id)->delete();
        $post2 = Category_product::where('id_product',$id)->delete();
   
        return Response::json($post);
    }
}