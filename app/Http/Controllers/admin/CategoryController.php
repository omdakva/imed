<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Category_product;
use App\Product;

use Response;

class CategoryController extends Controller
{
    

    public function index()
    {
       if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();

	     $Product = DB::table('products')
         ->get();

		$Categorycount = DB::table('category')
		->where('parent_id', '=', "0")
        ->count('id');
		
	    $Categorycount2 = DB::table('category')
		->join('category_products', 'category.id', '=', 'category_products.id_category');


	
        return view('admin.category.index',$data, compact('Categorycount', 'Product', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");
    }



    
    

    public function index_card()
    {
       if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();

	     $Product = DB::table('products')
         ->get();

		$Categorycount = DB::table('category')
		->where('parent_id', '=', "0")
        ->count('id');
		
	    $Categorycount2 = DB::table('category')
		->join('category_products', 'category.id', '=', 'category_products.id_category');


	
        return view('admin.category.index_card',$data, compact('Categorycount', 'Product', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");
    }




	
	
	public function create()
    {
	
	    if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
        return view('admin.category.create', compact('notifications'), [
            'category'   => [],
            'category' => Category::with('children')->where('parent_id', '0')->get(),
            ]);
			}
					return redirect::to("admin");

    }
	
	
	
	
			
	function check(Request $request)
   	{
   		if($request->get('name'))
   		{
   			$name = $request->get('name');
   			$data = DB::table("category")
   				->where('name', $name)
   				->count();
   			if($data > 0)
   			{
   				echo 'not_unique';
   			}
   			else
   			{
   				echo 'unique';
   			}
   		}
   	}
	
	
	public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required|max:255',
            'name_fr'=> 'required|max:255',
            'name_de'=> 'required|max:255',
            'parent_id'=> 'required|max:255',
            'active'=> 'required|max:255',
			'photo' =>'required',
            'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
		  ]);

		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/category_logo'),
		$photo->getClientOriginalName());
		
		
      $Category= new Category([
        'name' => $request->get('name'),
        'seo_title_en' => "0",
        'keyword_en' => "0",
        'meta_description_en' => "0",
        'name_url' => strtolower (str_replace(' ', '-', ''.$request->get('name').'')),
        'name_fr' => $request->get('name_fr'),
        'seo_title_fr' => "0",
        'keyword_fr' => "0",
        'meta_description_fr' => "0",
        'name_fr_url' => strtolower (str_replace(' ', '-', ''.$request->get('name_fr').'')),
        'name_de' => $request->get('name_de'),
        'seo_title_de' => "0",
        'keyword_de' => "0",
        'meta_description_de' => "0",
        'name_de_url' => strtolower (str_replace(' ', '-', ''.$request->get('name_de').'')),
        'parent_id' => $request->get('parent_id'),
        'active' => $request->get('active'),
	    'photo' => $inputValue['photo'],
        ]);
		
      $Category->save();

      return redirect::route('category.index')->with('success', 'Create with success!');
    }
    
	
	
	
	
	
	
	public function store_product(Request $request)
    {
        $this->validate($request,[
            'id_category'=> 'required|max:255',
            'id_product'=> 'required|max:255'
		  ]);
		
      $Category_product= new Category_product([
        'id_category' => $request->get('id_category'),
        'id_product' => $request->get('id_product'),
        ]);
		
      $Category_product->save();

	    return redirect()->to('admin/category/'.$request->get('id_category').'')->with('success', 'Create with success!');
    }
	
	
	
	
	
	
	
	public function edit($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Category = Category::find($id);

 
        return view('admin.category.edit', compact('Category', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
    
    

	public function update(Request $request, $id)
    {
        $request->validate([
            'name'=> 'required|max:2055',
            'name_de'=> 'required|max:2055',
            'name_fr'=> 'required|max:2055',
            'active'=> 'required|max:2055',
			]);

        $Category = Category::find($id);
        $Category->name = $request->get('name');
        $Category->name_url = strtolower (str_replace(' ', '-', ''.$request->get('name').''));
        $Category->name_de = $request->get('name_de');
        $Category->name_fr_url = strtolower (str_replace(' ', '-', ''.$request->get('name_fr').''));
        $Category->name_fr = $request->get('name_fr');
        $Category->name_de_url = strtolower (str_replace(' ', '-', ''.$request->get('name_de').''));
        $Category->active = $request->get('active');
        $Category->save();

        return redirect::route('category.index')->with('success', 'Update with success!');
    }


	

	
    public function update_photo(Request $request, $id) 
    {
        $this->validate($request,[
           'photo' =>'required',
           'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
       ]);
         
         $photo = $request->file('photo');
         $inputValue['photo'] = $photo->getClientOriginalName();
         $photo->move(public_path('photo/category_logo'),
         $photo->getClientOriginalName());
       
         
       
         DB::update('update category set photo = ? where id = ?',[$inputValue['photo'],$id]);
         return redirect()->to('admin/category/'.$id.'')->with('success', 'Create with success!');
    }
    
	
	
	
    
    
    
    public function show($id)
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();	 
        
		$Category = Category::find($id);

		 $product = DB::table('products')->where('category_id', '=', $id)

        ->get();
        
        
		$products_count = DB::table('category')->get();
     

        return view('admin.category.show', compact('Category', 'products_count', 'product', 'notifications'));
		}
	    
		return redirect::to("admin");
    }
    
    









    public function show_fr($id)
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();	 
        
		$Category = Category::find($id);

		 $product = DB::table('products')
        ->get();
        
        
		$products_count = DB::table('category')
		->join('category_products', 'category.id', '=', 'category_products.id_category')
		->join('products', 'category_products.id_product', '=', 'products.id')
		->select('category.id', 'category.name', 'products.product_name', 'category.parent_id', 'category_products.id_product')
		->where('category.id', '=', $id)
	    ->get();
     

        return view('admin.category.show_fr', compact('Category', 'products_count', 'product', 'notifications'));
		}
	    
		return redirect::to("admin");
    }







    
    
    public function show_de($id)
    {
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();	 
        
		$Category = Category::find($id);

		 $product = DB::table('products')
        ->get();
        
        
		$products_count = DB::table('category')
		->join('category_products', 'category.id', '=', 'category_products.id_category')
		->join('products', 'category_products.id_product', '=', 'products.id')
		->select('category.id', 'category.name', 'products.product_name', 'category.parent_id', 'category_products.id_product')
		->where('category.id', '=', $id)
	    ->get();
     

        return view('admin.category.show_de', compact('Category', 'products_count', 'product', 'notifications'));
		}
	    
		return redirect::to("admin");
    }



	
	public function translate($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Category = Category::find($id);

 
        return view('admin.category.translate', compact('Category', 'notifications'));
		}
		return redirect::to("admin");
    }
	
	
    
    
	
	
	public function view($id)
    {
        $data['Category'] = Category::find($id)
		->where('parent_id', '=', "0")
        ->get();
		

      $Category2 = DB::select('select * from category where id="'.$id.'"');
	  $subCategory = DB::select('select * from category where parent_id="'.$id.'"');

      $products = DB::select('select * from products where category_id="'.$id.'"');
      
	  $productscount = DB::table('products')
	  ->where('category_id', '=', "".$id."")
      ->count('id');
    return view('web.category',$data, compact('products' ,'Category2', 'productscount', 'subCategory'));
	}







    public function seo_en(Request $request, $id)
    {
        $request->validate([
            'seo_title_en'=> 'required|max:2055',
            'keyword_en'=> 'required|max:2055',
            'meta_description_en'=> 'required|max:2055',
			]);

        $Category = Category::find($id);
        $Category->seo_title_en = $request->get('seo_title_en');
        $Category->keyword_en = $request->get('keyword_en');
        $Category->meta_description_en = $request->get('meta_description_en');
        $Category->save();

        return redirect::to('admin/category/'.$id.'')->with('success', 'Update with success!');
    }



    public function seo_de(Request $request, $id)
    {
        $request->validate([
            'seo_title_de'=> 'required|max:2055',
            'keyword_de'=> 'required|max:2055',
            'meta_description_de'=> 'required|max:2055',
			]);

        $Category = Category::find($id);
        $Category->seo_title_de = $request->get('seo_title_de');
        $Category->keyword_de = $request->get('keyword_de');
        $Category->meta_description_de = $request->get('meta_description_de');
        $Category->save();

        return redirect::to('admin/category/de/'.$id.'')->with('success', 'Update with success!');
    }


    


    public function seo_fr(Request $request, $id)
    {
        $request->validate([
            'seo_title_fr'=> 'required|max:2055',
            'keyword_fr'=> 'required|max:2055',
            'meta_description_fr'=> 'required|max:2055',
			]);

        $Category = Category::find($id);
        $Category->seo_title_fr = $request->get('seo_title_fr');
        $Category->keyword_fr = $request->get('keyword_fr');
        $Category->meta_description_fr = $request->get('meta_description_fr');
        $Category->save();

        return redirect::to('admin/category/fr/'.$id.'')->with('success', 'Update with success!');
    }
    



	
	 
	public function destroy($id)
    {
        $post = Category::where('id',$id)->delete();
        return Response::json($post);
    }
	
	
	
     public function destroy_product(Request $request, $id)
    {
        $post = Category_product::where('id_product',$id)->delete();
		
       $Category_product= new Category_product([
        'category_id' => $request->get('category_id'),
        ]);

	    return redirect()->to('admin/category/'.$request->get('category_id').'')->with('success', 'Create with success!');
    }
}