<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Methoddelivery;
use App\Currency;

use Response;

class MethoddeliveryController extends Controller
{
    

    public function index()
    {
        
      if(Auth::check())
		{
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$data['Methoddelivery'] = DB::table('method-delivery')
	    ->get();

	   
		
		$Methoddeliverycount = DB::table('method-delivery')
		->count('id');

        return view('admin.setting.delivery.index',$data, compact('Methoddeliverycount','notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }



	
	
	
	    public function create()
    {
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
         $data['currency'] = DB::table('currency')
		 ->where('active', '=', "1")
         ->get();

	
        return view('admin.setting.delivery.create',$data, compact('notifications'));
    }
	
	
	    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required|max:255',
            'plugin'=> 'required|max:255',
            'price'=> 'required|max:255',
            'currency'=> 'required|max:255',
            'shipment_details'=> 'required|max:1055',
            'description'=> 'required|max:1055',
            'active'=> 'required|max:255'
		  ]);

      $Methoddelivery= new Methoddelivery([
            'name' => $request->get('name'),
            'plugin' => $request->get('plugin'),
            'price' => $request->get('price'),
            'currency' => $request->get('currency'),
            'shipment_details' => $request->get('shipment_details'),
            'description' => $request->get('description'),
            'active' => $request->get('active'),
        ]);
		
      $Methoddelivery->save();

      return redirect::route('method-delivery.index')->with('success', 'Create with success!');
  
    }
	
	public function edit($id)
    {
        if(Auth::check())
		{
       $Currency = Currency::find($id);

 
        return view('admin.method-delivery.edit', compact('Currency'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'name'=> 'required|max:2055',
            'symbole'=> 'required|max:2055',
            'active'=> 'required|max:2055',
			]);

        $Currency = Currency::find($id);
        $Currency->name = $request->get('name');
        $Currency->symbole = $request->get('symbole');
        $Currency->active = $request->get('active');
        $Currency->save();

        return redirect::route('settings-currency.index')->with('success', 'Update with success!');
    }


	
	public function show($id)
    {
        
		if(Auth::check())
		{
		$Methoddelivery = Methoddelivery::find($id);
        return view('admin.setting.delivery.show', compact('Methoddelivery'));
		}
	    
		return redirect::to("admin");

    }
	
	
	
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 public function destroy($id)
    {
        //
        $post = Methoddelivery::where('id',$id)->delete();
   
   
        return Response::json($post);
    }
}