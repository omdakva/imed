<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;
use App\Product_photo;
use App\Category_product;
use App\Orders;
use App\Items;
use Response;

class ProductPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($id)
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Product = DB::table('products')
		->where('products.id', '=', $id)
	    ->get();
		
		
		$product_photo = DB::table('products_photo')
		->where('products_id', '=', $id)
	    ->get();
	 

        return view('admin.product.show_photo', compact('Product', 'notifications', 'product_photo'));
		}
	    
		return redirect::to("admin");

    }   




	
	

    public function create()
    {
        if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
			$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
        $Category = Category::where('parent_id',0)->get();
		

        return view('admin.product.create',$data, compact('Category', 'notifications'));
		}
		return redirect::to("admin");
    }
	
	
	
	
	
	 public function store(Request $request)
    {
		
		    $inputValue= $request->validate([
            'products_id'=> 'required|max:255',
            'active'=> 'required|max:255',
			'photo' =>'required',
            'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'

		  ]);
		  
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/products_logo/seconde'),
		$photo->getClientOriginalName());
		

        $Product= new Product_photo([
            'products_id' => $request->get('products_id'),
            'active' => $request->get('active'),
		    'photo' => $inputValue['photo'],

        ]);
		
        $Product->save();

	    return redirect()->to('admin/product/photo/'.$request->get('products_id').'')->with('success', 'Create with success!');
    }
	
	
	

	
	
	
	
	
	
	
	public function edit($id)
    {
        if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Product = Product::find($id);

         $Category = Category::all();

        return view('admin.product.edit', compact('Product', 'Category', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	

	
	
	
	public function update(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
          'photo' =>'required',
          'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
        
		$photo = $request->file('photo');
        $inputValue['photo'] = $photo->getClientOriginalName();
        $photo->move(public_path('photo/products_logo/seconde'),
		$photo->getClientOriginalName());
	  
	    $products_id = $request->input('products_id');

	  
      DB::update('update products_photo set photo = ? where id = ?',[$inputValue['photo'],$id]);
	    return redirect()->to('admin/product/photo/'.$request->input('products_id').'')->with('success', 'Create with success!');

   }




	public function update3(Request $request, $id)
    {
        $request->validate([
        'active'=> 'required|max:2055',
		]);

        $Product_photo = Product_photo::find($id);
        $Product_photo->active = $request->get('active');
        $Product_photo->save();

        return redirect::route('category.index')->with('success', 'Update with success!');
    }
	
	
	
	
	
	
	
	
	 public function destroy($id)
    {
        //
        $post = Product_photo::where('id',$id)->delete();
   
        return Response::json($post);
    }
}