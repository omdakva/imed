<?php

namespace App\Http\Controllers\admin\seo;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\SEO_url;
use App\Pages;
use Response;

class HomeController extends Controller
{
    
	public function index()
    { 
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
	
        return view('admin.seo.index', compact('notifications'));
		
		
		}
	    
		return redirect::to("admin");
    }
	

	public function settings()
    {
       if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
	    return view('admin.seo.settings', compact('notifications'));
		} 
		return redirect::to("admin");
	}


	public function url()
    {
       if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
		->get();
		
		$SEO_url = DB::table('seo_url')
		->get();
		
		
	    return view('admin.seo.url', compact('SEO_url', 'notifications'));
		} 
		return redirect::to("admin");
	}





	public function category()
    {
       if(Auth::check())
		{
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
		->get();
		
		$SEO_url = DB::table('seo_url')
		->get();
		
		
	    return view('admin.seo.url', compact('SEO_url', 'notifications'));
		} 
		return redirect::to("admin");
	}


	public function create()
    {
		if(Auth::check())
		{
		   $notifications = DB::table('notifications')
			->where('status', '=', "0")
			->get();
			
			$SEO_url = DB::table('seo_url')
			->get();

			$Pages = DB::table('pages')
			->get();
		
			$SEO_url_en = DB::table('SEO_url')
			->where('lang', '=', "en")
			->get();

			$SEO_url_fr = DB::table('SEO_url')
			->where('lang', '=', "fr")
			->get();

			$SEO_url_de = DB::table('SEO_url')
			->where('lang', '=', "de")
			->get();
	
        return view('admin.seo.create', compact('Pages', 'SEO_url_en', 'SEO_url_fr', 'SEO_url_de', 'SEO_url', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");
    }
	
	
	
	
	
	public function store2(Request $request)
    {
		 $inputValue= $request->validate([
            'page'=> 'required|max:255',
            'lang'=> 'required|max:255',
            'seo_title'=> 'required|max:255',
			'keyword'=> 'required|max:255',
			'meta_description'=> 'required|max:3055',
			'active'=> 'required|max:255',
			'date'=> 'required|max:255'
	]);
	
	  if ($request->get('lang') == 'de')
	    {
			

			return DB::table('pages')
			->where('name', '=', "".$request->get('page')."")
			->get();

			return $pages = Pages::whereOwnerAndStatus($owner, 0)
			->take($count)->get();

		foreach ($pages as $page ) 
		{
			
		}


		}
	  elseif ($request->get('lang') == 'en')
		{
			return redirect()->intended('dashboard_en');

		}
    }
	













	public function store(Request $request)
    {
		 $inputValue= $request->validate([
            'page'=> 'required|max:255',
            'lang'=> 'required|max:255',
            'seo_title'=> 'required|max:255',
			'keyword'=> 'required|max:255',
			'meta_description'=> 'required|max:3055',
			'active'=> 'required|max:255',
			'date'=> 'required|max:255'
	]);
	
	 
		

		   $seo_url= new SEO_url([
            'page' => $request->get('page'),
            'lang' => $request->get('lang'),
			'domain' => $request->get('domain'),
			'url' => strtolower (str_replace(' ', '-', ''.$request->get('domain').'/'.$request->get('lang').'/'.$request->get('page').'')),
			'seo_title' => $request->get('seo_title'),
			'keyword' => $request->get('keyword'),
			'meta_description' => $request->get('meta_description'),
			'active' => $request->get('active'),
			'date' => $request->get('date'),

        ]);
		
		$seo_url->save();
	    return redirect()->route('seo.settings.url')->with('success', 'Create with success!');
	}





		
	
	public function show($id)
    {
        if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
	
		$SEO_url = SEO_url::find($id);

	

		
        return view('admin.seo.show', compact('SEO_url', 'notifications'));
		}
	    
		return redirect::to("admin");
    }
	




	public function edit($id)
    {
        if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
	
		$SEO_url = SEO_url::find($id);

	

		
        return view('admin.seo.edit', compact('SEO_url', 'notifications'));
		}
	    
		return redirect::to("admin");
    }


	public function update(Request $request, $id) 
	{
	 $this->validate($request,[
		'seo_title'=> 'required|max:22055',
		'keyword'=> 'required|max:22055',
		'meta_description'=> 'required|max:22055'
		]);
		 
		$seo_title = $request->input('seo_title');
		$keyword = $request->input('keyword');
		$meta_description = $request->input('meta_description');
 
	 DB::update('update seo_url set seo_title = ?, keyword = ?, meta_description = ? where id = ?',[$seo_title,$keyword,$meta_description,$id]);
	 return redirect()->to('admin/seo/settings/url/'.$id.'')->with('success', 'Create with success!');
	}















	public function store_product(Request $request)
    {
		 $inputValue= $request->validate([
            'id_product'=> 'required|max:255',
            'parent_id'=> 'required|max:255',
			'photo'=> 'required|max:255',
			'coordinate_x'=> 'required|max:255',
			'coordinate_y'=> 'required|max:255',
			'active'=> 'required|max:255',
			'updated_at'=> 'required|max:255',
			'created_at'=> 'required|max:255'


		  ]);
	

        $Catalogue= new Catalogue([
            'id_product' => $request->get('id_product'),
            'parent_id' => $request->get('parent_id'),
			'photo' => $request->get('photo'),
			'coordinate_x' => $request->get('coordinate_x'),
            'coordinate_y' => $request->get('coordinate_y'),
			'active' => $request->get('active'),
			'updated_at' => $request->get('updated_at'),
			'created_at' => $request->get('created_at'),

        ]);
		
		$Catalogue->save();
	    return redirect()->route('cms.catalogue')->with('success', 'Create with success!');
	}







	public function showproduct($id)
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Catalogue = DB::table('catalogue')
		->where('catalogue.id_catalogue', '=', $id)
		->get();
		
		$Catalogueproduct = DB::table('catalogue')
		->where('catalogue.parent_id', '=', $id)
	    ->get();
	
		$Productlist = DB::table('products')
		->get();

		 $Productlistview = DB::table('products')
         ->join('catalogue', 'products.id', '=', 'catalogue.id_product')
		 ->select('products.id', 'products.photo', 'products.product_name', 'products.long_description', 'products.short_description', 'products.photo', 'products.stock', 'products.remise', 'products.product_price', 'products.product_currency', 'products.created_at', 'products.active', 'catalogue.id_catalogue', 'catalogue.id_product', 'catalogue.parent_id', 'catalogue.coordinate_x', 'catalogue.coordinate_y')
		 ->where('catalogue.id_catalogue', '=', $id)
         ->get();
		
        return view('admin.cms.home.catalogue.showproduct', compact('Catalogue', 'Catalogueproduct', 'Productlistview', 'Productlist', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	


	
	public function updatephotoprin(Request $request, $id) 
	{
		$this->validate($request,[
			'photo' =>'required',
			'photo.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
	   ]);
		 
		 $photo = $request->file('photo');
		 $inputValue['photo'] = $photo->getClientOriginalName();
		 $photo->move(public_path('photo/cataloge'),
		 $photo->getClientOriginalName());
	   
	   
	     DB::update('update catalogue set photo = ? where id_catalogue = ?',[$inputValue['photo'],$id]);
		 return redirect()->to('admin/cms/home/catalogue/'.$id.'')->with('success', 'Create with success!');
 
	}









		
	public function addupdate(Request $request)
    {
		
		    $inputValue= $request->validate([
            'id_product'=> 'required|max:255',
            'parent_id'=> 'required|max:255',
            'photo'=> 'required|max:255',
			'active'=> 'required|max:255',
			'coordinate_x'=> 'required|max:255',
			'coordinate_y'=> 'required|max:255'


		  ]);
		  
		
		

        $Catalogue= new Catalogue([
            'id_product' => $request->get('id_product'),
            'parent_id' => $request->get('parent_id'),
            'photo' => $request->get('photo'),
            'active' => $request->get('active'),
            'coordinate_x' => $request->get('coordinate_x'),
            'coordinate_y' => $request->get('coordinate_y'),

        ]);
		
		$Catalogue->save();
	    return redirect()->to('admin/cms/home/catalogue/'.$request->get('parent_id').'')->with('success', 'Create with success!');
	}





public function updateproduct(Request $request, $id) 
   {
	$this->validate($request,[
		  'id'=> 'required|max:22055'
      ]);
        
   $id = $request->input('id');

	DB::update('update catalogue set id_product = ? where id_catalogue = ?',[$id_product,$id]);
    return redirect()->to('admin/cms/home/catalogue/product/'.$id.'')->with('success', 'Create with success!');
   }







public function update_pub_active(Request $request, $id) 
   {
	DB::update('update seo_url set active = "1"  where id = ?',[$id]);
    return redirect()->to('admin/seo/settings/url/'.$id.'')->with('success', 'Create with success!');
   }

public function update_pub_blocked(Request $request, $id) 
   {
	DB::update('update seo_url set active = "0"  where id = ?',[$id]);
    return redirect()->to('admin/seo/settings/url/'.$id.'')->with('success', 'Create with success!');
   }









   public function productdelete($id)
   {
	   $post = Catalogue::where('id_catalogue',$id)->delete();

  	   return redirect()->back()->with('success', 'Create with success!');
   }


   public function destroy($id)
    {
        $post = Catalogue::where('id_catalogue',$id)->delete();
   
   
        return Response::json($post);
    }


}