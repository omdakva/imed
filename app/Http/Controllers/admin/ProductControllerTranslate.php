<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;
use App\Product2;
use App\Category_product;
use App\Orders;
use App\Items;
use App\Currency;

use Response;

class ProductControllerTranslate extends Controller
{


  	public function translate($id)
    {
        if(Auth::check())
		{		
        $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Product = Product::find($id);


        return view('admin.product.translate', compact('Product', 'notifications'));
		}
		
		return redirect::to("admin");

    }  
    
	
	public function update(Request $request, $id) 
   {
	   $this->validate($request,[
	      'product_name_de'=> 'required|max:2055',
	      'long_description_de'=> 'required|max:2055',
	      'short_description_de'=> 'required|max:2055'
      ]);
        
		
	    $products_id = $request->input('products_id');
	    $product_name_de = $request->input('product_name_de');
	    $long_description_de = $request->input('long_description_de');
	    $short_description_de = $request->input('short_description_de');

	  
      DB::update('update products set product_name_de = ?, long_description_de = ?, short_description_de = ? where id = ?',[$product_name_de,$long_description_de,$short_description_de,$id]);
	    return redirect()->route('product.index')->with('success', 'Create with success!');

   }
   
   
	public function show($id)
    {
        
		if(Auth::check()){
				 
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Product = DB::table('products')
		->where('products.id', '=', $id)
	    ->get();
		
	
	 

        return view('admin.product.translateview', compact('Product', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	
	
	
	
	
	
	
	
	public function update_long_description(Request $request, $id) 
   {
	   $this->validate($request,[
	      'long_description_de'=> 'required|max:2055'
		  ]);
        
		
	    $products_id = $request->input('products_id');
	    $long_description_de = $request->input('long_description_de');

	  
      DB::update('update products set long_description_de = ? where id = ?',[$long_description_de,$id]);
	    return redirect()->to('admin/product/translate/'.$id.'')->with('success', 'Create with success!');

   }	
	
	
	
	public function update_short_description(Request $request, $id) 
   {
	   $this->validate($request,[
	      'short_description_de'=> 'required|max:2055'
		  ]);
        
		
	    $products_id = $request->input('products_id');
	    $short_description_de = $request->input('short_description_de');

	  
      DB::update('update products set short_description_de = ? where id = ?',[$short_description_de,$id]);
	    return redirect()->to('admin/product/translate/'.$id.'')->with('success', 'Create with success!');

   }
}