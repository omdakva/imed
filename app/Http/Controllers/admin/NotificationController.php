<?php
  
namespace App\Http\Controllers\admin;
  
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;


use DB;

use App\User;
use Notification;
use App\Notifications;
  
class NotificationController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }
  
   
	
	public function index()
    {
        $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		

        $notificationslist = DB::table('notifications')
		->orderBy('id', 'DESC')
       ->join('users', 'notifications.id_user', '=', 'users.id')
       ->select('notifications.id', 'notifications.notifiable_type', 'notifications.notifiable', 'notifications.status', 'notifications.read_at', 'notifications.created_at', 'notifications.updated_at', 'users.firstname', 'users.lastname')
       ->get();
		
		        return view('admin.notification.index', compact('notificationslist', 'notifications'));



    }
  
  
  
  
  
  	
	public function update2222(Request $request, Notifications $Notifications)
    {
        $request->validate([
        'status'=> 'required|max:2055',
		]);

		$Notifications->update([
        'status' => '1',
    ]);
	 return redirect::to('admin/notification')->with('success', 'Update with success!');
    }
	
	
	
	public function update(Request $request) 
   {
      $status = $request->input('status');
	  
      DB::update('update notifications set status = ?, read_at = "'.date('Y-m-d h:i:s').'" where status = "0"',[$status]);
      return redirect('admin/notification')->with('success', '!');

   }

  
}