<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;

use Response;

class ManageCmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		  
		$data['Category'] = DB::table('category')
	    ->get();

	   
		
		$Categorycount = DB::table('category')
		->count('id');

        return view('admin.cms.index',$data, compact('Categorycount', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
    }

  