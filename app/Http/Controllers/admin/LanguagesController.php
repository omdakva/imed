<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Language;

use Response;

class LanguagesController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		
        $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();

	     $data['language'] = DB::table('language')
       ->get();

	
        return view('admin.setting.language.index',$data, compact('notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }



	
	
	
	    public function create()
    {
        $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
        
        $data['language'] = DB::table('language')
       ->get();

	
        return view('admin.setting.language.create',$data, compact('notifications'));
    }
	
	
	    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required|max:255',
            'symbole'=> 'required|max:255',
            'active'=> 'required|max:255'
		  ]);

      $Language= new Language([
            'name' => $request->get('name'),
            'symbole' => $request->get('symbole'),
            'active' => $request->get('active'),
        ]);
		
      $Language->save();

      return redirect::route('settings-languages.index')->with('success', 'Create with success!');
  
    }
	
	public function edit($id)
    {
        if(Auth::check())
		{
      $notifications = DB::table('notifications')
     ->where('status', '=', "0")
        ->get();

       $Language = Language::find($id);

 
        return view('admin.setting.language.edit', compact('Language', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'name'=> 'required|max:2055',
            'symbole'=> 'required|max:2055',
            'active'=> 'required|max:2055',
			]);

        $Language = Language::find($id);
        $Language->name = $request->get('name');
        $Language->symbole = $request->get('symbole');
        $Language->active = $request->get('active');
        $Language->save();

        return redirect::route('settings-languages.index')->with('success', 'Update with success!');
    }


	
	
	
	
	
	public function show($id)
    {
        
		if(Auth::check())
		{
            $notifications = DB::table('notifications')
            ->where('status', '=', "0")
            ->get();

		
				$Language = Language::find($id);

	 

        return view('admin.setting.language.show', compact('Language', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	
	
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 public function destroy($id)
    {
        //
        $post = Language::where('id',$id)->delete();
   
   
        return Response::json($post);
    }
}