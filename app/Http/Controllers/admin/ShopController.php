<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

class ShopController extends Controller
{

    public function index()
    {
        $parentCategories = \App\Category::where('parent_id',0)->get();

        return view('admin.category.categoryTreeview', compact('parentCategories'));
    }
}
