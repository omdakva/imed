<?php
namespace App\Http\Controllers\admin\location;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Location;
use Response;

class HomeController extends Controller
{
    

    public function index()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Slider_image = DB::table('slider_image')
		->join('products', 'slider_image.id_product', '=', 'products.id')
		->select('products.product_name', 'products.id', 'slider_image.id_slider', 'slider_image.coordinate_x', 'slider_image.coordinate_y', 'products.product_price', 'products.product_currency')
	    ->get();
	
	
        return view('admin.location.index', compact('Slider_image', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }
	
	

	


    public function list()
    {
        
		if(Auth::check())
		{
		

		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		$Location = DB::table('location')
		->get();
	
	
        return view('admin.location.list', compact('Location', 'notifications'));
		
		
		}
	    
		return redirect::to("admin");

    }


	
	public function create()
    {
		if(Auth::check())
		{
        
         $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();

        
		$Location = DB::table('location')
		->where('location_type', '=', "1")
        ->get();
	
        return view('admin.location.create', compact('Location','notifications'));
		}
	     return redirect::to("admin");

    }
	
	

	
	
	public function store(Request $request)
    {
		

        $this->validate($request,[
            'location_type'=> 'required|max:1255',
            'country'=> 'required|max:1255',
            'city'=> 'required|max:255',
            'address'=> 'required|max:24255',
            'postcode'=> 'required|max:24255',
            'email'=> 'required|max:24255',
            'tel'=> 'required|max:24255',
            'mobile'=> 'required|max:24255',
            'active'=> 'required|max:255'
		  ]);

      $Location= new Location([
        'location_type' => $request->get('location_type'),
        'country' => $request->get('country'),
        'city' => $request->get('city'),
        'address' => $request->get('address'),
        'postcode' => $request->get('postcode'),
        'email' => $request->get('email'),
        'tel' => $request->get('tel'),
        'mobile' => $request->get('mobile'),
        'active' => $request->get('active'),
        ]);
	
      $Location->save();

		
return redirect::route('location.list')->with('success', 'Create with success!');
  
}
	
	public function edit($id)
    {
        if(Auth::check())
		{
		
		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
       $Location = Location::find($id);

 
        return view('admin.location.edit', compact('Location', 'notifications'));
		}
		return redirect::to("admin");

    }
	
	
	
	public function update(Request $request, $id) 
   {
	   
	   
	      $this->validate($request,[
            'location_type'=> 'required|max:1255',
            'country'=> 'required|max:2055',
            'city'=> 'required|max:2055',
            'address'=> 'required|max:2055',
            'postcode'=> 'required|max:2055',
            'email'=> 'required|max:2055',
            'tel'=> 'required|max:2055',
            'mobile'=> 'required|max:2055',
            'active'=> 'required|max:2055'
      ]);
        
		
       $location_type = $request->input('location_type');
       $country = $request->input('country');
       $city = $request->input('city');
        $address = $request->input('address');
        $postcode = $request->input('postcode');
        $email = $request->input('email');
        $tel = $request->input('tel');
        $mobile = $request->input('mobile');
        $active = $request->input('active');

	  
      DB::update('update location set location_type = ?, country = ?, city = ?, address = ?, postcode = ?, email = ?, tel = ?, mobile = ?, active = ? where id = ?',[$location_type,$country,$city,$address,$postcode,$email,$tel,$mobile,$active,$id]);
	    return redirect()->to('admin/location/list/'.$id.'')->with('success', 'Create with success!');

   }


	
	
	
	
	
	public function show($id)
    {
        
		if(Auth::check())
		{
		
		$notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();
		
		 $Location = Location::find($id);

	 

        return view('admin.location.show', compact('Location', 'notifications'));
		}
	    
		return redirect::to("admin");

    }
	
	
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 public function destroy($id)
    {
        //
        $post = Location::where('id',$id)->delete();
   
   
	    return redirect()->to('admin/location/list')->with('success', 'Create with success!');
    }
}