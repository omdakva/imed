<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Category;
use App\Product;
use App\Product2;
use App\Category_product;
use App\Orders;
use App\Items;
use Response;

class ProductDescriptionController extends Controller
{
 

   
   
   	public function update(Request $request, $id)
    {
        $request->validate([
            'long_description'=> 'required|max:22055',
			]);

        $Product = Product::find($id);
        $Product->long_description = $request->get('long_description');
        $Product->save();

	    return redirect()->to('admin/product/'.$id.'')->with('success', 'Create with success!');
    }

}