<?php

namespace App\Http\Controllers;
use App\Cart;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use App\Regulation;

use Illuminate\Http\Request;

class Strip extends Controller
{

    public function index()
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
            $amount=$cart->totalPrice;
          Ordre::where('orders_number', session()->get('cart')->ordre)->update(['statut' =>'1']);

            $regulation  =new RegulationController;
            $regulation ->store("In Progress","stripe",0,$cart->ordre);

            return view('payment.strip', compact('amount'));

        }else
        {
            return redirect("/cart")->with('echec', "Panier Vide");


        }


    }

    public function charge(Request $request) {
        $cart = new Cart(session()->get('cart'));

        $charge = Stripe::charges()->create([
            'currency' => 'USD',
            'source' => $request->stripeToken,
            'amount'   => $request->amount+$cart->cost,
            'description' => 'Dulexsis Shop'
        ]);

        $chargeId = $charge['id'];

        if ($chargeId) {
            $cart = new Cart(session()->get('cart'));
            //$ordre  =new OrdreController;
           // $ordre ->store(1);
 Regulation::where('id_ordre', session()->get('cart')->ordre)->update(['status' => 1,'transaction' => $chargeId]);

            session()->forget('cart');


            return redirect("/cart")->with('echec', "Paiement effectué avec succès");
        } else {
            return redirect()->back();
        }
    }
}
