<?php
namespace App\Http\Controllers\web\en;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Product;
use App\Pages;
use App\SEO_url;
use App\Product_photo;

use Session;

class IndexController extends Controller
{
     public function index()
     {
     $products = DB::table('products')
     ->get();

     $Pages = DB::table('Pages')
     ->get();


          $Pages = DB::table('pages')
		->join('seo_url', 'pages.name', '=', 'seo_url.page')
          ->select('pages.id', 'pages.name', 'seo_url.url')
          ->where('seo_url.lang', '=', "en")
         ->get();
         

	return view('web.en.index', compact('products', 'Pages'));
     }	
}