<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ajax_contact extends Model
{
    protected $fillable = ['name','email','mobile_number','subject','message'];
}