<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Methoddelivery extends Model
{
       protected $table = 'method-delivery';
       protected $fillable = ['name','plugin','price','currency','shipment_details','description','active'];

}
