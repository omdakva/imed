<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordre extends Model
{
    protected $fillable=['orders_number','customer_id','totalQty','total_amount','currency','items','cost','localisation_etats','active','statut'];

}
