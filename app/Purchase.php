<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
protected $table = 'purchase';
protected $fillable=['id_user','statut','active','list_product','total'];
}