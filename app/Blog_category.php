<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Blog_category extends Model
{
    protected $table = 'blog_category';
    protected $fillable = ['name','name_de','name_fr','active'];
		

    public function Category_product()
    {
        return $this->hasMany('App\Category_product', 'id_category');
    }
}
