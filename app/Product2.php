<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product2 extends Model
{
protected $fillable=['product_name','short_description','product_price','active','product_currency','long_description','statut','remise','photo','stock'];

public function user()
	{
		return $this->belongsTo(User::class); 
	}
	}


