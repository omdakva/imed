<?php

namespace App;
use App\Ordre;

use App\Traits\Image;
use Illuminate\Database\Eloquent\Model;

class Cart
{
    public $items = [];
    public $totalQty ;
    public $totalPrice;
    public $cost;
    public $ordre;

    public function __Construct($cart = null) {

        if($cart) {

            $this->items = $cart->items;
            $this->totalQty = $cart->totalQty;
            $this->totalPrice = $cart->totalPrice;
            $this->cost = $cart->cost;
            $this->ordre=$cart->ordre;
        } else {

            $this->items = [];
            $this->totalQty = 0;
            $this->totalPrice = 0;
            $this->cost = 10;
            $this->ordre ='C-'.time();

        }
    }

    public function add($product) {
        $item = [
            'id' =>  $product->id,
            'title' => $product->product_name,
            'price' => $product->product_price,
            'qty' => 0,
            'image' => "http://127.0.0.1:8000/photo/products_logo/".$product->photo
        ,
        ];

        if( !array_key_exists($product->id, $this->items)) {
            $this->items[$product->id] = $item ;
            $this->totalQty +=1;
            $this->totalPrice += $product->product_price;

        } else {

            $this->totalQty +=1 ;
            $this->totalPrice += $product->product_price;
        }

        $this->items[$product->id]['qty']  += 1 ;

    }

    public function remove($id) {

        if( array_key_exists($id, $this->items)) {
            $this->totalQty -= $this->items[$id]['qty'];
            $this->totalPrice -= $this->items[$id]['qty'] * $this->items[$id]['price'];
            unset($this->items[$id]);

        }

    }

    public function updateQty($id, $qty) {

        //reset qty and price in the cart ,
        $this->totalQty -= $this->items[$id]['qty'] ;
        $this->totalPrice -= $this->items[$id]['price'] * $this->items[$id]['qty']   ;
        // add the item with new qty
        $this->items[$id]['qty'] = $qty;

        // total price and total qty in cart
        $this->totalQty += $qty ;
        $this->totalPrice += $this->items[$id]['price'] * $qty   ;

    }


}
