<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider_image extends Model
{
       protected $table = 'slider_image';
        protected $fillable = ['photo','id_product','coordinate_x','coordinate_y','active'];
	

	
public function Product()
{
return $this->hasMany('App\Product', 'id_product');
}

}
