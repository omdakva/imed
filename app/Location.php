<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
       protected $table = 'location';
        protected $fillable = ['location_type', 'country','city','address','postcode','email','tel','mobile','active'];

		

}
