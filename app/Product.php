<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
protected $table = 'Products';
protected $fillable = ['product_name', 'product_name_de', 'product_name_fr', 'short_description', 'short_description_de', 'short_description_fr','product_price', 'product_currency', 'active', 'long_description', 'long_description_de', 'long_description_fr','statut', 'remise', 'photo', 'stock', 'offer'];


public function category()
{
return $this->belongsTo(Category::class); 
}

public function Category_product()
{
return $this->hasMany('App\Category_product', 'id_product');
}
}
