<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
	
protected $fillable=['orders_number','customer_id','total_amount','currency','items','cost','active','statut'];

public function user()
	{
		return $this->belongsTo(User::class); 
	}
	}


