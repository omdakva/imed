<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Our_article extends Model
{
    protected $table = 'our_article';
    protected $fillable = ['description','id_product_p','id_product_s1','id_product_s2','id_product_s3', 'id_product_s4','active'];
		
    public function products()
    {
        return $this->hasMany('App\Product', 'id_product_p');
    }
			

}
