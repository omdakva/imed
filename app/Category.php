<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Category extends Model
{
    protected $table = 'category';
    protected $fillable = ['name','seo_title_en','keyword_en','meta_description_en','name_url','name_fr','seo_title_fr','keyword_fr','meta_description_fr','name_fr_url','name_de','seo_title_de','keyword_de','meta_description_de','name_de_url','parent_id','photo','active'];
		
	public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
	
    public function products()
    {
        return $this->hasMany('App\Category_product', 'id_category');
    }
    
    public function Category_product()
    {
        return $this->hasMany('App\Category_product', 'id_category');
    }

	public function subcategory()
	{
    return $this->hasMany('App\Category', 'parent_id');
    }
		

}
