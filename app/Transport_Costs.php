<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transport_Costs extends Model
{
     protected $table = 'transport_costs';
     protected $fillable = ['type','kilometre','quantity','price','currency','active'];
		
	

}
