<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SEO_url extends Model
{
protected $table = 'seo_url';
protected $fillable = ['page','lang','url','seo_title','keyword','meta_description','active','date'];
public $timestamps = false;

}
