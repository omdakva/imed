<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Blog_category_list extends Model
{
    protected $table = 'blog_category_list';
    protected $fillable = ['id_blog', 'id_category'];

}
