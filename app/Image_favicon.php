<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Image_favicon extends Model
{
    protected $table = 'image_favicon';
    protected $fillable = ['photo', 'active'];

}
