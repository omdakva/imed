<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  /**
     * Run the migrations.
     *
     * @return void
     */
     protected $fillable = ['title'];
}