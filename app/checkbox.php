<?php
 namespace App;
 use Illuminate\Database\Eloquent\Model;
 class postRoom extends Model
 {
     protected $fillable = [
     'title',
    ];
	
	
	    public function setCatAttribute($value)
    {
        $this->attributes['title'] = json_encode($value);
    }
  
    /**
     * Get the categories
     *
     */
    public function getCatAttribute($value)
    {
        return $this->attributes['title'] = json_decode($value);
    }
 }