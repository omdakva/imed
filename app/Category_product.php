<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category_product extends Model
{
           protected $table = 'category_products';
           protected $fillable=['id_category','id_product'];
		   

public function Products()
{
return $this->belongsTo(Product::class); 
}

public function Product()
{
return $this->hasMany('App\Product', 'id');
}

}



