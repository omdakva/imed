<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalogue extends Model
{
    protected $table = 'catalogue';
    protected $fillable = ['id_product','parent_id','photo','coordinate_x','coordinate_y','active'];
		
	public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
	
    public function products()
    {
        return $this->hasMany('App\Product', 'id_product');
    }
	

	public function Catalogue_Product()
	{
    return $this->hasMany('App\Catalogue', 'parent_id');
    }
		

}
