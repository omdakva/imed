<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
       protected $table = 'country';
       protected $fillable = ['country','symbole','code','photo','parent_id','active'];
	   
	public function city()
	{
    return $this->hasMany('App\Country', 'parent_id');
    }
}
