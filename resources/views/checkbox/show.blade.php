
@extends('layouts.admin.dashbord')

@section('title', 'Changing the default title')
@section('content')
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Section details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('admindashboard') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('section.index') }}">Section /</a>
<a id="pagetitlesecondelink">Details</a>
</div>
</div>

<div class="card">
    <div class="card-body">
       <h3>{{ $sections->title }}</h3>
       <p>{{ $sections->description }}</p>
    </div>
</div>
@endsection