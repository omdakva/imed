
@extends('layouts.admin.dashbord')

@section('title', 'Changing the default title')
@section('content')
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Edite Section</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('admindashboard') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('section.index') }}">Section /</a>
<a id="pagetitlesecondelink">Edite</a>
</div>
</div>

<div class="row">
<div class="col-lg-6 mx-auto">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif

    <form method="POST" action="{{ route('section.update', $sections) }}">
     @csrf
     @method('PATCH') 
        <div class="form-group">
            <label for="post-title">Section</label>
            <input type="text" name="title" 
                   value="{{ $sections->title }}" class="form-control" id="post-title">
        </div>
        <div class="form-group">
            <label for="post-description">Description</label>
            <textarea name="description" class="form-control" id="post-description" rows="3">{{ $sections->description }}</textarea>
        </div>

        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>
</div>
@endsection