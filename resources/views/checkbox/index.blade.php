@extends('layout.layouttest')

@section('title', 'Changing the default title')
@section('content')


<a href="{{ route('checkbox.create') }}" class="btn btn-success">Add new</a>

 @if(session()->get('success'))
    <div class="alert alert-success mt-3">
      {{ session()->get('success') }}  
    </div>
@endif

<table class="table table-striped mt-3">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">checkbox</th>
      <th scope="col">Description</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
   @foreach($postRoom as $post)
    <tr>
      <th scope="row">{{ $post->id }}</th>
      <td>{{ $post->title }}</td>
      <td>{{ $post->description }}</td>
      <td class="table-buttons" style="text-align:right;">
        
        <form method="POST" action="{{ route('checkbox.destroy', $post) }}">
         <a href="{{ route('checkbox.show', $post) }}" class="btn btn-success">
          View
        </a>
        <a href="{{ route('checkbox.edit', $post) }}" class="btn btn-primary">
          Edite
        </a>@csrf
         @method('DELETE')
            <button type="submit" class="btn btn-danger">
              Delete
            </button>
        </form>
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
@endsection