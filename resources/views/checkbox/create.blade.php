
@extends('layout.layouttest')

@section('title', 'Changing the default title')
@section('content')


<div class="row">
<div class="col-lg-6 mx-auto">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif

    <form method="POST" action="{{ route('checkbox.store') }}">
     @csrf
<p>Sunday</p>
                <input type="checkbox" name="select-all-sunday" id="select-all-sunday" onclick="toggle(this);"> Check All
                <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-1" value="00:00"> 00:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-2" value="1"> 1:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-3" value="2"> 2:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-4" value="3:00"> 3:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-5" value="4:00"> 4:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-6" value="5:00"> 5:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-7" value="6:00"> 6:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-8" value="7:00"> 7:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-9" value="8:00"> 8:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-10" value="9:00"> 9:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-11" value="10:00"> 10:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-12" value="11:00"> 11:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-13" value="12:00"> 12:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-14" value="13:00"> 13:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-15" value="14:00"> 14:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-16" value="15:00"> 15:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-17" value="16:00"> 16:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-18" value="17:00"> 17:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-19" value="18:00"> 18:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-20" value="19:00"> 19:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-21" value="20:00"> 20:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-22" value="21:00"> 21:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-23" value="22:00"> 22:00 <br>
                <input type="checkbox" name="checkbox-sunday[]" id="checkbox-24" value="23:00"> 23:00 <br>
        <button type="submit" class="btn btn-success">Save</button>
    </form>
</div>
</div>
@endsection