@extends('layout.store2')
@section('title', 'STORE-TEC - Home')
@section('content')

<section class="text-center mb-4" style="margin-top:130px;">
<div class="col-sm-12 col-md-12" style="padding:0px 100px 0px 100px;">
<div style="padding:0px;text-align:center;">
<span class="text_title_home_blod">
@foreach($Category2 as $post)
Category {{ $post->name }} ( {{$productscount}} )
@endforeach
</span>
</div>
</div>

<div class="row wow fadeIn" style="margin-top:40px;">
<div class="container products">

<div class="row">
<div class="col-sm-3" style="border:1px solid #c0c0c0;text-align:left;">
@foreach($Category2 as $category)
<label class="text_title_home_blod" style="font-size:19px;width:100%;">{{ $category->name }}</label>

@foreach($subCategory as $subCategory)
<label class="text_title_home_normal" style="font-size:16px;width:100%;">{{ $subCategory->name }}</label>

@endforeach
@endforeach
</div>
<div class="col-sm-9 row" >
@if($productscount=='0')
<div class="col-sm-12 col-md-12" style="font-size:25px;">
No product for this category
</div>
@else
@foreach($products as $product)
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                        <a href="{{route('store.show', $product->id)}}">
						<img src="{{ asset('photo/products_logo/'.$product->photo) }}" style="width:100%;height:120px;">
						</a>
                        <div class="caption">
                            <h5 style="height:auto;">{{ $product->product_name }}</h5>
                            <p><strong>Price: </strong> {{ $product->product_price }} {{ $product->product_currency }}</p>
                            <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn nav-link border border-light rounded waves-effect text-center" role="button">Add to cart <i class="fa fa-shopping-cart" aria-hidden="true"></i></a> </p>
                        </div>
				
                    </div>
                </div>
@endforeach
@endif
</div>
</div>

    </div>
	</div>
</section>

      <hr>

      <!--Grid row-->
      <div class="row d-flex justify-content-center wow fadeIn">

        <!--Grid column-->
        <div class="col-md-6 text-center">

          <h4 class="my-4 h4">Additional information</h4>

          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus suscipit modi sapiente illo soluta odit
            voluptates,
            quibusdam officia. Neque quibusdam quas a quis porro? Molestias illo neque eum in laborum.</p>

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

      <!--Grid row-->
      <div class="row wow fadeIn">

        <!--Grid column-->
        <div class="col-lg-4 col-md-12 mb-4">

          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/11.jpg" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-4 col-md-6 mb-4">

          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/12.jpg" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-4 col-md-6 mb-4">

          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/13.jpg" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

      </div>

<script>
$(document).ready(function() {

        var product_id = $(this).data('id');
        var url = "/";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({

            type: "POST",
            url: url,
            data: { id: product_id },
            success: function (data) {
            console.log(data);

            },
            error: function (data) {
            console.log('Error:', data);
            }
        });
    });
</script>

@endsection
