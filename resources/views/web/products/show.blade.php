@foreach($Product as $post)
<div class="col-md-8 mb-3 mx-auto">
<label class="page-title2" >Details</label>
<label class="textblodfull">Name : <span class="textnormal">{{ $post->product_name }}</span></label>
<label class="textblodfull">Activation : <span class="textnormal">@if($post->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></label>
<label class="textblodfull">Creation date : <span class="textnormal">{{ $post->created_at }}</span></label>
</div>
@endforeach
