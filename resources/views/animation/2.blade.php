<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
<title>Animation</title>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/bootstrap.mins.css" rel="stylesheet">
<style>
.space{width:100%;height:80px;}
.space_div{width:100%;height:10px;}

.textblodfull-title{font-size:34px;font-weight:normal;font-weight:400;text-align:center;}
.textblodfull{font-weight:blod;font-weight:700;}
.textnormalfull{font-size:15px;font-weight:normal;font-weight:400;}

.margin-res{margin: 0px 0px 15px 0px;}
.padding0{padding:7px;}
</style>


<style>
.bg6{transition:2s;opacity:3;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg6:hover{opacity:0.8;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;
transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}


.bg8{transition:2s;opacity:3;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg8:hover{transition:2s;opacity:0.8;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}

.border_col{border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;}

.text_font{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-serif}
.text_font2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}
.btn_col{background-color:#ffffff;border:2px solid #9e844a;color:#000000;padding:10px 20px 10px 20px;}
.btn_col:hover{background-color:#9e844a;border:2px solid #9e844a;color:#ffffff;padding:10px 20px 10px 20px;transition:1s;}
</style>


</head>
<body>

<div class="space"></div>
<section class="container">
<div class="row">
<div class="col-md-7 margin-res padding0">
<div class="col-md-12 margin-res bg6"></div>
<div class="col-md-12 margin-res bg6"></div>
</div>

<div class="col-md-5 margin-res padding0">
<div class="col-md-12 margin-res border_col" style="min-height:300px;">
</br><label class="text_font">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur
Lorem ipsum dolor sit amet, consectetur adipiscing elit.</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col">View more</button>
</div>
</div>
<div class="col-md-12 margin-res bg8"></div>
<div class="col-md-12 margin-res border_col" style="min-height:277px;">
</br><label class="text_font">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
ex ea commodo consequat.Duis aute irure dolor .
</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col">View more</button>
</div>
</div>
</div>
</div>





</div>

</section>
</body>
</html>