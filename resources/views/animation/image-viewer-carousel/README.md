## Tekitizy Carousel v1.0.0##

***A jQuery Carousel to rule them all !***

*Tested on IE9, IE10, Chrome, Safari, Firefox, iPhone, iPad, Chrome on Google Nexus.*

### Collaborators

* [David Maarek](http://davidmaarek.fr/)
* [Mathieu Rodrigues](http://mathieurodrigues.com/)

### Features
* Adaptive
* Fully Customizable
* Legend from ALT attribute
* Choose the time of each items to be displayed
* Slide Effect
* Custimizable controls
  * Display / Hide Next and Prev button
  * Display / Hide Play and Pause button
  * Allow / Disallow Autoplay
  * Navigation with Thumbnails

### Upcoming features
* CSS3 3D Transitions
* Mouse Slide Events
* Keyboards controls


### Demos
See what Tekitizy can do:
* [Complete](http://mathieurodrigues.com/tekitizy-example/complete.html)

### Requirements (for install)
* [NodeJS](https://nodejs.org/en/)
* [NPM](https://www.npmjs.com/)
* [Bower](https://bower.io/)

### Requirements (for Tekitizy)
* [jQuery 3.1.1+](https://jquery.com/)
* [Font-Awesome 4.7+](http://fontawesome.io/)

*See next step to download automatically dependancies.*

## 1.Getting Started

Check if you have NodeJS on your computer. If not, install it easilly [here](https://nodejs.org/en/).


Download the archive of the Tekitizy Carousel on your computer, then open terminal or powershell in the root directory of the project and paste those commands:
> npm install -g bower

> bower install


## 2.Set up your HTML

Load jQuery and include Font-Awesome and Tekitizy Carousel on your HTML file

```html
  <!-- Include js plugin -->
  <script src="./js/tekitizy.js"></script>

  <!-- Include dependencies -->
  <script src="./bower_components/jquery/dist/jquery.js"></script>
  <link rel="stylesheet" href="./bower_components/font-awesome/css/font-awesome.css">

  <!-- Basic stylesheet -->
  <link rel="stylesheet" href="./css/tekitizy.css" >
```
See the example on archive.

## 3.Call the plugin
All you need is to choose a class (a wrapper) on your HTML to allow the Tekitizy Carousel to crawl image on it and replace `.post` on this example.

The third parameter is optional, but here are defaults value.

```html
<script>
Tekitizy.setup(jQuery, '.post img', {
  prevNext: 'true', // Display (true) or Hide (false) Prev and Next button
  play: 'true', // Display (true) or Hide (false) Play and Pause button
  autoPlay: 'true', // Play (true) or Pause (false) when init Tekitizy Carousel
  imageDuration: 2, // Choose the time of each items to be displayed (in seconds)
  effect: 'true', // Enable or Disable 'Slide Effect' (Opacity 0 to 1 if disabled)
  thumbnails: 'true' // Display (true) or Hide (false) the Navigation with Thumbnails
  })
</script>
```

Now call the Tekitizy initializer function before `</body>` and your carousel is ready.

Enjoy 😊

## 4. For more details join us:
* [David Maarek](mailto:davidmaarek55@gmail.com)
* [Mathieu Rodrigues](mailto:mathieu.rodrigues17@gmail.com)


License
------------
© Copyright 2016 - *David Maarek && Mathieu Rodrigues*
