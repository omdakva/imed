<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>jQuery Tekitizy Carousel Plugin Demo</title>
  <script src="http://localhost/store_project/resources/views/animation/image-viewer-carousel/js/tekitizy.js"></script>
  <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://localhost/store_project/resources/views/animation/image-viewer-carousel/css/tekitizy.css">
  
  <style>
  body {
    padding: 2em;
  }
  .post img {
    max-width: 640px;
    max-height: 480px;
    clear: both;
    border: thin solid #E0E0E0;
    padding: 10px;
    display: block;
  }
  p {
    max-width: 40em;
  }
  img.logo {
    float: right;
    clear: right;
    padding-left: 1em;
    padding-bottom: 1em;
    max-width: 100px;
  }
  </style>
</head>

<body>




<div class="post">
@foreach($Product as $post)
  <div class="jquery-script-ads" style="margin:20px auto;">
</div>
  <h2>{{ $post->product_name  }}</h2>
  <p>
    <img src="{{ asset('public/photo/products_logo/'.$post->photo) }}" style="width:100%;" alt="{{ $post->product_name  }}">
  </p>
@endforeach
</div>



<script>
Tekitizy.setup(jQuery, '.post img', {
  prevNext: 'true',
  play: 'true',
  autoPlay: 'true',
  imageDuration: 2,
  effect: 'true',
  thumbnails: 'true'
  })
</script>
</body>
</html>
