@extends('layouts.animation')
@section('title', 'Manage currency')
@section('content')
<style>
.full{margin-top:100px;background-color:#9e844a;padding:20px 0px 40px 0px;}
.div_col4{transition:2s;border:0px solid #021a96;border-radius:0px 0px 0px 0px;padding:20px;min-height:250px;background-color:#ffffff;}
.div_col4:hover{transition:2s;border:0px solid #9e844a;border-radius:0px 0px 0px 0px;padding:20px;min-height:250px;}
.div_col4 .div_col_text4{margin-top:0px;transition:2s;color:#000000;text-align:center;}
.div_col4:hover .div_col_text4{margin-top:0px;transition:1s;color:#ffffff;font-size:0px;}

.div_col_img_view4{width:0px;height:0px;margin-top:-70px;transition:2s;}
.div_col4:hover .div_col_img4 .div_col_img_view4{width:120px;height:120px;margin-top:-20px;transition:2s;}



.text_font{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-serif}
.text_font2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}
.div_col4 .btn_col2{background-color:transparent;border:0px solid transparent;color:#000000;padding:10px 20px 10px 20px;}
.div_col4:hover .btn_col2{margin-top:-10px;background-color:transparent;color:#000000;padding:10px 20px 10px 20px;transition:1s;border:2px solid #9e844a;}
</style>





<section class="full" style="">
<div class="container" style="">

<div class="row">

<div class="col-md-12">
<h1 class="title1_b">Lorem ipsum title</h1>
</div>
<div class="space"></div>
<div class="space"></div>
<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/dessin/plateforme2.png')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="title1">Lorem ipsum title</h1>
</div>
<label class="text_font div_col_text4">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>


<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/dessin/plateforme2.png')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="title1">Lorem ipsum title</h1>
</div>
<label class="text_font div_col_text4">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>

</div>
</div>
</section>
@endsection