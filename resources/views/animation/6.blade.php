
<!DOCTYPE html>
<html lang="en" id="html" dir="ltr">
<head>
<title>Slider </title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1">
<meta name="format-detection" content="telephone=no">
<link rel="shortcut icon" href="image/imdec.png" type="image/x-icon">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="all" />
<link rel="stylesheet" href="{{ asset('public/css/style_slider.css')}}" media="all">
<link href="css/bootstrap.mins.css" rel="stylesheet">





<style>
#text_font_noir{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#000000;text-align:left;font-size:20px;font-weight:blod;font-weight:600;text-decoration:none;}

#text_font_noir:hover{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#00306e;text-align:left;font-size:20px;font-weight:blod;font-weight:600;text-decoration:none;}

#text_font_noir01{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#717379;text-align:left;font-size:20px;font-weight:blod;font-weight:600;text-decoration:none;}

#text_font_noir2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#000000;text-align:left;font-size:14px;font-weight:normal;font-weight:500;text-decoration:none;}
.space{width:100%;height:10px;}
</style>

<style>
.bg6{transition:2s;opacity:3;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg6:hover{opacity:0.8;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;
transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}


.bg8{transition:2s;opacity:3;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg8:hover{transition:2s;opacity:0.8;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}

.div_col{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:50px;min-height:250px;background-color:#ffffff;text-align:center;}
.div_col:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col .div_col_text{margin-top:0px;transition:2s;color:#000000;}
.div_col:hover .div_col_text{margin-top:10px;transition:2s;}

.div_col_img_view{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col:hover .div_col_img .div_col_img_view{width:120px;height:120px;margin-top:0px;transition:2s;text-align:center;}



.div_col1{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#9e844a;}
.div_col1:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col1 .div_col_text1{margin-top:0px;transition:2s;color:#ffffff;}
.div_col1:hover .div_col_text1{margin-top:10px;transition:2s;}

.div_col_img_view1{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col1:hover .div_col_img1 .div_col_img_view1{width:120px;height:120px;margin-top:0px;transition:2s;}



.div_col3{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#2095b6;}
.div_col3:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col3 .div_col_text3{margin-top:0px;transition:2s;color:#ffffff;}
.div_col3:hover .div_col_text3{margin-top:10px;transition:2s;}

.div_col_img_view3{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col3:hover .div_col_img3 .div_col_img_view3{width:120px;height:120px;margin-top:0px;transition:2s;}






.div_col4{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#9e844a;}
.div_col4:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col4 .div_col_text4{margin-top:0px;transition:2s;color:#ffffff;}
.div_col4:hover .div_col_text4{margin-top:10px;transition:2s;}

.div_col_img_view4{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col4:hover .div_col_img4 .div_col_img_view4{width:120px;height:120px;margin-top:0px;transition:2s;}



.text_font{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-serif}
.text_font2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}
.div_col .btn_col2{background-color:transparent;border:0px solid transparent;color:#000000;padding:10px 20px 10px 20px;}
.div_col:hover .btn_col2{background-color:transparent;border:2px solid #9e844a;color:#9e844a;padding:10px 20px 10px 20px;transition:1s;}
</style>
</head>
<body class="homepage " id="body" data-toggler=".off-canvas-menu--active" >












<style>
.box {
  background-color: #218D9B;
  height: 390px;
  width: 170px;
  	transition: 2s;

}

.transform {
  -webkit-transition: all 2s ease;  
  -moz-transition: all 2s ease;  
  -o-transition: all 2s ease;  
  -ms-transition: all 2s ease;  
  transition: all 2s ease;
  background-image: url('{{ asset('public/image/dessin/anim4.png') }}');background-repeat: no-repeat;
background-size: 100% 100%;
}

.transform-active {
    background-image: url('{{ asset('public/image/dessin/anim4-1.png') }}');background-repeat: no-repeat;

  height: 390px;
  width: 170px;
  transform: rotate( -360deg );            
    transition: transform 150ms ease;
	transition: 2s;
}



.transform2 {
  -webkit-transition: all 2s ease;  
  -moz-transition: all 2s ease;  
  -o-transition: all 2s ease;  
  -ms-transition: all 2s ease;  
  transition: all 2s ease;
  background-image: url('{{ asset('public/image/dessin/anim5.png') }}');background-repeat: no-repeat;
background-size: 100% 100%;
}

.transform2-active {
  background-image: url('{{ asset('public/image/dessin/anim5-1.png') }}');background-repeat: no-repeat;
  height: 390px;
  width: 170px;
  transform: rotate( -360deg );            
    transition: transform 150ms ease;
	transition: 2s;
}
  


.transform3 {
  -webkit-transition: all 2s ease;  
  -moz-transition: all 2s ease;  
  -o-transition: all 2s ease;  
  -ms-transition: all 2s ease;  
  transition: all 2s ease;
  background-image: url('{{ asset('public/image/dessin/anim6.png') }}');background-repeat: no-repeat;
background-size: 100% 100%;
}

.transform3-active {
  background-image: url('{{ asset('public/image/dessin/anim6-1.png') }}');background-repeat: no-repeat;
  height: 390px;
  width: 170px;
  transform: rotate( -360deg );            
    transition: transform 150ms ease;
	transition: 2s;
}  




.transform4 {
  -webkit-transition: all 2s ease;  
  -moz-transition: all 2s ease;  
  -o-transition: all 2s ease;  
  -ms-transition: all 2s ease;  
  transition: all 2s ease;
  background-image: url('{{ asset('public/image/dessin/anim7.png') }}');background-repeat: no-repeat;
background-size: 100% 100%;
}

.transform4-active {
  background-image: url('{{ asset('public/image/dessin/anim7-1.png') }}');background-repeat: no-repeat;
  height: 390px;
  width: 170px;
  transform: rotate( -360deg );            
    transition: transform 150ms ease;
	transition: 2s;
}
</style>
<style>
.line_c{margin-top:120px;position:absolute;}
.line{-webkit-transform:
            translateY(20px)
            translateX(75px)
            rotate(-26deg);
        position: absolute;height:105px;width:5px;background-color:#17b202;margin-top:50px;}

.line_h{position: absolute;height:5px;width:300px;background-color:#17b202;margin-top:166px;margin-left:99px;}

.line_v1{-webkit-transform:
            translateY(20px)
            translateX(75px)
            rotate(-180deg);
        position: absolute;height:102px;width:5px;background-color:#17b202;margin-top:44px;margin-left:319px;}

.line_h1{position: absolute;height:5px;width:200px;background-color:#17b202;margin-top:63px;margin-left:394px;}

.line_v2{-webkit-transform:
            translateY(20px)
            translateX(75px)
            rotate(-180deg);
        position: absolute;height:100px;width:5px;background-color:#17b202;margin-top:44px;margin-left:517px;}
		
.line_h2{position: absolute;height:5px;width:270px;background-color:#17b202;margin-top:164px;margin-left:592px;}


.flesh{position:absolute;margin-left:47px;margin-top:-80px;background-image: url('{{ asset('public/photo/slider/fleche2.svg') }}');background-repeat: no-repeat;
background-size: 100% 100%;transition: 1s;height:100px;width:30px;animation: myfirst 5s 2;animation-direction: alternate;white-space: nowrap;
    animation: floatText 5s infinite alternate ease-in-out;}

</style>


<style>
@-webkit-keyframes floatText{
  from {
    left: 00%;
  }

  to {
    /* left: auto; */
    left: 100%;
  }
}
</style>


<div class="container">
<div class="row">

<div class="col-md-4"></div>

<div class="col-md-4" style="height:600px;">

<div class="line_c" id="button">
<div class="flesh"></div>
<button style="position:absolute;margin-left:47px;margin-top:65px;" type="button">
<i style="font-size:29px;color:#17b202;"class="fa fa-circle" aria-hidden="true"></i>
</button>
<div class="line"></div>
<div class="line_h"></div>
<div class="line_v1"></div>
<div class="line_h1"></div>
<div class="line_v2"></div>
<div class="line_h2"></div>
<button style="position:absolute;margin-left:851px;margin-top:156px;" type="button">
<i style="font-size:20px;color:#17b202;"class="fa fa-circle" aria-hidden="true"></i>
</button>
</div>

<div class="line_img">
<div style="margin-left:90px;position:absolute;" class="box transform" id="buttons1"></div>
<div style="margin-left:0px;position:absolute;margin-top:0px;margin-left:280px;" class="box transform2" id="buttons2"></div>
<div style="margin-left:0px;position:absolute;margin-top:0px;margin-left:470px;" class="box transform3" id="buttons3"></div>
<div style="margin-left:0px;position:absolute;margin-top:0px;margin-left:660px;" class="box transform4" id="buttons4"></div>
</div>
</div>
<div class="col-md-4"></div>

</div>
</div>

                      

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    

<script>
$("#button").click(function() {
  $('.transform').toggleClass('transform-active');
});

$("#buttons1").click(function() {
  $('.transform').toggleClass('transform-active');
});

$("#button").click(function() {
  $('.transform2').toggleClass('transform2-active');
});
$("#buttons2").click(function() {
  $('.transform2').toggleClass('transform2-active');
});

$("#button").click(function() {
  $('.transform3').toggleClass('transform3-active');
});
$("#buttons3").click(function() {
  $('.transform3').toggleClass('transform3-active');
});

$("#button").click(function() {
  $('.transform4').toggleClass('transform4-active');
});
$("#buttons4").click(function() {
  $('.transform4').toggleClass('transform4-active');
});
</script>
<script src="{{ asset('public/js/js-slider.js')}}"></script>



</body>
</html>