@extends('layouts.animation')
@section('title', 'Manage currency')
@section('content')




<style>
#text_font_noir{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#000000;text-align:left;font-size:20px;font-weight:blod;font-weight:600;text-decoration:none;}

#text_font_noir:hover{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#00306e;text-align:left;font-size:20px;font-weight:blod;font-weight:600;text-decoration:none;}

#text_font_noir01{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#717379;text-align:left;font-size:20px;font-weight:blod;font-weight:600;text-decoration:none;}

#text_font_noir2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#000000;text-align:left;font-size:14px;font-weight:normal;font-weight:500;text-decoration:none;}
.space{width:100%;height:10px;}
</style>

<style>
.bg6{transition:2s;opacity:3;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg6:hover{opacity:0.8;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;
transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}


.bg8{transition:2s;opacity:3;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg8:hover{transition:2s;opacity:0.8;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}

.div_col{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:50px;min-height:250px;background-color:#ffffff;text-align:center;}
.div_col:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col .div_col_text{margin-top:0px;transition:2s;color:#000000;}
.div_col:hover .div_col_text{margin-top:10px;transition:2s;}

.div_col_img_view{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col:hover .div_col_img .div_col_img_view{width:120px;height:120px;margin-top:0px;transition:2s;text-align:center;}



.div_col1{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#9e844a;}
.div_col1:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col1 .div_col_text1{margin-top:0px;transition:2s;color:#ffffff;}
.div_col1:hover .div_col_text1{margin-top:10px;transition:2s;}

.div_col_img_view1{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col1:hover .div_col_img1 .div_col_img_view1{width:120px;height:120px;margin-top:0px;transition:2s;}



.div_col3{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#2095b6;}
.div_col3:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col3 .div_col_text3{margin-top:0px;transition:2s;color:#ffffff;}
.div_col3:hover .div_col_text3{margin-top:10px;transition:2s;}

.div_col_img_view3{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col3:hover .div_col_img3 .div_col_img_view3{width:120px;height:120px;margin-top:0px;transition:2s;}






.div_col4{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#9e844a;}
.div_col4:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col4 .div_col_text4{margin-top:0px;transition:2s;color:#ffffff;}
.div_col4:hover .div_col_text4{margin-top:10px;transition:2s;}

.div_col_img_view4{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col4:hover .div_col_img4 .div_col_img_view4{width:120px;height:120px;margin-top:0px;transition:2s;}



.text_font{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-serif}
.text_font2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}
.div_col .btn_col2{background-color:transparent;border:0px solid transparent;color:#000000;padding:10px 20px 10px 20px;}
.div_col:hover .btn_col2{background-color:transparent;border:2px solid #9e844a;color:#9e844a;padding:10px 20px 10px 20px;transition:1s;}
</style>
</head>
<body class="homepage " id="body" data-toggler=".off-canvas-menu--active" >








<header class="hero hero--homepage hero--homepage-transparent" id="" style="">
    <div class="hero__carousel" data-homepage-carousel >
       
	   <div class="container hero__slide column">	

            <a href="#" class="hero__full-width-link" aria-label="En savoir plus"></a>
            <a href="#" aria-label="En savoir plus" class="hero__text-content large-5 column">
                <p class="hero__tag" data-slide-nav-title="Lorem ipsum dolor1" id="text_res">Lorem ipsum dolor1</p>
                <h2 class="showDesktopTitle hero__title" >consectetur adipiscing elit, sed do eiusmod</h2>
                <h2 class="showMobileTitle hero__title">consectetur adipiscing elit, sed do eiusmod</h2>
                <span class="hero__cta ">Learn More
                    <span class="fa fa-long-arrow-right"></span>
                </span>
            </a>
            <div class="hero__image center">
                <div class="hero__overlay"></div>
               <picture>
                    <source srcset="{{ asset('public/photo/slider/slide-3-2.png?width=1920&amp;height=1080&amp;mode=crop')}}" media="(min-width:768px)">
                    <source srcset="{{ asset('public/photo/slider/slide-3-2.png?width=1080&amp;height=1080&amp;mode=crop')}}" media="(min-width:0px)">
                    <img src="{{ asset('public/photo/slider/slide-3-2.png')}}" data-object-fit />
                </picture>
            </div>
        </div>
		
		
		
		
	   <div class="container hero__slide column">	
            <a href="#" class="hero__full-width-link" aria-label="En savoir plus"></a>
            <a href="#" aria-label="En savoir plus" class="hero__text-content large-5 column">
                <p class="hero__tag" data-slide-nav-title="Lorem ipsum dolor2" id="text_res">Lorem ipsum dolor2</p>
                <h2 class="showDesktopTitle hero__title">consectetur adipiscing elit, sed do eiusmod</h2>
                <h2 class="showMobileTitle hero__title">consectetur adipiscing elit, sed do eiusmod</h2>
                <span class="hero__cta ">Learn More
				<span class="fa fa-long-arrow-right"></span>
                </span>
            </a>
            <div class="hero__image center">
                <div class="hero__overlay"></div>
                <picture>
                    <source srcset="{{ asset('public/photo/slider/banner_img.png?width=1920&amp;height=1080&amp;mode=crop')}}" media="(min-width:768px)">
                    <source srcset="{{ asset('public/photo/slider/banner_img.png?width=1080&amp;height=1080&amp;mode=crop')}}" media="(min-width:0px)">
                    <img src="{{ asset('public/photo/slider/banner_img.png')}}" data-object-fit />
                </picture>
            </div>
        </div>
		
		
	   <div class="container hero__slide column">	
            <a href="#" class="hero__full-width-link" aria-label="Sustainable is the next Digital"></a>
            <a href="#" aria-label="Sustainable is the next Digital" class="hero__text-content large-5 column">
                <p class="hero__tag" data-slide-nav-title="Lorem ipsum dolor3" id="text_res">Lorem ipsum dolor3</p>
                <h2 class="showDesktopTitle hero__title">consectetur adipiscing elit, sed do eiusmod</h2>
                <h2 class="showMobileTitle hero__title">consectetur adipiscing elit, sed do eiusmod</h2>
                <span class="hero__cta ">Learn More
                    <span class="fa fa-long-arrow-right"></span>
                        </span>
            </a>
            <div class="hero__image center">
                <div class="hero__overlay"></div>
                <picture>
                    <source srcset="{{ asset('public/photo/slider/slider2.jpg?width=1920&amp;height=1080&amp;mode=crop')}}" media="(min-width:768px)">
                    <source srcset="{{ asset('public/photo/slider/slider2.jpg?width=1080&amp;height=1080&amp;mode=crop')}}" media="(min-width:0px)">
                    <img src="{{ asset('public/photo/slider/slider2.jpg')}}" data-object-fit />
                </picture>
            </div>
        </div>
		



<div class="container hero__slide column">	
<a href="#" class="hero__full-width-link" aria-label="En savoir plus"></a>
<a href="#" aria-label="En savoir plus" class="col-md-6 hero__text-content large-5 column">
<p class="hero__tag" data-slide-nav-title="Lorem ipsum dolor4" id="text_res">Lorem ipsum dolor4</p>
<h2 class="showDesktopTitle hero__title">consectetur adipiscing elit, sed do eiusmod</h2>
<h2 class="showMobileTitle hero__title">consectetur adipiscing elit, sed do eiusmod</h2>
<span class="hero__cta ">Learn More
<span class="fa fa-long-arrow-right"></span>
</span>
</a>
			
<div class="hero__image center">
<div class="hero__overlay"></div>
<picture>
<source srcset="{{ asset('public/photo/slider/10.jpg?width=1920&amp;height=1080&amp;mode=crop')}}" media="(min-width:768px)">
<source srcset="{{ asset('public/photo/slider/10.jpg?width=1080&amp;height=1080&amp;mode=crop')}}" media="(min-width:0px)">
<img src="{{ asset('public/photo/slider/10.jpg')}}" data-object-fit />
</picture>
</div>
</div>


		
    </div>
    <div class="hero__nav-container column container">
        <div class="column" data-carousel-nav></div>
    </div>
    <!--<div class="hero__scroll-indicator">-->
    <!--<span class="hero__scroll-text">Faire d&#233;filer</span>-->
    <!--<span class="icon-chevron-down" id="scroll-to-finder"></span>-->
    <!--</div>-->
</header>





   
   
   
<div class="container">
<div class="row">
<div class="space"></div>
<div class="space"></div>
<div class="col-md-12">
<h1 class="title1">Our last products</h1>
</div>
<div class="space50"></div>
@foreach($Product as $post)

<div class="modal fade bd-example-modal-lg" id="eye{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-6">
<img class="img_full" src="{{ asset('public/photo/products_logo/'.$post->photo) }}">
</div>
<div class="col-md-6" style="margin-top:0px;text-align:left;">
<h1 class="product_title_model" >{{ $post->product_name }}</h1>
<div class="space"></div>
<div class="space"></div>
<h1 class="product_price_model" >{{ $post->product_price }} {{ $post->product_currency }}</h1>
<div class="space"></div>
<p class="product_decription_model">{{ $post->long_description }}</p>

<div class="form-group" style="padding:0px;">
<div class="col-sm-12" style="padding:0px;">
<label  class="qty_model control-label textblod">Quantité</label>
<input type="number" class="qty_num_model form-control" id="name" name="name" value="1" style="width:250px;" required=""/>
</div>
<div class="space"></div>
<div class="space"></div>
<div class="col-sm-12" style="padding:0px;">
<button class="model_btn">Ajouter au panier</button>
</div>
</div>

</div>
</div>
</div>
<div class="modal-footer">
<a  href="javascript:void(0)" id="delete-post" data-id="{{ $post->id }}" class="model_btn_delete delete-post" data-dismiss="modal">Yes, Delete</a>
</div>
</div>
</div>
</div>


<div class="col-md-4">
<div class="col-md-12">
<div class="section">
<img src="{{ asset('public/photo/products_logo/'.$post->photo) }}">
<div class="space"></div>
<h1 class="product_title">{{ $post->product_name }}</h1>
<label class="product_price">{{ $post->product_price }} {{ $post->product_currency }}</label>
<div class="descriptions">
<div class="col-md-12 padding0">
<button href="javascript:void(0)" data-toggle="modal" data-target="#eye{{ $post->id }}"class="model_btn_product"><i class="fa fa-eye"></i> <span class="model_btn_product_text">Aperçu</span></button>
</div>
<div class="col-md-12 padding0">
<button class="model_btn_product"><i class="fa fa-heart"></i> <span class="model_btn_product_text">Souhaits</span></button>
</div>
<div class="col-md-12 padding0">
<button class="model_btn_product"><i class="fa fa-shopping-cart"></i> <span class="model_btn_product_text">Ajouter panier</span></button>

</div>
<h1 class="product_title_d" style="margin-top:90px;text-align:center;">{{ $post->product_name }}</h1>
<p class="product_decription_d">{{ $post->product_price }} {{ $post->product_currency }}</p>
</div>
</div>
</div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
</div>
@endforeach
</div>
</div>
         


















<style>
.bg6{transition:2s;opacity:3;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg6:hover{opacity:0.8;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;
transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}


.bg8{transition:2s;opacity:3;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg8:hover{transition:2s;opacity:0.8;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}

.div_col{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#2095b6;}
.div_col:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col .div_col_text{margin-top:0px;transition:2s;color:#ffffff;}
.div_col:hover .div_col_text{margin-top:10px;transition:2s;}

.div_col_img_view{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col:hover .div_col_img .div_col_img_view{width:120px;height:120px;margin-top:0px;transition:2s;}



.div_col1{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#9e844a;}
.div_col1:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col1 .div_col_text1{margin-top:0px;transition:2s;color:#ffffff;}
.div_col1:hover .div_col_text1{margin-top:10px;transition:2s;}

.div_col_img_view1{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col1:hover .div_col_img1 .div_col_img_view1{width:120px;height:120px;margin-top:0px;transition:2s;}



.div_col3{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#2095b6;}
.div_col3:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col3 .div_col_text3{margin-top:0px;transition:2s;color:#ffffff;}
.div_col3:hover .div_col_text3{margin-top:10px;transition:2s;}

.div_col_img_view3{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col3:hover .div_col_img3 .div_col_img_view3{width:120px;height:120px;margin-top:0px;transition:2s;}






.div_col4{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#9e844a;}
.div_col4:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col4 .div_col_text4{margin-top:0px;transition:2s;color:#ffffff;}
.div_col4:hover .div_col_text4{margin-top:10px;transition:2s;}

.div_col_img_view4{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col4:hover .div_col_img4 .div_col_img_view4{width:120px;height:120px;margin-top:0px;transition:2s;}



.text_font{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-serif}
.text_font2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}
.btn_col2{background-color:transparent;border:0px solid transparent;color:#000000;padding:10px 20px 10px 20px;}
.btn_col2:hover{background-color:transparent;border:0px solid transparent;color:#ffffff;padding:10px 20px 10px 20px;transition:1s;}
</style>		 

<section class="container">
<div class="row">
<div class="space50"></div>

<div class="col-md-12">
<h1 class="title1">Lorem ipsum title</h1>
</div>
<div class="space50"></div>

<div class="col-md-3 ">
<div class="col-md-12 div_col">
<div class="col-md-12 div_col_img" style="text-align:center;">
<img src="{{ asset('public/image/dessin/sitevitrine2.png')}}" class="div_col_img_view">
</div>
<label class="text_font div_col_text">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>



<div class="col-sm-3 ">
<div class="col-md-12 div_col1">
<div class="col-md-12 div_col_img1" style="text-align:center;">
<img src="{{ asset('public/image/dessin/ecom2-1.png')}}" class="div_col_img_view1">
</div>
<label class="text_font div_col_text1">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>


<div class="col-sm-3 ">
<div class="col-md-12 div_col3">
<div class="col-md-12 div_col_img3" style="text-align:center;">
<img src="{{ asset('public/image/dessin/qpp2.png')}}" class="div_col_img_view3">
</div>
<label class="text_font div_col_text3">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>



<div class="col-sm-3 ">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/dessin/plateforme2.png')}}" class="div_col_img_view4">
</div>
<label class="text_font div_col_text4">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>

</div>
</div>
</section>






















<style>
.full{margin-top:100px;background-color:#9e844a;padding:20px 0px 40px 0px;}
.div_col5{transition:2s;border:0px solid #021a96;border-radius:0px 0px 0px 0px;padding:20px;min-height:250px;background-color:#ffffff;}
.div_col5:hover{transition:2s;border:0px solid #9e844a;border-radius:0px 0px 0px 0px;padding:20px;min-height:250px;}
.div_col5 .div_col_text4{margin-top:0px;transition:2s;color:#000000;text-align:center;}
.div_col5:hover .div_col_text4{margin-top:0px;transition:1s;color:#ffffff;font-size:0px;}

.div_col_img_view5{width:0px;height:0px;margin-top:-70px;transition:2s;}
.div_col5:hover .div_col_img4 .div_col_img_view5{width:120px;height:120px;margin-top:-20px;transition:2s;}



.text_font{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-serif}
.text_font2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}
.div_col5 .btn_col2{background-color:transparent;border:0px solid transparent;color:#000000;padding:10px 20px 10px 20px;}
.div_col5:hover .btn_col2{margin-top:-10px;background-color:transparent;color:#000000;padding:10px 20px 10px 20px;transition:1s;border:2px solid #9e844a;}
</style>







<section class="full">
<div class="container">

<div class="row">

<div class="col-md-12">
<h1 class="title1_b">Lorem ipsum title</h1>
</div>
<div class="space"></div>
<div class="space"></div>
<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col5">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/dessin/plateforme2.png')}}" class="div_col_img_view5">
</div>
<div class="col-md-12">
<h1 class="title1">Lorem ipsum title</h1>
</div>
<label class="text_font div_col_text4">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>


<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col5">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/dessin/plateforme2.png')}}" class="div_col_img_view5">
</div>
<div class="col-md-12">
<h1 class="title1">Lorem ipsum title</h1>
</div>
<label class="text_font div_col_text4">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>

</div>
</div>
</section>


<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    

<script>
$("#button").click(function() {
  $('.transform').toggleClass('transform-active');
});

$("#buttons1").click(function() {
  $('.transform').toggleClass('transform-active');
});

$("#button").click(function() {
  $('.transform2').toggleClass('transform2-active');
});
$("#buttons2").click(function() {
  $('.transform2').toggleClass('transform2-active');
});

$("#button").click(function() {
  $('.transform3').toggleClass('transform3-active');
});
$("#buttons3").click(function() {
  $('.transform3').toggleClass('transform3-active');
});

$("#button").click(function() {
  $('.transform4').toggleClass('transform4-active');
});
$("#buttons4").click(function() {
  $('.transform4').toggleClass('transform4-active');
});
</script>
<script src="{{ asset('public/js/js-slider.js')}}"></script>
@endsection