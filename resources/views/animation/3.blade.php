<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
<title>Animation</title>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/bootstrap.mins.css" rel="stylesheet">

<style>
.bg6{transition:2s;opacity:3;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg6:hover{opacity:0.8;min-height:400px;text-align:center;background-image: url('{{ asset('public/photo/slider/10.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;
transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}


.bg8{transition:2s;opacity:3;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;}
.bg8:hover{transition:2s;opacity:0.8;min-height:200px;text-align:center;background-image: url('{{ asset('public/photo/slider/slide-3-1.jpg')}}');background-repeat: no-repeat;background-size: 100% 100%;transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(1);
    filter: blur(1px);}

.div_col{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#2095b6;}
.div_col:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col .div_col_text{margin-top:0px;transition:2s;color:#ffffff;}
.div_col:hover .div_col_text{margin-top:10px;transition:2s;}

.div_col_img_view{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col:hover .div_col_img .div_col_img_view{width:120px;height:120px;margin-top:0px;transition:2s;}



.div_col1{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#9e844a;}
.div_col1:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col1 .div_col_text1{margin-top:0px;transition:2s;color:#ffffff;}
.div_col1:hover .div_col_text1{margin-top:10px;transition:2s;}

.div_col_img_view1{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col1:hover .div_col_img1 .div_col_img_view1{width:120px;height:120px;margin-top:0px;transition:2s;}



.div_col3{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#2095b6;}
.div_col3:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col3 .div_col_text3{margin-top:0px;transition:2s;color:#ffffff;}
.div_col3:hover .div_col_text3{margin-top:10px;transition:2s;}

.div_col_img_view3{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col3:hover .div_col_img3 .div_col_img_view3{width:120px;height:120px;margin-top:0px;transition:2s;}






.div_col4{transition:2s;border:2px solid #021a96;border-radius:40px 0px 40px 0px;padding:20px;min-height:250px;background-color:#9e844a;}
.div_col4:hover{transition:2s;border:2px solid #9e844a;border-radius:40px 0px 40px 0px;padding:20px;min-height:350px;}
.div_col4 .div_col_text4{margin-top:0px;transition:2s;color:#ffffff;}
.div_col4:hover .div_col_text4{margin-top:10px;transition:2s;}

.div_col_img_view4{width:50px;height:50px;margin-top:-70px;transition:2s;}
.div_col4:hover .div_col_img4 .div_col_img_view4{width:120px;height:120px;margin-top:0px;transition:2s;}



.text_font{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-serif}
.text_font2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}
.btn_col2{background-color:transparent;border:0px solid transparent;color:#000000;padding:10px 20px 10px 20px;}
.btn_col2:hover{background-color:transparent;border:0px solid transparent;color:#ffffff;padding:10px 20px 10px 20px;transition:1s;}
</style>


</head>
<body>


<section class="container" style="margin-top:100px;">

<div class="row">


<div class="col-md-3 ">
<div class="col-md-12 div_col">
<div class="col-md-12 div_col_img" style="text-align:center;">
<img src="{{ asset('public/image/dessin/sitevitrine2.png')}}" class="div_col_img_view">
</div>
<label class="text_font div_col_text">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>



<div class="col-sm-3 ">
<div class="col-md-12 div_col1">
<div class="col-md-12 div_col_img1" style="text-align:center;">
<img src="{{ asset('public/image/dessin/ecom2-1.png')}}" class="div_col_img_view1">
</div>
<label class="text_font div_col_text1">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>


<div class="col-sm-3 ">
<div class="col-md-12 div_col3">
<div class="col-md-12 div_col_img3" style="text-align:center;">
<img src="{{ asset('public/image/dessin/qpp2.png')}}" class="div_col_img_view3">
</div>
<label class="text_font div_col_text3">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>
</div>



<div class="col-sm-3 ">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/dessin/plateforme2.png')}}" class="div_col_img_view4">
</div>
<label class="text_font div_col_text4">
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
</label>
</br></br>
<div class="col-md-12" style="text-align:center;">
<button class="btn_col2">View more</button>
</div>
</div>

</div>
</div>
</section>






</div>
</body>
</html>