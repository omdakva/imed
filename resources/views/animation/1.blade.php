@extends('layouts.animation')
@section('title', 'Manage currency')
@section('content')
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
   
   
   
   
   
<div class="container">




<div class="row">
<div class="col-md-12">
<h1 class="title1">Our last products</h1>
</div>
<div class="space"></div>
<div class="space"></div>
@foreach($Product as $post)

<div class="modal fade bd-example-modal-lg" id="eye{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-6">
<img class="img_full" src="{{ asset('public/photo/products_logo/'.$post->photo) }}">
</div>
<div class="col-md-6" style="margin-top:0px;text-align:left;">
<h1 class="product_title_model" >{{ $post->product_name }}</h1>
<div class="space"></div>
<div class="space"></div>
<h1 class="product_price_model" >{{ $post->product_price }} {{ $post->product_currency }}</h1>
<div class="space"></div>
<p class="product_decription_model">{{ $post->long_description }}</p>

<div class="form-group" style="padding:0px;">
<div class="col-sm-12" style="padding:0px;">
<label  class="qty_model control-label textblod">Quantité</label>
<input type="number" class="qty_num_model form-control" id="name" name="name" value="1" required="">
</div>
<div class="space"></div>
<div class="space"></div>
<div class="col-sm-12" style="padding:0px;">
<button class="model_btn">Ajouter au panier</button>
</div>
</div>

</div>
</div>
</div>
<div class="modal-footer">
<a  href="javascript:void(0)" id="delete-post" data-id="{{ $post->id }}" class="model_btn_delete delete-post" data-dismiss="modal">Yes, Delete</a>
</div>
</div>
</div>
</div>


<div class="col-md-4">
<div class="col-md-12">
<div class="section">
<img src="{{ asset('public/photo/products_logo/'.$post->photo) }}">
<div class="space"></div>
<h1 class="product_title">{{ $post->product_name }}</h1>
<label class="product_price">{{ $post->product_price }} {{ $post->product_currency }}</label>
<div class="descriptions">
<div class="col-md-12 padding0">
<button href="javascript:void(0)" data-toggle="modal" data-target="#eye{{ $post->id }}"class="model_btn_product"><i class="fa fa-eye"></i> <span class="model_btn_product_text">Aperçu</span></button>
</div>
<div class="col-md-12 padding0">
<button class="model_btn_product"><i class="fa fa-heart"></i> <span class="model_btn_product_text">Souhaits</span></button>
</div>
<div class="col-md-12 padding0">
<button class="model_btn_product"><i class="fa fa-shopping-cart"></i> <span class="model_btn_product_text">Ajouter panier</span></button>

</div>
<h1 class="product_title_d" style="margin-top:90px;text-align:center;">{{ $post->product_name }}</h1>
<p class="product_decription_d">{{ $post->product_price }} {{ $post->product_currency }}</p>
</div>
</div>
</div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
</div>
@endforeach
</div>







<div class="row">
<div class="col-md-12">
<h1 class="title1">Title lorem ipsum</h1>
</div>
<div class="col-md-4 col-md-4 margin-res">
<div class="col-md-12 border-1w-full">
<div class="col-md-12 border-1w">
</div>
<div class="space_div"></div>
<h1 class="col-md-11 border-1w-text textblodfull">
Lorem ipsum
</h1>
<div class="col-md-11 border-1w-text-show">
<p class="textnormalfull">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
<div class="space_div"></div>
<div class="col-md-12">
<button class="model_btn">View more <i class="fa fa-angle-right incons-right"></i></button>
</div>
</div>
</div>
</div>




<div class="col-md-4 col-md-4 margin-res">
<div class="col-md-12 border-1w-full">
<div class="col-md-12 border-1w">
</div>
<div class="space_div"></div>
<h1 class="col-md-11 border-1w-text textblodfull">
Lorem ipsum
</h1>
<div class="col-md-11 border-1w-text-show">
<p class="textnormalfull">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
<div class="space_div"></div>
<div class="col-md-12">
<button class="model_btn">View more <i class="fa fa-angle-right incons-right"></i></button>
</div>
</div>
</div>
</div>




<div class="col-md-4 col-md-4 margin-res">
<div class="col-md-12 border-1w-full">
<div class="col-md-12 border-1w">
</div>
<div class="space_div"></div>
<h1 class="col-md-11 border-1w-text textblodfull">
Lorem ipsum
</h1>
<div class="col-md-11 border-1w-text-show">
<p class="textnormalfull">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
<div class="space_div"></div>
<div class="col-md-12">
<button class="model_btn">View more <i class="fa fa-angle-right incons-right"></i></button>
</div>
</div>
</div>
</div>


</div>




<div class="space"></div>
<div class="space"></div>
<div class="row border-2w-full">
<div class="col-md-12">
<h1 class="title1">Title lorem ipsum</h1>
</div>
<div class="space"></div>
<div class="space"></div>
<div class="col-md-6 bg1">
</div>
<div class="col-md-6 bg1-text">
<div class="col-md-12 bg1-text-top">
<div class="col-md-12">
<img src="{{ asset('public/image/icons/10.jpg') }}" class="img_bg1">
</div>
<div class="space_div"></div>

<h1 class="col-md-112 border-1w-text textblodfull">
Lorem ipsum
</h1>
<div class="col-md-112 border-1w-text-show">
<p class="textnormalfull">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
<div class="space_div"></div>
<div class="col-md-12">
<button class="model_btn">View more <i class="fa fa-angle-right incons-right"></i></button>
</div>
</div>
</div>
</div>
</div>














    <div class="row">
      <div class="col-md-8 divbg row">
      <div class="col-md-7">
        <div class="row">        
        <img id="expandedImg" src="{{ asset('public/photo/products_logo/canape_231479.jpg') }}" width="100%" height="300px"/>

        <div class="column">
          <img src="{{ asset('public/photo/products_logo/images.jfif') }}" class="myimg" onclick="myFunction(this);">
        </div>
        <div class="column">
          <img src="{{ asset('public/photo/products_logo/shutterstock_1465769324.jpg') }}" class="myimg" onclick="myFunction(this);">
        </div>
        <div class="column">
          <img src="{{ asset('public/photo/products_logo/téléchargement (1).jfif') }}" class="myimg" onclick="myFunction(this);">
        </div>
        <div class="column">
          <img src="{{ asset('public/photo/products_logo/téléchargement (2).jfif') }}" class="myimg" onclick="myFunction(this);">
        </div>
        </div>
        <hr>
        <h2>PARTAGEZ CE PRODUIT</h2>
        <a href="#" class="fa fa-facebook"></a>
        <a href="#" class="fa fa-twitter"></a>
      </div>
      <div class="col-md-5">
        <h2>Product</h2>
        <p>Marque: Naturel | Produits similaires par Naturel</p>
      <div class="rating">
        <a href="#1" title="Donner 1 étoile">★</a>
        <a href="#2" title="Donner 2 étoiles">★</a>
        <a href="#3" title="Donner 3 étoiles">★</a>
        <a href="#4" title="Donner 4 étoiles">★</a>
        <a href="#5" title="Donner 5 étoiles">★</a>
      </div>
      <hr> 
      <p><b>29.90 TND</b></p>
      <p style="text-decoration: line-through;">158.00 TND</p>
      <button type="button" class="btn btn-warning myBtn"><i class="fa fa-shopping-cart" style="font-size:16px; padding:10px;" aria-hidden="true"></i>Ajouter au panier</button>
      </div>
      </div>
      <div class="col-md-4">
        <div class="divbg1">
          <p>LIVRAISON & RETOURS</p>
          <hr>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
        </div>
        <div class="divbg1">
          <p>INFORMATIONS SUR LE VENDEUR</p>
          <hr>
          <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature.</p>
          <hr>
          <p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil).</p>
        </div>
      </div>
    </div>

    <div class="row myDiv">
      <div class="col-md-8 divbg">
        <h2>Détails</h2>
        <hr>
        <h3>Why do we use it?</h3>
        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. 
          The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,
           content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem 
           Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. 
           Various versions have 
           evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
           <iframe width="100%" height="615" src="https://www.youtube.com/embed/i8GofKnae4E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="col-md-4">
        <div class="divbg1">
          <p>LIVRAISON & RETOURS</p>
          <hr>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
        </div>
        <div class="divbg1">
          <p>INFORMATIONS SUR LE VENDEUR</p>
          <hr>
          <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature.</p>
          <hr>
          <p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil).</p>
        </div>
      </div>
    </div>
	
	
	

      	
</div>
@endsection