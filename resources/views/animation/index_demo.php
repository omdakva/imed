
<!DOCTYPE html>
<html lang="en" id="html" dir="ltr">
<head>
<title>IMDTEC – HOME</title>
<link href="https://imdtec.org/fr-fr/Images/IMDTEC-NOV-201902.png" style="min-width:200px;min-height:200px;"rel="shortcut icon" type="image/png">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1">
<meta name="format-detection" content="telephone=no">
<link rel="shortcut icon" href="image/imdec.png" type="image/x-icon"><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="all" />
<link rel="stylesheet" href="https://imdtec.org/fr-fr/bootstrap/style.css" media="all">
<link href="https://imdtec.org/fr-fr/bootstrap/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://imdtec.org/fr-fr/bootstrap/css/custom.css">
<link rel="stylesheet" type="text/css" href="https://imdtec.org/fr-fr/bootstrap/footor/main.css">	
<link rel="stylesheet" href="https://imdtec.org/fr-fr/bootstrap/css/uhf.css" type="text/css" media="all"> 

<style>
#text_font_nav{font-weight:normal;font-weight:600;font-size:15px;font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}





#text_font{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-serif}
#text_font2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}
#btn11{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif}
</style>


<style>
#animation_btn{background-color:#ffffff;border:1px solid #17488a;color:#17488a;padding:15px 20px 15px 20px;margin-top:10px;}
#animation_btn:hover{background-color:#ebf2fc;border:1px solid #17488a;color:#17488a;padding:15px 20px 15px 20px;}

#animation_btn2{background-color:transparent;border:1px solid #ffffff;color:#ffffff;padding:15px 20px 15px 20px;margin-top:10px;}
#animation_btn2:hover{background-color:#113c76;border:1px solid #ffffff;color:#ffffff;padding:15px 20px 15px 20px;}
</style>


 <style>
#ul_footor {
  list-style: none;
}

#ul_footor li::before {
  content: "\2022";
  color:transparent;
  font-weight: bold;
  display: inline-block; 
  width: 1em;
  margin-left: -1em;
}

.ul_footor {
  list-style: none;
}

.ul_footor li::before {
  content: "\2022";
  color:transparent;
  font-weight: bold;
  display: inline-block; 
  width: 1em;
  margin-left: -1em;
}
</style>
    <style>
        .fa {

            font-size: 25px;
        }
        a:hover{

            text-decoration: none;
        }


        .columnimg {
            float: left;
            width: 33.33%;
            padding: 5px;
        }


        .rowimg::after {
            content: "";
            clear: both;
            display: table;
        }

        .imgContainer{

            float: left;
        }

        .imgContainer .txtlogo{

            max-height: none;width: 115px;
        }
        .imgContainer .logoself{

            display: block;max-height: none;width: 33px;
        }

        .primary-nav__off-canvas-menu{

            display: none;
        }


        .primary-nav__logo{

            margin: 0!important;
        }

        .square-menu{

            padding:15px;
            float: left;
        }

        .profile-menu{

            float: left;
        }


        .app-black , .login-black{

            display: none;
        }


        .primary-nav__right-wrap:hover .app-black , .primary-nav__right-wrap:hover .login-black,
        .primary-nav:hover .app-black ,.primary-nav:hover .login-black{

            display: block;
        }

        .primary-nav__right-wrap:hover .app-white , .primary-nav__right-wrap:hover .login-white,

        .primary-nav:hover .app-white , .primary-nav:hover .login-white{

            display: none;
        }

        .primary-nav__nav .primary-nav__level-one>li>a{

            font-weight: 700;
        }

        .pro-app-container{

            display: flex;
        }

  .hero__slide-nav-item:hover{

            color: #fff!important;
        }
        @media (max-width: 1080px) {

            .imgContainer .txtlogo{

                max-height: none;width: 100px;
            }

            .imgContainer .logoself{

                display: block;max-height: none;width: 30px;

            }

            .primary-nav__off-canvas-menu {

                display: block;
                margin-top:-5px;
            }

            .canvas-bars-only{

                display: none!important;
            }
            .primary-nav__logo{
                margin: 0 1.5rem;

            }

            .primary-nav__logo{
                margin-left: -15px!important;
            }

            .square-menu {

                padding: 5px;
            }

            .profile-menu{

                padding: 0;
            }

            .search-menu{

                display: none;
            }

            .fa {

                font-size: 17px;
            }

            .site-search__close{

                margin-left: -12px;
            }

            .pro-app-container{

                display: inline-block;
            }
        }

    </style>
	
	












<style>
@media only screen and (max-width: 20992px) 
{
.menu_header{padding:0px 1000px 0px 1000px;}
.body_page{padding:0px 1000px 0px 1000px;text-align:center;}
.body_page_contenu{max-width:1500px;margin: auto;}
}


@media only screen and (max-width: 5992px) 
{
.menu_header{padding:0px 1000px 0px 1000px;}
.body_page{padding:0px 1000px 0px 1000px;text-align:center;}
.body_page_contenu{max-width:1500px;margin: auto;}
}

@media only screen and (max-width: 3992px) 
{
.menu_header{padding:0px 0px 0px 0px;}
.body_page{padding:0px 400px 0px 400px;text-align:center;}
.body_page_contenu{max-width:1500px;margin: auto;}
}


@media only screen and (max-width: 1992px) 
{
.menu_header{padding:0px 0px 0px 0px;}
.body_page{padding:0px 0px 0px 0px;}
.body_page_contenu{max-width:100%;width:100%;margin: auto;}


}

@media only screen and (max-width:1377px) 
{
.menu_header{padding:0px 0px 0px 0px;}

.body_page{padding:0px 0px 0px 0px;}

}


@media only screen and (max-width:1130px) 
{
.body_page{padding:0px 0px 0px 0px;}
}












@media only screen and (max-width:1090px) 
{
.body_page{padding:0px 0px 0px 0px;}
}











@media only screen and (max-width:767px) 
{
.body_page{padding:0px 0px 0px 0px;}
}
</style>

	

<style>
@media only screen and (max-width: 10992px) 
{
#width_mobile 
{
}
}

@media only screen and (max-width: 1992px) 
{
#width_mobile 
{
}
}

@media only screen and (max-width:1377px) 
{
#width_mobile 
{
}
}



@media only screen and (max-width:1130px) 
{
#width_mobile 
{
	width:100%;

}

}



@media only screen and (max-width:1080px) 
{
#width_mobile 
{
width:100%;
}
}


@media only screen and (max-width:830px) 
{
#width_mobile 
{
width:100%;
}
}

@media only screen and (max-width:767px) 
{
#width_mobile 
{
width:100%;
}
}
</style>
<style>
@media only screen and (max-width: 10992px) 
{
#slider 
{
min-height:500px;max-height:500px;
}
}

@media only screen and (max-width: 1992px) 
{
#slider 
{
min-height:500px;max-height:700px;
}
}

@media only screen and (max-width:1377px) 
{
#slider 
{
min-height:500px;max-height:700px;
}
}



@media only screen and (max-width:1130px) 
{
#slider 
{
min-height:600px;max-height:700px;
}
}



@media only screen and (max-width:992px) 
{
#slider 
{
min-height:600px;max-height:700px;
}
}


@media only screen and (max-width:830px) 
{
#slider 
{
min-height:530px;
}
}

@media only screen and (max-width:767px) 
{
#slider 
{
min-height:530px;
}
}
</style>









<style>
.css_select{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-seriffont-weight:normal;font-weight:600;font-size:15px;color:#000000;height:40px;background-color:#ffffff;border:2px solid #f5f5f5;width:84%;padding:0px 0px 0px 50px;}

.css_select:hover{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-seriffont-weight:normal;font-weight:600;font-size:15px;color:#000000;height:40px;background-color:#ffffff;border:2px solid #3c599d;}

.css_select:focus{font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-seriffont-weight:normal;font-weight:600;font-size:15px;color:#000000;height:40px;background-color:#ffffff;border:2px solid #3c599d;}
</style>




<style>
@media screen and (max-width: 10992px) {

#title_find 
{
font-size:50px;
}
}

@media screen and (max-width: 1992px) {

#title_find 
{
font-size:50px;
}
}

@media screen and (max-width:1377px) {
#title_find 
{
font-size:40px;
}


@media screen and (max-width:1077px) {
#title_find 
{
font-size:20px;
}

@media screen and (max-width:777px) {
#title_find 
{
font-size:16px;
}
}
</style>

<style>
.nav-tabs>li>button{
   background-color: #f1f1f1;
   width:100%;
   color:#000000;
   border-radius:0px;
   border-top:4px solid #f1f1f1;
   font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;font-size:14px;
}




.nav-tabs>li.liii1.active>button{
   background-color: #ffffff;
   width:100%;
   color:#17244e;
    border-top:4px solid #17244e;
	top:-4px;
}
.nav-tabs>li.liii1.active>button:focus{
   background-color: #ffffff;
   color:#17244e;
       border-top:4px solid #17244e;

}
.nav-tabs>li.liii1.active>button:hover {
   background-color: #ffffff;
   color:#17244e;
       border-top:4px solid #17244e;

}






.nav-tabs>li.liii2.active>button{
   background-color: #ffffff;
   width:100%;
   color:#17488a;
    border-top:4px solid #17488a;
		top:-4px;

}
.nav-tabs>li.liii2.active>button:focus{
   background-color: #ffffff;
   color:#17488a;
       border-top:4px solid #17488a;

}
.nav-tabs>li.liii2.active>button:hover {
   background-color: #ffffff;
   color:#17488a;
       border-top:4px solid #17488a;

}
</style>



















   
<style>
@media screen and (max-width: 1992px) 
{

#slider_body 
{
padding:0px 130px 0px 130px;
}
}

@media screen and (max-width:1377px) {
#slider_body 
{
padding:0px 60px 0px 60px;
}
}




@media screen and (max-width:1077px) {
#slider_body 
{
padding:0px 60px 0px 60px;
}
}

@media screen and (max-width:777px) {
#slider_body 
{
padding:0px 0px 0px 0px;
}
}
</style>






  <style>
@media screen and (max-width: 1992px) 
{

#slider_text 
{
width:400px;
}
}

@media screen and (max-width:1377px) {
#slider_text 
{
width:400px;
}
}




@media screen and (max-width:1077px) {
#slider_text 
{
width:400px;
}
}

@media screen and (max-width:777px) {
#slider_text 
{
width:100px;
}
}
</style> 
<style type="text/css">
.overlay1 {
    position: absolute;
    width: 100%;
min-height:590px;max-height:670px;
    z-index: 1;
    background-color: transparent;
    opacity: .9;
	background-image: linear-gradient(to bottom right, #000000, transparent);
    background:linear-gradient(to right,#000000,rgba transparent);
	
}
</style>







<style>
@media only screen and (max-width: 10992px) 
{
.list_booking 
{
background-color:transparent;
}
}

@media only screen and (max-width: 1992px) 
{
.list_booking 
{
background-color:transparent;
}
}

@media only screen and (max-width:1377px) 
{
.list_booking 
{
background-color:transparent;
}
}



@media only screen and (max-width:1130px) 
{
.list_booking 
{
background-color:transparent;
}
}



@media only screen and (max-width:991px) 
{
.list_booking 
{
background-color:transparent;padding:0px;height:120px;
}
.list_booking_li 
{
width:100%;margin-top:-200px;
}
.list_booking_li_r 
{
width:100%;margin-top:-140px;
}
.list_booking_all
{
position:absolute;display:none;
}
}


@media only screen and (max-width:830px) 
{
.list_booking 
{
background-color:transparent;padding:0px;height:120px;
}
.list_booking_li 
{
width:100%;margin-top:-200px;
}
.list_booking_li_r 
{
width:100%;margin-top:-140px;
}
.list_booking_all
{
position:absolute;display:none;
}
}

@media only screen and (max-width:777px) 
{
.list_booking 
{
background-color:transparent;padding:0px;height:120px;
}
.list_booking_li 
{
width:100%;margin-top:-440px;
}
.list_booking_li_r 
{
width:100%;margin-top:-380px;
}
.list_booking_all
{
position:absolute;display:none;
}
}
</style>


<style>
@media only screen and (max-width: 10992px) 
{
.padding_bar_home
{
padding:0px 65px 0px 65px;
}
.padding_bar
{
padding:0px 65px 0px 65px;background-color:#ffffff;
}
}


@media only screen and (max-width: 1992px) 
{
.padding_bar_home
{
padding:0px 45px 0px 45px;
}
.padding_bar
{
padding:0px 45px 0px 45px;background-color:#ffffff;
}
}



@media only screen and (max-width:1377px) 
{
.padding_bar_home
{
padding:0px 45px 0px 45px;
}
.padding_bar
{
padding:0px 45px 0px 45px;background-color:#ffffff;
}
}



@media only screen and (max-width:1130px) 
{
.padding_bar_home
{
padding:0px 65px 0px 65px;
}
.padding_bar
{
padding:0px 65px 0px 65px;background-color:#ffffff;
}
}


@media only screen and (max-width:1100px) 
{
.padding_bar_home
{
padding:0px 65px 0px 65px;
}
.padding_bar
{
padding:0px 45px 0px 45px;background-color:#ffffff;
}
}


@media only screen and (max-width:991px) 
{
.padding_bar_home
{
padding:0px 0px 0px 0px;
}
.padding_bar
{
padding:0px 0px 0px 0px;background-color:#ffffff;
}
}







@media only screen and (max-width:830px) 
{
.padding_bar
{
padding:0px 0px 0px 0px;background-color:#ffffff;
}
}




@media only screen and (max-width:777px) 
{
.padding_bar
{
padding:0px 0px 0px 0px;background-color:#ffffff;
}
}

</style>



<style>
#text_font_noir{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#000000;text-align:left;font-size:20px;font-weight:blod;font-weight:600;text-decoration:none;}

#text_font_noir:hover{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#00306e;text-align:left;font-size:20px;font-weight:blod;font-weight:600;text-decoration:none;}

#text_font_noir01{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#717379;text-align:left;font-size:20px;font-weight:blod;font-weight:600;text-decoration:none;}

#text_font_noir2{font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;color:#000000;text-align:left;font-size:14px;font-weight:normal;font-weight:500;text-decoration:none;}

</style></head>
<body class="homepage " id="body" data-toggler=".off-canvas-menu--active" >

<header id="site-header" class="header" data-sticky-container dir="ltr" style="z-index:999999;height:60px;">


<div data-delay-toggle data-control-elements="[data-nav-menu-control], [data-site-search-control]" data-class-toggle="menu-open" data-remove-on-click-out="true" data-delay-time="0" data-reverse="true" class="header__wrapper expanded header--transparent" data-top-anchor="nav-anchor" data-navigation data-scroll-dependent data-sticky data-sticky-on="small" data-stick-to="top" data-margin-top="0">

<a class="primary-nav__overlay " data-toggle="html body" id="overlay_697"></a>

<div class="primary-nav__wrapper" id="nav-wrap" data-delay-toggle data-control-elements="[data-site-search-control]" data-class-toggle="search-active" data-delay-time="0">



<div class="column row padding_bar_home">


<div class="primary-nav column " >




<div class="primary-nav__off-canvas-menu " style="margin-top:3px;">
					
<a style="background-color:transparent;"class="openbtn" onclick="openNav()" id="main" href="javascript: void(0);">
<span id="one" style="font-size:40px;cursor:pointer" onclick="openNav()">
<span style="font-size:20px;" class="fa fa-bars"></span>
</span>
</a>

<a class="" data-site-search-control data-input-focus-trigger data-input-fields="#nav-site-search-input" data-delay-time="500">
<i style="font-size:20px;"class="fa fa-search"></i>
</a>

</div>



                    
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
<div class="primary-nav__off-canvas-menu  canvas-bars-only">
<a style="background-color:transparent;"class="openbtn" onclick="openNav()" id="main" href="javascript: void(0);">
<span id="one" style="font-size:40px;cursor:pointer" onclick="openNav()">
<span style="font-size:20px;" class="fa fa-bars"></span>
</span>
</a>

                        <!--<div id="" data-delay-toggle data-control-elements="[data-site-search-control]" data-class-toggle="is-expanded" data-delay-time="0" class="site-search nav-site-search">-->
                        <!--<a class="site-search__open nav-site-search" data-site-search-control data-input-focus-trigger data-input-fields="#nav-site-search-input" data-delay-time="500">-->
                        <!--&lt;!&ndash;<span class="site-search__search-text" >Explorer</span>&ndash;&gt;-->
                        <!--&lt;!&ndash;<span class="icon-search"></span>&ndash;&gt;-->
                        <!--<i class="fa fa-search"></i>-->
                        <!--</a>-->
                        <!--</div>-->
                        <div class="off-canvas-menu" id="off-canvas-menu-bars" data-auto-close>
                            <div class="off-canvas-menu__close">
                                <a href="javascript:void(0);" data-toggle="off-canvas-menu body html overlay_697">
                                    <!--<span class="icon-close"></span>-->
                                    <span class="fa fa-close"></span>
                                </a>
                            </div>
							

                        </div>
                    </div>





                    
	




















	
<div class="primary-nav__logo" id="logo_img">
<a href="https://imdtec.org/fr-fr/">
<div>
<div class="imgContainer">
<img class="txtlogo" src="https://imdtec.org/fr-fr/Images/logo.png"/>
</div>
<div class="imgContainer">
<img class="logoself" src="https://imdtec.org/fr-fr/Images/imdec.png"/>
</div>
</div>
</a>
</div>
					
					
					
					
                    <div class="primary-nav__logo primary-nav__logo--white">
                        <a href="https://imdtec.org/fr-fr/">
                            <div >
                                <div class="imgContainer">
                                    <img class="txtlogo"  src="https://imdtec.org/fr-fr/Images/logo2.png"/>
                                </div>
                                <div class="imgContainer">
                                    <img class="logoself" src="https://imdtec.org/fr-fr/Images/imdec.png"/>
                                </div>
                            </div>
                            <!--<div class=" user_logo_text imdtec_text" style="width:115px;height: 40px;margin-left:0px;float:left;background: url('images/logo2.png');background-size: cover"></div>-->

                            <!--<img style="display: block" src="https://imdtec.org/fr-fr/Images/imdec.png"/>-->
                        </a>
                    </div>

					
		
					
					
					
					
					
					
			



























			

					
					
<div class="primary-nav__right-wrap" data-delay-toggle data-control-elements="[data-site-search-control]" data-class-toggle="search-open" data-delay-time="800">
                    <div class="primary-nav__nav  ">
                        <nav>
                                <ul class="primary-nav__level-one" data-dropdown-menu data-hover-delay='200' >

                                    <li>
                                        <a href="#" aria-label="Azuregram" class=""id="text_font_nav"> Azuregram</a>
                                    </li>
                                    <li>
                                        <a href="#" aria-label="Businesses" class=""id="text_font_nav">Businesses</a>
                                        <ul class="primary-nav__mega-menu">
                                            <li class="primary-nav__mega-menu-wrapper">
                                                <section>
                                                    <div class="primary-nav__primary-list">
                                                        <div class="primary-nav__col-group">

                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two"  >
                                                                    <li style="margin-bottom: 10px;" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Business Functions</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Agile">Audit</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">Analytics</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Design">Design</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Marketing & Sales">Marketing & Sales</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Operation">Operation</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Risk">Risk</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Strategy">Strategy</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Restructuring">Restructuring</a>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Inteligent Enterprise</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/business-simulation.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">
                                                                            Intelligent Enterprise are
                                                                            Essential in Improving
                                                                            Performance
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Pyxis</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/digital_transformation.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">

                                                                            Connect everyone
                                                                            in your
                                                                            organization
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Industry</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Agile">Finance Service</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">Government</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Design">Health</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Marketing & Sales">Manufacturing & Sales</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Operation">Retail</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Risk">Transportation</a>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </li>
                                        </ul>
                                    </li>
                                    <li >
                                        <a href="" aria-label="Products℠" class=""id="text_font_nav">Products</a>
                                        <ul class="primary-nav__mega-menu">
                                            <li class="primary-nav__mega-menu-wrapper">
                                                <section>
                                                    <div class="primary-nav__primary-list">
                                                        <div class="primary-nav__col-group">
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">IMDTEC Certification</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Agile">For Personnel</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">For Businesses</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Design">Talent Measurement</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Marketing & Sales">Leadership Assessments</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Operation">Specialist Role Assessment</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Risk">Graduate Assessment</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">IMDTEC Assistant</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/digital_transformation.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">
                                                                            Ready to help, wherever you
                                                                            are.
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">IMDTEC Pay</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/digital_transformation.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">
                                                                            Ready to help, wherever you
                                                                            are.
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Others</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Agile">Insights</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">IMDTEC Entertainment</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Design">IMDTEC Sports</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Marketing & Sales">IMDTEC Startup</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Operation">IMDTEC Kids</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Risk">Live well</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Risk">IMDTEC Express</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Risk">IMDTEC Marketplace</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Risk">View All Products</a>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </li>
                                        </ul>
                                    </li>
                                    <li >
                                        <a href="" aria-label="Products℠" class=""id="text_font_nav">Learning</a>
                                        <ul class="primary-nav__mega-menu">
                                            <li class="primary-nav__mega-menu-wrapper">
                                                <section>
                                                    <div class="primary-nav__primary-list">
                                                        <div class="primary-nav__col-group">
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">University</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/university_slider.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">
                                                                            Find diploma suit your interests
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Education</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/digital_transformation.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">
                                                                            Empowering every student to achieve more
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Business Simulations</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/digital_transformation.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">
                                                                            Business Simulations are essential in improving performance
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Learning</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Agile">For Businesses</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">For Competencies</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Design">For Higher Education</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Marketing & Sales">Browse Training</a>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </li>
                                        </ul>
                                    </li>

                                    <li >
                                        <a href="" aria-label="Products℠" class=""id="text_font_nav">Digital Technology</a>
                                        <ul class="primary-nav__mega-menu">
                                            <li class="primary-nav__mega-menu-wrapper">
                                                <section>
                                                    <div class="primary-nav__primary-list">
                                                        <div class="primary-nav__col-group">
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Developers</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/university_slider.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">
                                                                            Build anything with IMDTEC
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Artificial Indigence</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/digital_transformation.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">
                                                                            Discover how IMDTEC AI helps transformation business.
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">IMTEC Glaxy</a>
                                                                    </li>
                                                                    <li >
                                                                        <img src="https://imdtec.org/fr-fr/Images/digital_transformation.jpg">
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">


                                                                            Innovative technology services to transform your business
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="primary-nav__col">
                                                                <ul class="primary-nav__level-two" >
                                                                    <li style="margin-bottom: 10px" >
                                                                        <a style="font-weight: bold" href="#" arial-label="Agile">Digital Solution</a>
                                                                    </li>

                                                                    <li >
                                                                        <a href="#" arial-label="Agile">Digital Service</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Analytics">Digital Transformation</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Design">Interactive</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Marketing & Sales">Applied Intelligence</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Marketing & Sales">Security</a>
                                                                    </li>
                                                                    <li >
                                                                        <a href="#" arial-label="Marketing & Sales">Technology</a>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#" aria-label="Store" class=""id="text_font_nav">Store</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
			
						
						
						
						
						
						
						
						
						
						
	

<style>
@media only screen and (max-width: 10992px) 
{
#fa_search 
{
color:#ffffff;
}
#fa_search_n 
{
color:#000000;
}
}

@media only screen and (max-width: 1992px) 
{
#fa_search 
{
color:#ffffff;
}
#fa_search_n 
{
color:#000000;
}
}

@media only screen and (max-width:1377px) 
{
#fa_search 
{
color:#ffffff;
}
#fa_search_n 
{
color:#000000;
}
}



@media only screen and (max-width:1130px) 
{
#fa_search 
{
color:#ffffff;
}
#fa_search_n 
{
color:#000000;
}
}



@media only screen and (max-width:1080px) 
{
#fa_search 
{
color:transparent;
}
#fa_search_n 
{
color:transparent;
}
}


@media only screen and (max-width:830px) 
{
#fa_search 
{
color:transparent;
}
#fa_search_n 
{
color:transparent;
}
}

@media only screen and (max-width:767px) 
{
#fa_search 
{
color:transparent;
}
#fa_search_n 
{
color:transparent;
}
}
</style>					
						
						
						
						
						
						
						
<div class="primary-nav__search" id="primary-nav__search">
<div  data-delay-toggle data-control-elements="[data-site-search-control]" data-class-toggle="is-expanded" data-delay-time="0" class="site-search nav-site-search">


<div class="primary-nav__logo" style="float:right;padding:5px 5px 5px 5px;height:60px;margin-left:0px;">
<a style="margin-top:13px;" class="site-search__open search-menu" data-site-search-control data-input-focus-trigger data-input-fields="#nav-site-search-input" data-delay-time="500">
<i id="fa_search_n" class="fa fa-search"></i>
</a>
</div>
            
								
<div class="primary-nav__logo primary-nav__logo--white" style="float:right;padding:5px 5px 5px 5px;height:60px;margin-left:0px;">
<a style="margin-top:13px;" class="site-search__open search-menu" data-site-search-control data-input-focus-trigger data-input-fields="#nav-site-search-input" data-delay-time="500">
<i id="fa_search" style="" class="fa fa-search"></i>
</a>
</div>










<style>
.dropdown-menu::-webkit-scrollbar{width:5px;height:10px}
.dropdown-menu::-webkit-scrollbar-thumb{width:5px;height:10px;background-color:#c0c0c0;;background-color:#c0c0c0;border-radius:300px;box-shadow:inset 2px 2px 2px hsla(0,0%,100%,.25),inset -2px -2px 2px rgba(0,0,0,.25)}
</style>


<style>
#bar_right_bord{border:1px solid #c0c0c0;height:auto;text-align:center;}
#bar_text{text-align:center;margin-top:-3px;font-size:15px;font-weight:normal;font-weight:400;width:100%;}
#bar_link{text-align:left;text-decoration:none;margin-top:0px;width:100%;font-size:17px;background-color:#ffffff;color:#000000;}
#bar_link:hover{text-align:left;margin-top:0px;width:100%;font-size:17px;background-color:#cdcfd2;color:#000000;}

#bar_link_view{padding:20px 3px 20px 20px ;text-align:left;text-decoration:none;margin-top:0px;width:100%;font-size:17px;background-color:#ffffff;color:#000000;}
#bar_link_view:hover{text-align:left;margin-top:0px;width:100%;font-size:17px;background-color:#cdcfd2;color:#000000;}

#bar_link_b{text-align:left;text-decoration:none;margin-top:0px;width:100%;font-size:18px;color:#000000;font-weight:blod;font-weight:700;border:1px solid #ffffff;}
#bar_link_sous{text-align:left;text-decoration:none;margin-top:0px;width:100%;font-size:14px;color:#000000;}
</style>

<style>
#bar_img{min-width:45px;max-width:55px;min-height:40px;max-height:45px;margin-left:15px;}
</style>

	<style type="text/css">


        #list1, #list2 {
            width: 100%;
            list-style-type: none;
            margin: 0px;
        }

        #list1 li, #list2 li {
            float: left;
            padding: 0px;
            width: 33%;
            height: 100px;
        }

        #list1 div, #list2 div {
            width: 100%;
            height: 100px;
            border: solid 1px #ffffff;
            background-color: #ffffff;
            text-align: center;
            padding-top: 0px;
			cursor: pointer;
        }

        #list2 {
            float: right;
        }

        .placeHolder div {
            background-color: white !important;
            border: 1px solid #ffffff;
						cursor: pointer;

        }
	</style>
	
	
	
	
	
	
	
								
<div class="primary-nav__logo dropdown" style="float:right;padding:15px 5px 5px 5px;height:60px;">
<button class="site-search__open square-menu dropdown-toggle " style="position:relative;float:left;padding:5px 25px 5px 15px;cursor: pointer;" data-toggle="dropdown">
<img class="txtlogo" style="width:20px;margin-top:0px;"src="https://imdtec.org/fr-fr/Images/apps.png"/>
</button>
<div style="margin-top:10px;height:420px;width:325px;margin-left:-240px;overflow-x: hidden;" class="dropdown-menu">



<div class="" style="margin-top:0px;padding:0px;height:500px;">



<label class="col-md-12"style="margin-top:20px;background-color:#ffffff;text-align:left;color:#000000;font-size:17px;padding:15px 0px 15px 15px;">

<div class="">





<ul id="list1" class="row col-md-12">

<li class="col-md-4">
<div onclick="location.href='https://account.imdtec.org/signin/index.php?webcreateaccount=mail&continue=account.imdtec.org/&gmb=exp&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp&hl=fr&url_log=https://imdtec.org/fr-fr/&sub_log=https://imdtec.org';"id="bar_right_bord" style="">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/account2.png">
<label id="bar_text">Account</label>
</div>
</li>



<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img" style="margin-top:-2px;"src="https://imdtec.org/fr-fr/Images/azuregram.png">
<label id="bar_text" style="margin-top:-8px;">Azuregram</label>
</div>
</li>


<li class="col-md-4">
<div onclick="location.href='https://learning.imdtec.org/en-eg/signin';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/Google-Mail-logo.png">
<label id="bar_text" style="margin-top:-9px;">Livewell</label>
</div>
</li>


<li class="col-md-4">
<div onclick="location.href='https://learning.imdtec.org/en-eg/signin';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/tv2.png">
<label id="bar_text" style="margin-top:2px;">Entertainment</label>
</div>
</li>


<li class="col-md-4">
<div onclick="location.href='https://learning.imdtec.org/en-eg/signin';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/marketplace-3.png">
<label id="bar_text" style="margin-top:10px;">Marketplace</label>
</div>
</li>


<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img"src="https://imdtec.org/fr-fr/Images/bar_incons8.png">
<label id="bar_text">Kids</label>
</div>
</li>



<li class="col-md-4">
<div onclick="location.href='https://learning.imdtec.org/en-eg/signin';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/bar_incons1.png">
<label id="bar_text" style="margin-top:-3px;">Training</label>
</div>
</li>




<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/CERTIFICATION.gif">
<label id="bar_text" style="margin-top:6px;">Certification</label>
</div>
</li>




<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/CERTIFICATION.gif">
<label id="bar_text" style="margin-top:6px;">Assistant</label>
</div>
</li>




	
	
<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img"src="https://imdtec.org/fr-fr/Images/bar_incons4.png">
<label id="bar_text">Galaxy </label>
</div>
</li>

<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/pyxis.png">
<label id="bar_text" style="margin-top:-5px;">Pyxis </label>
</div>
</li>

<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img"src="https://imdtec.org/fr-fr/Images/bar_incons6.png">
<label id="bar_text">Developer</label>
</div>
</li>






<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/bar_incons1.png">
<label id="bar_text">Sports</label>
</div>
</li>





<li class="col-md-4">
<div onclick="location.href='https://learning.imdtec.org/en-eg/signin';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/experess.png">
<label id="bar_text">Experess</label>
</div>
</li>




<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/news.png">
<label id="bar_text" style="margin-top:8px;">News</label>
</div>
</li>






<li class="col-md-12" style="width:100%;height:10px;"></li>




<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img"src="https://imdtec.org/fr-fr/Images/bar_incons8.png">
<label id="bar_text">Kids</label>
</div>
</li>




<li class="col-md-4">
<div onclick="location.href='https://learning.imdtec.org/en-eg/signin';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/bar_incons1.png">
<label id="bar_text">Learning</label>
</div>
</li>




<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/CERTIFICATION.gif">
<label id="bar_text" style="margin-top:6px;">Certification</label>
</div>
</li>



<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img" src="https://imdtec.org/fr-fr/Images/bar_incons8.png">
<label id="bar_text">Kids</label>
</div>
</li>




<li class="col-md-4">
<div onclick="location.href='https://learning.imdtec.org/en-eg/signin';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/bar_incons1.png">
<label id="bar_text">Learning</label>
</div>
</li>




<li class="col-md-4">
<div onclick="location.href='';"id="bar_right_bord">
<img id="bar_img" style=""src="https://imdtec.org/fr-fr/Images/CERTIFICATION.gif">
<label id="bar_text" style="margin-top:6px;">Certification</label>
</div>
</li>



</ul>
<style>
#menu_right_button{font-size:15px;background-color:#ffffff;color:#17488a;height:45px;border:1px solid #cecece;font-weight:normal;font-weight:400;padding:5px 18px 5px 18px;}
#menu_right_button:hover{background-color:#f9f9f9;color:#17488a;height:45px;border:1px solid #cecece;font-weight:normal;font-weight:400;padding:5px 18px 5px 18px;}
</style>
<div class="col-md-12" style="text-align:center;margin-top:15px;">
<button id="menu_right_button">More from IMDTEC</button>
</div>


</div>





</label>



</div>

</div>




<button onclick="location.href = 'https://account.imdtec.org/signin/index.php?webcreateaccount=mail&continue=account.imdtec.org/&gmb=exp&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp&hl=en&url_log=https://imdtec.org/fr-fr/&sub_log=https://imdtec.org';" class="site-search__open profile-menu" style="position:relative;float:left;padding:0px;cursor: pointer;"href="index.html">
<img class="txtlogo" style="width:28px;min-height:30px;margin-top:0px;" src="https://imdtec.org/fr-fr/Images/login-incon.png"/>
</button>
</div>

























<div class="primary-nav__logo primary-nav__logo--white" style="float:right;padding:15px 5px 5px 5px;height:60px;">
<button class="site-search__open square-menu" style="position:relative;float:left;padding:5px 25px 5px 15px;"href="index.html">
<img class="txtlogo" style="width:20px;margin-top:0px;"src="https://imdtec.org/fr-fr/Images/apps_bl.png"/>
</button>


<button onclick="location.href = 'https://account.imdtec.org/signin/index.php?webcreateaccount=mail&continue=account.imdtec.org/&gmb=exp&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp&hl=en&url_log=https://imdtec.org/fr-fr/&sub_log=https://imdtec.org';"class="site-search__open profile-menu" style="position:relative;float:left;padding:0px;"href="index.html">
<img class="txtlogo" style="width:28px;min-height:30px;margin-top:0px;" src="https://imdtec.org/fr-fr/Images/login-incon_b.png"/>
</button>
</div>

                                <div class="site-search__form-wrap">
                                    <form class="site-search__search-form" name="site-search" data-cookie-form data-cookie-name="previousSearches" data-cookie-element="#nav-site-search-input" data-cookie-max-length="5" action="#">
                                        <button class="site-search__search-submit">
                                            <span class="fa fa-search"></span>
                                        </button>
                                        <input id="nav-site-search-input" data-autocomplete data-autocomplete-container="#nav-site-search-autocomplete"
                                               data-search-results-container="#nav-site-search-results" data-char-threshold="3" autocomplete="off" data-enable-keyboard="true"
                                               class="site-search__search-input" name="searchValue" placeholder="Rechercher un point de vue, un service ou un expert" />
                                        <div class="site-search__suggestions-block">
                                            <section class="site-search__autocomplete-section">
                                                <ul id="nav-site-search-autocomplete" class="site-search__suggestion-list"></ul>
                                            </section>
                                            <section id="nav-site-search-results" class="site-search__search-results-section"></section>
                                            <section class="site-search__suggestions-section">
                                                <h6 class="site-search__suggestion-title">Recherches les plus fr&#233;quentes</h6>

                                                <ul class="site-search__suggestion-list">
                                                    <li class="site-search__suggestion-item">
                                                        <a data-input-element="#nav-site-search-input" data-form-submit-value="Agile">Agile</a>
                                                    </li>
                                                    <li class="site-search__suggestion-item">
                                                        <a data-input-element="#nav-site-search-input" data-form-submit-value="Digital">Digital</a>
                                                    </li>
                                                    <li class="site-search__suggestion-item">
                                                        <a data-input-element="#nav-site-search-input" data-form-submit-value="Strat&#233;gie">Strat&#233;gie</a>
                                                    </li>
                                                </ul>

                                            </section>
                                            <section class="site-search__suggestions-section">
                                                <h6 class="site-search__suggestion-title site-search__previous-suggestions">Vos recherches pr&#233;c&#233;dentes</h6>
                                                <ul class="site-search__suggestion-list" data-render-from-cookie="search-suggestion" data-cookie-name="previousSearches" data-input-element="#nav-site-search-input"></ul>
                                            </section>
                                            <section class="site-search__history-block">
                                                <h6 class="site-search__suggestion-title">Pages r&#233;cemment visit&#233;es</h6>
                                                <div class="site-search__history-items" data-render-from-cookie="history-item" data-cookie-name="PageHistory" data-fallback-image=""></div>
                                            </section>
                                        </div>
                                    </form>
                                    <a class="site-search__close" data-site-search-control>
                                        <span  class="fa fa-close"></span>
                                    </a>
                                </div>

                            </div>

                            <!--<div  class="pro-app-container" >-->

                                <!--<a class="site-search__open square-menu" >-->
                                    <!--&lt;!&ndash;<span class="site-search__search-text" >Explorer</span>&ndash;&gt;-->
                                    <!--&lt;!&ndash;<span class="icon-search"></span>&ndash;&gt;-->
                                    <!--&lt;!&ndash;<i class="fa fa-th-large"></i>&ndash;&gt;-->
                                    <!--<img class="app-black" style="width:20px" src="https://imdtec.org/fr-fr/Images/apps.png">-->
                                    <!--<img class="app-white" style="width:20px" src="https://imdtec.org/fr-fr/Images/apps_bl.png">-->

                                <!--</a>-->

                                <!--<a class="site-search__open profile-menu">-->
                                    <!--&lt;!&ndash;<span class="site-search__search-text" >Explorer</span>&ndash;&gt;-->
                                    <!--&lt;!&ndash;<span class="icon-search"></span>&ndash;&gt;-->
                                    <!--&lt;!&ndash;<i  class="fa fa-us er-circle"></i>&ndash;&gt;-->
                                    <!--<img class="login-black" style="width:28px" src="https://imdtec.org/fr-fr/Images/login-incon.png">-->
                                    <!--<img class="login-white" style="width:28px" src="https://imdtec.org/fr-fr/Images/login-incon_b.png">-->
                                <!--</a>-->

                            <!--</div> -->


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            //                    (function () {

            //                        //click on hamburger icon -> open the off-canvas, add the overlay
            //                          document.getElementById('js-hamburger-btn').addEventListener('click', function () {
            //                            document.getElementById('off-canvas-menu').classList.add('off-canvas-menu--expanded');
            //                            document.getElementById('overlay_697').classList.add('primary-nav__overlay--visible');
            //                        });
            //
            //                        document.getElementById('js-hamburger-btn-bars').addEventListener('click', function () {
            //                            document.getElementById('off-canvas-menu-bars').classList.add('off-canvas-menu--expanded');
            //                            document.getElementById('overlay_697').classList.add('primary-nav__overlay--visible');
            //                        });
            //
            //                        //click on close icon (off-canvas) -> close the off-canvas, the overlay closes from toggler
            //
            //                        document.querySelectorAll('.off-canvas-menu__close > a')[0].addEventListener('click', function () {
            //                            document.getElementById('off-canvas-menu').classList.remove('off-canvas-menu--expanded');
            //                            document.getElementById('overlay_697').classList.remove('primary-nav__overlay--visible');
            //                        });
            //
            //                        //click on overlay(outside of off-canvas) -> close the off-canvas and the overlay
            //
            //                        document.getElementById('overlay_697').addEventListener('click', function () {
            //                        document.getElementById('off-canvas-menu').classList.remove('off-canvas-menu--expanded');
            //                        document.getElementById('overlay_697').classList.remove('primary-nav__overlay--visible');

            //                        });
            //                    })();
        </script>
    </div>
</header>

<style>
@media only screen and (max-width: 10992px) 
{
#slider 
{
min-height:900px;
}
}

@media only screen and (max-width: 1992px) 
{
#slider 
{
min-height:900px;
}
}

@media only screen and (max-width:1377px) 
{
#slider 
{
min-height:800px;
}
}



@media only screen and (max-width:1130px) 
{
#slider 
{
min-height:600px;
}
}



@media only screen and (max-width:992px) 
{
#slider 
{
min-height:600px;
}
}


@media only screen and (max-width:830px) 
{
#slider 
{
min-height:600px;
}
}

@media only screen and (max-width:767px) 
{
#slider 
{
min-height:600px;
}
}
</style>













<style>
@media only screen and (max-width: 10992px) 
{
#text_res 
{
margin-top:0px;
}
}

@media only screen and (max-width: 1992px) 
{
#text_res 
{
margin-top:0px;

}
}

@media only screen and (max-width:1377px) 
{
#text_res 
{
margin-top:0px;
}
}



@media only screen and (max-width:1130px) 
{
#text_res 
{
margin-top:70px;
}
}



@media only screen and (max-width:992px) 
{
#text_res 
{
margin-top:70px;
}
}


@media only screen and (max-width:830px) 
{
#text_res 
{
margin-top:70px;
}
}

@media only screen and (max-width:767px) 
{
#text_res 
{
margin-top:70px;
}
}
</style>






<header class="hero hero--homepage hero--homepage-transparent" id="" style="">
    <div class="hero__carousel" data-homepage-carousel >
       <div class="hero__slide row column">	

            <a href="#" class="hero__full-width-link" aria-label="En savoir plus"></a>
            <a href="#" aria-label="En savoir plus" class="hero__text-content large-5 column">
                <p class="hero__tag" data-slide-nav-title="Internet Of Things" id="text_res">Internet Of Things</p>
                <h2 class="showDesktopTitle hero__title" >The truth about transformation people expect a lot</h2>
                <h2 class="showMobileTitle hero__title">The truth about transformation people expect a lot</h2>
                <span class="hero__cta ">Learn More
                    <!--<span class="fa fa-long-arrow-right"></span>-->
                        </span>
            </a>
            <div class="hero__image center">
                <div class="hero__overlay"></div>
                <picture>
                    <source srcset="https://imdtec.org/fr-fr/Images/s5.jpg?width=1920&amp;height=1080&amp;mode=crop" media="(min-width:768px)">
                    <source srcset="https://imdtec.org/fr-fr/Images/s5.jpg?width=1080&amp;height=1080&amp;mode=crop" media="(min-width:0px)">
                    <img src="https://imdtec.org/fr-fr/Images/s5.jpg" data-object-fit />
                </picture>
            </div>
        </div>
        <div class="hero__slide row column">
            <a href="#" class="hero__full-width-link" aria-label="En savoir plus"></a>
            <a href="#" aria-label="En savoir plus" class="hero__text-content large-5 column">
                <p class="hero__tag" data-slide-nav-title="Small & Midsize businesse" id="text_res">Small & Midsize businesses</p>
                <h2 class="showDesktopTitle hero__title">Intelligence for all business sizes transform your business</h2>
                <h2 class="showMobileTitle hero__title">Intelligence for all business sizes transform your business</h2>
                <span class="hero__cta ">Learn More
                    <!--<span class="fa fa-long-arrow-right"></span>-->
                        </span>
            </a>
            <div class="hero__image center">
                <div class="hero__overlay"></div>
                <picture>
                    <source srcset="https://imdtec.org/fr-fr/Images/s6.jpg?width=1920&amp;height=1080&amp;mode=crop" media="(min-width:768px)">
                    <source srcset="https://imdtec.org/fr-fr/Images/s6.jpg?width=1080&amp;height=1080&amp;mode=crop" media="(min-width:0px)">
                    <img src="https://imdtec.org/fr-fr/Images/s6.jpg" data-object-fit />
                </picture>
            </div>
        </div>
        <div class="hero__slide row column">
            <a href="#" class="hero__full-width-link" aria-label="Sustainable is the next Digital"></a>
            <a href="#" aria-label="Sustainable is the next Digital" class="hero__text-content large-5 column">
                <p class="hero__tag" data-slide-nav-title="Pyxis" id="text_res">Pyxis</p>
                <h2 class="showDesktopTitle hero__title">Discover the world's most intelligent application pyxis</h2>
                <h2 class="showMobileTitle hero__title">Discover the world's most intelligent application pyxis</h2>
                <span class="hero__cta ">Learn More
                    <!--<span class="fa fa-long-arrow-right"></span>-->
                        </span>
            </a>
            <div class="hero__image center">
                <div class="hero__overlay"></div>
                <picture>
                    <source srcset="https://imdtec.org/fr-fr/Images/s4.jpg?width=1920&amp;height=1080&amp;mode=crop" media="(min-width:768px)">
                    <source srcset="https://imdtec.org/fr-fr/Images/s4.jpg?width=1080&amp;height=1080&amp;mode=crop" media="(min-width:0px)">
                    <img src="https://imdtec.org/fr-fr/Images/s4.jpg" data-object-fit />
                </picture>
            </div>
        </div>
        <div class="hero__slide row column">
            <a href="#" class="hero__full-width-link" aria-label="En savoir plus"></a>
            <a href="#" aria-label="En savoir plus" class="hero__text-content large-5 column">
                <p class="hero__tag" data-slide-nav-title="Competence" id="text_res">Competence</p>
                <h2 class="showDesktopTitle hero__title">Empowering Talent Transformation Embrace the talent revolution</h2>
                <h2 class="showMobileTitle hero__title">Empowering Talent Transformation Embrace the talent revolution</h2>
                <span class="hero__cta ">Learn More
                    <!--<span class="fa fa-long-arrow-right"></span>-->
                        </span>
            </a>
            <div class="hero__image center">
                <div class="hero__overlay"></div>
                <picture>
                    <source srcset="https://imdtec.org/fr-fr/Images/s2.jpg?width=1920&amp;height=1080&amp;mode=crop" media="(min-width:768px)">
                    <source srcset="https://imdtec.org/fr-fr/Images/s2.jpg?width=1080&amp;height=1080&amp;mode=crop" media="(min-width:0px)">
                    <img src="https://imdtec.org/fr-fr/Images/s2.jpg" data-object-fit />
                </picture>
            </div>
        </div>
    </div>
    <div class="hero__nav-container row column">
        <div class="column" data-carousel-nav></div>
    </div>
    <!--<div class="hero__scroll-indicator">-->
    <!--<span class="hero__scroll-text">Faire d&#233;filer</span>-->
    <!--<span class="icon-chevron-down" id="scroll-to-finder"></span>-->
    <!--</div>-->
</header>
<style>
.sidepanel  {
  width: 0;
  position: fixed;
  z-index: 1;
  height: 100%;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 0px;
}

.sidepanel a {
  padding: 8px 8px 8px 8px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidepanel a:hover {
  color: #f1f1f1;
}

.sidepanel .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
}

.openbtn {
  font-size: 20px;
  cursor: pointer;
  background-color: #111;
  color: white;
  padding: 10px 15px;
  border: none;
}

.openbtn:hover {
  background-color:#444;
}
</style>

<div id="mySidepanel" class="sidepanel" style="z-index:999;position:fixed;margin-top:0px;z-index:999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999;background-color:#ffffff;-webkit-box-shadow: 10px 0px 19px -2px rgba(0,0,0,0.75);
-moz-box-shadow: 10px 0px 19px -2px rgba(0,0,0,0.75);
box-shadow: 10px 0px 19px -2px rgba(0,0,0,0.75);">

<div style="margin-top:0px;padding:0px;height:100%;">



<label class="col-md-12"style="width:100%;margin-top:0px;background-color:#17488a;text-align:left;color:#ffffff;font-size:20px;font-weight:blod;font-weight:600;border:1px solid #17488a;padding:15px 0px 15px 30px;">
<div class="primary-nav__logo primary-nav__logo--white">
<a href="index.html">
<div >
<div class="imgContainer">
<img class="txtlogo"  src="https://imdtec.org/fr-fr/Images/logo2.png"/>
</div>
<div class="imgContainer">
<img class="logoself" src="https://imdtec.org/fr-fr/Images/imdec.png"/>
</div>
</div>
</a>
</div>
<a href="javascript:void(0)" style="float:right;color:#ff0000;margin-top:12px;margin-right:0px;"class="closebtn" onclick="closeNav()">
<img  src="https://imdtec.org/fr-fr/Images/close_blanche.png" style="position:absolute;min-width:21px;height:21px;margin-top:2px;margin-left:0px;"class="">
</a>
</label>



<style>
.nav_left_s{width:100%;margin-top:0px;background-color:#ffffff;border:1px solid #ffffff;color:#000000;text-align:left;padding:10px;border-radius:0px;font-weight:normal;font-weight:500;font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;border-bottom:1px solid #ececec;}

.nav_left_s:hover{margin-top:0px;background-color:#f1f1f1;border:1px solid #ffffff;color:#000000;text-align:left;}
.nav_left_s:active{margin-top:0px;background-color:#f1f1f1;border:1px solid #ffffff;color:#000000;text-align:left;}
.nav_left_s:focus{margin-top:0px;background-color:#f1f1f1;border:1px solid #ffffff;color:#000000;text-align:left;}

.nav_left_s_ul{position:relative;width:100%;margin-top:0px;background-color:#ffffff;border:1px solid #ffffff;color:#000000;text-align:left;padding:0px;border-radius:0px;font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;}
.nav_left_s_ul_a_title{margin-top:0px;width:100%;background-color:#ffffff;border:1px solid #ffffff;color:#000000;text-align:left;padding:10px;border-radius:0px;font-size:17px;font-weight:blod;font-weight:600;font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;}
.nav_left_s_ul_a{margin-top:0px;width:100%;background-color:#ffffff;border:1px solid #ffffff;color:#000000;text-align:left;padding:10px;border-radius:0px;font-size:15px;font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif;font-weight:normal;font-weight:400;}

</style>

<div style="padding:0px;margin-top:-4px;width:100%;">
<label class="col-md-12"style="width:100%;margin-top:-10px;background-color:#ffffff;text-align:left;color:#000000;font-size:17px;border:1px solid transparent;padding:0px 0px 0px 0px;">
<div class="col-md-12" style="padding:0px;">



<div class="dropdown">
<button class="nav_left_s btn dropdown-toggle" id="text_font2" type="button">Azuregram</button>
</div>

<div class="dropdown" style="width:100%;">
  <button style="background-color:#c0c0c0;"class="nav_left_s btn dropdown-toggle" id="text_font2"type="button" data-toggle="dropdown">Businesses
  <span style="float:right;"class="caret fa_menu_mobile"></span></button>
<ul class="dropdown-menu nav_left_s_ul">
<button class="nav_left_s_ul_a_title">Business Functions</button>
<button class="nav_left_s_ul_a" href="#">Audit</button>
<button class="nav_left_s_ul_a" href="#">Analytics</button>
<button class="nav_left_s_ul_a" href="#">Design</button>
<button class="nav_left_s_ul_a" href="#">Marketing & Sales</button>
<button class="nav_left_s_ul_a" href="#">Operation</button>
<button class="nav_left_s_ul_a" href="#">Risk</button>
<button class="nav_left_s_ul_a" href="#">Strategy</button>
<button class="nav_left_s_ul_a" href="#">Restructuring</button>

<button class="nav_left_s_ul_a_title">Inteligent Enterprise</button>

<button class="nav_left_s_ul_a_title">Pyxis</button>



<button class="nav_left_s_ul_a_title">Industry</button>
<button class="nav_left_s_ul_a" href="#">Finance Service</button>
<button class="nav_left_s_ul_a" href="#">Government</button>
<button class="nav_left_s_ul_a" href="#">Health</button>
<button class="nav_left_s_ul_a" href="#">Manufacturing & Sales</button>
<button class="nav_left_s_ul_a" href="#">Retail</button>
<button class="nav_left_s_ul_a" href="#">Transportation</button>
 </ul>
</div>





<div class="dropdown">
<button class="nav_left_s btn dropdown-toggle" id="text_font2" type="button" data-toggle="dropdown">Products
<span class="caret fa_menu_mobile "></span></button>
<ul class="dropdown-menu nav_left_s_ul">
<button class="nav_left_s_ul_a_title">IMDTEC Certification</button>
<button class="nav_left_s_ul_a" href="#">For Personnel</button>
<button class="nav_left_s_ul_a" href="#">For Businesses</button>
<button class="nav_left_s_ul_a" href="#">Talent Measurement</button>
<button class="nav_left_s_ul_a" href="#">Leadership Assessments</button>
<button class="nav_left_s_ul_a" href="#">Specialist Role Assessment</button>
<button class="nav_left_s_ul_a" href="#">Graduate Assessment</button>

<button class="nav_left_s_ul_a_title">IMDTEC Assistant</button>
<button class="nav_left_s_ul_a_title">IMDTEC Pay</button>



<button class="nav_left_s_ul_a_title">Others</button>
<button class="nav_left_s_ul_a" href="#">Insights</button>
<button class="nav_left_s_ul_a" href="#">IMDTEC Entertainment</button>
<button class="nav_left_s_ul_a" href="#">IMDTEC Sports</button>
<button class="nav_left_s_ul_a" href="#">IMDTEC Startup</button>
<button class="nav_left_s_ul_a" href="#">IMDTEC Kids</button>
<button class="nav_left_s_ul_a" href="#">Live well</button>
<button class="nav_left_s_ul_a" href="#">IMDTEC Express</button>
<button class="nav_left_s_ul_a" href="#">IMDTEC Marketplace</button>
<button class="nav_left_s_ul_a" href="#">View All Products</button>
</ul>
</div>







<div class="dropdown">
<button class="nav_left_s btn dropdown-toggle" id="text_font2" type="button" data-toggle="dropdown">Learning
<span class="caret fa_menu_mobile "></span></button>
<ul class="dropdown-menu nav_left_s_ul">

<button class="nav_left_s_ul_a_title">University</button>
<button class="nav_left_s_ul_a_title">Education</button>
<button class="nav_left_s_ul_a_title">Business Simulations</button>



<button class="nav_left_s_ul_a_title">Learning</button>
<button class="nav_left_s_ul_a" href="#">For Businesses</button>
<button class="nav_left_s_ul_a" href="#">For Competencies</button>
<button class="nav_left_s_ul_a" href="#">For Higher Education</button>
<button class="nav_left_s_ul_a" href="#">Browse Training</button>
</ul>
</div>





<div class="dropdown">
<button class="nav_left_s btn dropdown-toggle" id="text_font2" type="button" data-toggle="dropdown">Digital Technology
<span class="caret fa_menu_mobile "></span></button>
<ul class="dropdown-menu nav_left_s_ul">

<button class="nav_left_s_ul_a_title">Developers</button>
<button class="nav_left_s_ul_a_title">Artificial Indigence</button>
<button class="nav_left_s_ul_a_title">IMTEC Glaxy</button>




<button class="nav_left_s_ul_a_title">Digital Solution</button>
<button class="nav_left_s_ul_a" href="#">Digital Service</button>
<button class="nav_left_s_ul_a" href="#">Digital Transformation</button>
<button class="nav_left_s_ul_a" href="#">Interactive</button>
<button class="nav_left_s_ul_a" href="#">Applied Intelligence</button>
<button class="nav_left_s_ul_a" href="#">Security</button>
<button class="nav_left_s_ul_a" href="#">Technology</button>
</ul>
</div>


<div class="dropdown">
<button class="nav_left_s btn dropdown-toggle" id="text_font2" type="button" data-toggle="dropdown">Store</button>
</div>


</div>


















</label>

</div>






</div>
</div><div class="body_page" style="">
<div class="body_page_contenu" style="">

<div class="col-sm-12" style="margin-top:0px;">


<style>
@media only screen and (max-width: 10992px) 
{
.featured_content_row 
{
margin-left:0px;
}
}

@media only screen and (max-width: 1992px) 
{
.featured_content_row 
{
margin-left:0px;
}
}

@media only screen and (max-width:1377px) 
{
.featured_content_row 
{
margin-left:0px;
}
}



@media only screen and (max-width:1130px) 
{
.featured_content_row 
{
margin-left:0px;
}
}



@media only screen and (max-width:1090px) 
{
.featured_content_row 
{
margin-left:0px;
}
}


@media only screen and (max-width:830px) 
{
.featured_content_row 
{
margin-left:-14px;
}
}

@media only screen and (max-width:767px) 
{
.featured_content_row
{
margin-left:-14px;
}
}
</style>
























































<div class="col-md-12" style="padding:0px;margin-top:27px;">






<div id=""class="col-md-12"style="border-bottom:1px solid #ffffff;background-color:#FFFFFF;text-align:left;">


<div id="text_title"class="col-md-12"style="line-height:1.2;color:#000000;padding:0px 0px 10px 6px;text-align:left;margin-left:2px;">
<label id="text_font2" style="font-weight:blod;font-weight:600;text-align:left;text-decoration:none;font-size:32px;color:#000000;padding:0px 0px 0px 0px;">
Featured content
</label>
</div>


<div id="width_mobile"class="col-md-6 row"style="padding:0px;text-align:left;">

<div id="media_style2"class="col-sm-6"style="padding:10px;text-align:left;">
<a href="https://certification.imdtec.org/en-eg/dashboard/">
<img src="https://imdtec.org/fr-fr/Images/img1.png" class="image"style="width:100%;height:173px;" />

<div style="padding:30px 0px 0px 0px;border-bottom:0px solid #ffffff;">
<a href="https://imdtec.org/en-eg/insights/"id="text_font_noir" style="font-size:21px;text-decoration:none;">
Medical Brain 
</a>
</div>

<div style="padding:5px 0px 10px 0px;">
<p class="banner-subheadline headline-subheadline-26" id="text_font_noir" style="font-size:15px;line-height:19px;color:#000000;font-weight:normal;font-weight:400;">
IMDTEC Live well is committed to apply data intelligence to help doctors and nurses offer better healthcare services to patients and ultimately save more lives.
</p>
</div>
<div class="" style="text-align:left;">
<a id="btn11"href=""style="color:#004290;">Learn more <i style="margin-top:2px;margin-left:14px;position:absolute;font-size:25px;"class="fa fa-angle-right"></i></a>
</div>
</a>
</div>







<div id="media_style2"class="col-sm-6"style="padding:10px;text-align:left;">
<a href="https://certification.imdtec.org/en-eg/dashboard/">
<img src="https://imdtec.org/fr-fr/Images/img2.png" class="image"style="width:100%;height:173px;" />

<div style="padding:30px 0px 0px 0px;border-bottom:0px solid #f1f1f1;">
<a href="https://imdtec.org/en-eg/artificial-intelligence/"id="text_font_noir" style="font-size:21px;text-decoration:none;">
Artificial Intelligence
</a>
</div>

<div style="padding:5px 0px 10px 0px;">
<p class="banner-subheadline headline-subheadline-26" id="text_font_noir" style="font-size:15px;line-height:19px;color:#000000;font-weight:normal;font-weight:400;">
Business functions are driven by your target business outcomes. At IMDTEC we start by understanding your business drivers and objectives.
</p>
</div>
<div class="" style="text-align:left;">
<a id="btn11"href="https://imdtec.org/en-eg/artificial-intelligence/" style="color:#004290;">Learn more <i style="margin-top:2px;margin-left:14px;position:absolute;font-size:25px;"class="fa fa-angle-right"></i></a>
</div>

</a>
</div>

</div>




<div id="width_mobile"class="col-md-6 row"style="padding:0px;text-align:left;">

<div id="media_style2"class="col-sm-6"style="padding:10px;text-align:left;">
<a href="https://certification.imdtec.org/en-eg/">
<img src="https://imdtec.org/fr-fr/Images/img3.png" class="image"style="width:100%;height:173px;" />

<div style="padding:30px 0px 0px 0px;border-bottom:0px solid #f1f1f1;">
<a href="https://pay.imdtec.org/en-eg/"id="text_font_noir" style="font-size:21px;text-decoration:none;">
FINTECH</a>
</div>

<div style="padding:5px 0px 10px 0px;">
<p class="banner-subheadline headline-subheadline-26" id="text_font_noir" style="font-size:15px;line-height:19px;color:#000000;font-weight:normal;font-weight:400;">
The future of payments begins with your big idea Powered by IMDTEC’s global network, your fintech will go the distance. And then some.
</p>
</div>
<div class="" style="text-align:left;">
<a id="btn11"href="https://pay.imdtec.org/en-eg/"style="color:#004290;">Learn more <i style="margin-top:2px;margin-left:14px;position:absolute;font-size:25px;"class="fa fa-angle-right"></i></a>
</div>

</a>
</div>




<div id="media_style2"class="col-sm-6"style="padding:10px;text-align:left;">
<a href="https://certification.imdtec.org/en-eg/">
<img src="https://imdtec.org/fr-fr/Images/img4.png" class="image"style="width:100%;height:173px;" />

<div style="padding:30px 0px 0px 0px;border-bottom:0px solid #f1f1f1;">
<a href="https://learning.imdtec.org/en-eg/"id="text_font_noir" style="font-size:21px;text-decoration:none;">
IMDTEC Learning
</a>
</div>

<div style="padding:5px 0px 10px 0px;">
<p class="banner-subheadline headline-subheadline-26" id="text_font_noir" style="font-size:15px;line-height:19px;color:#000000;font-weight:normal;font-weight:400;">
New world. New skills
Upskilling is more than training. It’s about being equipped to participate and adapt in an increasingly digital world..
</p>
</div>
<div class="" style="text-align:left;">
<a id="btn11"href="https://learning.imdtec.org/en-eg/"style="color:#004290;">Learn more <i style="margin-top:2px;margin-left:14px;position:absolute;font-size:25px;"class="fa fa-angle-right"></i></a>
</div>

</a>
</div>

</div>










</div>  
</div></div>
<div class="col-sm-12 row" style="margin-top:20px;max-width:100%;">
<div class="col-md-12" style="padding:0px;margin-top:0px;">




<div id=""class="col-md-12"style="border-bottom:1px solid #ffffff;background-color:#ffffff;text-align:left;">


<div id="text_title"class="col-md-12"style="line-height:1.2;color:#000000;padding:0px 0px 10px 6px;text-align:left;margin-left:2px;">
<label id="text_font2" style="font-weight:blod;font-weight:600;text-align:left;text-decoration:none;font-size:32px;color:#000000;padding:0px 0px 0px 0px;">
Our top interests
</label>
</div>
</div>
</div>


<div id="" class="col-md-12" style="margin-top:0">
            <div class="" style="">
                <div class="">
                    <div class="">
                        <div class="">
                            <div class="">



<div class="box component section box-dark-background">   
 <div class="component-content" style="padding:0"> 
  <div class="paragraphSystem content">

   <div class="topicLink row col-md-12" style=""> 
    <div class="topicLink-positioner" data-desktop-timer="5000" data-mobile-timer="5000"> 
     <div class="topicLink-background"> 
      <div class="current"></div> 
     </div> 
	 
	 
	 
	 
     <div class="topicLink-slider"> 

        <a href="https://pay.imdtec.org/en-eg/" class="topicLink-slide" data-image="https://imdtec.org/fr-fr/Images/img1.png" data-thumb="https://imdtec.org/fr-fr/Images/img1.png"> 
            <div class="description"> 
                <h3 class="topic-name">IMDTEC PAY</h3> 
                <div class="featured">
                <span class="article-title" id="text_font_noir" style="font-weight:normal;font-weight:400;color:#ffffff;">Analytics are reports that provide insight</span> 
                
                <div class="topic-discover" style="margin-top:10px"> 
                <span class="text" id="text_font_noir" style="font-weight:normal;font-weight:400;color:#ffffff;">Learn more</span> 
                <div class="topicLink-arrow topicLink-arrowRight small">
                
                <div class="topicLink-arrowHitbox"></div> 
                </div> 
                </div> 

                </div> 
                
            </div>
            <script class="topicLink-renditions skip" data-src="https://imdtec.org/fr-fr/Images/img1.png" data-bgset="[https://imdtec.org/fr-fr/Images/img1.png, (min-width: 0px) and (max-width: 599px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img1.png, (min-width: 0px) and (max-width: 599px)],[https://imdtec.org/fr-fr/Images/img1.png, (min-width: 600px) and (max-width: 899px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img1.png, (min-width: 600px) and (max-width: 899px)],[https://imdtec.org/fr-fr/Images/img1.png, (min-width: 900px) and (max-width: 1199px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img1.png, (min-width: 900px) and (max-width: 1199px)],[https://imdtec.org/fr-fr/Images/img1.png, (min-width: 1200px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img1.png, (min-width: 1200px) and (max-width: 1799px)],[https://imdtec.org/fr-fr/Images/img1.png, (min-width: 1800px)]"></script> 
        </a>
        <a href="https://learning.imdtec.org/en-eg/" class="topicLink-slide" data-image="https://imdtec.org/fr-fr/Images/img2.png" data-thumb="https://imdtec.org/fr-fr/Images/img2.png"> 
            <div class="description"> 
                <h3 class="topic-name">Learning</h3> 
                <div class="featured">
                <span class="article-title"id="text_font_noir" style="font-weight:normal;font-weight:400;color:#ffffff;">Learning are reports that provide insight</span> 
                
                <div class="topic-discover" style="margin-top:10px"> 
                <span class="text"id="text_font_noir" style="font-weight:normal;font-weight:400;color:#ffffff;">Learn more</span> 
                <div class="topicLink-arrow topicLink-arrowRight small">
                
                <div class="topicLink-arrowHitbox"></div> 
                </div> 
                </div> 

                </div> 
            </div> 
            <script class="topicLink-renditions skip" data-src="https://imdtec.org/fr-fr/Images/img2.png" data-bgset="[https://imdtec.org/fr-fr/Images/img2.png, (min-width: 0px) and (max-width: 599px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img2.png, (min-width: 0px) and (max-width: 599px)],[https://imdtec.org/fr-fr/Images/img2.png, (min-width: 600px) and (max-width: 899px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img2.png, (min-width: 600px) and (max-width: 899px)],[https://imdtec.org/fr-fr/Images/img2.png, (min-width: 900px) and (max-width: 1199px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img2.png, (min-width: 900px) and (max-width: 1199px)],[https://imdtec.org/fr-fr/Images/img2.png, (min-width: 1200px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img2.png, (min-width: 1200px) and (max-width: 1799px)],[https://imdtec.org/fr-fr/Images/img2.png, (min-width: 1800px)]"></script> 
        </a> 
        <a href="https://www.imdtec.org/en-eg/enterprise/" class="topicLink-slide" data-image="https://imdtec.org/fr-fr/Images/img3.png" data-thumb="https://imdtec.org/fr-fr/Images/img3.png"> 
            <div class="description"> 
            <h3 class="topic-name">Industry</h3> 
                <div class="featured"> 
                
                <span class="article-title"id="text_font_noir" style="font-weight:normal;font-weight:400;color:#ffffff;">Industry are reports that provide insight</span> 
                
                <div class="topic-discover" style="margin-top:10px"> 
                <span class="text"id="text_font_noir" style="font-weight:normal;font-weight:400;color:#ffffff;">Learn more</span> 
                <div class="topicLink-arrow topicLink-arrowRight small">
                
                <div class="topicLink-arrowHitbox"></div> 
                </div> 
                </div> 

                </div> 
            </div>
            <script class="topicLink-renditions skip" data-src="https://imdtec.org/fr-fr/Images/img3.png" data-bgset="[https://imdtec.org/fr-fr/Images/img3.png, (min-width: 0px) and (max-width: 599px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img3.png, (min-width: 0px) and (max-width: 599px)],[https://imdtec.org/fr-fr/Images/img3.png, (min-width: 600px) and (max-width: 899px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img3.png, (min-width: 600px) and (max-width: 899px)],[https://imdtec.org/fr-fr/Images/img3.png, (min-width: 900px) and (max-width: 1199px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img3.png, (min-width: 900px) and (max-width: 1199px)],[https://imdtec.org/fr-fr/Images/img3.png, (min-width: 1200px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img3.png, (min-width: 1200px) and (max-width: 1799px)],[https://imdtec.org/fr-fr/Images/img3.png, (min-width: 1800px)]"></script>
        </a> 
        <a href="https://imdtec.org/en-eg/business-functions/" class="topicLink-slide" data-image="https://imdtec.org/fr-fr/Images/img4.png" data-thumb="https://imdtec.org/fr-fr/Images/img4.png"> 
            <div class="description"> 
                <h3 class="topic-name">Business Fonction</h3> 
                <div class="featured">
                <span class="article-title"id="text_font_noir" style="font-weight:normal;font-weight:400;color:#ffffff;">Business Fonction are reports that provide insight</span> 
                
                <div class="topic-discover" style="margin-top:10px"> 
                <span class="text"id="text_font_noir" style="font-weight:normal;font-weight:400;color:#ffffff;">Learn more</span> 
                <div class="topicLink-arrow topicLink-arrowRight small">
                
                <div class="topicLink-arrowHitbox"></div> 
                </div> 
                </div> 

                </div> 
                 
            </div>
            <script class="topicLink-renditions skip" data-src="https://imdtec.org/fr-fr/Images/img4.png" data-bgset="[https://imdtec.org/fr-fr/Images/img4.png, (min-width: 0px) and (max-width: 599px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img4.png, (min-width: 0px) and (max-width: 599px)],[https://imdtec.org/fr-fr/Images/img4.png, (min-width: 600px) and (max-width: 899px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img4.png, (min-width: 600px) and (max-width: 899px)],[https://imdtec.org/fr-fr/Images/img4.png, (min-width: 900px) and (max-width: 1199px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img4.png, (min-width: 900px) and (max-width: 1199px)],[https://imdtec.org/fr-fr/Images/img4.png, (min-width: 1200px) and (-webkit-min-device-pixel-ratio: 2.0)],[https://imdtec.org/fr-fr/Images/img4.png, (min-width: 1200px) and (max-width: 1799px)],[https://imdtec.org/fr-fr/Images/img4.png, (min-width: 1800px)]"></script>
        </a>
      
     </div>
     
    </div> 
   </div>

  </div> 
 </div> 
</div>


</div>
</div>
</div>
</div>
</div>
</div>




</div>
<div class="col-sm-12 row" style="margin-top:20px;max-width:100%;">

<style>
@media only screen and (max-width: 10992px) 
{
.au_travail_row 
{
margin-left:0px;
}
}

@media only screen and (max-width: 1992px) 
{
.au_travail_row 
{
margin-left:0px;
}
}

@media only screen and (max-width:1377px) 
{
.au_travail_row 
{
margin-left:0px;
}
}



@media only screen and (max-width:1130px) 
{
.au_travail_row 
{
margin-left:0px;
}
}



@media only screen and (max-width:1090px) 
{
.au_travail_row 
{
margin-left:0px;
}
}


@media only screen and (max-width:830px) 
{
.au_travail_row 
{
margin-left:-14px;
}
}

@media only screen and (max-width:767px) 
{
.au_travail_row
{
margin-left:-14px;
}
}
</style>


<div class="col-md-12" style="padding:0px;margin-top:0px;">




<div id=""class="col-md-12"style="border-bottom:1px solid #ffffff;background-color:#ffffff;text-align:left;">


<div id="text_title"class="col-md-12"style="line-height:1.2;color:#000000;padding:0px 0px 10px 6px;text-align:left;margin-left:2px;">
<label id="text_font2" style="font-weight:blod;font-weight:600;text-align:left;text-decoration:none;font-size:32px;color:#000000;padding:0px 0px 0px 0px;">
Au travail
</label>
</div>






<div id="width_mobile" class="col-md-6 row"style="padding:0px;text-align:left;margin:0px;">

<div id="media_style2"class="col-sm-6"style="padding:10px;text-align:left;">
<a href="https://certification.imdtec.org/en-eg/dashboard/">
<img src="https://imdtec.org/fr-fr/Images/img4.png" class="image"style="width:100%;height:173px;" />

<div style="padding:30px 0px 0px 0px;border-bottom:0px solid #ffffff;">
<a href="https://imdtec.org/en-eg/business-functions/"id="text_font_noir" style="font-size:21px;text-decoration:none;">
Business Function
</a>
</div>

<div style="padding:5px 0px 10px 0px;">
<p class="banner-subheadline headline-subheadline-26" id="text_font_noir" style="font-size:15px;line-height:19px;color:#000000;font-weight:normal;font-weight:400;">
Business functions are driven by your target business outcomes. At IMDTEC we start by understanding your business drivers and objectives. This means that our advice is business-led – driving business value.
</p>
</div>
<div class="" style="text-align:left;">
<a id="btn11"href="https://imdtec.org/en-eg/business-functions/"style="color:#004290;">Learn more <i style="margin-top:2px;margin-left:14px;position:absolute;font-size:25px;"class="fa fa-angle-right"></i></a>
</div>
</a>

</div>







<div id="media_style2"class="col-sm-6"style="padding:10px;text-align:left;">
<a href="https://certification.imdtec.org/en-eg/dashboard/">
<img src="https://imdtec.org/fr-fr/Images/img3.png" class="image"style="width:100%;height:173px;" />

<div style="padding:30px 0px 0px 0px;border-bottom:0px solid #f1f1f1;">
<a href="https://www.imdtec.org/en-eg/enterprise/"id="text_font_noir" style="font-size:21px;text-decoration:none;">
Industry
</a>
</div>

<div style="padding:5px 0px 10px 0px;">
<p class="banner-subheadline headline-subheadline-26" id="text_font_noir" style="font-size:15px;line-height:19px;color:#000000;font-weight:normal;font-weight:400;">
Connect everyone in your organization 
Improve efficiency, integrate automation and stay ahead of the curve by transforming your business into an Intelligent Enterprise.
</p>
</div>
<div class="" style="text-align:left;">
<a id="btn11"href="https://www.imdtec.org/en-eg/enterprise/" style="color:#004290;">Learn more <i style="margin-top:2px;margin-left:14px;position:absolute;font-size:25px;"class="fa fa-angle-right"></i></a>
</div>
</a>

</div>
</div>


<div id="width_mobile" class="col-md-6 row"style="padding:0px;text-align:left;margin:0px;">


<div id="media_style2"class="col-sm-6"style="padding:10px;text-align:left;">
<a href="https://certification.imdtec.org/en-eg/">
<img src="https://imdtec.org/fr-fr/Images/img1.png" class="image"style="width:100%;height:173px;" />

<div style="padding:30px 0px 0px 0px;border-bottom:0px solid #f1f1f1;">
<a href="https://certification.imdtec.org/en-eg/"id="text_font_noir" style="font-size:21px;text-decoration:none;">
Certification
</a>
</div>

<div style="padding:5px 0px 10px 0px;">
<p class="banner-subheadline headline-subheadline-26" id="text_font_noir" style="font-size:15px;line-height:19px;color:#000000;font-weight:normal;font-weight:400;">
Connect everyone in your organization 
Improve efficiency, integrate automation and stay ahead of the curve by transforming your business into an Intelligent Enterprise
</p>
</div>
<div class="" style="text-align:left;">
<a id="btn11"href="https://certification.imdtec.org/en-eg/"style="color:#004290;">Learn more <i style="margin-top:2px;margin-left:14px;position:absolute;font-size:25px;"class="fa fa-angle-right"></i></a>
</div>

</a>
</div>




<div id="media_style2"class="col-sm-6"style="padding:10px;text-align:left;">
<a href="https://certification.imdtec.org/en-eg/">
<img src="https://imdtec.org/fr-fr/Images/img2.png" class="image"style="width:100%;height:173px;" />

<div style="padding:30px 0px 0px 0px;border-bottom:0px solid #f1f1f1;">
<a href="https://pyxis.imdtec.org/"id="text_font_noir" style="font-size:21px;text-decoration:none;">
Pyxis
</a>
</div>

<div style="padding:5px 0px 10px 0px;">
<p class="banner-subheadline headline-subheadline-26" id="text_font_noir" style="font-size:15px;line-height:19px;color:#000000;font-weight:normal;font-weight:400;">
Connect everyone in your organization 
Improve efficiency, integrate automation and stay ahead of the curve by transforming your business into an Intelligent Enterprise.
</p>
</div>
<div class="" style="text-align:left;">
<a id="btn11"href="https://workplace.imdtec.org/en-eg"style="color:#004290;">Learn more <i style="margin-top:2px;margin-left:14px;position:absolute;font-size:25px;"class="fa fa-angle-right"></i></a>
</div>
</a>

</div>
</div>




</div>  
</div></div> 											
<div class="col-sm-12" style="margin-top:20px;padding:0px;">
			
<style>
@media screen and (max-width: 12992px) 
{

#slider_mobile3_2 
{
width:330px;margin-top:80px;
}
}
@media screen and (max-width: 2992px) 
{

#slider_mobile3_2 
{
width:330px;margin-top:80px;
}
}
@media screen and (max-width: 1992px) 
{

#slider_mobile3_2 
{
width:330px;margin-top:80px;

}
}

@media screen and (max-width:1377px) {
#slider_mobile3_2 
{
width:330px;margin-top:80px;
}
}

@media screen and (max-width:767px) {
#slider_mobile3_2 
{
width:100%;margin-top:-10px;
}
}
</style>




		
<style>
@media screen and (max-width: 10992px) 
{

.slider_mobile3
{
padding:0px 50px;margin-top:0px;padding:0px;background: url(https://imdtec.org/fr-fr/Images/img1.jpg);background-repeat: no-repeat;background-size: 100% 100%;min-height:488px;0px 50px;
}
}
@media screen and (max-width: 1992px) 
{

.slider_mobile3 
{
padding:0px 50px;margin-top:0px;padding:0px;background: url(https://imdtec.org/fr-fr/Images/img1.jpg);background-repeat: no-repeat;background-size: 100% 100%;min-height:488px;0px 50px;
}
}


@media screen and (max-width:1377px) {
.slider_mobile3 
{
margin-top:0px;padding:0px;background: url(https://imdtec.org/fr-fr/Images/img1.jpg);background-repeat: no-repeat;background-size: 100% 100%;min-height:488px;}
}
@media screen and (max-width:1130px) {
.slider_mobile3 
{
margin-top:0px;padding:0px;background: url(https://imdtec.org/fr-fr/Images/img1.jpg);background-repeat: no-repeat;background-size: 100% 100%;min-height:550px;}
}






@media screen and (max-width:1024px) 
{
.slider_mobile3 
{
margin-top:0px;padding:0px;background: url(https://imdtec.org/fr-fr/Images/img1.jpg);background-repeat: no-repeat;background-size: 100% 100%;min-height:620px;}
}





@media screen and (max-width:767px) 
{
.slider_mobile3 
{
margin-top:0px;padding:0px;background: url(https://imdtec.org/fr-fr/Images/back_not.png);background-repeat: no-repeat;background-size: 100% 100%;min-height:188px;
}

.slider_mobile3_sous 
{
margin-top:0px;padding:0px;background: url(https://imdtec.org/fr-fr/Images/img1.jpg);background-repeat: no-repeat;background-size: 100% 100%;min-height:288px;
}
}

</style>








<div class="slider_mobile3" style="">

<div class="col-sm-1 slider_mobile3_sous" style="padding:0px;background-color:transparent;"></div>

<div class="col-sm-3" id="slider_mobile3_2" style="padding:0px;text-align:left;background-color:transparent;">



<label class="slider_text_top" id="title_find"style="background-color:#ffffff;padding:20px;z-index:3;width:100%;color:#ffffff;text-align:left;">

<label class="control-label" id="title_find"style="font-size:25px;width:100%;color:#17488a;text-align:center;margin-top:10px;font-weight:blod;font-weight:600;">
Créez votre compte
</label>

<label class="control-label" id="title_find"style="font-size:25px;width:100%;color:#000000;text-align:center;margin-top:10px;font-weight:blod;font-weight:600;">
We want whatever business functions are not only be effective,
</label>


<label class="control-label" id="title_find2"style="font-size:16px;line-height:1.3;width:100%;color:#000000;text-align:center;margin-top:17px;margin-left:5px;">
but also be constructive by emotional connection with end users.
</label>

<label class="control-label" id="title_find"style="font-size:17px;width:100%;color:#17488a;text-align:center;margin-top:20px;font-weight:blod;font-weight:600;">
<a>S'inscrire maintenant</a>
</label>
</label>



</div>


<div class="col-sm-8" style="padding:0px;background-color:transparent;"></div>

</div></div> 	
<div class="col-sm-12" style="margin-top:10px;padding:0px;">
<style type="">
    #menu_title{
font-family:"Segoe UI Bold","Segoe UI Web Bold","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Bold,"Helvetica Neue",Arial,sans-serif,
    clear: both;
    color: #8a8989;
    font-weight: bold;
	font-weight:600;
    word-break: break-word;
    display: inline-block;
    font-size: 16px;
    line-height: 0px;
    padding:10px 0px 0px 0px;
    margin-top: 0;
    margin-bottom: 0;
    margin: 0px 0;
    -webkit-margin-before: 1.33em;
    -webkit-margin-after: 1.33em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
    content: " ";
    display: table;
    }
</style>
<style type="">
#c-uhff-link{
    color:#d0cccc;
    font-weight: normal;
	font-weight: 400;
	font-size:14px;
	font-family:"Segoe UI Normal","Segoe UI Web Normal","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Normal,"Helvetica Neue",Arial,sans-serif,
   }


#c-uhff-link:hover{
    color:#ffffff;
    font-weight: normal;
	font-weight: 400;
	font-size:14px;
	font-family:"Segoe UI Normal","Segoe UI Web Normal","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Normal,"Helvetica Neue",Arial,sans-serif,
   }   

#li_menu_bas
{
background-color:transparent;
padding:0px;
height:30px;	
color:#ffffff;
}

#li_menu_bas:hover
{
background-color:transparent;
padding:0px;
height:30px;	
}


.c-list{margin-top:25px;}
 
</style>

<style>
#option_lang_menu{border: 1px solid transparent;background-color:#212121;
color:#ffffff;padding:0px 0px 0px 26px;margin-left:0px;border-radius:50px;height:40px;}
#option_lang_menu:focus{border: 1px solid #ffffff;color:#ffffff;}
</style>




<div style="background-color:#000000;"id="footerArea" class="uhf">
<div id="footerRegion" data-region-key="footerregion">
<div style="background-color:#000000;"id="footerUniversalFooter" data-module-id="Category|footerRegion|coreui-region|footerUniversalFooter|coreui-universalfooter">


<footer style="background-color:#000000;margin-top:20px;" id="uhf-footer" class="c-uhff context-uhf" data-uhf-mscc-rq="false" data-footer-footprint="//Footer, fromService: True">




<nav style="background-color:#000000;list-style-type:circle;"class="c-uhff-nav showLinks" aria-label="Liens des ressources du pied de page">
			
<div style="margin-top:20px;"class="c-uhff-nav-row">
<div class="c-uhff-nav-group">
<div class="c-heading-4" id="menu_title">Store &amp; Support
</div>
<div style="margin-top:25px;text-align:left;list-style-type:none;"class="c-list f-bare">
<li id="li_menu_bas">
<a id="c-uhff-link" href="https://store.imdtec.org/en-eg/" >Order tracking</a>
</li>
<li id="li_menu_bas">
<a id="c-uhff-link" href="https://store.imdtec.org/en-eg/">Store locations</a>
</li>
<li id="li_menu_bas">
<a id="c-uhff-link" href="https://support.imdtec.org/en-eg">Support</a>
</li>
<li id="li_menu_bas">
<a id="c-uhff-link" href="https://partner.imdtec.org/en-eg/">Partner</a>
</li>
</div>
</div>
        


<div class="c-uhff-nav-group">
<div class="c-heading-4" id="menu_title">For Businesses</div>
<div style="margin-top:25px;text-align:left;"class="c-list f-bare">
<li id="li_menu_bas">
<a id="c-uhff-link" href="" data-m="{&quot;cN&quot;:&quot;Footer_Education_InEducation_nav&quot;,&quot;id&quot;:&quot;n1c3c1c1m1r1a2&quot;,&quot;sN&quot;:1,&quot;aN&quot;:&quot;c3c1c1m1r1a2&quot;}" >Business Functions</a>
</li>
<li id="li_menu_bas">
<a id="c-uhff-link" href="" data-m="{&quot;cN&quot;:&quot;Footer_Education_OfficeForStudents_nav&quot;,&quot;id&quot;:&quot;n2c3c1c1m1r1a2&quot;,&quot;sN&quot;:2,&quot;aN&quot;:&quot;c3c1c1m1r1a2&quot;}" >Industry</a>
</li>
<li id="li_menu_bas">
<a id="c-uhff-link" href="" data-m="{&quot;cN&quot;:&quot;Footer_Education_Office365ForSchools_nav&quot;,&quot;id&quot;:&quot;n3c3c1c1m1r1a2&quot;,&quot;sN&quot;:3,&quot;aN&quot;:&quot;c3c1c1m1r1a2&quot;}" >Find a solutions</a>
</li>
</div>
</div>
						
						





						
<div class="c-uhff-nav-group">
<div class="c-heading-4" id="menu_title">For Learning
</div>
<div style="margin-top:25px;text-align:left;"class="c-list f-bare">
<li id="li_menu_bas">
<a id="c-uhff-link" href="" data-m="{&quot;cN&quot;:&quot;Footer_Education_InEducation_nav&quot;,&quot;id&quot;:&quot;n1c3c1c1m1r1a2&quot;,&quot;sN&quot;:1,&quot;aN&quot;:&quot;c3c1c1m1r1a2&quot;}" >Browse training</a>
</li>
<li id="li_menu_bas">
<a id="c-uhff-link" href="" data-m="{&quot;cN&quot;:&quot;Footer_Education_OfficeForStudents_nav&quot;,&quot;id&quot;:&quot;n2c3c1c1m1r1a2&quot;,&quot;sN&quot;:2,&quot;aN&quot;:&quot;c3c1c1m1r1a2&quot;}" >Business Simulations</a>
</li>
</div>                           
</div>




					
						
						

</div>






<div class="c-uhff-nav-row">




<div class="c-uhff-nav-group">
<div class="c-heading-4" id="menu_title">For Education
</div>
<div style="margin-top:25px;text-align:left;"class="c-list f-bare">
<li id="li_menu_bas">
<a id="c-uhff-link" href="" data-m="{&quot;cN&quot;:&quot;Footer_Education_InEducation_nav&quot;,&quot;id&quot;:&quot;n1c3c1c1m1r1a2&quot;,&quot;sN&quot;:1,&quot;aN&quot;:&quot;c3c1c1m1r1a2&quot;}" >IMDTEC in Education</a>
</li>
<li id="li_menu_bas">
<a id="c-uhff-link" href="" data-m="{&quot;cN&quot;:&quot;Footer_Education_OfficeForStudents_nav&quot;,&quot;id&quot;:&quot;n2c3c1c1m1r1a2&quot;,&quot;sN&quot;:2,&quot;aN&quot;:&quot;c3c1c1m1r1a2&quot;}" >Business Simulations</a>
</li>
</div>
                            
</div>



<div class="c-uhff-nav-group">
<div class="c-heading-4" id="menu_title">Developer
</div>
<div style="margin-top:25px;text-align:left;"class="c-list f-bare">
<li id="li_menu_bas">
<a id="c-uhff-link" href="https://developers.imdtec.org/en-eg/community/women-techmake">Women Techmakers</a>
</li>

<li id="li_menu_bas">
<a id="c-uhff-link" href="https://developers.imdtec.org/en-eg/community/ibg">IMDTEC Business</a>
</li>
                                       
                                      
<li id="li_menu_bas">
<a id="c-uhff-link" href="https://developers.imdtec.org/en-eg/community/experts">IMDTEC Developers</a>
</li>
                                       
<li id="li_menu_bas">
<a id="c-uhff-link" href="https://developers.imdtec.org/en-eg/community/Launchpad">Launchpad</a>
</li>

<li id="li_menu_bas">
<a id="c-uhff-link" href="https://developers.imdtec.org/en-eg/community/dsc">Developer student clubs</a>
</li>
</div>
                            
</div>




<div class="c-uhff-nav-group">
<div class="c-heading-4" id="menu_title">Company</div>
<div style="margin-top:25px;text-align:left;"class="c-list f-bare">
                               <li id="li_menu_bas">
<a id="c-uhff-link" href="https://careers.imdtec.org/en-eg/">Careers</a>
</li>

<li id="li_menu_bas">
<a id="c-uhff-link" href="https://imdtec.org/en-eg/about/">About IMDTEC</a>
</li>

<li id="li_menu_bas">
<a id="c-uhff-link" href="https://imdtec.org/en-eg/diversity">Diversity</a>
</li>

<li id="li_menu_bas">
<a id="c-uhff-link" href="https://imdtec.org/en-eg/supplier">Supplier</a>
</li>

<li id="li_menu_bas">
<a id="c-uhff-link" href="https://privacy.imdtec.org/en-eg/">Privacy at IMDTEC</a>
</li>
</div>
</div>
</div>
</nav>
		
		
		













<style>	
.lang_country{font-size:17px;font-weight:blod;font-weight:700;padding:2px 0px 2px 0px;font-family:"Segoe UI Light","Segoe UI Web Light","Segoe UI Web Regular","Segoe UI","Segoe UI Symbol",HelveticaNeue-Light,"Helvetica Neue",Arial,sans-serif}
.lang_country_view{font-size:17px;font-weight:normal;font-weight:400;}
</style>















<style>
@media only screen and (max-width: 10992px) 
{
.lang_padding 
{
}
}

@media only screen and (max-width: 1992px) 
{
.lang_padding 
{
}
}

@media only screen and (max-width:1377px) 
{
.lang_padding 
{
}
}



@media only screen and (max-width:1130px) 
{
.lang_padding 
{
}
}



@media only screen and (max-width:992px) 
{
.lang_padding 
{
}
}


@media only screen and (max-width:830px) 
{
.lang_padding 
{
}
}

@media only screen and (max-width:767px) 
{
.lang_padding 
{
padding:3px;
}
}
</style>	


		
<div style="background-color:#000000;margin-top:20px;padding:0px;" class="c-uhff-base x-hidden-focus">
<div class="languageSelector" style="text-align:left;padding:0px;"> 

 
<div class="dropdown dropup col-md-4" style="margin-left:0px;padding:0px;margin-top:10px;min-width:200px;">

<a class="langup" class="inputBarA dropdown-toggle"  style="color:#ffffff;margin-top:0px;margin-left:50px;">
<span style=""id="pickedLanguage" class="language" aria-hidden="true">
<i class="fa fa-globe" style="color:#ffffff;font-size:17px;margin-top:5px;margin-left:0px;"></i>
</span> 
<span style="margin-left:10px;margin-top:0px;"> 
France - Français
</span>
</a>
   
</div>
   
   
   
   
   
   
   
 
 </div>
 
 
 
 
 
 
 
 <style>
#ul_footor {
  list-style: none;
}

#ul_footor li::before {
  content: "\2022";
  color:transparent;
  font-weight: bold;
  display: inline-block; 
  width: 1em;
  margin-left: -1em;
}
</style>
 

        <nav aria-label="Liens IMDTEC" class="showLinks col-md-8" style="margin-top:4px;">
            <ul class="c-list f-bare" id="ul_footor">
                                <li>
                    <a  id="c-uhff-link" data-mscc-ic="false">Sitemap</a>
                </li>
                <li>
                    <a id="c-uhff-link" href="https://support.imdtec.org/en-eg/contact-us/" data-mscc-ic="false">Contact us</a>
                </li>
				
				<li>
                    <a id="c-uhff-link" href="https://support.imdtec.org/en-eg/contact-us/" data-mscc-ic="false">About</a>
                </li>

                <li>
                    <a id="c-uhff-link" href="https://imdtec.org/en-eg/privacy" data-mscc-ic="false">Privacy & cookies</a>
                </li>
            
			

                <li id="c-uhff-link">© IMDTEC 2020</li>
                
            </ul>
        </nav>
    </div>
    
</footer>





























<div class="lang" style="display: none;">
  <footer class="mt-5 pt-5 mb-5 pb-5">
  <div class="container">
    <div class="row">
     







<div class="" style="padding:10px 0px 10px 90px;text-align:left;">
<div id=" "class="col-sm-12 lang_country" style="padding:10px 0px 10px 0px;text-align:left;font-size:25px;color:#000000;margin-left:-20px;">
Choose your country / region
</div>

<div id="width_mobile"class="col-md-6 row"style="padding:0px;text-align:left;">

<div id=""class="col-sm-6" style="padding:0px;text-align:left;">
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Argentina - <span class="lang_country_view">Español</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Australia - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">België - <span class="lang_country_view">Nederlands</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Belgium - <span class="lang_country_view">French</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Brasil - <span class="lang_country_view">Português</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Canada - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Canada - <span class="lang_country_view">French</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Chile - <span class="lang_country_view">Español</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Colombia - <span class="lang_country_view">Español</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Danmark - <span class="lang_country_view">Dansk</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Deutschland - <span class="lang_country_view">Deutsch</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">España - <span class="lang_country_view">Español</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Estados Unidos - <span class="lang_country_view">Français</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">France - <span class="lang_country_view">French</span></div>
</div>


<div id=""class="col-sm-6"style="padding:0px;text-align:left;">
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Hong Kong - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">India - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">India - <span class="lang_country_view">हिंदी</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Indonesia - <span class="lang_country_view">Bahasa Indonesia</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Ireland - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Israel - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Italia - <span class="lang_country_view">Italiano</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Malaysia - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">México - <span class="lang_country_view">Français</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Nederland - <span class="lang_country_view">Nederlands</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">New Zealand - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Norge - <span class="lang_country_view">Bokmål</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Österreich - <span class="lang_country_view">Deutsch</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Pakistan - <span class="lang_country_view">English</span></div>
</div>

</div>

<div id="width_mobile"class="col-md-6 row"style="padding:0px;text-align:left;">

<div id=""class="col-sm-6"style="padding:0px;text-align:left;">
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Perú - <span class="lang_country_view">Français</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Philippines - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Polska - <span class="lang_country_view">Polski</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Portugal - <span class="lang_country_view">Português</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Schweiz - <span class="lang_country_view">Deutsch</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Singapore - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">South Africa - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Switzerland - <span class="lang_country_view">French</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Suomi - <span class="lang_country_view">Suomi</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Sverige - <span class="lang_country_view">Svenska</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Svizzera - <span class="lang_country_view">Italiano</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Türkiye - <span class="lang_country_view">Türkçe</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">United Arab Emirates - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">United Kingdom - <span class="lang_country_view">English</span></div>
</div>




<div id=""class="col-sm-6"style="padding:0px;text-align:left;">
<div id=""class="col-sm-12 lang_country"style="text-align:left;">United States - <span class="lang_country_view">English</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Venezuela - <span class="lang_country_view">Español</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Việt Nam - <span class="lang_country_view">Tiếng Việt</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Ελλάδα - <span class="lang_country_view">Ελληνικά</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Россия - <span class="lang_country_view">Русский</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">Україна - <span class="lang_country_view">Українська</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">ไทย - <span class="lang_country_view">ไทย</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">대한민국 - <span class="lang_country_view">한국어</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">台灣 - <span class="lang_country_view">繁體 中文</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">新加坡 - <span class="lang_country_view">简体 中文</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">日本 - <span class="lang_country_view">日本語</span></div>
<div id=""class="col-sm-12 lang_country"style="text-align:left;">香港 - <span class="lang_country_view">繁體 中文</span></div>		
</div>
</div>

<div id=""class="col-sm-12" style="height:30px;text-align:left;font-size:30px;color:#000000;">
</div>
</ul>
</div>
   
 
    </div>
  </div>
</footer>
</div>

    </div>
        </div>

    </div>
   
</div> 								
</div>				
</div>
<script src="https://imdtec.org/fr-fr/bootstrap/js-top.js"></script>
<script src="https://imdtec.org/fr-fr/bootstrap/jquery-1.11.2.min.js"></script>
<script src="https://imdtec.org/fr-fr/bootstrap/jquery.unobtrusive-ajax.min.js"></script>
<script src="https://imdtec.org/fr-fr/bootstrap/jquery.form.js"></script>
<script src="https://imdtec.org/fr-fr/bootstrap/footor/jsfile.js"></script>
<script src="https://imdtec.org/fr-fr/bootstrap/js/jquery_file.min.js"></script>
<script src="https://imdtec.org/fr-fr/bootstrap/js/bootstrap_file.min.js"></script>
<script src="https://imdtec.org/fr-fr/bootstrap/js/jquery_file3.min.js"></script>
<script src="https://imdtec.org/fr-fr/bootstrap/js/js-bottom.js"></script>
<script src="https://imdtec.org/fr-fr/bootstrap/js/plugin.js"></script>
<script>
function openNav() {
  document.getElementById("mySidepanel").style.width = "390px";
}

function closeNav() {
  document.getElementById("mySidepanel").style.width = "0";
}
</script>
<script>
    $(document).scroll(function() {
        var y = $(this).scrollTop();
        if (y > 1) {
            $('.canvas-bars-only').css('display' , 'block');
            $('.app-black').css('display' , 'block');
            $('.login-black').css('display' , 'block');
            $('.app-white').css('display' , 'none');
            $('.login-white').css('display' , 'none');
        } else {
            $('.canvas-bars-only').css('display' , 'none');
            $('.app-black').css('display' , 'none');
            $('.login-black').css('display' , 'none');
            $('.app-white').css('display' , 'block');
            $('.login-white').css('display' , 'block');
        }
    });
</script>
<script>
    var digitaldata = {"page":{"template":"R1","country":"us","siteSection":"Generic Content","pageType":"Generic content pages","eYProduct":"EY.com","isLocalPage":"NO","language":"en","pageName":"What we do","headline":"","eYApplication":"EY.com","liveCopyReference":"/en_gl/what-we-do","primary":{"tag":{"term":"General EY"}}},"onclick":{},"onload":{},"component":{"link":{}}};
    if (window.sessionStorage && window.sessionStorage.getItem('refsrc')) {
        digitaldata.referrer = window.sessionStorage.getItem('refsrc');
        window.sessionStorage.removeItem('refsrc');
    } else if (document.referrer) {
        digitaldata.referrer = document.referrer;
    }
</script>
</script>
</body>
</html>