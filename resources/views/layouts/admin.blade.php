<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('title')</title>
<meta name="description" content="D'luxxis  store.">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@yield('css')

<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<link href="{{ URL::asset('themes/main.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{URL::asset('css/boos4.min.css')}}" type="text/css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="{{ URL::asset('css/boos4-1.min.css')}}" type="text/css" rel="stylesheet" media="screen">

<link href="{{URL::asset('css/store_tables.min')}}" type="text/css" rel="stylesheet" media="screen">
<link rel="stylesheet" id="main-stylesheet" href="{{ URL::asset('master_assets/styles/shards-dashboards.1.1.0.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('master_assets/styles/extras.1.1.0.min.css')}}"> 
<link href="{{ URL::asset('css/store.css')}}" type="text/css" rel="stylesheet" media="screen">

<script type="text/javascript" src="{{ URL::asset('js/buttons.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/all.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/strap.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/Chart.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/shards.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.sharrre.min.js')}}"></script>
 <script type="text/javascript" src="{{URL::asset('assets/plugins/raphael/raphael.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('assets/js/check-all-mail.js')}}"></script>




<script src="{{URL::asset('assets/js/apexcharts.js')}}"></script>
@yield('js')

</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header closed-sidebar">
@include('layouts.admin.adminnavbarre')       
@include('layouts.admin.layout-options')
<div class="app-main pr">
@include('layouts.admin.adminsidebar')       
<div class="app-main__outer">
<div  class="app-main__inner app-body body-shadow {{ Auth::user()->body }}">
<div class="body_top">
@section('content')
@show
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="{{ URL::asset('js/table/jquery.table.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/table/tables.view.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/input/search.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/back.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('themes/assets/scripts/main.js')}}"></script>

</body>
</html>
