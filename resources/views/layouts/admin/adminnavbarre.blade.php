<div class="header_index app-header header-shadow {{ Auth::user()->header }}">
            <div class="app-header__logo">
                <div class="logo-src" style="width:140px;height:43px;">
                </div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>

                </div>
            </div>

            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
        
        
            
            <div class="app-header__content">
                <div class="app-header-left">
                    <div class="search-wrapper">
                        <div class="input-holder">
                            <input type="text" class="search-input" placeholder="Type to search">
                            <button class="search-icon"><span></span></button>
                        </div>
                        <button class="close"></button>
                    </div>
                    <ul class="header-menu nav">
                        <li class="nav-item">
                            <a href="{{route('cms.index')}}" class="nav-link">
                                <i class="nav-link-icon fa fa-database"> </i>
                                D luxxis  CMS
                            </a>
                        </li>
                        <li class="btn-group nav-item">
                            <a href="{{route('product.index')}}" class="nav-link">
                                <i class="nav-link-icon fa fa-edit"></i>
                                D'luxxis products
                            </a>
                        </li>
                        <li class="dropdown nav-item">
                            <a href="{{route('settings.index')}}" class="nav-link">
                                <i class="nav-link-icon fa fa-cog"></i>
                                D'luxxis settings
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                

<form class="p-0 btn" action = "{{ url('admin/notification/update')}}" method = "POST">
@csrf
<div class="btn-group">
<input type="hidden" name="_method" value="PATCH">
<input type="hidden" name="status" value="1"/>
<button type="submit" class="dropdown  p-0 btn">
@if(count($notifications)=='0')
@else
<span class="notification_incons">{{ count($notifications) }}</span>
@endif
<span class="nav-link"><i class="nav-link-icon fa fa-bell"></i>
Notification</span>
</button>
</div>
</form>




<div class="btn-group">
<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
<img class="user-avatar rounded-circle mr-2" src="{{ asset('public/profile_pic/admin/'.Auth::user()->image) }}" style="width:40px;height:40px;border-radius:50%;"alt="User Avatar">
<span class="d-none d-md-inline-block"> {{ Auth::user()->firstname }}</span>
<i class="fa fa-angle-down ml-2 opacity-8"></i>
</a>
<div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
<a href="{{ route('admin.profile')}}" type="button" tabindex="0" class="dropdown-item">Profile</a>
<a href="{{route('admin.edit.profile')}}" type="button" tabindex="0" class="dropdown-item">Change info</a>
<a href="{{route('change.password.adminview')}}" type="button" tabindex="0" class="dropdown-item">Change password</a>
<a href="{{route('mysettings.index')}}" type="button" tabindex="0" class="dropdown-item">Account settings</a>
<div tabindex="-1" class="dropdown-divider"></div>
<button href="{{ route('logout') }}" onclick="event.preventDefault();
document.getElementById('logout-form').submit();" type="button" tabindex="0" class="dropdown-item">Logout</button>
 <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
@csrf
</form>
</div>
</div>
</div>
                             
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
		