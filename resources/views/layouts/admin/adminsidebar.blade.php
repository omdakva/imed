<div  class="app-sidebar sidebar-shadow  {{ Auth::user()->sidebar }}">
                    <div class="app-header__logo">
                        <div class="logo-src"></div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                   
                    </div>    
                    
                    <div  class="scrollbar-sidebar {{ Auth::user()->sidebar_position }}">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li>
                                    <a class="nav-link" href="{{ route('home') }}" class="mm-active">
                                        <i class="metismenu-icon pe-7s-rocket"></i>
                                        Dashboard
                                    </a>
                                </li>
                                <li>
                                    <a  class="nav-link"href="{{ route('seo') }}">
                                        <i class="metismenu-icon pe-7s-diamond"></i>
                                        SEO
                                    </a>
                              
                                </li>
                                <li >
                                    <a class="nav-link"href="{{ route('cms.index') }}">
                                        <i class="metismenu-icon pe-7s-car"></i>
                                        D'luxxis CMS
                                    </a>
                                   
                                </li>
                                <li  >
                                    <a class="nav-link"href="{{ route('location') }}">
                                        <i class="metismenu-icon pe-7s-display2"></i>
                                        D'luxxis location
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link"href="{{ route('blog.index') }}">
                                        <i class="metismenu-icon pe-7s-display2"></i>
                                        Manage blog
                                    </a>
                                </li>

                                <li>
                                    <a class="nav-link"href="{{ route('contact.index') }}">
                                        <i class="metismenu-icon pe-7s-mouse">
                                        </i>Manage contact
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link"href="{{ route('manage-category.index') }}">
                                        <i class="metismenu-icon pe-7s-eyedropper">
                                        </i>Manage category
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link"href="{{ route('product.index') }}">
                                        <i class="metismenu-icon pe-7s-pendrive">
                                        </i>Manage product
                                    </a>
                                </li>

                                <li>
                                    <a class="nav-link"href="{{ route('user.index') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>Manage user
                                    </a>
                                </li>


                                <li>
                                    <a class="nav-link"href="{{ route('manage-admin.index') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>Manage admin
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link"href="{{ route('booking.index') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>Booking
                                    </a>
                                </li>

                                <li>
                                    <a class="nav-link"href="{{ route('stock.index') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>In Stock
                                    </a>
                                </li>

                                <li>
                                    <a class="nav-link"href="{{ route('settings.index') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>D'luxxis settings
                                    </a>
                                </li>


                                <li>
                                    <a class="nav-link"href="{{ route('manage-statistics.index') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>Statistics
                                    </a>
                                </li>

                                <li>
                                    <a class="nav-link"href="{{ route('newsletter.index') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>Subscribe newsletter
                                    </a>
                                </li>

                                <li>
                                    <a class="nav-link"href="{{ route('newsletter.index') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>Documents
                                    </a>
                                </li>
                                  <li>
                                    <a class="nav-link"href="{{ route('newsletter.inbox') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>mailbox
                                    </a>
                                </li>
                               <li>
                                    <a class="nav-link"href="{{ route('chat.index') }}">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>Chat Room
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div> 
  