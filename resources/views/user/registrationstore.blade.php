<!DOCTYPE html>
<html>
<head>
<title>Registration Form - w3alert.com</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--Bootsrap 4 CDN-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="{{url('style.css')}}">
<style type="text/css">
        /* Custom Radio Button Start*/

.radiotextsty {
  color: #A5A4BF;
  font-size: 18px;
}

.customradio {
  display: block;
  position: relative;
  padding-left: 30px;
  margin-bottom: 0px;
  cursor: pointer;
  font-size: 18px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.customradio input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 22px;
  width: 22px;
  background-color: white;
  border-radius: 50%;
  border:1px solid #BEBEBE;
}

/* On mouse-over, add a grey background color */
.customradio:hover input ~ .checkmark {
  background-color: transparent;
}

/* When the radio button is checked, add a blue background */
.customradio input:checked ~ .checkmark {
  background-color: white;
  border:1px solid #BEBEBE;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.customradio input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.customradio .checkmark:after {
  top: 2px;
  left: 2px;
  width: 16px;
  height: 16px;
  border-radius: 50%;
  background: #A3A0FB;
}

/* Custom Radio Button End*/
    </style>
</head>
<body>
<div class="container-fluid">
  <div class="row no-gutter">

<div class="d-none d-md-flex col-md-7 col-lg-7 bg-image">
<img src="{{ asset('image/vector/100.jpg') }}" style="width:100%;height:80%;">
</div>
	
    <div class="col-md-5 col-lg-5">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-lg-12 mx-auto">
              <h3 class="login-heading mb-4">Register here!</h3>
               <form action="{{url('user/registration')}}" method="POST" id="regForm">
                 {{ csrf_field() }}
				<input type="hidden" name="id_user" value="<?php echo date('dmy')?><?php echo date('his')?>">
				<input type="hidden" name="image" value="0c3b3adb1a7530892e55ef36d3be6cb8.png">
				<input type="hidden" name="role" value="0">
				<input type="hidden" name="country" value="0">
				<input type="hidden" name="status" value="1">
				<input type="hidden" name="date" value="<?php echo date('d/m/Y')?>">
				<input type="hidden" name="id_date" value="<?php echo date('dmY')?>">
				<input type="hidden" name="month" value="<?php echo date('FY')?>">
				<input type="hidden" name="id_month" value="<?php echo date('mY')?>">
				<input type="hidden" name="year" value="<?php echo date('Y')?>">
				<input type="hidden" name="vue" value="0">

				<input type="hidden" name="block_description" value="Description">
                   @if ($errors->has('image'))
                  <span class="error">{{ $errors->first('image') }}</span>
                  @endif 
                
				<div class="row">
				<div class="form-label-group col-md-6">
                <label for="inputName">First Name</label>
                <input type="text" id="inputName" name="firstname" class="form-control" placeholder="First name" autofocus>
                  @if ($errors->has('firstname'))
                  <span class="error">{{ $errors->first('firstname') }}</span>
                  @endif       
                </div> 
				<div class="form-label-group col-md-6">
                <label for="inputName">Last Name</label>
                <input type="text" id="inputName" name="lastname" class="form-control" placeholder="Last name" autofocus>
                  @if ($errors->has('lastname'))
                  <span class="error">{{ $errors->first('lastname') }}</span>
                  @endif       
                </div>
                </div>
				
                </br>
                <div class="form-label-group">
                <label for="inputEmail">Email address</label>
                <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" >
                 @if ($errors->has('email'))
                  <span class="error">{{ $errors->first('email') }}</span>
                  @endif    
                </div> 
				
            
                </br> 
			
                <div class="row">
			    <div class="form-label-group col-md-12">
                <label for="inputName">Birthday</label>
                </div> 
				
				<div class="form-label-group col-md-4">
                <input type="text" id="inputName" name="birthday" class="form-control" placeholder="Day" autofocus>
                  @if ($errors->has('birthday'))
                  <span class="error">{{ $errors->first('birthday') }}</span>
                  @endif       
                </div> 
				
				<div class="form-label-group col-md-4">
                <input type="text" id="inputName" name="birthmonth" class="form-control" placeholder="Month" autofocus>
                  @if ($errors->has('birthmonth'))
                  <span class="error">{{ $errors->first('birthmonth') }}</span>
                  @endif       
                </div>
				<div class="form-label-group col-md-4">
                <input type="text" id="inputName" name="birthyear" class="form-control" placeholder="Year" autofocus>
                  @if ($errors->has('birthyear'))
                  <span class="error">{{ $errors->first('birthyear') }}</span>
                  @endif       
                </div>
                </div>
</br>				
	 <div class="form-group">
		      <label class="">Gender</label></br>
		          <div class="form-check-inline">

					<label class="customradio"><span class="radiotextsty">Male</span>
					  <input type="radio" checked="checked" name="gender" value="1">
					  <span class="checkmark"></span>
					</label> &nbsp; &nbsp; &nbsp; &nbsp;
					<label class="customradio"><span class="radiotextsty">Female</span>
					  <input type="radio" name="gender"  value="2">
					  <span class="checkmark"></span>
					</label>
					@if ($errors->has('gender'))
                  <span class="error">{{ $errors->first('gender') }}</span>
                  @endif

				</div>
		  </div>	  
                <div class="form-label-group">
                <label for="inputPassword">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
                  
                  
                  @if ($errors->has('password'))
                  <span class="error">{{ $errors->first('password') }}</span>
                  @endif  
                </div>
                </br>
                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Sign Up</button>
                <div class="text-center">If you have an account?
                  <a class="small" href="{{url('user/login')}}">Sign In</a></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>