@extends('layout.store2')
@section('title', 'STORE-TEC - My orders')
@section('content')
</br>
</br>
</br>
</br>
<nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Dashbord</a></li>
<li class="breadcrumb-item active" aria-current="page">Requests</li>
</ol>
</nav>
<h1 class="mt-4">My orders</h1>
  
<div class="row">
@foreach($orders as $post)
<div class="col-md-12" style="border:1px solid #e0e0e0;text-align:left;padding:10px;">
<div class="col-md-9">
<p class="textblod">Order number : <span class="textnormal">{{ $post->orders_number }}</span></p>
<p class="textblod">Amount : <span class="textnormal">{{ $post->total_amount }} {{ $post->currency }}</span></p>
<p class="textblod">Date : <span class="textnormal">{{ $post->created_at }}</span></p>
</div>
<div class="col-md-3">
<a id="dashhomelink" href="{{ url('user/request/show') }}/{{$post->id}}">View more</a>
</div>
</div>
<div class="col-md-12" style="height:10px;"></div>

@endforeach
</div>

@endsection