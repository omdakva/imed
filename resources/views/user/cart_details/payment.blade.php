@extends('layout.store2')
@section('title', 'STORE-TEC - Cart')
@section('content')
<div class="divcam">
<div class="row">
<div class="col-sm-12" style="padding:0px;"><label class="text_title_home_blod">Finalization of the order</label></div>
<div class="col-lg-8" >





<div class="card card-small mb-4 pt-3">
<div class="card-header border-bottom text-left" style="margin-top:-16px;">
<i class="fa fa-check-circle fa_gris"></i> 1 . Details   
@foreach($Orders as $postid)
<a class="edit_btn_cart" style="float:right;" href = "{{ url('cart/details/'.$postid->id) }}"><i class="fa fa-edit"></i> Edit</a>
@endforeach
</div>
<div class="" style="padding:10px;">
<label class="textblodfull">{{ ucfirst(Auth()->user()->firstname) }} {{ ucfirst(Auth()->user()->lastname) }}</label>
<label class="textblodfull"><span class="textnormal">{{ ucfirst(Auth()->user()->address) }},{{ ucfirst(Auth()->user()->postcode) }} ,{{ ucfirst(Auth()->user()->city) }},{{ ucfirst(Auth()->user()->country) }}</span></label>
<label class="textblodfull"><span class="textnormal">{{ ucfirst(Auth()->user()->email) }}</span></label>
<label class="textblodfull"><span class="textnormal">{{ ucfirst(Auth()->user()->tel) }}</span></label>
</div>

</div>



<div class="card card-small mb-4 pt-3">
<div class="card-header border-bottom text-left" style="margin-top:-16px;">
<i class="fa fa-check-circle fa_gris"></i> 2 . Delivery method    
@foreach($Orders as $postid)
<a class="edit_btn_cart" style="float:right;" href = "{{ url('cart/delivery/'.$postid->id) }}"><i class="fa fa-edit"></i> Edit</a>
@endforeach
</div>
<div class="" style="padding:10px;">
@foreach($Orders as $postid)
<label class="textblodfull">{{ $postid->delivery }}</label>
@endforeach
</div>

</div>


<div class="card card-small mb-4 pt-3">
<div class="card-header border-bottom text-left" style="margin-top:-16px;">
<i class="fa fa-check-circle fa_gris"></i> 3 . Payment method          
</div>
<div class="" style="padding:10px;">
@foreach($Orders as $postid)
<form action = "{{ url('cart/delivery/'.$postid->id) }}" method = "post">
<input type = "hidden" name = "id_order" value = "{{$postid->id}}">
@endforeach
<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<div class="row">

<div class="form-group col-lg-12">
<label for="1" class="col-sm-12 control-label textblod">
<input type="radio"  id="1" name="delivery" value="D1" required=""/>
<img src="{{ asset('public/image/payment/i-service-cash.png') }}" style="margin-top:-5px;margin-left:10px;"> Cash on delivery</label>
</div>

<div class="form-group col-lg-12">
<label for="1" class="col-sm-12 control-label textblod">
<input type="radio"  id="1" name="delivery" value="D1" required=""/>
<img src="{{ asset('public/image/payment/master-visa.png') }}" style="margin-top:-5px;margin-left:10px;"> 
Debit and Credit Accepted - Use this payment method for contactless delivery.</label>
</div>






<div class="form-group col-lg-12" style="text-align:right;">
<button class="btn btn-info" type="submit"> Continue</button>
</div>
</div>
</form>
                  
</div>
</div>









</div>
			  
			  
			  
			  
<div class="col-lg-4">
<div class="card card-small mb-4 pt-3">
<div class="card-header border-bottom text-center" style="margin-top:-16px;">
YOUR ORDER ( <span class="textnormal">{{ $Items }}</span> )                
</div>

@foreach($Product as $post)
<div class="row items_list"style="padding:10px;">
<div class="col-sm-3">
<img src="{{ asset('public/photo/products_logo/'.$post->photo) }}" style="width:60px;height:60px;">
</div>
<div class="col-sm-9">
<label class="textnormalfull">{{ $post->product_name }}</label>
<label class="textnormalfull_count">{{ $post->product_price }} {{ $post->currency }}</label>
<label class="textnormalfull_qty">Qty : {{ $post->quantity }}</label>
</div>
</div>
@endforeach

@foreach($Orders as $row)
<div class="row items_list"style="padding:10px;">
<div class="col-sm-6" style="text-align:left;">
<label class="textnormalfull" style="margin-top:5px;">Total</label>
</div>
<div class="col-sm-6" style="text-align:right;">
<label class="textblodfull" style="font-size:22px;">{{ $row->total_amount }} {{ $row->currency }}</label>
</div>
</div>
@endforeach
</div>
</div>
			  
			  


</div>
</div>
@section('scripts')






<div class="modal fade" id="login" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Login to your account</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
<form action="{{url('user/login')}}" method="POST" id="logForm">

                 {{ csrf_field() }}
<div class="modal-body">

   <div class="form-label-group">
                        <label for="inputEmail">Email address</label>
                         <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" >

                  @if ($errors->has('email'))
                  <span class="error">{{ $errors->first('email') }}</span>
                  @endif    
                </div> 
                </br>
                <div class="form-label-group">
                <label for="inputPassword">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
                  
                  @if ($errors->has('password'))
                  <span class="error">{{ $errors->first('password') }}</span>
                  @endif  
                </div>
</div>

<div class="modal-footer"style="text-align:right;">
<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-info" id="btn-save" value="create">Sign In</button>
</div>
</form>
</div>
</div>
</div>
    <script type="text/javascript">

        $(".update-cart").click(function (e) {
           e.preventDefault();

           var ele = $(this);

            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });

        $(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            if(confirm("Are you sure")) {
                $.ajax({
                    url: '{{ url('remove-from-cart') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

    </script>

@endsection
@endsection