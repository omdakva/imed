@extends('layout.store2')
@section('title', 'STORE-TEC - Cart')
@section('content')
<div class="divcam">
<div class="row">
<div class="col-sm-12" style="padding:0px;"><label class="text_title_home_blod">Finalization of the order</label></div>
<div class="col-lg-8" >
<div class="card card-small mb-4 pt-3">
<div class="card-header border-bottom text-left" style="margin-top:-16px;">
<i class="fa fa-check-circle fa_gris"></i> 1 . Details              
</div>
<div class="" style="padding:10px;">
@foreach($cart->items as $Product)
<form action = "{{ url('cart/details/') }}" method = "post">
<input type = "hidden" name = "id_order" value = "">
@endforeach
<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<div class="row">
<div class="form-group col-lg-6">
<label for="name" class="col-sm-12 control-label textblod">First name</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="firstname" name="firstname" value="{{ ucfirst(Auth()->user()->firstname) }}" required="">
</div>
</div>

<div class="form-group col-lg-6">
<label for="name" class="col-sm-12 control-label textblod">Last name</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="lastname" name="lastname" value="{{ ucfirst(Auth()->user()->lastname) }}" required="">
</div>
</div>

<div class="form-group col-lg-6">
<label for="name" class="col-sm-12 control-label textblod">Tel</label>
<div class="col-sm-12">
<input type="tel" class="form-control" id="tel" name="tel" value="@if(ucfirst(Auth()->user()->tel=='0'))
@else
{{ ucfirst(Auth()->user()->tel) }}
@endif" required=""/>
</div>
</div>

<div class="form-group col-lg-6">
<label for="name" class="col-sm-12 control-label textblod">Phone</label>
<div class="col-sm-12">
<input type="tel" class="form-control" id="mobile" name="mobile" value="@if(ucfirst(Auth()->user()->mobile=='0'))
@else
{{ ucfirst(Auth()->user()->mobile) }}
@endif" required="">
</div>
</div>

<div class="form-group col-lg-6">
<label for="name" class="col-sm-12 control-label textblod">Country</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="country" name="country" value="@if(ucfirst(Auth()->user()->country=='0'))
@else
{{ ucfirst(Auth()->user()->country) }}
@endif" required="">
</div>
</div>

<div class="form-group col-lg-6">
<label for="name" class="col-sm-12 control-label textblod">City</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="city" name="city" value="@if(ucfirst(Auth()->user()->country=='0'))
@else
{{ ucfirst(Auth()->user()->country) }}
@endif" required="">
</div>
</div>

<div class="form-group col-lg-9">
<label for="name" class="col-sm-12 control-label textblod">Address</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="address" name="address" value="@if(ucfirst(Auth()->user()->address=='0'))
@else
{{ ucfirst(Auth()->user()->address) }}
@endif" required="">
</div>
</div>

<div class="form-group col-lg-3">
<label for="name" class="col-sm-12 control-label textblod">Postcode</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="postcode" name="postcode" value="@if(ucfirst(Auth()->user()->postcode=='0'))
@else
{{ ucfirst(Auth()->user()->postcode) }}
@endif" required="">
</div>
</div>

<div class="form-group col-lg-12" style="text-align:right;">
</div>
</div>
</form>
                  
</div>
</div>

<div class="card card-small mb-4 pt-3">
<div class="card-header border-bottom text-left" style="margin-top:-16px;">
<i class="fa fa-check-circle fa_gris"></i> 2 . Delivery method            
</div>
</div>

<div class="card card-small mb-4 pt-3">
<div class="card-header border-bottom text-left" style="margin-top:-16px;">
<i class="fa fa-check-circle fa_gris"></i> 3 . Payment method            
</div>
<form method="POST" action="{{ route('payer') }}">
        {{ csrf_field() }}

        <div class="row">
        <div class="col-xl-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <!-- Shopping Cart-->
                    <div class="product-details table-responsive text-nowrap">
                        <table class="table table-bordered table-hover mb-0 text-nowrap">
                            <thead>
                            <tr>
                                <th class="text-center">Payment Method</th>
                                <th colspan="3"  >additional costs</th>
                                <th>To Choose</th>
                            </tr>
                            </thead>
                            <tbody>

                                <tr>
                                <td>
                                    <div class="media">
                                        <div class="card-aside-img">
                                            <img src="https://i0.wp.com/www.ecommerce-nation.com/wp-content/uploads/2018/01/paypal.png" width="50" >
                                        </div>
                                        <div class="media-body">
                                            <div class="card-item-desc mt-0">
                                                <h6  style="margin-left:15px;">Paypal</h6>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td colspan="3" ></td>

                                <td class="text-center"> <input type="radio" id="payer" name="payer" value="Paypal">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="media">
                                        <div class="card-aside-img">
                                            <img src="https://jbjproductionhaiti.com/wp-content/uploads/2020/07/Stripe-1.jpg" alt="img" width="50">
                                        </div>
                                        <div class="media-body">
                                            <div class="card-item-desc mt-0">
                                                <h6  style="margin-left:15px;">Stripe</h6>
                                                <dl class="card-item-desc-1">
                                                    <dt></dt>
                                                    <dd></dd>
                                                </dl>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td colspan="3"></td>

                                <td class="text-center"> <input type="radio" id="payer" name="payer" value="Strip"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="media">
                                        <div class="card-aside-img">
                                            <img src="https://www.alxmic.com/wp-content/uploads/2016/07/paiement-sur-livraison-piece-auto-300x189@2x.png" alt="img" width="50">
                                        </div>
                                        <div class="media-body">
                                            <div class="card-item-desc mt-0">
                                                <h6  style="margin-left:15px;">A La Laivraison</h6>
                                                <dl class="card-item-desc-1">
                                                    <dt></dt>
                                                    <dd></dd>
                                                </dl>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td colspan="3"  class="text-center"> Frais de lavraison  {{ $cart->cost }}$.</td>

                                <td class="text-center"> <input type="radio" id="payer" name="payer" value="Adomicile"></td>
                            </tr>
                                <tr>
                                    <td>
                                        <div class="media">
                                            <div class="card-aside-img">
                                                <img src="https://www.360-surf.com/wp-content/uploads/2017/11/virement-bancaire.jpg" alt="img" width="50">
                                            </div>
                                            <div class="media-body">
                                                <div class="card-item-desc mt-0">
                                                    <h6  style="margin-left:15px;">virement </h6>
                                                    <dl class="card-item-desc-1">
                                                        <dt></dt>
                                                        <dd></dd>
                                                    </dl>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="3"></td>

                                    <td class="text-center"> <input type="radio" id="payer" name="payer" value="Transfer">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="media">
                                            <div class="card-aside-img">
                                                <img src="https://labouillettetarnaise.com/wp-content/uploads/2017/12/paiement-cheque.png" alt="img" width="50">
                                            </div>
                                            <div class="media-body">
                                                <div class="card-item-desc mt-0">
                                                    <h6  style="margin-left:15px;">Par Chèque</h6>
                                                    <dl class="card-item-desc-1">
                                                        <dt></dt>
                                                        <dd></dd>
                                                    </dl>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="3"></td>

                                    <td class="text-center"> <input type="radio" id="payer" name="payer" value="Check">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="shopping-cart-footer">
                        <div class="column">
                            <button class="btn btn-success" type="submit">
                                Checkout
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
</div>
			  
			  
			  
			  
<div class="col-lg-4">
<div class="card card-small mb-4 pt-3">
<div class="card-header border-bottom text-center" style="margin-top:-16px;">
YOUR ORDER                
</div>

@foreach($cart->items as $product)
<div class="row items_list"style="padding:10px;">
<div class="col-sm-3">
<img src="{{$product['image'] }}" style="width:60px;height:60px;">
</div>
<div class="col-sm-9">
<label class="textnormalfull">{{ $product['title'] }}</label>
<label class="textnormalfull_count">{{ $product['price'] }} $</label>
<label class="textnormalfull_qty">Qty : {{ $product['qty'] }}</label>
</div>
</div>
@endforeach


<div class="row items_list"style="padding:10px;">
<div class="col-sm-6" style="text-align:left;">
<label class="textnormalfull" style="margin-top:5px;">Total</label>
</div>
<div class="col-sm-6" style="text-align:right;">
<label class="textblodfull" style="font-size:22px;">{{ $cart->totalPrice}} $</label>
</div>
</div>
</div>
</div>
			  
			  


</div>
</div>
@section('scripts')




    <script type="text/javascript">

        $(".update-cart").click(function (e) {
           e.preventDefault();

           var ele = $(this);

            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });

        $(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            if(confirm("Are you sure")) {
                $.ajax({
                    url: '{{ url('remove-from-cart') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

    </script>

@endsection
@endsection