@extends('layouts.user.dashbord')
@section('title', 'Education - Evaluation')
@section('content')
<h1 class="mt-4">My Evaluation</h1>
<p class="textblod">Name : <span class="textnormal">{{ ucfirst(Auth()->user()->firstname) }} {{ ucfirst(Auth()->user()->lastname) }}</span></p>
<p class="textblod">Email : <span class="textnormal">{{ ucfirst(Auth()->user()->email) }}</span></p>
<p class="textblod">Evaluation : <span class="textnormal">{{ ucfirst(Auth()->user()->evaluation) }} / 20</span></p>
<p class="lastinfo">Last evaluation : {{ ucfirst(Auth()->user()->last_evaluation) }}</p>
@endsection