@extends('layouts.user.dashbord')
@section('title', 'Education - Evaluation')
@section('content')
<h1 class="mt-4">My Section ( {{ $sectioncount }} )</h1>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">

<ul id="myUL" class="row" style="padding:0px;">
@foreach($sectionlist as $post)
<li class="col-md-3" style="border:1px solid #e0e0e0;text-align:center;">
<div class="col-md-12">
<img src="{{ asset('public/profile_pic/user/'.$post->image) }}" style="width:100px;height:100px;border-radius:50%;"></br></br>
</div>
<div class="col-md-12">
<p class="textblod">{{ $post->firstname }} {{ $post->lastname }}</p>
<p class="textnormal">{{ $post->email }}</p>
</div>
</li>
@endforeach
</ul>
<script>
function myFunction() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
</script>
@endsection