@extends('layout.store2')
@section('title', 'Edite Photo')
@section('content')

<div class="containerdash">
       <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Dashbord</a></li>
				<li class="breadcrumb-item"><a href="{{route('user.setting')}}">Setting</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edite Photo</li>
            </ol>
        </nav>
<h1 class="mt-4">Edit Your photo</h1>
<div class="container">
<div class="row">
<div class="col-md-12 col-lg-12 mx-auto">
<form action = "{{ route('change.pic') }}" method = "post" accept-charset="utf-8" enctype="multipart/form-data">
         <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
		 <input type = 'hidden' name = 'updated_at' class="form-control" value = '<?php echo date('Y-m-d'); ?> <?php echo date('h:i:s'); ?>'/>

         <table style="width:60%;">
            <tr>
               <td style="width:20%;">Photo</td>
               <td >
                  <input type = 'file' name = 'image[]' class="form-control"/>
               </td>
            </tr>
			 
		
			
            <tr>
               <td colspan = '2'>
                  </br><input type = 'submit' class="btn btn-primary"value = "Update" />
               </td>
            </tr>
         </table>
      </form>
	  
	  </div>
	  </div>
</div>
</div>
      
@endsection