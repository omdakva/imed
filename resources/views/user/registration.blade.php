@extends('layout.store2')

@section('title', 'STORE-TEC - Regiter')

@section('content')
</br>
</br>
</br>

<div class="container-fluid">
  <div class="row no-gutter">

	<div class="d-none d-md-flex col-md-3 col-lg-3 bg-image"></div>

	
    <div class="col-md-6 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-lg-12 mx-auto">
              <h3 class="login-heading mb-4">Register here!</h3>
               <form action="{{route('user.registration.save')}}" method="POST" id="regForm">
                 @csrf
				<input type="hidden" name="image" value="0c3b3adb1a7530892e55ef36d3be6cb8.png">
				<input type="hidden" name="role" value="0">
				<input type="hidden" name="country" value="0">
				<input type="hidden" name="city" value="0">
				<input type="hidden" name="address" value="0">
				<input type="hidden" name="tel" value="0">
				<input type="hidden" name="mobile" value="0">
				<input type="hidden" name="postcode" value="0">
				<input type="hidden" name="status" value="1">
				<input type="hidden" name="year" value="{{date('Y')}}">
				<input type="hidden" name="month_number" value="{{date('mY')}}">

				<input type="hidden" name="block_description" value="Description">
                  
				  
                
				<div class="row">
				<div class="form-label-group col-md-6">
                <label for="inputName">First Name</label>
                <input type="text" id="inputName" name="firstname" class="form-control" placeholder="First name" autofocus>
                  @if ($errors->has('firstname'))
                  <span class="error">{{ $errors->first('firstname') }}</span>
                  @endif       
                </div> 
				<div class="form-label-group col-md-6">
                <label for="inputName">Last Name</label>
                <input type="text" id="inputName" name="lastname" class="form-control" placeholder="Last name" autofocus>
                  @if ($errors->has('lastname'))
                  <span class="error">{{ $errors->first('lastname') }}</span>
                  @endif       
                </div>
                </div>
				
                </br>
                <div class="form-label-group">
                <label for="inputEmail">Email address</label>
                <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" >
                 @if ($errors->has('email'))
                  <span class="error">{{ $errors->first('email') }}</span>
                  @endif    
                </div> 
				
            
                </br> 
			
                <div class="row">
			    <div class="form-label-group col-md-12">
                <label for="inputName">Birthday</label>
                </div> 
				
				<div class="form-label-group col-md-12">
                <input type="date" id="inputName" name="birthday" class="form-control" autofocus>
                  @if ($errors->has('birthday'))
                  <span class="error">{{ $errors->first('birthday') }}</span>
                  @endif       
                </div> 
				
				
                </div>
</br>				
	 <div class="form-group">
		      <label class="">Gender</label></br>
		          <div class="form-check-inline">

					<label class="customradio"><span class="radiotextsty">Male</span>
					  <input type="radio" checked="checked" name="gender" value="1">
					  <span class="checkmark"></span>
					</label> &nbsp; &nbsp; &nbsp; &nbsp;
					<label class="customradio"><span class="radiotextsty">Female</span>
					  <input type="radio" name="gender"  value="2">
					  <span class="checkmark"></span>
					</label>
					@if ($errors->has('gender'))
                  <span class="error">{{ $errors->first('gender') }}</span>
                  @endif

				</div>
		  </div>	  
                <div class="form-label-group">
                <label for="inputPassword">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
                  
                  
                  @if ($errors->has('password'))
                  <span class="error">{{ $errors->first('password') }}</span>
                  @endif  
                </div>
                </br>
                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Sign Up</button>
                <div class="text-center">If you have an account?
                  <a class="small" href="{{route('login')}}">Sign In</a></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
		<div class="d-none d-md-flex col-md-3 col-lg-3 bg-image"></div>

  </div>
</div>
@endsection
