@extends('layouts.user.dashbord')
@section('title', 'Changing the default title')
@section('content')
<h1 class="mt-4">My task {{$users}}</h1>
 
<div class="dashhome">
<p class="dashhometitre">My task</p>
<p>The starting state of the menu will appear collapsed on smaller screens, and will appear non-collapsed on larger screens. When toggled using the button below, the menu will change.
</p>
<a id="dashhomelink" href="{{ route('user.task') }}">View more</a>
</div>


@endsection