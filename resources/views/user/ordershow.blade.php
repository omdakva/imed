@extends('layout.store2')
@section('title', 'STORE-TEC - My orders')
@section('content')

       <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Dashbord</a></li>
                <li class="breadcrumb-item"><a href="{{route('user.request')}}">Requests</a></li>
                <li class="breadcrumb-item active" aria-current="page">Orders</li>
            </ol>
        </nav>
<h1 class="mt-4">
My orders
</h1>

<div class="row">
<div class="col-md-6">
<span class="textnormal">{{ $orders->orders_number }}</span>
</div>
<div class="col-md-6" style="text-align:right;">
<h1 class="mt-4">
{{ $orders->total_amount }} {{ $orders->currency }}
</h1>
</div> 
</div> 
<div class="row">

@foreach(App\Traits\Decode::Items($orders->items) as $row=>$post)
<div class="col-md-12 row" style="border:1px solid #e0e0e0;text-align:left;padding:10px;">
<div class="col-md-3">
<img src="{{ $post->image }}" style="width:100%;max-width:200px;height:200px;">
</div>
<div class="col-md-9">
<p class="textblod">Product name : <span class="textnormal">{{ $post->title }}</span></p>
<p class="textblod">Quantity : <span class="textnormal">{{ $post->qty }}</span></p>
<p class="textblod">Amount : <span class="textnormal">{{ $post->price }}</span></p>
<p class="textblod">Total s : <span class="textnormal">{{ $orders->total_amount }} {{ $orders->currency }}</span></p>

</div>
</div>
<div class="col-md-12" style="height:10px;"></div>

@endforeach
</div>
@endsection