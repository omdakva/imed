@extends('layout.store2')
@section('title', 'Edit Profile')
@section('content')

<div class="containerdash">
       <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Dashbord</a></li>
                <li class="breadcrumb-item"><a href="{{route('user.profile')}}">Profile</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
            </ol>
        </nav>
<h1 class="mt-4">Edit profile</h1>
<div class="container">
<div class="row">
<div class="col-md-12 col-lg-12 mx-auto">



<form action = "{{ url('user/edit') }}" method = "post">
         <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
		 <input type = 'hidden' name = 'updated_at' class="form-control" value = '<?php echo date('Y-m-d'); ?> <?php echo date('h:i:s'); ?>'/>

         <table style="width:60%;">
            <tr>
               <td style="width:20%;">First name</td>
               <td >
                  <input type = 'text' name = 'firstname' class="form-control" value = '<?php echo$users[0]->firstname; ?>'/>
               </td>
            </tr>
			
			<tr>
               <td style="width:20%;">Last name</td>
               <td >
                  <input type = 'text' name = 'lastname' class="form-control" value = '<?php echo$users[0]->lastname; ?>'/>
               </td>
            </tr>
			 
			 <tr>
               <td>Email</td>
               <td>
                  <input type = 'text' name = 'email' class="form-control" value = '<?php echo$users[0]->email; ?>'/>
               </td>
            </tr>
			
			<tr>
               <td>Country</td>
               <td>
                  <input type = 'text' name = 'country' class="form-control" value = '<?php echo$users[0]->country; ?>'/>
               </td>
            </tr>
			<tr>
               <td>City</td>
               <td>
                  <input type = 'text' name = 'city' class="form-control" value = '<?php echo$users[0]->city; ?>'/>
               </td>
            </tr>
			
            <tr>
               <td colspan = '2'>
                  </br><input type = 'submit' class="btn btn-primary"value = "Update" />
               </td>
            </tr>
         </table>
      </form>
	  
	  </div>
	  </div>
        </div>
        </div>
      
@endsection