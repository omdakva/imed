@extends('layout.store2')
@section('title', 'Profile')
@section('content')
<div class="containerdash">
<nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Dashbord</a></li>
<li class="breadcrumb-item active" aria-current="page">Profile</li>
</ol>
</nav>

<div class="row">
<div class="col-sm-2">
<img src="{{ asset('public/profile_pic/user/'.ucfirst(Auth()->user()->image)) }}" style="width:100px;height:100px;border-radius:50%;">
</div>
<div class="col-sm-10">
<h1 class="mt-4">My profile</h1>
<label class="textblodfull">Name : <span class="textnormal">{{ ucfirst(Auth()->user()->firstname) }} {{ ucfirst(Auth()->user()->lastname) }}</span></label>
<label class="textblodfull">Email : <span class="textnormal">{{ ucfirst(Auth()->user()->email) }}</span></label>
<label class="textblodfull">Country : <span class="textnormal">{{ ucfirst(Auth()->user()->country) }}</span></label>
<label class="textblodfull">City : <span class="textnormal">{{ ucfirst(Auth()->user()->city) }}</span></label>
<label class="textblodfull">Birthday : <span class="textnormal">{{ ucfirst(Auth()->user()->Birthday) }}</span></label>
<p class="lastinfo">Last update : {{ ucfirst(Auth()->user()->updated_at) }}</p>
<a href="{{ url('user/edit') }}" class="btn btn-primary">
Edite
</a>
</div>
</div>

</div>
@endsection