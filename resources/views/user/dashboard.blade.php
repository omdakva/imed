@extends('layout.store2')
@section('title', 'STORE-TEC - Dashbord')
@section('content')
</br>
</br>
</br>
</br>
<h1 class="mt-4">Dashbord</h1>
  <div class="container bootstrap snippet">
  <div class="row">
  <div class="col-md-9 row" style="padding:0px;">

    <div class="col-lg-4 col-sm-4">
      <div class="circle-tile ">
        <a href="#"><div class="circle-tile-heading blue"><i class="fa fa-paper-plane fa-fw fa-3x"></i></div></a>
        <div class="circle-tile-content blue">
          <div class="circle-tile-description text-faded"> Requests</div>
          <div class="circle-tile-number text-faded ">{{ $myorders }}</div>
          <a class="circle-tile-footer" href="{{route('user.request')}}">View More <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>
     
    <div class="col-lg-4 col-sm-4">
      <div class="circle-tile ">
        <a href="#"><div class="circle-tile-heading red"><i class="fa fa-list-alt fa-fw fa-3x"></i></div></a>
        <div class="circle-tile-content red">
          <div class="circle-tile-description text-faded"> Items </div>
          <div class="circle-tile-number text-faded ">{{ $myitems }}</div>
          <a class="circle-tile-footer" href="{{route('cart')}}">View More <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div> 
	
	    <div class="col-lg-4 col-sm-4">
      <div class="circle-tile ">
        <a href="#"><div class="circle-tile-heading green"><i class="fa fa-check fa-fw fa-3x"></i></div></a>
        <div class="circle-tile-content green">
          <div class="circle-tile-description text-faded"> Purchases </div>
          <div class="circle-tile-number text-faded ">0</div>
          <a class="circle-tile-footer" href="{{route('cart')}}">View More <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div> 
  

<div class="col-md-3">
<p class="dashhometitre">Details</p>
<p class="textblod">Email : <span class="textnormal">{{ ucfirst(Auth()->user()->email) }}</span></p>
<p class="textblod">Birthday : <span class="textnormal">{{ ucfirst(Auth()->user()->birthday) }}</span></p>
<p class="textblod">Gender : 
	  @if(ucfirst(Auth()->user()->gender) =='1')         
      <span class="textnormal">Male</span>       
      @else
      <span class="textnormal">Female</span>   
      @endif
</p>

</div>

  </div> 
</div>








 
<div class="row">
<div class="col-md-7">
<div class="dashhome">
<p class="dashhometitre">View your address</p>
<p>
Change password now
</p>
<a id="dashhomelink" href="{{route('user.address')}}">View more</a>
</div>

<div class="dashhome">
<p class="dashhometitre">Change password</p>
<p>
Change password now
</p>
<a id="dashhomelink" href="{{route('change.password.view')}}">View more</a>
</div>
<div class="dashhome">
<p class="dashhometitre">Change profile photo</p>
<p>
Change your profile photo now
</p>
<a id="dashhomelink" href="{{route('change.pic.view')}}">View more</a>
</div>
</div>

<div class="col-md-5">
<img src="{{ asset('image/vector/img02.jpg') }}" style="max-width:100%;height:300px;">
</div>
</div>


@endsection