@extends('layout.store2')
@section('title', 'Setting')
@section('content')

<div class="containerdash">
       <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Dashbord</a></li>
                <li class="breadcrumb-item active" aria-current="page">Setting</li>
            </ol>
        </nav>
<h1 class="mt-4">Setting</h1>
<div class="border_list">
<p>Edite your password</p>
<a href="{{ route('change.password.view') }}" class="btn btn-primary">
Change password
</a>
</div>
<div class="border_list">
<p>Edite your profile photo</p>

<img src="{{ asset('public/profile_pic/user/'.ucfirst(Auth()->user()->image)) }}" style="width:100px;height:100px;border-radius:50%;"></br></br>
<a href="{{ route('change.pic.view') }}" class="btn btn-primary">
Change photo
</a>
</div>
</div>
@endsection