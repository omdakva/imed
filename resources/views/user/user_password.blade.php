@extends('layouts.user.dashbord')
@section('title', 'Changing the default title')
@section('content')
<h1 class="mt-4">Edite my password</h1>
<div class="container">
<div class="row">


<div class="col-md-12 col-lg-12 mx-auto">
<form action = "{{ url('user/edit') }}" method = "post">
         <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
		 
         <table style="width:60%;">
            <tr>
               <td style="width:20%;">Name</td>
               <td >
                  <input type = 'text' name = 'name' class="form-control" value = '<?php echo$users[0]->name; ?>'/>
               </td>
            </tr>
			 
			 <tr>
               <td>Email</td>
               <td>
                  <input type = 'text' name = 'email' class="form-control" value = '<?php echo$users[0]->email; ?>'/>
               </td>
            </tr>
			
			<tr>
               <td>Country</td>
               <td>
                  <input type = 'text' name = 'country' class="form-control" value = '<?php echo$users[0]->country; ?>'/>
               </td>
            </tr>
            <tr>
               <td colspan = '2'>
                  <input type = 'submit' class="btn btn-primary"value = "Update" />
               </td>
            </tr>
         </table>
      </form>
	  
	  </div>
	  </div>
        </div>
      
@endsection