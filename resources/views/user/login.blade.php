@extends('layout.store2')

@section('title', 'STORE-TEC - Login')

@section('content')
</br>
</br>
</br>
<div class="container-fluid">
<div class="row no-gutter">
	<div class="d-none d-md-flex col-md-3 col-lg-3 bg-image"></div>


	
    <div class="col-md-6 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">

            <div class="col-md-12 col-lg-12 mx-auto">
              <h3 class="login-heading mb-4">Welcome back!</h3>
               <form action="{{url('login')}}" method="POST" id="logForm">

                 {{ csrf_field() }}

                <div class="form-label-group">
                        <label for="inputEmail">Email address</label>
                         <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" >

                  @if ($errors->has('email'))
                  <span class="error">{{ $errors->first('email') }}</span>
                  @endif    
                </div> 
                </br>
                <div class="form-label-group">
                <label for="inputPassword">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
                  
                  @if ($errors->has('password'))
                  <span class="error">{{ $errors->first('password') }}</span>
                  @endif  
                </div>
                 </br>
                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Sign In</button>
                <div class="text-center">If you have an account?
                  <a class="small" href="{{route('user.registration')}}">Sign Up</a></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
	<div class="d-none d-md-flex col-md-3 col-lg-3 bg-image"></div>

  </div>
</div>
@endsection
