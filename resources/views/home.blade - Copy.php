@extends('layout.store')

@section('title', 'STORE-TEC - Home')

@section('content')

<section class="text-center mb-4" style="margin-top:20px;">
<div class="col-sm-12 col-md-12" style="padding:0px;">
<img src="{{ asset('image/produts/160289887350e8778fdf61ba1e4bea0d3b16fe5df5.jpg') }}" style="width:100%;">
</div>
</section>





<section class="text-center mb-4" style="margin-top:0px;">
<div class="col-sm-12 col-md-12" style="padding:0px;">
<div style="padding:0px;text-align:center;">
<span class="text_title_home_blod">
New collections
</span>
</div>
<img src="{{ asset('image/produts/1999.jpg') }}" style="width:100%;margin-top:20px;">
</div>

<div class="row wow fadeIn" style="margin-top:20px;">
<div class="container products">

        <div class="row">

            @foreach($products_women as $product)
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                        <a href="{{route('store.show', $product->id)}}">
						<img src="{{ asset('image/produts/'.$product->photo) }}" style="">
						</a>
                        <div class="caption">
                            <h5 style="height:60px;">{{ $product->name }}</h5>
                            <p><strong>Price: </strong> {{ $product->price }}$</p>
                            <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn nav-link border border-light rounded waves-effect text-center" role="button">Add to cart <i class="fa fa-shopping-cart" aria-hidden="true"></i></a> </p>
                        </div>
				
                    </div>
                </div>
            @endforeach

        </div><!-- End row -->

    </div>
	</div>
<!--Grid row-->
</section>








<section class="col-sm-12 col-md-12" style="padding:0px;">
<div class="row">
<div class="col-sm-6 col-md-6">
<div style="padding:0px;text-align:center;">
<span class="text_title_home_light">
The Women Collection
</span>
</div>
<img src="{{ asset('image/produts/women.jpg') }}" style="width:100%;min-width:200px;height:550px;margin-top:10px;">
</div>

<div class="col-sm-6 col-md-6">
<div style="padding:0px;text-align:center;">
<span class="text_title_home_light">
The Men Collection
</span>
</div>
<img src="{{ asset('image/produts/men.jpg') }}" style="width:100%;min-width:200px;height:550px;margin-top:10px;">
</div>
</div>
</section>













<section class="text-center mb-4" style="margin-top:20px;">
<div style="padding:0px;text-align:center;">
<span class="text_title_home_blod">
Buy by Trend
</span>
</div>
<div class="row wow fadeIn">
<div class="container products">

        <div class="row">

            @foreach($products_men as $product)
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                        <a href="{{route('store.show', $product->id)}}">
						<img src="{{ asset('image/produts/'.$product->photo) }}" style="">
						</a>
                        <div class="caption">
                            <h5 style="height:60px;">{{ $product->name }}</h5>
                            <p><strong>Price: </strong> {{ $product->price }}$</p>
                            <p class="btn-holder"><a  id="add" data-id="{{ $product->id }}" class="btn nav-link border border-light rounded waves-effect text-center" role="button">Add to cart <i class="fa fa-shopping-cart" aria-hidden="true"></i></a> </p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div><!-- href="{{ url('add-to-cart/'.$product->id) }}" End row -->

    </div>
	</div>
<!--Grid row-->
</section>

<script>
$(document).ready(function() {
        $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });

        $('#add').click( function() {
            var id = $(this).data('id');
            var url = "add-to-cart/9";



            $.ajax({

            type: "POST",
            url: url,
            data: { id: id },
            success: function (data) {
            console.log(data);

            },
            error: function (data) {
            console.log('Error:', data);
            }
            });
        });
    });
</script>

@endsection
