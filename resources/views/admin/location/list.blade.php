@extends('layouts.admin')
@section('title', 'Dluxxis location')
@section('content')

<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis location</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Dluxxis location</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Location</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('category.create') }}" class="btn-switch" title="Excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a> 
<a href="{{ route('category.create') }}" class="btn-switch" title="PDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> 
@if(Auth::user()->role=='3')
@else
<a href="{{route('location.create')}}" class="btn btn-success mb-2" title="PDF">Add new</a> 
@endif
</div>
</div>
<div class="space"></div>
<div class="space"></div>





<div class="row">
<div class="col-12">
@if(session()->get('success'))
<div class="alert alert-success mt-3">
{{ session()->get('success') }}  
</div>
@endif 



<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Type</th>
<th>Country</th>
<th>City</th>
<th>Address</th>
<th style="text-align:center;">Etat</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Location as $post)


<tr id="post_id_{{ $post->id }}">
<td>
@if($post->location_type =='1')
<span class="active_back">Location principle</span>
@else
<span class="blocked_back">Location secondary</span>
@endif
</td>

<td>{{ $post->country  }}</td>
<td>{{ $post->city  }}</td>
<td>{{ $post->address  }}</td>
<td style="text-align:center;">
@if($post->active =='1')
<span class="active_back">published</span>
@else
<span class="blocked_back">Not published</span>
@endif
</td>
<td class="td_btn">
<a href="{{ route('location.show', $post->id) }}" class="btn-delete"><i class="fa fa-eye"></i></a>
@if(Auth::user()->role=='3')
@else
<a href="{{ route('location.edit', $post->id) }}" class="btn-delete"><i class="fa fa-edit"></i></a>
@endif
</td>
</tr>
@endforeach
</tbody>
</table>        

       </div> 
    </div>
</div>



<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection