@extends('layouts.admin')
@section('title', 'Dluxxis location')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis location</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('location') }}">Dluxxis location /</a>
<a id="pagetitleprimerlink" href="{{ route('location.list') }}">location /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new location</h3>
</div>
<div class="row">
<div class="col-lg-12 cadre_filter">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
	
<form id="file-upload-form" class="uploader" action="{{ route('location.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="active" id="active" value="0">

<div class="row">

<div class="form-group col-md-4">
<label for="name" class="col-sm-12 control-label textblod">Location type</label>
<div class="col-sm-12">



<select type="text" class="form-control" id="location_type" name="location_type" value="" required="">
<option value=""></option>
<option value="1" @if(count($Location)=='1')
disabled style="color:#c0c0c0;"
@else
@endif>Location principle</option>
<option value="2">Location secondary</option>
</select>
</div>
</div>

<div class="form-group col-md-4">
<label for="name" class="col-sm-12 control-label textblod">Country</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="country" name="country" value="" required="">
</div>
</div>

<div class="form-group col-md-4">
<label for="name_de" class="col-sm-12 control-label textblod">City</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="city" name="city" value="" required="">
</div>
</div>

<div class="form-group col-md-6">
<label for="name_de" class="col-sm-12 control-label textblod">Address</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="address" name="address" value="" required="">
</div>
</div>

<div class="form-group col-md-6">
<label for="name_de" class="col-sm-12 control-label textblod">Postcode</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="postcode" name="postcode" value="" required="">
</div>
</div>


<div class="form-group col-md-6">
<label for="name_de" class="col-sm-12 control-label textblod">Email</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="email" name="email" value="" required="">
</div>
</div>


<div class="form-group col-md-6">
<label for="name_de" class="col-sm-12 control-label textblod">Tel</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="tel" name="tel" value="" required="">
</div>
</div>



<div class="form-group col-md-6">
<label for="name_de" class="col-sm-12 control-label textblod">Mobile</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="mobile" name="mobile" value="" required="">
</div>
</div>
</div>

            
<div class="modal-footer"style="text-align:right;">
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>
</div>
</div>

</BR>


<script src="{{ asset('public/jquery/example/jQuery.maxlength.js')}}"></script>

<script>
$('div.form-group-max').maxlength();
</script>
<script>
$(document).ready(function () {
    $(".text").hide();
    $("#r1").click(function () {
        $(".text").show();
    });
	
    $("#r2").click(function () {
        $(".text").hide();
    });
	
	$(".text2").hide();
    $("#r1").click (function () {
        $(".text2").hide();
    });
	
    $("#r2").click(function () {
        $(".text2").show();
    });
});
</script>


<script>
   function loadPreview(input){
       var data = $(input)[0].files; //this file data
       $.each(data, function(index, file){
           if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
               var fRead = new FileReader();
               fRead.onload = (function(file){
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image thumb element
                       $('#thumb-output').append(img);
                   };
               })(file);
               fRead.readAsDataURL(file);
           }
       });
   }
</script>
@endsection