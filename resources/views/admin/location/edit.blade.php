@extends('layouts.admin')
@section('title', 'Dluxxis location')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis location</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('location.list') }}">Dluxxis location /</a>
<a id="pagetitlesecondelink">Edit</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Edit location </span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('location.list') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="Individual cadre_add">
<form action = "{{ route('location.update', $Location->id) }}" method = "POST">
@csrf
@method('PATCH') 

<div class="row">
<h3 class="page-title2" >Loaction</h3>



<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Location type</label>
<select type="text" class="form-control" id="location_type" name="location_type" value="" required="">
<option value="1" @if($Location->location_type=='1')
SELECTED
@else
@endif >Location principle</option>

<option value="2" @if($Location->location_type=='2')
SELECTED
@else
@endif>Location secondary</option>
</select>
</div>

<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Country</label>
<input type="text" name="country" class="form-control" value="{{$Location->country}}" />
</div>


<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">City</label>
<input type="text" name="city" class="form-control" value="{{$Location->city}}" />
</div>

<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Address</label>
<input type="text" name="address" class="form-control" value="{{$Location->address}}" />
</div>


<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Postcode</label>
<input type="text" name="postcode" class="form-control" value="{{$Location->postcode}}" />
</div>




<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Email</label>
<input type="text" name="email" class="form-control" value="{{$Location->email}}" />
</div>


<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Tel</label>
<input type="text" name="tel" class="form-control" value="{{$Location->tel}}" />
</div>


<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Mobile</label>
<input type="text" name="mobile" class="form-control" value="{{$Location->mobile}}" />
</div>


<div class="form-group col-md-6">
<label for="post-description" style="width:100%;" class="textblod">Activation</label>
<label for="r2" class="textnormal">
<input type="radio" name="active" id="r2"onClick="getResults()" value="1" 
@if($Location->active == 1)
checked  
@else
@endif > Active
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="active" id="r1" onClick="getResults()" value="0"
@if($Location->active == 0)
checked  
@else
@endif> Blocked
</label>		
</div>




</div>


<div class="modal-footer"style="text-align:right;">
<a href="{{ route('location.show', $Location->id) }}" class="model_btn_close">Cancel</a>
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection