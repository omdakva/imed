@extends('layouts.admin')
@section('title', 'Dluxxis location')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis location</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('location.list') }}">Dluxxis location /</a>
<a id="pagetitlesecondelink">Details</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Location details</span>
</div>
<div class="col-md-5" style="text-align:right;">
@if(Auth::user()->role=='3')
@else
<a href="javascript:void(0)" data-toggle="modal" data-target="#delete" class="btn btn-success mb-2">Delete</a> 
<a href="{{ route('location.edit', $Location->id) }}" class="btn btn-success mb-2" id="create-new-post">Edit</a> 
@endif
<a href="{{ route('location.list') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
<form action = "{{ route('location.destroy', $Location->id) }}" method = "POST">
@csrf
@method('PATCH') 
<button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
<button type="submit" class="model_btn_delete delete-post">Yes, Delete</button>
</form>
</div>
    </div>
  </div>
</div>





<div class="row">

<div class="space"></div>

<div class="col-lg-6 col-sm-12 mb-12">
<div class="card card-small h-100">
<div class="card-header border-bottom">
<h6 class="m-0">Information</h6>
</div>
<div class="space"></div>
<div class="card-body d-flex py-0">
<div class="form-group col-md-12">
<label class="textblodfull">Country : <span class="textnormal">{{ $Location->country }}</span></label>
<label class="textblodfull">City : <span class="textnormal">{{ $Location->city }}</span></label>
<label class="textblodfull">Address : <span class="textnormal">{{ $Location->address }}</span></label>
<label class="textblodfull">Postcode : <span class="textnormal">{{ $Location->postcode }}</span></label>
<label class="textblodfull">Localisation etat : 
<span class="textnormal">
@if($Location->active =='1')
<span class="active_back">published</span>
@else
<span class="blocked_back">Not published</span>
@endif
</span>
</label>
</div>
</div>
<div class="card-header border-top text-center">
<a href="javascript:void(0)" data-toggle="modal" data-target="#localisation" class="btn btn-success mb-2" id="create-new-post">Show Localisation</a> 
</div>
</div>
</div>
			  
	






<div class="modal fade" id="localisation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-body">
<h5 id="exampleModalLabel" class="textbloddelete">Localisation</h5>
<div class="mapouter">
<div class="gmap_canvas">
<iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q={{ $Location->address }}&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</div>
<style>.mapouter{position:relative;text-align:right;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}</style>
</div>

<div class="space"></div>

</div>

<div class="modal-footer">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>




		

<div class="col-lg-6 col-sm-12 mb-12">
<div class="card card-small h-100">
<div class="card-header border-bottom">
<h6 class="m-0">Contact</h6>
</div>
<div class="space"></div>
<div class="card-body d-flex py-0">
<div class="form-group col-md-12">
<label class="textblodfull">Email : <span class="textnormal">{{ $Location->email }}</span></label>
<label class="textblodfull">Tel : <span class="textnormal">{{ $Location->tel }}</span></label>
<label class="textblodfull">Mobile : <span class="textnormal">{{ $Location->mobile }}</span></label>
</div>
</div>
<div class="card-header border-top text-center">
</div>
</div>
</div>	

</div>

	

</div>
</div>
</div>


@endsection