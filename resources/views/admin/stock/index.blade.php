@extends('layouts.admin')
@section('title', 'Manage stock')
@section('content')
<script src="{{ asset('public/ajax/libs/jquery.validate.js')}}"></script>




<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage stock</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Stock</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Stock</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="" class="btn-switch" title="Excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a> 
<a href="" class="btn-switch" title="PDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> 
<a href="{{ route('product.index') }}" class="btn btn-success mb-2" id="create-new-post">View product</a> 
</div>
</div>



<div class="space"></div>
<div class="space"></div>


<div class="row">
<div class="col-12">
@if(session()->get('success'))
<div class="alert alert-success mt-3">
{{ session()->get('success') }}  
</div>
@endif 



<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Name</th>
<th>Price</th>
<th>In stock</th>
<th style="text-align:center;">Etat</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Product as $post)

<tr id="post_id_{{ $post->id }}">
<td> <img class="user-avatar rounded-circle mr-2" src="{{ asset('photo/products_logo/'.$post->photo) }}" style="width:40px;height:40px;border-radius:50%;"alt="User Avatar">
{{ $post->product_name  }}</td>
<td>{{ $post->product_price  }} {{ $post->product_currency  }}</td>
<td>
@if($post->stock =='0')
<span class="blocked_back">{{ $post->stock  }}</span>
@else
<span class="">{{ $post->stock  }}</span>
@endif
</td>
<td style="text-align:center;">
@if($post->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif
</td>
<td class="td_btn" style="text-align:center;">
@if(Auth::user()->role=='3')
<a class="btn-delete"><i class="fa fa-lock"></i></a>
@else
<a href="{{ route('stock.edit', $post->id) }}" class="btn-delete"><i class="fa fa-edit"></i></a>
@endif
</td>
</tr>
@endforeach
</tbody>
</table>        

</div> 
</div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection