@extends('layouts.admin')
@section('title', 'Manage stock')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage stock</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Stock</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Edit stock</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('stock.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="Individual cadre_add">
<form action = "{{ route('stock.update', $Product->id) }}" method = "POST">
@csrf
@method('PATCH') 

<input type="hidden" name="image" value="0c3b3adb1a7530892e55ef36d3be6cb8.png" />
<input type="hidden" name="type" value="1" />
<div class="row">
<h3 class="page-title2" >Product details</h3>
<input type="hidden" name="product_name" class="form-control" value="{{$Product->product_name}}"/>
<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Stock</label>
<input type="number" name="stock" class="form-control" value="{{$Product->stock}}" min="0"/>
</div>







</div>


<div class="modal-footer"style="text-align:right;">
<a href="{{ route('stock.index') }}" class="model_btn_close">Cancel</a>
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection