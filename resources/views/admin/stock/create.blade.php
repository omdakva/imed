@extends('layouts.admin')
@section('title', 'Manage product')
@section('content')

<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage product</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('product.index') }}">Product /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<div class="col-lg-12">		
<h3 class="page-title" style="padding:0px;">Add new product</h3>
</div>
</div>



<div class="row">
<div class="col-lg-12">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
	
<form id="file-upload-form" class="uploader" action="{{ route('product.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
<input type="hidden" value="1" name="active">		
<input type="hidden" value="1" name="statut">		
		
		
<div class="row">
<div class="col-lg-7">		
<div class="col-lg-12 cadre_filter">		

<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Category</label>
<div class="col-sm-12">
<select type="text" class="form-control" id="category_id" name="category_id" value="" required="">
<option value=""></option>
@foreach($Category as $post_category)
<option value="{{ $post_category->id  }}">{{ $post_category->name  }}</option>
@endforeach

</select>
</div>
</div>


<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Name</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="name" name="name" value="" required="">
</div>
</div>

						
<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Description</label>
<div class="col-sm-12">
<textarea type="text" class="form-control" id="long_description" name="long_description" style="height:170px;" value="" required=""></textarea>
</div>
</div>


<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Short description</label>
<div class="col-sm-12">
<textarea type="text" class="form-control" id="short_description" name="short_description" style="height:170px;" value="" required=""></textarea>
</div>
</div>


</div>
</div>

<div class="col-lg-5">
<div class="col-lg-12 cadre_filter">
<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Price</label>
<div class="col-sm-7">
<input type="number" class="form-control" id="product_price" name="product_price" value="" required="">
</div>
<div class="col-sm-5">
<select type="number" class="form-control" id="product_currency" name="product_currency" value="" required="">
<option value="€">€</option>
<option value="$">$</option>
</select>
</div>
</div>

<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Stock</label>
<div class="col-sm-12">
<input type="number" class="form-control" id="stock" name="stock" value="" required="">
</div>
</div>


<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Image</label>
<div class="col-sm-12">
<input type="file" class="form-control" id="photo" name="photo">
</div>
</div>


<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Remise</label>
<div class="col-sm-12">
<input type="number" class="form-control" id="remise" name="remise" value="" required="">
</div>
</div>
</div>
</div>
</div>
  
<div class="space"></div>
<div class="space"></div>



<div class="row">
<div class="col-lg-12">		
<div class="col-lg-12 cadre_filter" style="text-align:right;">
<button type="submit" class="model_btn_save">Save</button>
</div>
</div>
</div>

</form>
</div>
</div>
</div>

</BR>


<script src="{{ asset('public/jquery/example/jQuery.maxlength.js')}}"></script>

<script>
$('div.form-group-max').maxlength();
</script>
<script>
$(document).ready(function () {
    $(".text").hide();
    $("#r1").click(function () {
        $(".text").show();
    });
	
    $("#r2").click(function () {
        $(".text").hide();
    });
	
	$(".text2").hide();
    $("#r1").click (function () {
        $(".text2").hide();
    });
	
    $("#r2").click(function () {
        $(".text2").show();
    });
});
</script>


<script>
   function loadPreview(input){
       var data = $(input)[0].files; //this file data
       $.each(data, function(index, file){
           if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
               var fRead = new FileReader();
               fRead.onload = (function(file){
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image thumb element
                       $('#thumb-output').append(img);
                   };
               })(file);
               fRead.readAsDataURL(file);
           }
       });
   }
</script>
@endsection