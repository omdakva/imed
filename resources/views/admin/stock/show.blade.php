@extends('layouts.admin')
@section('title', 'Manage product')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage product</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">product</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Product details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('product.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="Individual cadre_add">
<div class="row">
<div class="col-md-8">



<div class="cadre_add">
<label class="page-title2" >Details</label>
<label class="textblodfull">Name : <span class="textnormal">{{ $Product->product_name }}</span></label>
<label class="textblodfull">Activation : <span class="textnormal">@if($Product->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></label>
<label class="textblodfull">Creation date : <span class="textnormal">{{ $Product->created_at }}</span></label>
<label class="textblodfull">Update date : <span class="textnormal">{{ $Product->updated_at }}</span></label>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2" >Description</label>
<label class="textblodfull"><span class="textnormal">{{ $Product->long_description }}</span></label>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2" >Short Description</label>
<label class="textblodfull"><span class="textnormal">{{ $Product->short_description }}</span></label>
</div>
</div>

<div class="col-md-4">
<div class="cadre_add ">


<div class="card-header border-bottom text-center">
<label class="textblodfull" style="font-size:25px;">{{ $Product->product_price }} {{ $Product->product_currency }}</span></label>
<div class="mb-3 mx-auto">
<img class="rounded-circle" src="{{ asset('image/products_logo/'.$Product->photo) }}" alt="User Avatar" width="150" height="160"> </div>
<h4 class="mb-0">@if($Product->type =='1')  
{{ $Product->firstname }} {{ $Product->lastname }}
@else
{{ $Product->company_name }}
@endif</h4>
<span class="text-muted d-block mb-2">{{ $Product->email }}</span>
<a href="{{route('change.pic.product')}}" class="btn_add"> <i class="material-icons mr-1">person_add</i>Edite pic</a> 
</div>
<div class="space"></div>
<div class="space"></div>

<label class="textblodfull">In stock : <span class="textnormal">{{ $Product->stock }}</span></label>
<label class="textblodfull">Remise : <span class="textnormal">{{ $Product->remise }} %</span></label>
<label class="textblodfull">Remise amount : <span class="textnormal">{{ ($Product->remise / 100) * $Product->product_price}} {{ $Product->product_currency }}</span></label>
<label class="textblodfull">Price after remise : 
<span style="font-size:35px;" class="textnormal">{{ (($Product->product_price) - (($Product->remise / 100) * $Product->product_price)) }} {{ $Product->product_currency }}</span></label>


</div>
</div>

</div>



</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection