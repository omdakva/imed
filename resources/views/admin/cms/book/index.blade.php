@extends('layouts.admin')
@section('title', 'Dluxxis cms')
@section('content')

<script type="text/javascript" src="https://code.jquery.com/jquery-1.4.4.min.js"></script>
		<script src="https://aymenaboudi.tn/book/public/booklet/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="https://aymenaboudi.tn/book/public/booklet/jquery.booklet.1.1.0.min.js" type="text/javascript"></script>

		<link href="https://aymenaboudi.tn/book/public/booklet/jquery.booklet.1.1.0.css" type="text/css" rel="stylesheet" media="screen" />
		<link rel="stylesheet" href="https://aymenaboudi.tn/book/public/css/style.css" type="text/css" media="screen"/>
        


<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >D'luxxis cms</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Dluxxis cms /</a>
<a id="pagetitlesecondelink">Book reader</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 10px 0px;">
<div class="col-md-12">
<span class="mt-4 pagetitle" >Book reader</span>
</div>
</div>

<h1 class="title" style="text-align:center;">D'luxxis</h1>
<div class="container" style="padding:10px;">
<div class="book_wrapper" style="background-color:#ffffff;">
<a id="next_page_button"></a>
<a id="prev_page_button"></a>
<div id="loading" class="loading">Loading pages...</div>
<div id="mybook" style="display:none;">
<div class="b-load">
			


<div style="text-align:center;margin-top:180px;">
<h2 style="padding:13px;font-size:30px;">D'luxxis</h2>
<img src="{{ asset('public/photo/logo/my-shop-logo-1583225903.jpg')}}">
<p style="margin-top:50px;">2021</p>
</div>
			

<div style="text-align:center;margin-top:50px;">
<h2 style="padding:13px;font-size:20px;">Introduction</h2>
<p style="font-size:15px;line-height:1.4;">
 Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit. 2050 about admin test Lorem ipsum dolor sit amet consectetur.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsumdicta impedit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
</p>
</div>
			
@foreach($Category as $category)
<div>
<h2 class="text-center">
{{ $category->name  }}
</h2>
<div>
<img id="img_book" src="{{ asset('public/photo/category_logo/'.$category->photo) }}"alt="category logo">
</div>
<p style="font-size:15px;line-height:1.4;">
@if(count($category->subcategory))
@include('layout.subCategorybook',['subcategory' => $category->subcategory])
@endif
</p>
</div>
@endforeach





				
					
					
<div style="text-align:center;margin-top:50px;">
<img src="https://aymenaboudi.tn/book/public/images/14.jpg" style="width:0px;height:0px;" />
<h2 style="padding:13px;font-size:20px;">Conclusion</h2>
<p style="font-size:15px;line-height:1.4;">
 Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit. 2050 about admin test Lorem ipsum dolor sit amet consectetur.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.Lorem ipsumdicta impedit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
</p></div>
				

				</div>
			</div>
		</div>
		</div>
        <div>

		</div>


        <script type="text/javascript">
			$(function() {
				var $mybook 		= $('#mybook');
				var $bttn_next		= $('#next_page_button');
				var $bttn_prev		= $('#prev_page_button');
				var $loading		= $('#loading');
				var $mybook_images	= $mybook.find('img');
				var cnt_images		= $mybook_images.length;
				var loaded			= 0;

				$mybook_images.each(function(){
					var $img 	= $(this);
					var source	= $img.attr('src');
					$('<img/>').load(function(){
						++loaded;
						if(loaded == cnt_images){
							$loading.hide();
							$bttn_next.show();
							$bttn_prev.show();
							$mybook.show().booklet({
								name:               null,                            // name of the booklet to display in the document title bar
								width:              970,                             // container width
								height:             763,                             // container height
								speed:              600,                             // speed of the transition between pages
								direction:          'LTR',                           // direction of the overall content organization, default LTR, left to right, can be RTL for languages which read right to left
								startingPage:       0,                               // index of the first page to be displayed
								easing:             'easeInOutQuad',                 // easing method for complete transition
								easeIn:             'easeInQuad',                    // easing method for first half of transition
								easeOut:            'easeOutQuad',                   // easing method for second half of transition

								closed:             true,                           // start with the book "closed", will add empty pages to beginning and end of book
								closedFrontTitle:   null,                            // used with "closed", "menu" and "pageSelector", determines title of blank starting page
								closedFrontChapter: null,                            // used with "closed", "menu" and "chapterSelector", determines chapter name of blank starting page
								closedBackTitle:    null,                            // used with "closed", "menu" and "pageSelector", determines chapter name of blank ending page
								closedBackChapter:  null,                            // used with "closed", "menu" and "chapterSelector", determines chapter name of blank ending page
								covers:             false,                           // used with  "closed", makes first and last pages into covers, without page numbers (if enabled)

								pagePadding:        10,                              // padding for each page wrapper
								pageNumbers:        true,                            // display page numbers on each page

								hovers:             false,                            // enables preview pageturn hover animation, shows a small preview of previous or next page on hover
								overlays:           false,                            // enables navigation using a page sized overlay, when enabled links inside the content will not be clickable
								tabs:               false,                           // adds tabs along the top of the pages
								tabWidth:           60,                              // set the width of the tabs
								tabHeight:          20,                              // set the height of the tabs
								arrows:             false,                           // adds arrows overlayed over the book edges
								cursor:             'pointer',                       // cursor css setting for side bar areas

								hash:               false,                           // enables navigation using a hash string, ex: #/page/1 for page 1, will affect all booklets with 'hash' enabled
								keyboard:           true,                            // enables navigation with arrow keys (left: previous, right: next)
								next:               $bttn_next,          			// selector for element to use as click trigger for next page
								prev:               $bttn_prev,          			// selector for element to use as click trigger for previous page

								menu:               null,                            // selector for element to use as the menu area, required for 'pageSelector'
								pageSelector:       false,                           // enables navigation with a dropdown menu of pages, requires 'menu'
								chapterSelector:    false,                           // enables navigation with a dropdown menu of chapters, determined by the "rel" attribute, requires 'menu'

								shadows:            true,                            // display shadows on page animations
								shadowTopFwdWidth:  166,                             // shadow width for top forward anim
								shadowTopBackWidth: 166,                             // shadow width for top back anim
								shadowBtmWidth:     50,                              // shadow width for bottom shadow

								before:             function(){},                    // callback invoked before each page turn animation
								after:              function(){}                     // callback invoked after each page turn animation
							});
							Cufon.refresh();
						}
					}).attr('src',source);
				});
				
			});
</script>
@endsection