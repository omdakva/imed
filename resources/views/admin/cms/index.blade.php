@extends('layouts.admin')
@section('title', 'Manage category')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis cms</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Dluxxis cms</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row">
<div class="space"></div>
<div class="space"></div>

<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('image/icons/envol.png')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">D'luxxis pages</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate D'luxxis pages
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{route('cms.pages')}}" class="btn_col2">View more</a>
</div>
</div>
</div>



<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('image/icons/home.webp')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Home page</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate home page
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{route('cms.home')}}" class="btn_col2">View more</a>
</div>
</div>
</div>


<div class="space"></div>
<div class="space"></div>



<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('image/icons/home.webp')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Manage image and favicon</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate image and favicon
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{route('cms.image-favicon')}}" class="btn_col2">View more</a>
</div>
</div>
</div>


<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
</div>
</div>
@endsection