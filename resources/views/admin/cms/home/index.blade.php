@extends('layouts.admin')
@section('title', 'Manage category')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis cms</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Dluxxis cms /</a>
<a id="pagetitlesecondelink">Home</a>
</div>
</div>





<div class="space"></div>
<div class="space"></div>



<div class="row">




<div class="col-md-6 col-sm-12 mb-12" onclick="window.location.href='{{ route('cms.home.slider') }}'">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/time_management.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3">Slider</h6>
<span class="stats-small__label text-uppercase textnormal">Users</span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>



<div class="col-md-6 col-sm-12 mb-12" onclick="window.location.href='{{ route('cms.about') }}'">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/treasure.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3">Who are we</h6>
<span class="stats-small__label text-uppercase textnormal">Users</span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>

<div class="space"></div>
<div class="space"></div>


<div class="col-md-6 col-sm-12 mb-12" onclick="window.location.href='{{ route('cms.catalogue') }}'">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/unexpected_friends.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3">Catalogue</h6>
<span class="stats-small__label text-uppercase textnormal">Users</span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>



<div class="col-md-6 col-sm-12 mb-12" onclick="window.location.href='{{ route('cms.our_article') }}'">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/usability_testing.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3">Our articles</h6>
<span class="stats-small__label text-uppercase textnormal">Users</span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>



</div>
@endsection