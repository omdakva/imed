@extends('layouts.admin')
@section('title', 'Dluxxis cms')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis cms</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Dluxxis cms /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.home') }}">Home /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.catalogue') }}">Catalogue /</a>
<a id="pagetitlesecondelink">Product</a>
</div>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Catalogue details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="javascript:void(0)" data-toggle="modal" data-target="#change_product" class="btn btn-success mb-2" id="create-new-post">Change product</a> 
<a href="{{ route('cms.catalogue') }}" class="btn btn-success mb-2" id="create-new-post">Back</a> 
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif

@foreach($Productlistview as $post)

<div class="modal fade" id="change_product" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Change product</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('catalogue.updateproduct', $post->id_catalogue) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<input type="hidden" name="id_catalogue" id="id_catalogue" value="{{ $post->id_catalogue }}">
<div class="form-group form-group-max">
<label for="title" class="textblod">Product list</label>
<select name="id_product" id="id_product" class="form-control" >
@foreach($Productlist as $postlist)
<option value="{{ $postlist->id }}" @if($postlist->id == $post->id_product)
SELECTED
@else
@endif >{{ $postlist->product_name }}</option>
@endforeach
</select>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>






<div class="row">
<div class="col-md-8">
<div class="cadre_add">	
<div class="col-md-12 mb-3 mx-auto">
<div class="space"></div>
<div class="space"></div>
<label class="textblodfull">Product : <span class="textnormal">{{ $post->product_name }}</span></label>
<label class="textblodfull">Activation : <span class="textnormal">@if($post->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></label>
<label class="textblodfull">Creation date : <span class="textnormal">{{ $post->created_at }}</span></label>
</div>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >Description 
</label>
<label class="textblodfull"><span class="textnormal">{{ $post->long_description }}</span></label>
</div>

<div class="space"></div>
<div class="space"></div>


<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >Short Description
</label>
<label class="textblodfull"><span class="textnormal">{{ $post->short_description }}</span></label>
</div>

</div>




<div class="col-md-4">
<div class="cadre_add">
<div class="card-header border-bottom text-center">
@if($post->remise=='0')
<label class="textblodfull" style="font-size:25px;">{{ $post->product_price }} {{ $post->product_currency }}</span></label>
@else
<label class="textnormalfull" style="font-size:20px;">{{ $post->product_price }} {{ $post->product_currency }}</span></label>
<label class="line_v1"></label>
<span style="font-size:35px;" class="textnormal">{{ (($post->product_price) - (($post->remise / 100) * $post->product_price)) }} {{ $post->product_currency }}</span>
@endif	

<div class="col-md-12 mb-3 mx-auto" style="text-align:center;">
<img src="{{ asset('public/photo/products_logo/'.$post->photo) }}" alt="User Avatar" style="width:90%;height:200px;"> 
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<label class="textblodfull">In stock : <span class="textnormal">{{ $post->stock }}</span></label>
<label class="textblodfull">Remise : <span class="textnormal">{{ $post->remise }} %</span></label>
<label class="textblodfull">Remise amount : <span class="textnormal">{{ ($post->remise / 100) * $post->product_price}} {{ $post->product_currency }}</span></label>
<label class="textblodfull">Price after remise : 
<span style="font-size:35px;" class="textnormal">{{ (($post->product_price) - (($post->remise / 100) * $post->product_price)) }} {{ $post->product_currency }}</span>
</label>

<div class="space"></div>
<div class="space"></div>
</div>
</div>

</div>
</div>
@endforeach
</div>
</div>
</div>

@endsection
