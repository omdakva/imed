@extends('layouts.admin')
@section('title', 'Dluxxis cms')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis cms</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Dluxxis cms /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.home') }}">Home /</a>
<a id="pagetitlesecondelink">About</a>
</div>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Who are we</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('cms.home') }}" class="btn btn-success mb-2" id="create-new-post">Back</a> 
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif


@foreach($Crm_about as $post)

<div class="modal fade" id="edit_text" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit text</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('cms.about.update', $post->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body" >
<input type="hidden" name="about_de" id="about_de" value="{{ $post->about_de }}">
<div class="form-group form-group-max">
<label for="title" class="textblod">Text</label>
<textarea name="about" id="about" class="form-control" style="height:280px;">{{ $post->about }}</textarea>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>






<div class="modal fade" id="edit_photo" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit image</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form  action = "{{ route('cms.about.updatephoto', $post->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<div class="row">
<div class="col-md-12" style="text-align:center;">
<img class="user-avatar mr-2" src="{{ asset('public/photo/about/'.$post->photo) }}" style="width:250px;height:150px;border-radius:0%;">
</div>
</div>

<div class="form-group form-group-max" style="height:70px;">
<label for="title" class="textblod">Edit photo</label>
<input type="file" name="photo" id="photo" class="form-control" required />
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>






<div class="row">
<div class="col-md-7">
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_text" class="btn_add"> <i class="fa fa-edit"></i> Edit</a> 
<div class="cadre_add">
<div class="space"></div>
<label class="textblodfull"><span class="textnormal">{{ $post->about }}</span></label>
</div>
</div>





<div class="col-md-5">
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_photo" class="btn_add"> <i class="fa fa-edit"></i> Edit</a> 

<div class="cadre_add " style="padding:0px;">
<div class="space"></div>
<div class="space"></div>
<div class="card-header border-bottom text-center" style="padding:5px;">
<img src="{{ asset('public/photo/about/'.$post->photo) }}" alt="Photo about" style="width:100%;height:260px;"> 
</div>
</div>
</div>

</div>





@endforeach
	

</div>
</div>









</div>





@endsection