@extends('layouts.admin')
@section('title', 'Dluxxis cms')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis cms</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Dluxxis cms /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.home') }}">Home /</a>
<a id="pagetitlesecondelink">Our articles</a>
</div>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Our articles</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('cms.home') }}" class="btn btn-success mb-2" id="create-new-post">Back</a> 
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif






@foreach($Our_article as $post)
<div class="modal fade" id="edit_text" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit description</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('cms.our_article.updatedescription', $post->id_article) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body" >
<div class="form-group form-group-max">
<label for="title" class="textblod">Description</label>
<textarea name="description" id="description" class="form-control" style="height:280px;">{{ $post->description }}</textarea>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>
@endforeach







<div class="row">
<div class="col-md-5 row" style="margin-left:0px;">
@foreach($Our_article as $post)
<div class="col-md-12">
<div class="card card-small padding10">
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_text" class="btn_add"> <i class="fa fa-edit"></i> Edit</a> 
<div class="space"></div>
<div class="space"></div>
<label class="textblodfull"><span class="textnormal" style="font-size:13px;">{{ $post->description }}</span></label>
</div>
</div>
@endforeach


<div class="space"></div>
<div class="space"></div>









<style>
.product_div {border:1px solid #f1f1f1;}
.product_div:hover {border:1px solid #f1f1f1;}
.product_button {width:40px;text-align:center;border:1px solid #f1f1f1;border-radius:50%;}

.product_div .product_incon{color:transparent;}
.product_div .product_button {text-align:center;border:1px solid transparent;background-color:transparent;color:#000000;padding:5px;-webkit-box-shadow: 0px 0px 0px 0px rgba(138,134,138,1);
-moz-box-shadow: 0px 0px 0px 0px rgba(138,134,138,1);
box-shadow: 0px 0px 0px 0px rgba(138,134,138,1);}

.product_div:hover .product_button {text-align:center;background-color:#edebeb;border:1px solid #edebeb;color:#000000;padding:5px;-webkit-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
-moz-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);}
.product_div:hover .product_incon{color:#000000;}
</style>




@include('admin.cms.home.our_article.product1')
@include('admin.cms.home.our_article.product2')
@include('admin.cms.home.our_article.product3')
@include('admin.cms.home.our_article.product4')



<div class="col-md-7">
<div class="card card-small padding10 product_div">
<div class="space"></div>
@if(count($productview)==0)
<div class="col-md-12" style="font-size:20px;">
<button href="javascript:void(0)" data-toggle="modal" data-target="#add_product_p" class="product_button floatright"><i class="fa fa-plus product_incon"></i></button>
</div>
<img src="{{ asset('public/photo/category_logo/image-not-found-99.png') }}" alt="Photo about" style="width:100%;height:300px;"> 
<div class="col-md-12 textcenter">Select product</div>
@else
@endif
@if(count($productview)==0)
@else
@endif
@foreach($productview as $p1)
<div class="modal fade" id="edit_product_p" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header" style="text-align:left;">
<h4 class="modal-title" id="postCrudModal">Edit product</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('cms.our_article.update_product_p', $post->id_article) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body" >
<div class="col-md-12 textcenter" style="font-size:20px;">{{ $p1->product_name }}</div>
<div class="card-header border-bottom text-center" style="padding:5px;">
<img src="{{ asset('public/photo/products_logo/'.$p1->photo) }}" alt="Photo about" style="width:100%;height:200px;"> 
</div>
<div class="form-group form-group-max">
<label for="title" class="textblod">Product</label>
<select name="id_product_p" id="id_product_p" class="form-control" >
@foreach($Productlist as $postlist)
<option value="{{ $postlist->id }}" @if($postlist->id == $p1->id_product_p)
SELECTED
@else
@endif >{{ $postlist->product_name }}</option>
@endforeach
</select>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>



<div class="col-md-12" style="font-size:20px;">{{ $p1->product_name }}
<button href="javascript:void(0)" data-toggle="modal" data-target="#edit_product_p" class="product_button floatright"><i class="fa fa-edit product_incon"></i></button>
</div>
<div class="card-header border-bottom text-center" style="padding:5px;">
<img src="{{ asset('public/photo/products_logo/'.$p1->photo) }}" alt="Photo about" style="width:100%;height:700px;"> 
</div>
@endforeach

<div class="modal fade" id="add_product_p" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header" style="text-align:left;">
<h4 class="modal-title" id="postCrudModal">Add product</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('cms.our_article.update_product_p', $post->id_article) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body" >
<div class="form-group form-group-max">
<label for="title" class="textblod">Product</label>
<select name="id_product_p" id="id_product_p" class="form-control" >
<option value=""></option>
@foreach($Productlist as $postlist)
<option value="{{ $postlist->id }}">{{ $postlist->product_name }}</option>
@endforeach
</select>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="space"></div>
<div class="space"></div>
@endsection