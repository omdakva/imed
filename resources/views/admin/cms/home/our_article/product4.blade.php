
<div class="col-md-6">
<div class="card card-small product_div" style="padding:5px;">
@if(count($productview4)==0)
<div class="col-md-12" style="font-size:20px;">
<button href="javascript:void(0)" data-toggle="modal" data-target="#add_product_s4" class="product_button floatright"><i class="fa fa-plus product_incon"></i></button>
</div>
<img src="{{ asset('public/photo/category_logo/image-not-found-99.png') }}" alt="Photo about" style="width:100%;height:180px;"> 
<div class="col-md-12 textcenter">Select product</div>
@else
@endif
@if(count($productview4)==0)
@else
@endif
@foreach($productview4 as $p1)
<div class="modal fade" id="edit_product_s4" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header" style="text-align:left;">
<h4 class="modal-title" id="postCrudModal">Edit product</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('cms.our_article.update_product_s4', $post->id_article) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body" >
<div class="col-md-12 textcenter" style="font-size:20px;">{{ $p1->product_name }}</div>
<div class="card-header border-bottom text-center" style="padding:5px;">
<img src="{{ asset('public/photo/products_logo/'.$p1->photo) }}" alt="Photo about" style="width:100%;height:200px;"> 
</div>
<div class="form-group form-group-max">
<label for="title" class="textblod">Product</label>
<select name="id_product_s4" id="id_product_s4" class="form-control" >
@foreach($Productlist as $postlist)
<option value="{{ $postlist->id }}" @if($postlist->id == $p1->id_product_s4)
SELECTED
@else
@endif >{{ $postlist->product_name }}</option>
@endforeach
</select>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>
<div class="col-md-12" style="font-size:20px;">
<button href="javascript:void(0)" data-toggle="modal" data-target="#edit_product_s4" class="product_button floatright"><i class="fa fa-edit product_incon"></i></button>
</div>
<img src="{{ asset('public/photo/products_logo/'.$p1->photo) }}" alt="Photo about" style="width:100%;height:180px;"> 
<div class="col-md-12 textcenter">{{ $p1->product_name }}</div>
@endforeach
</div>
</div>
</div>


<div class="modal fade" id="add_product_s4" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header" style="text-align:left;">
<h4 class="modal-title" id="postCrudModal">Add product</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('cms.our_article.update_product_s4', $post->id_article) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body" >
<div class="form-group form-group-max">
<label for="title" class="textblod">Product</label>
<select name="id_product_s4" id="id_product_s4" class="form-control" >
<option value=""></option>
@foreach($Productlist as $postlist)
<option value="{{ $postlist->id }}">{{ $postlist->product_name }}</option>
@endforeach
</select>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>