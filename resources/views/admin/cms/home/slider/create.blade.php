@extends('layouts.admin')
@section('title', 'Dluxxis cms')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis cms</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Dluxxis cms /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.home') }}">Home /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.home.slider') }}">Slider /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>



<div class="row titleappseconde">
<div class="col-lg-12">		
<h3 class="page-title" style="padding:0px;">Add new product</h3>
</div>
</div>




<div class="row">
<div class="col-lg-12">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
	

<form id="file-upload-form" class="uploader" action="{{ route('cms.home.slider.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
			
<input type="hidden" value="1" name="active"/>		
<div class="row">

<div class="cadre_filter">
<div class="row">

<div class="col-lg-6">
<label for="name" class="col-sm-12 control-label textblod">Product</label>
<div class="col-sm-12">
<select class="form-control" name="id_product" required>
<option></option>
@foreach($Productlist as $row)
<option value="{{$row->id}}">{{$row->product_name}}</option>
@endforeach
</select>
</div>
</div>


<div class="col-lg-6">
<label for="name" class="col-sm-12 control-label textblod">Coordinate X</label>
<div class="col-sm-12">
<input type="text" class="form-control"  value="{{request()->route('x')}}" disabled >
<input type="hidden" class="form-control" name="coordinate_x" value="{{request()->route('x')}}">
</div>
</div>


<div class="col-lg-6">
<label for="name" class="col-sm-12 control-label textblod">Coordinate Y</label>
<div class="col-sm-12">
<input type="text" class="form-control"  value="{{request()->route('y')}}" disabled>
<input type="hidden" class="form-control" name="coordinate_y" value="{{request()->route('y')}}">
</div>
</div>


</div>
</div>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row">
<div class="col-lg-12 cadre_filter" style="text-align:right;">
<button type="submit" class="model_btn_save">Save</button>
</div>
</div>
</form>



</div>
</div>
</div>



<script>
   function loadPreview(input){
       var data = $(input)[0].files; //this file data
       $.each(data, function(index, file){
           if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
               var fRead = new FileReader();
               fRead.onload = (function(file){
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image thumb element
                       $('#thumb-output').append(img);
                   };
               })(file);
               fRead.readAsDataURL(file);
           }
       });
   }
</script>


<script type="text/javascript">
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$(document).ready(function () {
$('#category').on('change',function(e) {
var cat_id = e.target.value;
$.ajax({
url:"{{ route('subcat') }}",
type:"POST",
data: {
cat_id: cat_id
},
success:function (data) {
$('#subcategory').empty();
$.each(data.subcategory[0].subcategory,function(index,subcategory){
$('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.name+'</option>');
})
}
})
});
});
</script>
@endsection