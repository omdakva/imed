@extends('layouts.admin')
@section('title', 'Manage category')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis cms</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Dluxxis cms /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.home') }}">Home /</a>
<a id="pagetitlesecondelink">Slider</a>
</div>
</div>




<div class="row">

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
 
  <script>
    var anno;
	


    $(document).ready(function () {

      anno = $("#imageExample").annotable({
        draggable: ".annotation"
        
  });

  
  
    
  
      $(".annotation").mouseover(function () {
        anno.highlightAnnotation($(this).attr("annotation-id"));
		
		
        
      });

      anno.on('onMouseMoveOverItem', function (event, coordinate) {
        $('#showPixel').html("[x: " + coordinate.x + " - y: " + coordinate.y + "]");
        
   
  });
	  
	  
	  
    
		
		

      anno.on('onAnnotationCreated', function (event, annotation) {
        console.log(annotation);
        
      });

      anno.on('onAnnotationUpdated', function (event, annotation) {
        console.log(annotation);
      });

      anno.on('onAnnotationRemoved', function (event, annotation) {
        console.log(annotation);
      });

@foreach($Slider_image as $post)
  var myAnnotation{{ $post->id_slider   }} = {
        id: {{ $post->id_slider   }},
        text: "<a style='color:#ff0000;text-decoration:none;' href='{{url('admin/cms/home/slider')   }}/{{$post->id_slider}}'>{{ $post->product_name  }}</br>{{ $post->product_price  }} {{ $post->product_currency  }}</a>",

        position: {
        center: { x: {{ $post->coordinate_x  }}, y: {{ $post->coordinate_y  }} }
        },
        width: 30,
        height: 30
		};
       anno.addAnnotation(myAnnotation{{ $post->id_slider   }});
@endforeach


  
	   
      var annos = $(".annotableImage").annotable({ //return array of annotable
        draggable: ".annotation",
        annotationStyle: { //annotation style
          hiBorderColor: 'red', // border color for highlighted annotation
          hiBorderSize: 3.2,  //border width for highlighted annotation  [1-12]
          imageBorder: false, //if false, not show the border on annotation
        }
      });

      $(".annotableImage").on('onAnnotationCreated', function (event, annotation) {
        console.log(annotation);
      });
    });

  </script>




  <div class="main" style="padding:0px;">
    <div id="carbon-block" style="margin:0px auto"></div>







      <div style="margin-top: 50px;" id="showPixel">[x: 0 - y: 0]</div>
      <div id="img_coordinate" onclick="showCoords(event)">
	  <img  id="imageExample" src="{{ asset('public/photo/slider/dluxxis_slide.png') }}" style="width:100%;height:600px;">
      </div>


  </div>
  
  


  
  
  
  
<style>
img {
    max-width: 100%;
    height: auto;
}

.sidenav {
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    padding-top: 20px;
    color: white;
    text-align: center;
}

.sidenav a {
    padding: 6px 8px 6px 16px;
    text-decoration: none;
    color: #818181;
    display: block;
}

.sidenav a:hover {
    color: #f1f1f1;
}

.main {
    padding: 0px 0px;
    text-align: center;
}

@media screen and (max-height: 450px) {
    .sidenav {
        padding-top: 15px;
    }
}

.image-caption {
    line-height: normal;
    font-weight: bold;
    font-size: 12px;
}
 </style>
  
  
  
<style>
.ui-draggable{opacity:.8;cursor:all-scroll}
.ui-draggable:hover{opacity:1}
.dda-annotationlayer{position:relative;display:inline-block}
.dda-annotationlayer .dda-canvas{height:100%;width:100%;position:absolute;top:0;left:0;z-index:0;opacity:.8}
.dda-annotationlayer .dda-popup{position:absolute;z-index:1;line-height:135%;font-family:Arial,Verdana,Sans;font-style:italic;font-size:32px;color:#ff0000;background-color:transparent;border:1px solid transparent;margin-top:10px;margin-left:-20px;}






.dda-annotationlayer .dda-popup-text{float:left;margin:7px 8px 4px 8px;flex:auto;cursor:default;min-width:30px;text-align:left}
.dda-annotationlayer .dda-popup-buttons{float:right;display:-webkit-inline-box;display:-ms-inline-flexbox;-moz-transition-property:opacity;-moz-transition-duration:1s;-moz-transition-delay:0s;-webkit-transition-property:opacity;-webkit-transition-duration:1s;-webkit-transition-delay:0s;-o-transition-property:opacity;-o-transition-duration:1s;-o-transition-delay:0s;transition-property:opacity;transition-duration:1s;transition-delay:0s}
.dda-annotationlayer .dda-popup-buttons button{font-weight:700;opacity:.8;padding:.25rem .5rem;font-size:.875rem;line-height:1.5;display:block;cursor:pointer;text-align:center;user-select:none;background-color:transparent;border:none;border-radius:2px;text-transform:none;-moz-transition-property:opacity;-moz-transition-duration:.5s;-moz-transition-delay:0s;-webkit-transition-property:opacity;-webkit-transition-duration:.5s;-webkit-transition-delay:0s;-o-transition-property:opacity;-o-transition-duration:.5s;-o-transition-delay:0s;transition-property:opacity;transition-duration:.5s;transition-delay:0s}
.dda-annotationlayer .dda-popup-buttons button:hover{opacity:1}
.dda-annotationlayer .dda-popup-buttons .dda-popup-button-remove{color:#e74a3b}
.dda-annotationlayer .dda-popup-buttons .dda-popup-button-remove:hover{color:#fff;background-color:#e74a3b}.dda-annotationlayer .dda-popup-buttons .dda-popup-button-rotate{color:#f6c23e}
.dda-annotationlayer .dda-popup-buttons .dda-popup-button-rotate:hover{color:#fff;background-color:#f6c23e}.dda-annotationlayer .dda-popup-buttons .dda-popup-button-move{color:#1cc88a}
.dda-annotationlayer .dda-popup-buttons .dda-popup-button-move:hover{color:#fff;background-color:#1cc88a}
.dda-annotationlayer .dda-hint{white-space:nowrap;position:absolute;top:0;left:0;pointer-events:none;opacity:.8}
.dda-annotationlayer .dda-hint .dda-hint-msg{pointer-events:auto;background-color:rgba(0,0,0,.2);margin:4px;padding:8px 15px 8px 30px;font-family:'lucida grande',tahoma,verdana,arial,sans-serif;line-height:normal;font-size:12px;color:#fff;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;-khtml-border-radius:4px;-moz-transition-property:opacity;-moz-transition-duration:.5s;-moz-transition-delay:0;-webkit-transition-property:opacity;-webkit-transition-duration:.5s;-webkit-transition-delay:0;-o-transition-property:opacity;-o-transition-duration:.5s;-o-transition-delay:0;transition-property:opacity;transition-duration:.5s;transition-delay:0}
.dda-annotationlayer .dda-hint .dda-hint-icon{pointer-events:auto;position:absolute;top:6px;left:5px;width:19px;height:22px;margin:2px 4px 0 6px;color:#fff}
</style>

<script>
"use strict"; !function (t) { var o = "onMouseMoveOverItem", e = "onAnnotationRemoved", n = "onAnnotationCreated", i = "onAnnotationUpdated"; function a(t) { return { x: t.x, y: t.y } } var r = function (t, o = 50, e = 50, n = 0) { this.center = t, this.width = o, this.height = e, this.rotation = n, this.area = o * e; var i = void 0, r = this, s = function () { var t = r.rotation * Math.PI / 180, o = Math.sin(t), e = Math.cos(t), n = r.width / 2, a = r.height / 2, s = n * e, d = a * o, h = n * o, c = a * e; (i = {}).topR = { x: r.center.x + s - d, y: r.center.y - h - c }, i.topL = { x: r.center.x - s - d, y: r.center.y + h - c }, i.botL = { x: r.center.x - s + d, y: r.center.y + h + c }, i.botR = { x: r.center.x + s + d, y: r.center.y - h + c } }; this.intersect = function (t, o) { i || s(); var e = Math.abs(t * i.topL.y - i.topL.x * o + (i.botR.x * o - t * i.botR.y) + (i.topL.x * i.botR.y - i.botR.x * i.topL.y)) / 2; return e += Math.abs(t * i.botR.y - i.botR.x * o + (i.botL.x * o - t * i.botL.y) + (i.botR.x * i.botL.y - i.botL.x * i.botR.y)) / 2, e += Math.abs(t * i.botL.y - i.botL.x * o + (i.topR.x * o - t * i.topR.y) + (i.botL.x * i.topR.y - i.topR.x * i.botL.y)) / 2, (e += Math.abs(t * i.topR.y - i.topR.x * o + (i.topL.x * o - t * i.topL.y) + (i.topR.x * i.topL.y - i.topL.x * i.topR.y)) / 2) <= r.area }, this.update = function (t, o) { t && (r.center = t), o && (r.rotation = o), i = void 0 }, this.print = function () { return i || s(), { position: { center: a(r.center), topL: a(i.topL), topR: a(i.topR), botL: a(i.botL), botR: a(i.botR) }, rotation: r.rotation, width: o, height: e } } }, s = function (t, o, e, n, i = !0) { this.id = t, this.text = e, o instanceof Image && (this.image = o), this.geometry = n, this.editable = i, this.created_at = Date.now(); var a = this; this.print = function () { return { id: a.id, image: a.image ? a.image.src : void 0, text: a.text, ...a.geometry.print(), editable: a.editable, created_at: a.created_at } }, this.isEqualPrint = function (t) { return !(!t instanceof Object) && t.created_at == a.created_at } }, d = function (o, e) { if (e.hint.enabled) { var n = t('<div class="dda-hint-msg dda-opacity-fade" style="opacity: 0;">' + e.hint.message + "</div>"), i = t('<div class="dda-hint-icon">' + e.hint.icon + "</div>"), a = t('<div class="dda-hint"></div>'); a.append(n), a.append(i), o.append(a); var r = void 0, s = this; i.mouseover(function () { s.show() }), i.mouseout(function () { s.hide() }), t(e.draggable).mouseover(function () { s.show() }), this.show = function (t, o) { clearTimeout(r), t && i.html(t), o && n.html(o), n.css("opacity", 1), r = setTimeout(function () { s.hide() }, 3e3) }, this.hide = function () { clearTimeout(r), n.css("opacity", 0), n.html(e.hint.message), i.html(e.hint.icon) } } }, h = function (o, e, n) { var i = t('<div class="dda-popup-buttons"></div>'), a = t('<button role="button" class="dda-popup-button-move" data-toggle="tooltip" title="' + n.popup.tooltipMove + '">' + n.popup.buttonMove + "</button>"), r = t('<button role="button" class="dda-popup-button-rotate" data-toggle="tooltip" title="' + n.popup.tooltipRotate + '">' + n.popup.buttonRotate + "</button>"), s = t('<button role="button" class="dda-popup-button-remove" data-toggle="tooltip" title="' + n.popup.tooltipRemove + '">' + n.popup.buttonRemove + "</button>"); i.append(a), i.append(r), i.append(s); var d = t('<div class="dda-popup"></div>').hide(); d.append(t('<div class="dda-popup-text" data-toggle="tooltip" title="' + n.popup.tooltipText + '"></div>')), d.append(i), e.append(d); var h = !1, c = void 0, l = this; d.mouseover(function (t) { h = !0 }), d.mouseout(function (t) { h = !1 }), a.click(function (t) { c.editable && o.startMoveAnnotation(c) }), r.click(function (t) { c.editable && o.startRotateAnnotation(c) }), s.click(function (t) { c.editable && o.removeAnnotation(c) }), this.show = function (t, o) { (c = t).text ? d.children(".dda-popup-text").show().html(c.text) : d.children(".dda-popup-text").hide(), c.editable ? i.show() : i.hide(), d.css({ top: o.y, left: o.x }).show() }, this.hide = function (t = !1) { if (!(l.isHidden() || h && !t)) var o = setTimeout(function () { h && !t || (c = void 0, d.hide()), clearTimeout(o) }, 150) }, this.isHidden = function () { return d.is(":hidden") } }, c = function (c, l, u) { var p = t('<canvas class="dda-canvas"></canvas>'); c.append(p); var f = new h(this, c, l), g = new d(c, l), v = [], m = void 0, b = void 0, y = void 0, w = p[0].getContext("2d"), x = this; p.droppable({ drop: function (t, o) { b.geometry.update(R(o.offset.left, o.offset.top)), x.addAnnotation(b), b = void 0 }, over: function (t, o) { o.helper.is(":hidden") || o.helper.hide(), b = new s(o.draggable.attr("annotation-id"), o.draggable[0], o.draggable.attr("annotation-text"), new r(R(o.offset.left, o.offset.top), o.draggable.attr("annotation-width") || o.draggable[0].naturalWidth, o.draggable.attr("annotation-height") || o.draggable[0].naturalHeight, o.draggable.attr("annotation-rotation"), o.draggable.attr("annotation-editable"))) }, out: function (t, o) { o.helper.is(":hidden") && o.helper.show(), b = void 0, x.redrawAnnotations() } }), p.mousemove(function (t) { var e = A(t.offsetX, t.offsetY); if (u(o, a(e)), b) return x.redrawAnnotations(), b.geometry.update(e), void M(b); if (m) m(e); else { var n = L(e); if (0 == n.length) return y = n = void 0, f.hide(), void x.redrawAnnotations(); (f.isHidden() || y != n[0]) && (y != n[0] && (y = n[0], x.redrawAnnotations(y)), f.show(y, { x: t.offsetX, y: t.offsetY })) } }), p.mouseup(function (t) { if (m) { var o = m(); u(i, [o.new.print(), o.old_print]), m = void 0 } }); var A = function (t, o) { return { x: parseInt(t / p.width() * p[0].width), y: parseInt(o / p.height() * p[0].height) } }, R = function (t, o) { return A(t - p.offset().left, o - p.offset().top) }, M = function (t, o = !1) { var e, n, i = -t.geometry.width / 2, a = -t.geometry.height / 2; (w.save(), w.beginPath(), w.translate(t.geometry.center.x, t.geometry.center.y), w.rotate(-t.geometry.rotation * Math.PI / 180), t.image && w.drawImage(t.image, i, a, t.geometry.width, t.geometry.height), !t.image || l.annotationStyle.imageBorder) && (o ? (n = l.annotationStyle.hiBorderColor, e = l.annotationStyle.hiBorderSize) : (n = l.annotationStyle.borderColor, e = l.annotationStyle.borderSize), w.lineJoin = "round", w.lineWidth = 1, w.strokeStyle = "#000000", w.strokeRect(i + .5, a + .5, t.geometry.width - 1, t.geometry.height - 1), w.lineJoin = "miter", w.lineWidth = e, w.strokeStyle = n, w.strokeRect(i + 1 + e / 2, a + 1 + e / 2, t.geometry.width - 2 - e, t.geometry.height - 2 - e)); w.restore() }, L = function (t) { var o = []; return v.forEach(e => { e.geometry.intersect(t.x, t.y) && o.push(e) }), o = o.reverse() }; this.resize = function (t, o) { p[0].width = t, p[0].height = o }, this.clear = function () { w.clearRect(0, 0, p[0].width, p[0].height) }, this.redrawAnnotations = function (t) { x.clear(); var o = !!t; v.forEach(e => { o ? M(e, t instanceof Object ? e == t : e.id === t) : M(e) }) }, this.getAnnotations = function () { return v }, this.addAnnotation = function (t, o, e = !0) { if (o) { var i = v.indexOf(o); i > -1 && (v[i] = t, x.redrawAnnotations()) } else { var a = !0; for (let o = 0; o < v.length; o++) { if (v[o].geometry.area < t.geometry.area) { v.splice(o, 0, t), a = !1; break } } a && v.push(t), M(t), e && u(n, [t.print()]) } }, this.startMoveAnnotation = function (t) { f.hide(!0), g.show && g.show(l.hint.iconMove, l.hint.messageMove); var o = t.print(); m = function (e) { return e && (t.geometry.update(e), x.redrawAnnotations(y)), { new: t, old_print: o } } }, this.startRotateAnnotation = function (t) { f.hide(!0), g.show && g.show(l.hint.iconRotate, l.hint.messageRotate); var o = t.print(); m = function (e) { return e && (t.geometry.update(void 0, 180 * -Math.atan2(e.y - t.geometry.center.y, e.x - t.geometry.center.x) / Math.PI), x.redrawAnnotations(y)), { new: t, old_print: o } } }, this.removeAnnotation = function (t, o = !0) { f.hide(!0), v.splice(v.indexOf(t), 1), o && u(e, [t.print()]), x.redrawAnnotations() }, this.removeAll = function (t) { if (t) return v = v.filter(o => o.id !== t), void x.redrawAnnotations(); v = [], x.clear() } }, l = function (o, e) { this.image = o instanceof t ? o : t(o), !this.image[0] instanceof Image && t.error("Annotable it must be an image."), this.image.wrap(t('<div class="dda-annotationlayer"></div>')); var n = this, i = new c(this.image.parent(), e, function (t, o) { n.image.trigger(t, o) }); this.image[0].complete ? i.resize(this.image[0].naturalWidth, this.image[0].naturalHeight) : this.image[0].onload = function () { i.resize(this.naturalWidth, this.naturalHeight) }; var a = function (t, o, e) { var n = e ? d(e) : void 0; i.addAnnotation(new s(t.id, o, t.text, new r(t.position.center, t.width || o.naturalWidth, t.height || o.naturalHeight, t.position.rotation), t.editable), n, !1) }, d = function (o) { var e = i.getAnnotations().find(function (t) { return t.isEqualPrint(o) }); return e || t.error("Annotation not found."), e }; this.addAnnotation = function (o, e) { if (o.position && o.position.center && o.position.center.x && o.position.center.y || t.error("Invalid annotation."), o.image) if (o.image instanceof Image) a(o, o.image, e); else { (!o.image instanceof String || !o.image.includes("http")) && t.error("Image must be a URL or an instance of Image."); var n = new Image; n.src = o.image, n.onload = function () { a(o, this, e) } } else a(o, {}, e) }, this.getAnnotations = function () { var t = []; return i.getAnnotations().forEach(o => { t.push(o.print()) }), t }, this.removeAnnotation = function (t) { i.removeAnnotation(d(t), !1) }, this.removeAll = function (t) { i.removeAll(t) }, this.hideAnnotations = function () { i.clear() }, this.showAnnotations = function () { i.redrawAnnotations() }, this.highlightAnnotation = function (t) { i.redrawAnnotations(t instanceof Object ? d(t) : t) }, this.on = function (t, o) { this.image.on(t, o) } }; t.fn.annotable = function (o) { if ("object" == typeof o || !o) { var e = t.extend(!0, {}, t.fn.annotable.defaults, o), n = this.length > 1 ? [] : void 0; t(e.draggable).is(":ui-draggable") || t(e.draggable).draggable({ helper: "clone", ghosting: !0, cursorAt: { top: 0, left: 0 }, revert: "invalid" }) } var i = Array.prototype.slice.call(arguments, 1); return this.each(function () { var a = t(this), r = a.data("annotable"); if (r) { if ("string" == typeof o && r[o]) return r[o].apply(r, i); t.error("Method " + o + " does not exist on Annotable") } else e ? (r = new l(this, e), a.data("annotable", r), n ? n.push(r) : n = r) : t.error("Annotable is not initialized.") }), n }, t.fn.annotable.defaults = { draggable: ".draggable-annotation", hint: { enabled: !0, message: "", icon: '', messageMove: "Move to set new annotation position", iconMove: '', messageRotate: "Move to set new annotation rotation", iconRotate: '<i class="fas fa-info"></i>' }, popup: { buttonMove: '', tooltipMove: "Change the position of annotation", buttonRotate: '', tooltipRotate: "Change the rotation of annotation", buttonRemove: '', tooltipRemove: "Remove the annotation", tooltipText: "" }, annotationStyle: { borderColor: "#ffffff", borderSize: 0, hiBorderColor: "#fff000", hiBorderSize: 0.2, imageBorder: !0 } } }(jQuery);
</script>




</div>



</div>




 
@endsection