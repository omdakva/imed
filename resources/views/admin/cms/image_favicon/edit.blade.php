@extends('layouts.admin')
@section('title', 'Manage CMS')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage CMS</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Manage CMS /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.image-favicon') }}">Image favicon /</a>
<a id="pagetitlesecondelink" href="{{ route('cms.index') }}">Image favicon</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Image favicon</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a onclick="goBack()" href="javascript:void(0)"  class="btn-switch" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a> 
<a href="{{route('cms.image-favicon')}}" class="btn-switch" title="List"><i class="fa fa-list" aria-hidden="true"></i></a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif




<div class="Individual cadre_add">
<form action = "{{ route('cms.image-favicon.update', $Image_favicon->id) }}" method = "POST">
@csrf
@method('PATCH')


<div class="row">
<h3 class="page-title2" >Favicon details</h3>



<div class="form-group col-md-6">
<label for="post-description" style="width:100%;" class="textblod">Activation</label>
<label for="r2" class="textnormal">
<input type="radio" name="active" id="r2"onClick="getResults()" value="1" 
@if($Image_favicon->active == 1)
checked  
@else
@endif > Active
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="active" id="r1" onClick="getResults()" value="0"
@if($Image_favicon->active == 0)
checked  
@else
@endif> Blocked
</label>		
</div>
</div>
<div class="modal-footer"style="text-align:right;">
<a onclick="goBack()" href="javascript:void(0)" class="model_btn_close">Cancel</a>
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>

</div>
</div>
</div>
@endsection