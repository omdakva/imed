@extends('layouts.admin')
@section('title', 'Manage CMS')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage CMS</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Manage CMS /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.image-favicon') }}">Image favicon /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new</h3>
</div>
<div class="row">
<div class="col-lg-12 cadre_filter">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
	
<form id="file-upload-form" class="uploader" action="{{ route('cms.image-favicon.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="active" id="active" value="0">
<div class="row">
<div class="form-group col-md-6">
<label for="name_de" class="col-sm-12 control-label textblod">Photo</label>
<div class="col-sm-12">
<input type="file" class="form-control" id="photo" name="photo" value="" required="">
</div>
</div>
</div>

            
<div class="modal-footer"style="text-align:right;">
<button type="submit" name="register" id="register"  class="model_btn_save">Save</button>
</div>
</form>
</div>
</div>
</div>


<script>
$('div.form-group-max').maxlength();
</script>
<script>
$(document).ready(function(){

	$('#name').blur(function(){
		var error_email = '';
		var name = $('#name').val();
		var _token = $('input[name="_token"]').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($.trim(name).length > 0)
		{
			if(!(name))
			{				
				$('#error_email').html('<label class="text-danger">Invalid category</label>');
				$('#name').addClass('has-error');
				$('#register').attr('disabled', 'disabled');
			}
			else
			{
				$.ajax({
					url:"{{ route('category.check') }}",
					method:"POST",
					data:{name:name, _token:_token},
					success:function(result)
					{
						if(result == 'unique')
						{
							$('#error_email').html('<label class="text-success">Category Available</label>');
							$('#name').removeClass('has-error');
							$('#register').attr('disabled', false);
						}
						else
						{
							$('#error_email').html('<label class="text-danger">Category not Available</label>');
							$('#name').addClass('has-error');
							$('#register').attr('disabled', 'disabled');
						}
					}
				})
			}
		}
		else
		{
			$('#error_email').html('<label class="text-danger">Category is required</label>');
			$('#name').addClass('has-error');
			$('#register').attr('disabled', 'disabled');
		}
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('#emailcompany').blur(function(){
		var error_email = '';
		var email = $('#emailcompany').val();
		var _token = $('input[name="_token"]').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($.trim(emailcompany).length > 0)
		{
			if(!filter.test(email))
			{				
				$('#error_emailcompany').html('<label class="text-danger">Invalid Email</label>');
				$('#emailcompany').addClass('has-error');
				$('#registercompany').attr('disabled', 'disabled');
			}
			else
			{
				$.ajax({
					url:"",
					method:"POST",
					data:{email:email, _token:_token},
					success:function(result)
					{
						if(result == 'unique')
						{
							$('#error_emailcompany').html('<label class="text-success">Email Available</label>');
							$('#emailcompany').removeClass('has-error');
							$('#registercompany').attr('disabled', false);
						}
						else
						{
							$('#error_emailcompany').html('<label class="text-danger">Email not Available</label>');
							$('#emailcompany').addClass('has-error');
							$('#registercompany').attr('disabled', 'disabled');
						}
					}
				})
			}
		}
		else
		{
			$('#error_email').html('<label class="text-danger">Email is required</label>');
			$('#email').addClass('has-error');
			$('#register').attr('disabled', 'disabled');
		}
	});
	
});
</script>
@endsection