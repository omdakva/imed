@extends('layouts.admin')
@section('title', 'Dluxxis cms')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >D'luxxis cms</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.index') }}">Dluxxis cms /</a>
<a id="pagetitleprimerlink" href="{{ route('cms.pages') }}">Pages /</a>
<a id="pagetitlesecondelink">Details</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >{{ $Pages->name }} @if($Pages->front_page =='1')
( Front Page  )
@else
@endif
</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('cms.pages') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif



<div class="">
<div class="row">
<div class="col-md-8">
<div class="cadre_add">
<div class="form-group">
<p class="textblodfull">Page name : <span class="textnormal">{{ $Pages->name }}</span></p>
<p class="textblodfull">Page name ( De ) : <span class="textnormal">{{ $Pages->name_de }}</span></p>
<p class="textblodfull">Page name ( Fr ) : <span class="textnormal">{{ $Pages->name_fr }}</span></p>


<p class="textblodfull">Activation : <span class="textnormal">@if($Pages->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></p>

</div>
</div>
</div>



<div class="col-md-4">
<div class="cadre_add">
<div class="text-left">
<p class="textblodfull">Creation date : <span class="textnormal">{{ $Pages->created_at }}</span></p>
<p class="textblodfull">Update date : <span class="textnormal">{{ $Pages->updated_at }}</span></p>
</div>
<div class="space"></div>
<div class="space"></div>
</div>
</div>
</div>
</div>

  
 

</div>
</div>
</div>


@endsection