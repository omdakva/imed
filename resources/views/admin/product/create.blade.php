@extends('layouts.admin')
@section('title', 'Manage product')
@section('content')

<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage product</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('product.index') }}">Product /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>

<div class="row">
<div class="col-lg-12 step_full">		
<button class="step_active">Add new</button>
<button class="step_seconde">Translate</button>
</div>
</div>


<div class="row titleappseconde">
<div class="col-lg-12">		
<h3 class="page-title" style="padding:0px;">Add new product</h3>
</div>
</div>



<div class="row">
<div class="col-lg-12">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
	
<form id="file-upload-form" class="uploader" action="{{ route('product.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" value="1" name="active">		
<input type="hidden" value="1" name="statut">
<input type="hidden" value="0" name="offer">		

<input type="hidden" value="1" name="product_name_de">		
<input type="hidden" value="1" name="product_name_fr">		
<input type="hidden" value="1" name="short_description_de">		
<input type="hidden" value="1" name="long_description_de">		
<input type="hidden" value="1" name="short_description_fr">		
<input type="hidden" value="1" name="long_description_fr">
<div class="row">


<div class="col-lg-7">		
<div class="col-lg-12 cadre_filter">		


<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Name</label>
<div class="col-sm-12">
<input type="text" name="product_name" id="product_name" class="form-control input-lg" value="" required="">
<span id="error_email"></span>
</div>
</div>

						
<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Description</label>
<div class="col-sm-12">
<textarea type="text" class="form-control" id="long_description" name="long_description" style="height:170px;" value="" required=""></textarea>
</div>
</div>


<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Short description</label>
<div class="col-sm-12">
<textarea type="text" class="form-control" id="short_description" name="short_description" style="height:170px;" value="" required=""></textarea>
</div>
</div>



<div class="form-group row">
<div class="col-sm-7">
<label for="name" class="control-label textblod">Price</label>
<input type="number" class="form-control" id="product_price" name="product_price" value="" required="">
</div>
<div class="col-sm-5">
@if($currency->count()=='0') 
<label class="control-label">
<a href="{{route('settings-currency.index')}}" class="error_text">
Please select currency <i class="fa fa-link" aria-hidden="true"></i>
</a>
</label>
@else
<label class="control-label textblod">Currency</label>
@endif
<select type="number" class="form-control" id="product_currency" name="product_currency" value="" required="" disabled>
@foreach($currency as $row)
<option value="{{ $row->symbole  }}">{{ $row->symbole  }}</option>
@endforeach
</select>
<input type="hidden" name="product_currency" value="@foreach($currency as $row)
{{ $row->symbole  }}
@endforeach">
</div>
</div>



<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Stock</label>
<div class="col-sm-12">
<input type="number" class="form-control" id="stock" name="stock" value="" required="">
</div>
</div>


<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Image Principale</label>
<div class="col-sm-12">
<input type="file" class="form-control" id="photo" name="photo">
</div>

</div>
<div class="form-group row" id="oneimg" > </div>



<div class="form-group row">
<label for="name" class="col-sm-12 control-label textblod">Remise</label>
<div class="col-sm-12">
<input type="number" class="form-control" id="remise" name="remise" value="" required="">
</div>
</div>

</div>
</div>


<input type="hidden" class="form-control" id="remise" name="orders_number" value="22" required="">
<input type="hidden" class="form-control" id="remise" name="customer_name" value="22" required="">
<input type="hidden" class="form-control" id="remise" name="customer_id" value="22" required="">
<input type="hidden" class="form-control" id="remise" name="currency" value="22" required="">
<input type="hidden" class="form-control" id="remise" name="delivery" value="22" required="">
<input type="hidden" class="form-control" id="remise" name="localisation_etats" value="22" required="">
<input type="hidden" class="form-control" id="remise" name="total_amount" value="22" required="">


<div class="col-lg-5">
<div class="col-lg-12 cadre_filter">


@foreach($Category as $category)
<label class="textblod" style="width:100%;margin-left:0px;">
<input type="checkbox" name="category_id[]" id="category" value="{{$category->id}}"> {{$category->name}}
</label>
@if(count($category->subcategory))
@include('layout.subCategoryListproduct',['subcategory' => $category->subcategory])
@endif
@endforeach


</div>
</div>
</div>
  
<div class="space"></div>
<div class="space"></div>



<div class="row">
<div class="col-lg-12">		
<div class="col-lg-12 cadre_filter" style="text-align:right;">
<button type="submit" name="register" id="register"  class="model_btn_save">Save</button>
</div>
</div>
</div>






</form>
</div>
</div>
</div>

</BR>


<script src="{{ asset('jquery/example/jQuery.maxlength.js')}}"></script>

<script>
$('div.form-group-max').maxlength();
</script>
















<script>
$(document).ready(function(){
	function readURL(input) {
    var reader = new FileReader();
   
    reader.onload = function(e) {
      $('#oneimg').append('<img  src="'+e.target.result+'" width="100%" height="350" />');

    }
    
    reader.readAsDataURL(input.files[0]); 

}
$("#photo").change(function(e) {


readURL(this);

});

	$('#product_name').blur(function(){
		var error_email = '';
		var product_name = $('#product_name').val();
		var _token = $('input[name="_token"]').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($.trim(product_name).length > 0)
		{
			if(!(product_name))
			{				
				$('#error_email').html('<label class="text-danger">Invalid product</label>');
				$('#product_name').addClass('has-error');
				$('#register').attr('disabled', 'disabled');
			}
			else
			{
				$.ajax({
					url:"{{ route('product.check') }}",
					method:"POST",
					data:{product_name:product_name, _token:_token},
					success:function(result)
					{
						if(result == 'unique')
						{
							$('#error_email').html('<label class="text-success">Product Available</label>');
							$('#product_name').removeClass('has-error');
							$('#register').attr('disabled', false);
						}
						else
						{
							$('#error_email').html('<label class="text-danger">Product not Available</label>');
							$('#product_name').addClass('has-error');
							$('#register').attr('disabled', 'disabled');
						}
					}
				})
			}
		}
		else
		{
			$('#error_email').html('<label class="text-danger">Product is required</label>');
			$('#product_name').addClass('has-error');
			$('#register').attr('disabled', 'disabled');
		}
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('#emailcompany').blur(function(){
		var error_email = '';
		var email = $('#emailcompany').val();
		var _token = $('input[name="_token"]').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($.trim(emailcompany).length > 0)
		{
			if(!filter.test(email))
			{				
				$('#error_emailcompany').html('<label class="text-danger">Invalid Email</label>');
				$('#emailcompany').addClass('has-error');
				$('#registercompany').attr('disabled', 'disabled');
			}
			else
			{
				$.ajax({
					url:"",
					method:"POST",
					data:{email:email, _token:_token},
					success:function(result)
					{
						if(result == 'unique')
						{
							$('#error_emailcompany').html('<label class="text-success">Email Available</label>');
							$('#emailcompany').removeClass('has-error');
							$('#registercompany').attr('disabled', false);
						}
						else
						{
							$('#error_emailcompany').html('<label class="text-danger">Email not Available</label>');
							$('#emailcompany').addClass('has-error');
							$('#registercompany').attr('disabled', 'disabled');
						}
					}
				})
			}
		}
		else
		{
			$('#error_email').html('<label class="text-danger">Email is required</label>');
			$('#email').addClass('has-error');
			$('#register').attr('disabled', 'disabled');
		}
	});
	
});
</script>
@endsection