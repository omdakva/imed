<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>jQuery Dark Slideshow</title>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="{{ asset('public/css_story/style.css')}}" type="text/css" rel="stylesheet" media="screen">
</head>
<body>
    <a href="ggggg">
    <div  class="fullscreen-container hidden">
        <div class='fullscreen-div'>
            <img class="remove-fullscreen" src="{{ asset('public/css_story/icons/remove_icon.webp')}}" width="60" />
        </div>
    </div>
    </a>

    <div id="gallery">

        <div id="slide">
            <div class="counter"></div>
            <a class="prev">❮</a>
            <a class="next">❯</a>
            <img id='preview' />
        </div>


        <div class="caption-container">
            <p id="caption"></p>
        </div>

<div id="thumbnails">
<div class="wrapper">
@foreach($Product as $post)
<img src="{{ asset('photo/products_logo/'.$post->photo) }}" class="thumbnail" alt="{{ $post->product_name  }}">
{{ $post->product_price  }} {{ $post->product_currency  }}
@endforeach
</div>
</div>

</div>
<script src="{{ asset('css_story/js/slider.js')}}"></script>
</body>
</html>
