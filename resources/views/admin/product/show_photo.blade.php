@extends('layouts.admin')
@section('title', 'Manage product')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage product</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">product</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Product details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('product.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif


@foreach($Product as $post)

<div class="col-md-12 cadre_add" >
<a href="{{ route('product.show', $post->id) }}" class="btn_add2">Details</a> 
<a href="{{ route('product.photo.index', $post->id) }}" class="btn_add2active">Product photo</a> 
@if($post->product_name_de=='1')
<a href="" class="btn_add2blocked">Translate</a>
@else
<a href="{{ route('product.translate.view', $post->id) }}" class="btn_add2">Translate</a>
@endif
</div>
<div class="space"></div>

<div class="row">

<div class="col-md-4">
<div class="cadre_add ">
<div class="text-center">
<label class="textblodfull" style="font-size:15px;">Principal photo </span></label>
<div class="mb-3 mx-auto">
<img src="{{ asset('photo/products_logo/'.$post->photo) }}" alt="User Avatar" style="width:90%;height:160px;"> </div>
</div>
<div class="space"></div>
<div class="space"></div>


</div>
</div>


<div class="col-md-8">
<div class="cadre_add row">

<div class="col-md-7">
<label class="textblodfull" style="font-size:15px;">Seconde photo ( {{count($product_photo)}} )</span></label>
</div>
<div class="col-md-5" style="text-align:right;">
@if(count($product_photo)>='4')
<a style="color:#ffffff;" class="btn btn-danger mb-2" id="create-new-post">Max 4 photos</a> 
@else()
<a href="javascript:void(0)" data-toggle="modal" data-target="#new_photo" class="btn btn-success mb-2" id="create-new-post">Add new</a> 
@endif()
</div>




<div class="modal fade" id="new_photo" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Add new image</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form action="{{ route('photo.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<div class="modal-body">
<input type="hidden" name="active" id="active" value="1">
<input type="hidden" name="products_id" id="products_id" value="{{ $post->id }}">

<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max" style="height:70px;">
<label for="title" class="textblod">Image</label>
<input type="file" name="photo" id="photo" class="form-control" required />
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>




<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Photo</th>
<th>Date</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($product_photo as $row)
<div class="modal fade" id="exampleModal{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
		<a  href="javascript:void(0)" id="delete-post" data-id="{{ $row->id }}" class="model_btn_delete delete-post" data-dismiss="modal">Yes, Delete</a>
      </div>
    </div>
  </div>
</div>








<div class="modal fade" id="edit{{ $row->id }}" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit image</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form  action = "{{ route('photo.update', $row->id) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
@method('PATCH') 
<div class="modal-body">

<div class="row">
<div class="col-md-12" style="text-align:center;">
<img class="user-avatar mr-2" src="{{ asset('photo/products_logo/seconde/'.$row->photo) }}" style="width:250px;height:150px;border-radius:0%;">
</div>
</div>

<input type="hidden" name="products_id" id="products_id" value="{{ $post->id }}">

<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max" style="height:70px;">
<label for="title" class="textblod">Edit photo</label>
<input type="file" name="photo" id="photo" class="form-control" required />
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>



<tr id="post_id_{{ $row->id }}">
<td style="text-align:center;">
<img class="user-avatar mr-2" src="{{ asset('photo/products_logo/seconde/'.$row->photo) }}" style="width:40px;height:40px;border-radius50%;">
</td>

<td>{{ $row->created_at  }}</td>
<td class="td_btn">
<a href="{{ route('product.show', $row->id) }}" class="btn-delete"><i class="fa fa-eye"></i></a>
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit{{ $row->id }}"  class="btn-delete"><i class="fa fa-edit"></i></a>
<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal{{ $row->id }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>




</div>

<div class="space"></div>
<div class="space"></div>

</div>



</div>
@endforeach




  
 
  





		

</div>
</div>
</div>
</br>




<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#create-new-post').click(function () {
        $('#btn-save').val("create-post");
        $('#postForm').trigger("reset");
        $('#postCrudModal').html("Add New Category");
        $('#ajax-crud-modal').modal('show');
    });
 
    $('body').on('click', '#edit-post', function () {
      var post_id = $(this).data('id');
      $.get(+post_id+'/edit', function (data) {
         $('#postCrudModal').html("Edit Documents");
          $('#btn-save').val("edit-post");
          $('#ajax-crud-modal').modal('show');
          $('#post_id').val(data.id);
          $('#id_client').val(data.id_client);
          $('#crm_type').val(data.crm_type);
		  
		          
		  
          $('#title').val(data.title);
          $('#fichier').val(data.fichier);
          $('#crm_date').val(data.crm_date);  
          $('#crm_time').val(data.crm_time);  
          $('#note').val(data.note);  
          $('#id_admin').val(data.id_admin);  
          $('#id_day').val(data.id_day);  
          $('#id_month').val(data.id_month);  
          $('#month').val(data.month);  
          $('#id_year').val(data.id_year);  
      })
   });

    $('body').on('click', '.delete-post', function () {
        var post_id = $(this).data("id");		
		
		
		
		

 
        $.ajax({
            type: "DELETE",
            url: "{{ url('admin/product/photo')}}"+'/'+post_id,
            success: function (data) {
                $("#post_id_" + post_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });   
  });
 
 if ($("#postForm").length > 0) {
      $("#postForm").validate({
 
     submitHandler: function(form) {

      var actionType = $('#btn-save').val();
      $('#btn-save').html('Sending..');


      $.ajax({
          data: $('#postForm').serialize(),
          url: "{{ route('category.store') }}",
        
          type: "POST",
          dataType: 'json',
          success: function (data) {
              var post = '<tr id="post_id_' + data.id + '"><td>' + data.crm_type + '</td><td>' + data.title + ' </td><td>' + data.crm_date + ' ' + data.crm_time + '</td><td>' + data.month + '</td>';
              post += '<td class="td_btn"><a href="{{ url("clients/documents") }}/' + data.id_client + '/' + data.id + '" class="btn-edite"><i class="fa fa-eye"></i></a> <a href="javascript:void(0)" id="edit-post" data-id="' + data.id + '" class="btn-edite"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)" id="delete-post" data-id="' + data.id + '" class="btn-delete delete-post"><i class="fa fa-trash"></i></a></td></tr>';
              post += '';
               
              
              if (actionType == "create-post") {
                  $('#posts-crud').prepend(post);
              } else {
                  $("#post_id_" + data.id).replaceWith(post);
              }
 
              $('#postForm').trigger("reset");
              $('#ajax-crud-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn-save').html('Save Changes');
          }
      });
    }
  })
}
</script>


<script>
$(document).ready(function () {
    $(".Call").hide();
    $("#r1").click(function () {
        $(".Call").show();
    });
	
    $("#r2").click(function () {
        $(".Call").hide();
    });
	
	$(".Visit").hide();
    $("#r1").click (function () {
        $(".Visit").hide();
    });
	
    $("#r2").click(function () {
        $(".Visit").show();
    });
});
</script>



<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection