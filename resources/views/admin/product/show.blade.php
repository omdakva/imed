@extends('layouts.admin')
@section('title', 'Manage product')
@section('content')
<div class="container" >
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage product</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">product</a>
</div>
</div>



<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Product details</span>
</div>
<div class="col-md-5" style="text-align:right;">
@foreach($Product as $post)
<a href="{{ route('products.seo', $post->product_name) }}" class="btn btn-success mb-2">Preview product</a> 
<a href="javascript:void(0)" data-toggle="modal" data-target="#affiche" class="btn btn-success mb-2" id="create-new-post">Affiche</a> 

@if(Auth::user()->role=='3')
@else
@if($post->product_name_de=='1')
<a href="{{ route('product.translate', $post->id) }}" class="btn btn-success mb-2" id="create-new-post" ><span>Add translate</span></a>
@else
<a href="{{ route('product.translate.view', $post->id) }}" class="btn btn-success mb-2" id="create-new-post"><span>View translate</span></a>
@endif
@endif


@endforeach
<a href="{{ route('product.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif


@foreach($Product as $post)
<div class="modal fade" id="edit_photo" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit image</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form  action = "{{ route('photoprincipal.update', $post->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<div class="row">
<div class="col-md-12" style="text-align:center;">
<img class="user-avatar mr-2" src="{{ asset('photo/products_logo/'.$post->photo) }}" style="width:250px;height:150px;border-radius:0%;">
</div>
</div>

<input type="hidden" name="products_id" id="products_id" value="{{ $post->id }}">

<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max" style="height:70px;">
<label for="title" class="textblod">Edit photo</label>
<input type="file" name="photo" id="photo" class="form-control" required />
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>






<div class="modal fade" id="edit_description" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit Description</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('description.update', $post->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<input type="hidden" name="products_id" id="products_id" value="{{ $post->id }}">
<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max">
<label for="title" class="textblod">Description</label>
<textarea name="long_description" id="long_description" class="form-control" style="height:180px;">{{ $post->long_description }}</textarea>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>









<div class="modal fade" id="edit_short_description" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit Short Description</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('shortdescription.update', $post->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<input type="hidden" name="products_id" id="products_id" value="{{ $post->id }}">
<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max">
<label for="title" class="textblod">Short Description</label>
<textarea name="short_description" id="short_description" class="form-control" style="height:180px;">{{ $post->short_description }}</textarea>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>



<div class="col-md-12 cadre_add" >
<a href="{{ route('product.show', $post->id) }}" class="btn_add2active">Details</a> 
<a href="{{ route('product.photo.index', $post->id) }}" class="btn_add2">Product photo</a>
@if($post->product_name_de=='1')
<a href="" class="btn_add2blocked">Translate</a>
@else
<a href="{{ route('product.translate.view', $post->id) }}" class="btn_add2">Translate</a>
@endif
</div>
<div class="space"></div>
















<div class="row">
<div class="col-md-8">



<div class="cadre_add row">
<div class="col-md-4 mb-3 mx-auto">
{!! QrCode::size(100)->generate('http://localhost/store_project/product/'.$post->id.''); !!}
</div>


				
<div class="col-md-8 mb-3 mx-auto">
<label class="page-title2" >Details</label>
<label class="textblodfull">Name : <span class="textnormal">{{ $post->product_name }}</span></label>
<label class="textblodfull">Activation : <span class="textnormal">@if($post->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></label>
<label class="textblodfull">Creation date : <span class="textnormal">{{ $post->created_at }}</span></label>
</div>

</div>

<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >Description
@if(Auth::user()->role=='3')
@else
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#edit_description" class="icons-edit"> 
<i class="fa fa-edit"></i>
</a> 
@endif
</label>
<label class="textblodfull"><span class="textnormal">{{ $post->long_description }}</span></label>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >Short Description
@if(Auth::user()->role=='3')
@else
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#edit_short_description" class="icons-edit"> 
<i class="fa fa-edit"></i>
</a> 
@endif
</label>
<label class="textblodfull"><span class="textnormal">{{ $post->short_description }}</span></label>
</div>



<div class="space"></div>
<div class="space"></div>













<div class="modal fade" id="add_category" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Add category</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('product.store_category', $post->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<div class="modal-body">
<input type="hidden" name="products_id" id="products_id" value="{{ $post->id }}">
<input type="hidden" name="post_id" id="post_id">
<div class="col-lg-12 cadre_filter">
@foreach($Category as $category_list)

<label class="textblod" style="width:100%;margin-left:0px;">
<input type="checkbox" name="category_id[]" id="category" value="{{$category_list->id}}" @foreach($Category_product as $category_view)
 @if($category_view->id==$category_list->id)
CHECKED disabled
@else
no
@endif
@endforeach > {{$category_list->name}}
</label>
@if(count($category_list->subcategory))
@include('layout.subCategoryListproduct',['subcategory' => $category_list->subcategory])
@endif
@endforeach


</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>



<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >Category and Subcategory ( {{count($Category_product)}} ) 
@if(Auth::user()->role=='3')
@else
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#add_category" class="icons-edit"> 
<i class="fa fa-plus"></i>
</a> 
@endif
</label>
<div class="space"></div>
<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Name</th>
<th>Type</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Category_product as $category)
<div class="modal fade" id="delete{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
        <form action="{{ route('product.destroy_category', $category->id) }}" METHOD="post">
		@csrf
		<input type="hidden" value="{{ $category->id }}" name="id_category" />
		<button  type="submit" class="model_btn_delete delete-post">Yes, Delete</button>
		</form>
		</div>
    </div>
  </div>
</div>


<tr id="post_id_{{ $category->id }}">
<td>
@if($category->parent_id=='0')
<span class="textblod">{{ $category->name }}</span>
@else
<span class="textnormal">{{ $category->name }}</span>
@endif
</td>

<td>
@if($category->parent_id=='0')
<span class="textblod">Category</span>
@else
<span class="textnormal">Subcategory</span>
@endif
</td>

<td class="td_btn">
@if($category->parent_id=='0')
<a href="{{ route('category.show', $category->id) }}" class="btn-delete"><i class="fa fa-eye"></i></a>
@else
<a href="{{ route('subcategory.show', $category->id) }}" class="btn-delete"><i class="fa fa-eye"></i></a>
@endif
@if(Auth::user()->role=='3')
@else
<a href="javascript:void(0)" data-toggle="modal" data-target="#delete{{ $category->id }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
@endif
</td>
</tr>
@endforeach
</tbody>
</table>


</div>
</div>





<div class="col-md-4">
<div class="cadre_add ">


<div class="card-header border-bottom text-center">
@if($post->remise=='0')
<label class="textblodfull" style="font-size:25px;">{{ $post->product_price }} {{ $post->product_currency }}</span></label>
@else
<label class="textnormalfull" style="font-size:20px;">{{ $post->product_price }} {{ $post->product_currency }}</span></label>
<label class="line_v1"></label>
<span style="font-size:35px;" class="textnormal">{{ (($post->product_price) - (($post->remise / 100) * $post->product_price)) }} {{ $post->product_currency }}</span>
@endif
<div class="mb-3 mx-auto">
<img src="{{ asset('photo/products_logo/'.$post->photo) }}" alt="product logo" style="width:90%;height:160px;"> </div>
@if(Auth::user()->role=='3')
@else
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_photo" class="btn_add"> <i class="material-icons mr-1">person_add</i>Edit pic</a> 
@endif
</div>
<div class="space"></div>
<div class="space"></div>

<label class="textblodfull">In stock : <span class="textnormal">{{ $post->stock }}</span></label>
<label class="textblodfull">Remise : <span class="textnormal">{{ $post->remise }} %</span></label>
<label class="textblodfull">Remise amount : <span class="textnormal">{{ ($post->remise / 100) * $post->product_price}} {{ $post->product_currency }}</span></label>
<label class="textblodfull">Price after remise : 
<span style="font-size:35px;" class="textnormal">{{ (($post->product_price) - (($post->remise / 100) * $post->product_price)) }} {{ $post->product_currency }}</span>
</label>


</div>
</div>

</div>










<div class="modal fade right" id="affiche" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog-full-width modal-dialog momodel modal-fluid" role="document">
        <div class="modal-content-full-width modal-content ">
            
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Product poster</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>


<div class="" id="html-content-holder" style="padding:15px;border:2px solid #9e844a;height:auto;">
<div class="row">
<div class="col-md-12 textblod" style="text-align:center;font-size:25px;">
{{ $post->product_name }}
</div>
<div class="space"></div>
<div class="space"></div>
<div class="col-md-6" style="text-align:center;">
<img class="user-avatar mr-2" src="{{ asset('public/photo/products_logo/'.$post->photo) }}" style="width:100%;height:300px;border-radius:0%;">
</div>
<div class="col-md-6" style="text-align:left;">
<div class="col-md-12" style="text-align:left;font-size:45px;color:#9e844a;">
{{ $post->product_price }} {{ $post->product_currency }}
</div>
<div class="col-md-12 textnormal" style="text-align:left;font-size:15px;">
{{ $post->long_description }}
</div>
</div>
</div>
</div>

<div class="col-md-12 textblod" style="text-align:center;font-size:25px;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<input class="model_btn_close" id="btn-Preview-Image" type="button" value="Preview"/>
<a href="javascript:void(0)" class="model_btn_save" id="btn-Convert-Html2Image" style="color:#ffffff;text-decoration:none;" >Download</a>
</div>
<div id="previewImage">
</div>

</div>
</div>
</div>


@endforeach
	

</div>
</div>








<style>
    .modal-dialog-full-width {
		
		z-index:99999999999999999999999999999;
        width: 900px !important;
        height: 100% !important;
        margin: 0 !important;
        padding: 0 !important;
        max-width:none !important;
		float:right;

    }

    .modal-content-full-width  {
        height: auto !important;
        min-height: 100% !important;
        border-radius: 0 !important;
        background-color: #ececec !important 
    }

    .modal-header-full-width  {
        border-bottom: 1px solid #9ea2a2 !important;
    }

    .modal-footer-full-width  {
        border-top: 1px solid #9ea2a2 !important;
    }
</style>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>


<script>
$(document).ready(function(){

	
var element = $("#html-content-holder"); // global variable
var getCanvas; // global variable
 
    $("#btn-Preview-Image").on('click', function () {
         html2canvas(element, {
         onrendered: function (canvas) {
                $("#previewImage").append(canvas);
                getCanvas = canvas;
             }
         });
    });

	$("#btn-Convert-Html2Image").on('click', function () {
    var imgageData = getCanvas.toDataURL("image/png");
    // Now browser starts downloading it instead of just showing it
    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
    $("#btn-Convert-Html2Image").attr("download", "your_pic_name.png").attr("href", newData);
	});

});

</script>





</div>
</br>


<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#create-new-post').click(function () {
        $('#btn-save').val("create-post");
        $('#postForm').trigger("reset");
        $('#postCrudModal').html("Add New Category");
        $('#ajax-crud-modal').modal('show');
    });
 
    $('body').on('click', '#edit-post', function () {
      var post_id = $(this).data('id');
      $.get(+post_id+'/edit', function (data) {
         $('#postCrudModal').html("Edit Documents");
          $('#btn-save').val("edit-post");
          $('#ajax-crud-modal').modal('show');
          $('#post_id').val(data.id);
          $('#id_client').val(data.id_client);
          $('#crm_type').val(data.crm_type);
		  
		          
		  
          $('#title').val(data.title);
          $('#fichier').val(data.fichier);
          $('#crm_date').val(data.crm_date);  
          $('#crm_time').val(data.crm_time);  
          $('#note').val(data.note);  
          $('#id_admin').val(data.id_admin);  
          $('#id_day').val(data.id_day);  
          $('#id_month').val(data.id_month);  
          $('#month').val(data.month);  
          $('#id_year').val(data.id_year);  
      })
   });

    $('body').on('click', '.delete-post', function () {
        var post_id = $(this).data("id");		
		
		
		
		

 
        $.ajax({
            type: "DELETE",
            url: "{{ url('admin/product/destroy_category')}}"+'/'+post_id,
            success: function (data) {
                $("#post_id_" + post_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });   
  });
 
 if ($("#postForm").length > 0) {
      $("#postForm").validate({
 
     submitHandler: function(form) {

      var actionType = $('#btn-save').val();
      $('#btn-save').html('Sending..');


      $.ajax({
          data: $('#postForm').serialize(),
          url: "{{ route('category.store') }}",
        
          type: "POST",
          dataType: 'json',
          success: function (data) {
              var post = '<tr id="post_id_' + data.id + '"><td>' + data.crm_type + '</td><td>' + data.title + ' </td><td>' + data.crm_date + ' ' + data.crm_time + '</td><td>' + data.month + '</td>';
              post += '<td class="td_btn"><a href="{{ url("clients/documents") }}/' + data.id_client + '/' + data.id + '" class="btn-edite"><i class="fa fa-eye"></i></a> <a href="javascript:void(0)" id="edit-post" data-id="' + data.id + '" class="btn-edite"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)" id="delete-post" data-id="' + data.id + '" class="btn-delete delete-post"><i class="fa fa-trash"></i></a></td></tr>';
              post += '';
               
              
              if (actionType == "create-post") {
                  $('#posts-crud').prepend(post);
              } else {
                  $("#post_id_" + data.id).replaceWith(post);
              }
 
              $('#postForm').trigger("reset");
              $('#ajax-crud-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn-save').html('Save Changes');
          }
      });
    }
  })
}
</script>


<script>
$(document).ready(function () {
    $(".Call").hide();
    $("#r1").click(function () {
        $(".Call").show();
    });
	
    $("#r2").click(function () {
        $(".Call").hide();
    });
	
	$(".Visit").hide();
    $("#r1").click (function () {
        $(".Visit").hide();
    });
	
    $("#r2").click(function () {
        $(".Visit").show();
    });
});
</script>



@endsection