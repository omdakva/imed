@extends('layouts.admin')
@section('title', 'Manage product')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage product</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">product</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Product details</span>
</div>
<div class="col-md-5" style="text-align:right;">
@foreach($Product as $post)
@if(empty($post->product_name_de))
<a href="{{ route('product.translate', $post->id) }}" class="btn btn-success mb-2" id="create-new-post" ><span>Add translate</span></a>
@else
<a href="{{ route('product.translate.view', $post->id) }}" class="btn btn-success mb-2" id="create-new-post"><span>View translate</span></a>
@endif
@endforeach
<a href="{{ route('product.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif


@foreach($Product as $post)

<div class="modal fade" id="edit_photo" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit image</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form  action = "{{ route('photoprincipal.update', $post->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<div class="row">
<div class="col-md-12" style="text-align:center;">
<img class="user-avatar mr-2" src="{{ asset('public/photo/products_logo/'.$post->photo) }}" style="width:250px;height:150px;border-radius:0%;">
</div>
</div>

<input type="hidden" name="products_id" id="products_id" value="{{ $post->id }}">

<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max" style="height:70px;">
<label for="title" class="textblod">Edit photo</label>
<input type="file" name="photo" id="photo" class="form-control" required />
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>






<div class="modal fade" id="edit_description" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit Description</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ url('admin/product/translate/') }}/{{$post->id}}/update_long_description" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<input type="hidden" name="products_id" id="products_id" value="{{ $post->id }}">
<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max">
<label for="title" class="textblod">Description</label>
<textarea name="long_description_de" id="long_description_de" class="form-control" style="height:180px;">{{ $post->long_description_de }}</textarea>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>









<div class="modal fade" id="edit_short_description" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit Short Description</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ url('admin/product/translate/') }}/{{$post->id}}/update_short_description" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<input type="hidden" name="products_id" id="products_id" value="{{ $post->id }}">
<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max">
<label for="title" class="textblod">Short Description</label>
<textarea name="short_description_de" id="short_description_de" class="form-control" style="height:180px;">{{ $post->short_description_de }}</textarea>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>



<div class="col-md-12 cadre_add" >
<a href="{{ route('product.show', $post->id) }}" class="btn_add2">Details</a> 
<a href="{{ route('product.photo.index', $post->id) }}" class="btn_add2">Product photo</a> 
<a href="{{ route('product.translate.view', $post->id) }}" class="btn_add2active">Translate</a> 
</div>
<div class="space"></div>

<div class="row">
<div class="col-md-8">



<div class="cadre_add">
<label class="page-title2" >Details in Deutsch</label>
<label class="textblodfull">Name : <span class="textnormal">{{ $post->product_name_de }}</span></label>
<label class="textblodfull">Activation : <span class="textnormal">@if($post->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></label>
<label class="textblodfull">Creation date : <span class="textnormal">{{ $post->created_at }}</span></label>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >Description
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#edit_description" class="icons-edit"> 
<i class="fa fa-edit"></i>
</a> 
</label>
<label class="textblodfull"><span class="textnormal">{{ $post->long_description_de }}</span></label>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >Short Description
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#edit_short_description" class="icons-edit"> 
<i class="fa fa-edit"></i>
</a>
</label>
<label class="textblodfull"><span class="textnormal">{{ $post->short_description_de }}</span></label>
</div>

<div class="space"></div>
<div class="space"></div>













</div>

<div class="col-md-4">
<div class="cadre_add ">


<div class="card-header border-bottom text-center">
<label class="textblodfull" style="font-size:25px;">{{ $post->product_price }} {{ $post->product_currency }}</span></label>
<div class="mb-3 mx-auto">
<img src="{{ asset('public/photo/products_logo/'.$post->photo) }}" alt="User Avatar" style="width:90%;height:160px;"> </div>

<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_photo" class="btn_add"> <i class="material-icons mr-1">person_add</i>Edite pic</a> 
</div>
<div class="space"></div>
<div class="space"></div>

<label class="textblodfull">In stock : <span class="textnormal">{{ $post->stock }}</span></label>
<label class="textblodfull">Remise : <span class="textnormal">{{ $post->remise }} %</span></label>
<label class="textblodfull">Remise amount : <span class="textnormal">{{ ($post->remise / 100) * $post->product_price}} {{ $post->product_currency }}</span></label>
<label class="textblodfull">Price after remise : 
<span style="font-size:35px;" class="textnormal">{{ (($post->product_price) - (($post->remise / 100) * $post->product_price)) }} {{ $post->product_currency }}</span></label>


</div>
</div>

</div>
@endforeach




  
 
  





		

</div>
</div>
</div>
</br>


<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#create-new-post').click(function () {
        $('#btn-save').val("create-post");
        $('#postForm').trigger("reset");
        $('#postCrudModal').html("Add New Category");
        $('#ajax-crud-modal').modal('show');
    });
 
    $('body').on('click', '#edit-post', function () {
      var post_id = $(this).data('id');
      $.get(+post_id+'/edit', function (data) {
         $('#postCrudModal').html("Edit Documents");
          $('#btn-save').val("edit-post");
          $('#ajax-crud-modal').modal('show');
          $('#post_id').val(data.id);
          $('#id_client').val(data.id_client);
          $('#crm_type').val(data.crm_type);
		  
		          
		  
          $('#title').val(data.title);
          $('#fichier').val(data.fichier);
          $('#crm_date').val(data.crm_date);  
          $('#crm_time').val(data.crm_time);  
          $('#note').val(data.note);  
          $('#id_admin').val(data.id_admin);  
          $('#id_day').val(data.id_day);  
          $('#id_month').val(data.id_month);  
          $('#month').val(data.month);  
          $('#id_year').val(data.id_year);  
      })
   });

    $('body').on('click', '.delete-post', function () {
        var post_id = $(this).data("id");		
		
		
		
		

 
        $.ajax({
            type: "DELETE",
            url: "{{ url('admin/product')}}"+'/'+post_id,
            success: function (data) {
                $("#post_id_" + post_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });   
  });
 
 if ($("#postForm").length > 0) {
      $("#postForm").validate({
 
     submitHandler: function(form) {

      var actionType = $('#btn-save').val();
      $('#btn-save').html('Sending..');


      $.ajax({
          data: $('#postForm').serialize(),
          url: "{{ route('category.store') }}",
        
          type: "POST",
          dataType: 'json',
          success: function (data) {
              var post = '<tr id="post_id_' + data.id + '"><td>' + data.crm_type + '</td><td>' + data.title + ' </td><td>' + data.crm_date + ' ' + data.crm_time + '</td><td>' + data.month + '</td>';
              post += '<td class="td_btn"><a href="{{ url("clients/documents") }}/' + data.id_client + '/' + data.id + '" class="btn-edite"><i class="fa fa-eye"></i></a> <a href="javascript:void(0)" id="edit-post" data-id="' + data.id + '" class="btn-edite"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)" id="delete-post" data-id="' + data.id + '" class="btn-delete delete-post"><i class="fa fa-trash"></i></a></td></tr>';
              post += '';
               
              
              if (actionType == "create-post") {
                  $('#posts-crud').prepend(post);
              } else {
                  $("#post_id_" + data.id).replaceWith(post);
              }
 
              $('#postForm').trigger("reset");
              $('#ajax-crud-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn-save').html('Save Changes');
          }
      });
    }
  })
}
</script>


<script>
$(document).ready(function () {
    $(".Call").hide();
    $("#r1").click(function () {
        $(".Call").show();
    });
	
    $("#r2").click(function () {
        $(".Call").hide();
    });
	
	$(".Visit").hide();
    $("#r1").click (function () {
        $(".Visit").hide();
    });
	
    $("#r2").click(function () {
        $(".Visit").show();
    });
});
</script>



@endsection