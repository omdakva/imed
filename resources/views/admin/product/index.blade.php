@extends('layouts.admin')
@section('title', 'Manage product')
@section('content')




<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage product</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Product</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>


<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Product ( {{ count($Product) }} )</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('product.story') }}" class="btn btn-success mb-2">Story</a> 
<a href="{{ route('category.create') }}" class="btn-switch" title="Excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a> 
<a href="{{ route('category.create') }}" class="btn-switch" title="PDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> 
@if(Auth::user()->role=='3')
@else
<a href="{{ route('product.create') }}" class="btn btn-success mb-2" id="create-new-post">Add new</a>
@endif 
<a href="{{ route('product.index') }}" class="btn-switch-active" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a> 
<a href="{{route('product.card')}}" class="btn-switch" title="List"><i class="fa fa-list" aria-hidden="true"></i></a> 
</div>
</div>




<div class="space"></div>
<div class="space"></div>





<div class="row">
<div class="col-12">
@if(session()->get('success'))
<div class="alert alert-success mt-3">
{{ session()->get('success') }}  
</div>
@endif 

<div class="card row">
<div class="card-body row">

<div class="form-group col-md-4">
<label><strong>Status</strong></label>
<select id='status' class="form-control">
<option value="">--Select status--</option>
<option value="active">Active</option>
<option value="0">Deactive</option>
</select>
</div>

<div class="form-group col-md-4">
<label><strong>Translate</strong></label>
<select id='status' class="form-control">
<option value="">--Select translate--</option>
<option value="active">Translate</option>
<option value="0">In progress</option>
</select>
</div>

<div class="form-group col-md-4">
<label><strong>Offer</strong></label>
<select id='status' class="form-control">
<option value="">--Select offer--</option>
<option value="active">Yes</option>
<option value="0">No</option>
</select>
</div>

</div>
</div>
<div class="space"></div>
<div class="space"></div>
<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Name</th>
<th>Price</th>
<th>Date</th>
<th style="text-align:center;">Etat</th>
<th style="text-align:center;">Translate</th>
<th style="text-align:center;">Offer</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Product as $post)
<div class="modal fade" id="delete{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
		<a  href="javascript:void(0)" id="delete-post" data-id="{{ $post->id }}" class="model_btn_delete delete-post" data-dismiss="modal">Yes, Delete</a>
      </div>
    </div>
  </div>
</div>


<tr id="post_id_{{ $post->id }}">
<td> <img class="user-avatar rounded-circle mr-2" src="{{ asset('photo/products_logo/'.$post->photo) }}" style="width:40px;height:40px;border-radius:50%;"alt="product logo">
{{ $post->product_name  }}</td>
<td>{{ $post->product_price  }} {{ $post->product_currency  }}</td>
<td>{{ $post->created_at  }}</td>
<td style="text-align:center;">
@if($post->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif
</td>

<td style="text-align:center;">
@if(Auth::user()->role=='3')
@if($post->product_name_de=='1')
<a href="javascript:void(0)" class="in_progress_back"><span>In progress</span></a>
@else
<a href="javascript:void(0)" class="active_back"><span>Translate</span></a>
@endif
@else
@if($post->product_name_de=='1')
<a href="{{ route('product.translate', $post->id) }}"class="in_progress_back"><span>In progress</span></a>
@else
<a href="{{ route('product.translate.view', $post->id) }}" class="active_back"><span>Translate</span></a>
@endif
@endif
</td>


<td style="text-align:center;">
@if(Auth::user()->role=='3')
@if($post->remise=='0')
<a><i class="fa fa-star" style="color:#c0c0c0;" aria-hidden="true"></i></a>
@else
<a><i class="fa fa-star" style="color:#229902;" aria-hidden="true"></i></a>
@endif
@else
@if($post->remise=='0')
<a href="{{ route('product.offer', $post->id) }}"><i class="fa fa-star" style="color:#c0c0c0;" aria-hidden="true"></i></a>
@else
<a href="{{ route('product.offer', $post->id) }}"><i class="fa fa-star" style="color:#229902;" aria-hidden="true"></i></a>
@endif
@endif
</td>

<td class="td_btn">
<a href="{{ route('product.show', $post->id) }}" class="btn-delete"><i class="fa fa-eye"></i></a>
@if(Auth::user()->role=='3')
@else
<a href="{{ route('product.edit', $post->id) }}" class="btn-delete"><i class="fa fa-edit"></i></a>
<a href="javascript:void(0)" data-toggle="modal" data-target="#delete{{ $post->id }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
@endif
</td>
</tr>
@endforeach
</tbody>
</table>        

</div> 
</div>
</div>





<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#create-new-post').click(function () {
        $('#btn-save').val("create-post");
        $('#postForm').trigger("reset");
        $('#postCrudModal').html("Add New Category");
        $('#ajax-crud-modal').modal('show');
    });
 
    $('body').on('click', '#edit-post', function () {
      var post_id = $(this).data('id');
      $.get(+post_id+'/edit', function (data) {
         $('#postCrudModal').html("Edit Documents");
          $('#btn-save').val("edit-post");
          $('#ajax-crud-modal').modal('show');
          $('#post_id').val(data.id);
          $('#id_client').val(data.id_client);
          $('#crm_type').val(data.crm_type);
		  
		          
		  
          $('#title').val(data.title);
          $('#fichier').val(data.fichier);
          $('#crm_date').val(data.crm_date);  
          $('#crm_time').val(data.crm_time);  
          $('#note').val(data.note);  
          $('#id_admin').val(data.id_admin);  
          $('#id_day').val(data.id_day);  
          $('#id_month').val(data.id_month);  
          $('#month').val(data.month);  
          $('#id_year').val(data.id_year);  
      })
   });

    $('body').on('click', '.delete-post', function () {
        var post_id = $(this).data("id");		
		
		
		
		

 
        $.ajax({
            type: "DELETE",
            url: "{{ url('admin/product')}}"+'/'+post_id,
            success: function (data) {
                $("#post_id_" + post_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });   
  });
 
 if ($("#postForm").length > 0) {
      $("#postForm").validate({
 
     submitHandler: function(form) {

      var actionType = $('#btn-save').val();
      $('#btn-save').html('Sending..');


      $.ajax({
          data: $('#postForm').serialize(),
          url: "{{ route('category.store') }}",
        
          type: "POST",
          dataType: 'json',
          success: function (data) {
              var post = '<tr id="post_id_' + data.id + '"><td>' + data.crm_type + '</td><td>' + data.title + ' </td><td>' + data.crm_date + ' ' + data.crm_time + '</td><td>' + data.month + '</td>';
              post += '<td class="td_btn"><a href="{{ url("clients/documents") }}/' + data.id_client + '/' + data.id + '" class="btn-edite"><i class="fa fa-eye"></i></a> <a href="javascript:void(0)" id="edit-post" data-id="' + data.id + '" class="btn-edite"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)" id="delete-post" data-id="' + data.id + '" class="btn-delete delete-post"><i class="fa fa-trash"></i></a></td></tr>';
              post += '';
               
              
              if (actionType == "create-post") {
                  $('#posts-crud').prepend(post);
              } else {
                  $("#post_id_" + data.id).replaceWith(post);
              }
 
              $('#postForm').trigger("reset");
              $('#ajax-crud-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn-save').html('Save Changes');
          }
      });
    }
  })
}
</script>
<script>
$(document).ready(function () {
    $(".Call").hide();
    $("#r1").click(function () {
        $(".Call").show();
    });
	
    $("#r2").click(function () {
        $(".Call").hide();
    });
	
	$(".Visit").hide();
    $("#r1").click (function () {
        $(".Visit").hide();
    });
	
    $("#r2").click(function () {
        $(".Visit").show();
    });
});
</script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>







     
     
<script type="text/javascript">
  $(function () {
      
    var table = $('#example').DataTable2({
        processing: true,
        serverSide: true,
        ajax: {
          url: "{{ route('users.index') }}",
          data: function (d) {
                d.status = $('#status').val(),
                d.search = $('input[type="search"]').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'fisrtname', name: 'fisrtname'},
            {data: 'email', name: 'email'},
            {data: 'status', name: 'status'},
        ]
    });
  
    $('#status').change(function(){
        table.draw();
    });
      
  });
</script>
@endsection