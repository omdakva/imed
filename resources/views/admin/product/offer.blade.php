@extends('layouts.admin')
@section('title', 'Manage product')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage product</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('product.index') }}">Product /</a>
<a id="pagetitlesecondelink">Offer</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Offer product</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('product.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="">
<form action = "{{ route('product.offer.save', $Product->id) }}" method = "POST">
@csrf
@method('PATCH') 
<input type="hidden" name="offer" value="1" />
<div class="row">

<div class="col-lg-8">		
<div class="col-lg-12 cadre_filter">		


<h3 class="page-title2" >Information</h3>


<div class="row">
<div class="col-sm-6" style="padding:0px;">
<label for="name" class="col-sm-12 control-label textblod">Price before offer</label>
<div class="col-sm-12">
<input type="text" class="form-control" value="{{ $Product->product_price }} {{ $Product->product_currency }}" disabled />
</div>
</div>

<div class="col-sm-6" style="padding:0px;">
<label for="name" class="col-sm-12 control-label textblod">Offer ( % )</label>
<div class="col-sm-12">
<input type="number" class="form-control" id="remise" name="remise" value="{{ $Product->remise }}">
</div>
</div>					
</div>					



</div>
</div>




<div class="col-md-4">
<div class="cadre_add ">


<div class="card-header border-bottom text-center">
<label class="textblodfull" style="font-size:25px;">{{ $Product->product_price }} {{ $Product->product_currency }}</span></label>
<div class="mb-3 mx-auto">
<img src="{{ asset('public/photo/products_logo/'.$Product->photo) }}" alt="User Avatar" style="width:90%;height:160px;"> </div>

</div>
<div class="space"></div>
<div class="space"></div>

<label class="textblodfull">In stock : <span class="textnormal">{{ $Product->stock }}</span></label>
<label class="textblodfull">Remise : <span class="textnormal">{{ $Product->remise }} %</span></label>
<label class="textblodfull">Remise amount : <span class="textnormal">{{ ($Product->remise / 100) * $Product->product_price}} {{ $Product->product_currency }}</span></label>
<label class="textblodfull">Price after remise : 
<span style="font-size:35px;" class="textnormal">{{ (($Product->product_price) - (($Product->remise / 100) * $Product->product_price)) }} {{ $Product->product_currency }}</span></label>


</div>
</div>


<div class="space"></div>
<div class="space"></div>



<div class="col-lg-12 cadre_filter" style="text-align:right;">
<a href="{{ route('product.index') }}" class="model_btn_close">Cancel</a>
<button type="submit" class="model_btn_save">Save</button>
</div>


</form>
</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection