@extends('layouts.admin')
@section('title', 'Manage setting')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('settings.index') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('country.index') }}">Country /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new country</h3>
</div>
<div class="row">
<div class="col-lg-12 cadre_filter">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
	
<form id="file-upload-form" class="uploader" action="{{ route('country.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="active" id="active" value="1">
<input type="hidden" name="parent_id" id="parent_id" value="0">

            <div class="row">


<div class="form-group col-sm-6">
<label for="name" class="col-sm-12 control-label textblod">Logo</label>
<div class="col-sm-12">
<input type="file" class="form-control" name="photo" id="photo" class="form-control">
</div>
</div>


<div class="form-group col-sm-6">
<label for="name" class="col-sm-12 control-label textblod">Title</label>
<div class="col-sm-12">
<input type="text" class="form-control" name="country" id="country" class="form-control input-lg" required="">
<span id="error_email"></span>
</div>
</div>

			<div class="form-group col-sm-6">
                <label for="name" class="col-sm-12 control-label textblod">Symbole</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="symbole" name="symbole" value="" required="">
                </div>
            </div>
			
			<div class="form-group col-sm-6">
                <label for="name" class="col-sm-12 control-label textblod">Tel code</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control"  name="code" value="" required="">
                </div>
            </div>
            </div>

            
<div class="modal-footer"style="text-align:right;">
<button type="submit" name="register" id="register"  class="model_btn_save">Save</button>
</div>
</form>
</div>
</div>
</div>



<script>
$(document).ready(function(){

	$('#country').blur(function(){
		var error_email = '';
		var country = $('#country').val();
		var _token = $('input[name="_token"]').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($.trim(country).length > 0)
		{
			if(!(country))
			{				
				$('#error_email').html('<label class="text-danger">Invalid Country</label>');
				$('#country').addClass('has-error');
				$('#register').attr('disabled', 'disabled');
			}
			else
			{
				$.ajax({
					url:"{{ route('country.check') }}",
					method:"POST",
					data:{country:country, _token:_token},
					success:function(result)
					{
						if(result == 'unique')
						{
							$('#error_email').html('<label class="text-success">Country Available</label>');
							$('#country').removeClass('has-error');
							$('#register').attr('disabled', false);
						}
						else
						{
							$('#error_email').html('<label class="text-danger">Country not Available</label>');
							$('#country').addClass('has-error');
							$('#register').attr('disabled', 'disabled');
						}
					}
				})
			}
		}
		else
		{
			$('#error_email').html('<label class="text-danger">Country is required</label>');
			$('#country').addClass('has-error');
			$('#register').attr('disabled', 'disabled');
		}
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('#emailcompany').blur(function(){
		var error_email = '';
		var email = $('#emailcompany').val();
		var _token = $('input[name="_token"]').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($.trim(emailcompany).length > 0)
		{
			if(!filter.test(email))
			{				
				$('#error_emailcompany').html('<label class="text-danger">Invalid Email</label>');
				$('#emailcompany').addClass('has-error');
				$('#registercompany').attr('disabled', 'disabled');
			}
			else
			{
				$.ajax({
					url:"",
					method:"POST",
					data:{email:email, _token:_token},
					success:function(result)
					{
						if(result == 'unique')
						{
							$('#error_emailcompany').html('<label class="text-success">Email Available</label>');
							$('#emailcompany').removeClass('has-error');
							$('#registercompany').attr('disabled', false);
						}
						else
						{
							$('#error_emailcompany').html('<label class="text-danger">Email not Available</label>');
							$('#emailcompany').addClass('has-error');
							$('#registercompany').attr('disabled', 'disabled');
						}
					}
				})
			}
		}
		else
		{
			$('#error_email').html('<label class="text-danger">Email is required</label>');
			$('#email').addClass('has-error');
			$('#register').attr('disabled', 'disabled');
		}
	});
	
});
</script>
@endsection