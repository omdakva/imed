@extends('layouts.admin')
@section('title', 'Manage setting')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('settings.index') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('country.index') }}">Country /</a>
<a id="pagetitlesecondelink">Details</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Country details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('country.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="cadre_add">
<div class="row">
<h3 class="page-title2" >Country details</h3>
<div class="col-md-3">
<div class="mb-3 mx-auto">
<img src="{{ asset('photo/country_logo/'.$Country->photo) }}" alt="product logo" style="width:90%;height:160px;"> </div>
<div class="col-md-12 text-center">
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_photo" class="btn_add"> <i class="material-icons mr-1">person_add</i>Edit pic</a> 
</div>
<div calss="space"></div>
<div calss="space"></div>
</div>

<div class="col-md-9">
<div class="form-group col-md-12">
<label class="textblodfull">Country : <span class="textnormal">{{ $Country->country }}</span></label>
<label class="textblodfull">Symbole : <span class="textnormal">{{ $Country->symbole }}</span></label>
<label class="textblodfull">Activation : <span class="textnormal">@if($Country->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></label>
<label class="textblodfull">Creation date : <span class="textnormal">{{ $Country->created_at }}</span></label>
<label class="textblodfull">Update date : <span class="textnormal">{{ $Country->updated_at }}</span></label>
</div>
</div>
</div>
</div>







<div class="modal fade" id="edit_photo" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit logo</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form  action = "{{ route('country.update_logo', $Country->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<div class="row">
<div class="col-md-12" style="text-align:center;">
<img class="user-avatar mr-2" src="{{ asset('photo/country_logo/'.$Country->photo) }}" style="width:250px;height:150px;border-radius:0%;">
</div>
</div>

<input type="hidden" name="id" id="id" value="{{ $Country->id }}">

<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max" style="height:70px;">
<label for="title" class="textblod">Edit photo</label>
<input type="file" name="photo" id="photo" class="form-control" required />
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>
  
  
  
<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >City ( {{count($City)}} )
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#add_city" class="icons-edit"> 
<i class="fa fa-plus"></i>
</a> 
</label>
<div class="space"></div>







<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>City</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($City as $post)
<div class="modal fade" id="exampleModal{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
		<a  href="javascript:void(0)" id="delete-post" data-id="{{ $post->id }}" class="model_btn_delete delete-post" data-dismiss="modal">Yes, Delete</a>
      </div>
    </div>
  </div>
</div>


<tr id="post_id_{{ $post->id }}">
<td>{{ $post->country }}</td>
<td class="td_btn">
<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal{{ $post->id }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>











<div class="modal fade" id="add_city" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit Short Description</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('city.store') }}"  method ="POST" accept-charset="utf-8">
@csrf
<div class="modal-body">
<input type="hidden" name="parent_id" id="parent_id" value="{{ $Country->id }}">
<input type="hidden" name="symbole" id="symbole" value="{{ $Country->symbole }}">
<input type="hidden" name="code" id="code" value="{{ $Country->code }}">
<input type="hidden" name="active" id="active" value="1">
<input type="hidden" name="post_id" id="post_id">
<input type="hidden" name="photo" id="photo" value="0" />
<div class="form-group form-group-max">
<label for="title" class="textblod">City</label>
<input type="text" name="country" id="country" class="form-control" />
</div>



</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>

</div>
 
  





		

</div>
</div>
</div>
</br>


@endsection