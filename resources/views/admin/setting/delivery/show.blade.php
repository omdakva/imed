@extends('layouts.admin')
@section('title', 'Manage setting')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('settings.index') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('method-delivery.index') }}">Methods of Delivery /</a>
<a id="pagetitlesecondelink">Details</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Method of Delivery details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('method-delivery.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="Individual cadre_add">
<div class="row">
<h3 class="page-title2" >Delivery details</h3>



<div class="form-group col-md-12">
<label class="textblodfull">Name : <span class="textnormal">{{ $Methoddelivery->name }}</span></label>
<label class="textblodfull">Published : <span class="textnormal">@if($Methoddelivery->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span>
</label>
<label class="textblodfull">Plugin : <span class="textnormal">{{ $Methoddelivery->plugin }}</span></label>
<label class="textblodfull">Price : <span class="textnormal">{{ $Methoddelivery->price }} {{ $Methoddelivery->currency }}</span></label>

<div class="space"></div>
<div class="space"></div>

<label class="textblodfull">Shipment details :</label>
<label class="textblodfull"><span class="textnormal">{{ $Methoddelivery->shipment_details }}</span></label>
<label class="textblodfull">Description :</label>
<label class="textblodfull"><span class="textnormal">{{ $Methoddelivery->description }}</span></label>

<label class="textblodfull">Creation date : <span class="textnormal">{{ $Methoddelivery->created_at }}</span></label>
<label class="textblodfull">Update date : <span class="textnormal">{{ $Methoddelivery->updated_at }}</span></label>
</div>





</div>


</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection