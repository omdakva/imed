@extends('layouts.admin')
@section('title', 'Manage setting')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('settings.index') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('method-delivery.index') }}">Methods of Delivery /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new methods of Delivery</h3>
</div>
<div class="row">
<div class="col-lg-12">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
	
<form id="file-upload-form" class="uploader" action="{{ route('method-delivery.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="active" id="active" value="0">
<div class="row">

<div class="col-lg-8 col-sm-12 mb-12">
<div class="card card-small h-100">
<div class="space"></div>

<div class="form-group">
<label for="name" class="col-sm-12 control-label textblod">Title</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="name" name="name" value="" required="">
</div>
</div>

<div class="form-group">
<label for="name" class="col-sm-12 control-label textblod">Plugin</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="plugin" name="plugin" value="" required="">
</div>
</div>

<div class="row">
<div class="form-group col-sm-6">
<label for="name" class="col-sm-12 control-label textblod">Price</label>
<div class="col-sm-12">
<input type="number" class="form-control" id="price" name="price" value="" required="">
</div>
</div>

<div class="form-group col-sm-6">
<label for="name" class="col-sm-12 control-label textblod">Currency</label>
<div class="col-sm-12">
<select type="text" class="form-control" id="currency" name="currency" value="" required="">
@foreach($currency as $post)
<option value="{{$post->symbole}}">{{$post->symbole}}</option>
@endforeach
</select>
</div>
</div>
</div>

<div class="form-group">
<label for="name" class="col-sm-12 control-label textblod">Shipment details</label>
<div class="col-sm-12">
<textarea type="text" class="form-control" id="shipment_details" name="shipment_details" value="" required="" style="height:120px;"></textarea>
</div>
</div>

<div class="form-group">
<label for="name" class="col-sm-12 control-label textblod">Description</label>
<div class="col-sm-12">
<textarea type="text" class="form-control" id="description" name="description" value="" required="" style="height:120px;"></textarea>
</div>
</div>
</div>
</div>




<div class="col-lg-4 col-sm-12 mb-12">
<div class="card card-small h-100" style="">
<div class="space"></div>
<div class="card-body d-flex py-0">
<label class="textblod" style="width:100%;margin-left:0px;">
<input type="checkbox" name="active" id="active" value="1" checked> Published
</label>
</div>
</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="col-lg-12 col-sm-12 mb-12">
<div class="card card-small h-100" style="text-align:right;padding:10px;">
<div class="" style="text-align:right;">
<button type="submit" class="model_btn_save">Save</button>
</div>
</div>
</div>


</form>
</div>
</div>
</div>

</BR>


<script src="{{ asset('public/jquery/example/jQuery.maxlength.js')}}"></script>

<script>
$('div.form-group-max').maxlength();
</script>
<script>
$(document).ready(function () {
    $(".text").hide();
    $("#r1").click(function () {
        $(".text").show();
    });
	
    $("#r2").click(function () {
        $(".text").hide();
    });
	
	$(".text2").hide();
    $("#r1").click (function () {
        $(".text2").hide();
    });
	
    $("#r2").click(function () {
        $(".text2").show();
    });
});
</script>


<script>
   function loadPreview(input){
       var data = $(input)[0].files; //this file data
       $.each(data, function(index, file){
           if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
               var fRead = new FileReader();
               fRead.onload = (function(file){
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image thumb element
                       $('#thumb-output').append(img);
                   };
               })(file);
               fRead.readAsDataURL(file);
           }
       });
   }
</script>
@endsection