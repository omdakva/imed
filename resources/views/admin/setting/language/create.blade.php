@extends('layouts.admin')
@section('title', 'Manage setting')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('settings.index') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('settings-languages.index') }}">Languages /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new language</h3>
</div>
<div class="row">
<div class="col-lg-12 cadre_filter">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
	
<form id="file-upload-form" class="uploader" action="{{ route('settings-languages.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="active" id="active" value="0">

            <div class="row">
            <div class="form-group col-sm-6">
                <label for="name" class="col-sm-12 control-label textblod">Title</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" value="" required="">
                </div>
            </div>
			<div class="form-group col-sm-6">
                <label for="name" class="col-sm-12 control-label textblod">Symbole</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="symbole" name="symbole" value="" required="">
                </div>
            </div>
            </div>

            
<div class="modal-footer"style="text-align:right;">
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>
</div>
</div>

</BR>


<script src="{{ asset('public/jquery/example/jQuery.maxlength.js')}}"></script>

<script>
$('div.form-group-max').maxlength();
</script>
<script>
$(document).ready(function () {
    $(".text").hide();
    $("#r1").click(function () {
        $(".text").show();
    });
	
    $("#r2").click(function () {
        $(".text").hide();
    });
	
	$(".text2").hide();
    $("#r1").click (function () {
        $(".text2").hide();
    });
	
    $("#r2").click(function () {
        $(".text2").show();
    });
});
</script>


<script>
   function loadPreview(input){
       var data = $(input)[0].files; //this file data
       $.each(data, function(index, file){
           if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
               var fRead = new FileReader();
               fRead.onload = (function(file){
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image thumb element
                       $('#thumb-output').append(img);
                   };
               })(file);
               fRead.readAsDataURL(file);
           }
       });
   }
</script>
@endsection