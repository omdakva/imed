@extends('layouts.admin')
@section('title', 'Manage setting')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('settings.index') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('settings-languages.index') }}">Languages /</a>
<a id="pagetitlesecondelink">Details</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Language details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('settings-languages.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="Individual cadre_add">
<div class="row">
<h3 class="page-title2" >Language details</h3>



<div class="form-group col-md-12">
<label class="textblodfull">Name : <span class="textnormal">{{ $Language->name }}</span></label>
</div>
<div class="form-group col-md-12">
<label class="textblodfull">Symbole : <span class="textnormal">{{ $Language->symbole }}</span></label>
</div>
<div class="form-group col-md-12">
<label class="textblodfull">Activation : <span class="textnormal">@if($Language->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></label>
</div>

<div class="form-group col-md-12">
<label class="textblodfull">Creation date : <span class="textnormal">{{ $Language->created_at }}</span></label>
</div>

<div class="form-group col-md-12">
<label class="textblodfull">Update date : <span class="textnormal">{{ $Language->updated_at }}</span></label>
</div>





</div>


</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection