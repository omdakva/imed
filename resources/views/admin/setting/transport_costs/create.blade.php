@extends('layouts.admin')
@section('title', 'Manage setting')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('settings.index') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('transport_costs') }}">Transport Costs /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new transport costs</h3>
</div>
<div class="row">
<div class="col-lg-12">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif








<div class="row cadre_filter">

<div class="form-group col-sm-6">
<label for="name" class="col-sm-12 control-label textblod">Type</label>
<div class="col-sm-12">

<label for="r1">
<input type="radio" id="r1" name="type" value="" required="">
<span  class="col-sm-12 control-label textnormal">By kilometres</span>
</label>

<label for="r2">
<input type="radio" id="r2" name="type" value="" required="">
<span  class="col-sm-12 control-label textnormal">By quantity</span>
</label>

<label for="r3">
<input type="radio" id="r3" name="type" value="" required="">
<span  class="col-sm-12 control-label textnormal">By totals</span>
</label>

</div>
</div>

</div>










<div class="space"></div>
<div class="space"></div>




<div class="kilometres cadre_filter">
<form id="file-upload-form" class="uploader" action="{{ route('transport_costs.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="quantity" id="quantity" value="0">
<input type="hidden" name="type" id="type" value="1">
<input type="hidden" name="active" id="active" value="0">

<div class="row">
<div class="form-group col-sm-6">
<label for="name" class="col-sm-12 control-label textblod">kilometres</label>
<div class="col-sm-12">
<input type="text" class="form-control" value="1 kilometre" required="" disabled >
<input type="hidden" class="form-control" id="name" name="kilometre" value="1" required="" >
</div>
</div>

<div class="form-group col-sm-6 row">
<div class="col-sm-7">
<label for="name" class="control-label textblod">Price</label>
<input type="number" class="form-control" id="price" name="price" value="" required="">
</div>
<div class="col-sm-5">
@if($currency->count()=='0') 
<label class="control-label">
<a href="{{route('settings-currency.index')}}" class="error_text">
Please select currency <i class="fa fa-link" aria-hidden="true"></i>
</a>
</label>
@else
<label class="control-label textblod">Currency</label>
@endif
<select type="number" class="form-control" value="" required="" disabled>
@foreach($currency as $row)
<option value="{{ $row->symbole  }}">{{ $row->symbole  }}</option>
@endforeach
</select>
<input type="hidden" name="currency" value="@foreach($currency as $row)
{{ $row->symbole  }}
@endforeach">
</div>
</div>
</div>
<div class="modal-footer"style="text-align:right;">
<button type="submit" class="model_btn_save">Save</button>
</div>
</div>
</form>
</div>















<div class="quantity cadre_filter">
<form id="file-upload-form" class="uploader" action="{{ route('transport_costs.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="kilometre" id="kilometre" value="0">
<input type="hidden" name="type" id="type" value="2">
<input type="hidden" name="active" id="active" value="0">

<div class="row">
<div class="form-group col-sm-6">
<label for="name" class="col-sm-12 control-label textblod">Quantity</label>
<div class="col-sm-12">
<input type="number" name="quantity" class="form-control" required="" >
</div>
</div>

<div class="form-group col-sm-6 row">
<div class="col-sm-7">
<label for="name" class="control-label textblod">Price</label>
<input type="number" class="form-control" id="price" name="price" value="" required="">
</div>
<div class="col-sm-5">
@if($currency->count()=='0') 
<label class="control-label">
<a href="{{route('settings-currency.index')}}" class="error_text">
Please select currency <i class="fa fa-link" aria-hidden="true"></i>
</a>
</label>
@else
<label class="control-label textblod">Currency</label>
@endif
<select type="number" class="form-control" value="" required="" disabled>
@foreach($currency as $row)
<option value="{{ $row->symbole  }}">{{ $row->symbole  }}</option>
@endforeach
</select>
<input type="hidden" name="currency" value="@foreach($currency as $row)
{{ $row->symbole  }}
@endforeach">
</div>
</div>
</div>
<div class="modal-footer"style="text-align:right;">
<button type="submit" class="model_btn_save">Save</button>
</div>
</div>
</form>
</div>













<div class="totals cadre_filter">
<form id="file-upload-form" class="uploader" action="{{ route('transport_costs.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="kilometre" id="kilometre" value="0">
<input type="hidden" name="quantity" id="quantity" value="0">
<input type="hidden" name="type" id="type" value="3">
<input type="hidden" name="active" id="active" value="0">

<div class="row">

<div class="form-group col-sm-6 row">
<div class="col-sm-7">
<label for="name" class="control-label textblod">Price</label>
<input type="number" class="form-control" id="price" name="price" value="" required="">
</div>
<div class="col-sm-5">
@if($currency->count()=='0') 
<label class="control-label">
<a href="{{route('settings-currency.index')}}" class="error_text">
Please select currency <i class="fa fa-link" aria-hidden="true"></i>
</a>
</label>
@else
<label class="control-label textblod">Currency</label>
@endif
<select type="number" class="form-control" value="" required="" disabled>
@foreach($currency as $row)
<option value="{{ $row->symbole  }}">{{ $row->symbole  }}</option>
@endforeach
</select>
<input type="hidden" name="currency" value="@foreach($currency as $row)
{{ $row->symbole  }}
@endforeach">
</div>
</div>
</div>
<div class="modal-footer"style="text-align:right;">
<button type="submit" class="model_btn_save">Save</button>
</div>
</div>
</form>
</div>
























</div>
</div>
</div>



<script>
$(document).ready(function () {
    $(".kilometres").hide();
    $("#r1").click(function () {
        $(".kilometres").show();
    });
	
    $("#r2").click(function () {
        $(".kilometres").hide();
    });

    $("#r3").click(function () {
        $(".kilometres").hide();
    });


	$(".quantity").hide();
    $("#r1").click (function () {
        $(".quantity").hide();
    });
	
    $("#r2").click(function () {
        $(".quantity").show();
    });

    $("#r3").click(function () {
        $(".quantity").hide();
    });



    $(".totals").hide();
    $("#r1").click(function () {
        $(".totals").hide();
    });
	
    $("#r2").click(function () {
        $(".totals").hide();
    });

    $("#r3").click(function () {
        $(".totals").show();
    });
});
</script>


<script>
   function loadPreview(input){
       var data = $(input)[0].files; //this file data
       $.each(data, function(index, file){
           if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
               var fRead = new FileReader();
               fRead.onload = (function(file){
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image thumb element
                       $('#thumb-output').append(img);
                   };
               })(file);
               fRead.readAsDataURL(file);
           }
       });
   }
</script>
@endsection