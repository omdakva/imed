@extends('layouts.admin')
@section('title', 'Manage setting')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('settings.index') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('settings-languages.index') }}">Language /</a>
<a id="pagetitlesecondelink">Details</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Language details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('settings-languages.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="Individual cadre_add">
<form action = "{{ route('settings-languages.update', $Language->id) }}" method = "POST">
@csrf
@method('PATCH') 

<input type="hidden" name="image" value="0c3b3adb1a7530892e55ef36d3be6cb8.png" />
<input type="hidden" name="type" value="1" />
<input type="hidden"name="company_name" value="0"/>
<div class="row">
<h3 class="page-title2" >Language details</h3>



<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Name</label>
<input type="text" name="name" class="form-control" value="{{$Language->name}}" />
</div>

<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Symbole</label>
<input type="text" name="symbole" class="form-control" value="{{$Language->symbole}}" />
</div>





<div class="form-group col-md-6">
<label for="post-description" style="width:100%;" class="textblod">Activation</label>
<label for="r2" class="textnormal">
<input type="radio" name="active" id="r2"onClick="getResults()" value="1" 
@if($Language->active == 1)
checked  
@else
@endif > Active
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="active" id="r1" onClick="getResults()" value="0"
@if($Language->active == 0)
checked  
@else
@endif> Blocked
</label>		
</div>




</div>


<div class="modal-footer"style="text-align:right;">
<a href="{{ route('settings-languages.index') }}" class="model_btn_close">Cancel</a>
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection