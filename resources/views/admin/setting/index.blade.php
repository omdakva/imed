@extends('layouts.admin')
@section('title', 'Manage Setting')
@section('content')
<style>
.img-setting{width:60px;height:60px;}
</style>


<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Manage setting</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>


<div class="row">
<div class="space"></div>
<div class="space"></div>

<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4 card card-small">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/sac.png') }}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Methods of Delivery</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate Methods of Delivery
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{ route('method-delivery.index') }}" class="btn_col2">View more</a>
</div>
</div>
</div>



<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4 card card-small">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/check.png') }}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Currency</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate Currency
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{ route('settings-currency.index') }}" class="btn_col2">View more</a>
</div>
</div>
</div>


<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>


<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4 card card-small">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/global.png') }}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Languages</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate Languages
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{ route('settings-languages.index') }}" class="btn_col2">View more</a>
</div>
</div>
</div>



<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4 card card-small">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/sac.png') }}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Transport Costs</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate Transport Costs
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{ route('transport_costs') }}" class="btn_col2">View more</a>
</div>
</div>
</div>





<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>


<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4 card card-small">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/global.png') }}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Country</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate Languages
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{ route('country.index') }}" class="btn_col2">View more</a>
</div>
</div>
</div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>


<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4 card card-small">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">
<i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>
</button>
</div>
<div class="col-md-12">
<h1 class="pagetitle">Country</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate Languages
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{ route('country.index') }}" class="btn_col2">View more</a>
</div>
</div>
</div>

</div>

</div>








</div>




 
@endsection