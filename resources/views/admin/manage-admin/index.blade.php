@extends('layouts.admin')
@section('title', 'Manage admin')
@section('content')
<script src="{{ asset('public/ajax/libs/jquery.validate.js')}}"></script>




<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage admin</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Manage admin</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Admin ( {{count($Users)}} )</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('category.create') }}" class="btn-switch" title="Excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a> 
<a href="{{ route('category.create') }}" class="btn-switch" title="PDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> 
@if(Auth::user()->role=='3')
@else
<a href="{{route('manage-admin.create')}}" class="btn btn-success mb-2">Add new</a> 
@endif
</div>
</div>
<div class="space"></div>
<div class="space"></div>


<div class="card row">
<div class="card-body row">

<div class="form-group col-md-4">
<label><strong>Role</strong></label>
<select id='status' class="form-control">
<option value="">--Select role--</option>
<option value="1">Super admin</option>
<option value="2">Admin</option>
<option value="3">Read-Only Admin</option>
</select>
</div>


<div class="form-group col-md-4">
<label><strong>Status</strong></label>
<select id='status' class="form-control">
<option value="">--Select status--</option>
<option value="active">Active</option>
<option value="0">Deactive</option>
</select>
</div>



</div>
</div>
<div class="space"></div>
<div class="space"></div>



<div class="row">
<div class="col-12">
@if(session()->get('success'))
<div class="alert alert-success mt-3">
{{ session()->get('success') }}  
</div>
@endif

<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Name</th>
<th>Role</th>
<th>Email</th>
<th>Date</th>
<th style="text-align:center;">Etat</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Users as $post)
<div class="modal fade" id="exampleModal{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
		<a  href="javascript:void(0)" id="delete-post" data-id="{{ $post->id }}" class="model_btn_delete delete-post" data-dismiss="modal">Yes, Delete</a>
      </div>
    </div>
  </div>
</div>



<tr id="post_id_{{ $post->id }}">
<td>{{ $post->firstname  }} {{ $post->lastname  }}</td>
<td>
@if($post->role =='1')
<span class="">Admin</span>
@elseif($post->role =='2')
<span class="">Super admin</span>
@elseif($post->role =='3')
<span class="">Read-Only Admin</span>
@endif
</td>
<td>{{ $post->email  }}</td>
<td>{{ $post->created_at  }}</td>
<td style="text-align:center;">
@if($post->status =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif
</td>
<td class="td_btn">
<a href="{{ route('manage-admin.show', $post->id) }}" class="btn-delete"><i class="fa fa-eye"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>        

       </div> 
    </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script> 
@endsection