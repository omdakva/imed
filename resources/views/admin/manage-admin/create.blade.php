@extends('layouts.admin')
@section('title', 'Manage admin')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage admin</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('manage-admin.index') }}">Manage admin /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new admin</h3>
</div>
<div class="row">
<div class="col-lg-12">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif



	
<form id="file-upload-form" class="uploader" action="{{ route('manage-admin.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="status" value="1"/>
<input type="hidden" name="image" value="0c3b3adb1a7530892e55ef36d3be6cb8.png">
<input type="hidden" name="year" value="{{date('Y')}}">
<input type="hidden" name="month_number" value="{{date('mY')}}">
<input type="hidden" name="block_description" value="Description">

<div class="row">
<div class="col-md-7">


<div class="cadre_add row">
<div class="col-md-12 padding0">
<label class="page-title2 col-sm-12">Personal information</label>
</div>
<div class="col-md-4 padding0">
<label for="name" class="col-sm-12 control-label textblod">Gender</label>
<div class="col-sm-12">
<div class="btn-group btn-group-toggle mb-3" data-toggle="buttons">
<label class="btn btn-white active">
<input type="radio" name="gender" id="option1" autocomplete="off" value="1" checked> Male </label>
<label class="btn btn-white">
<input type="radio" name="gender" id="option2" autocomplete="off" value="2"> Female </label>
</div>
</div>
</div>

<div class="col-md-4 padding0">
<label for="name" class="col-sm-12 control-label textblod">First name</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="firstname" name="firstname" value="" required="">
</div>
</div>
	



<div class="col-md-4 padding0">
<label for="name" class="col-sm-12 control-label textblod">First name</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="lastname" name="lastname" value="" required="">
</div>
</div>	


<div class="col-md-6 padding0">
<label for="name" class="col-sm-12 control-label textblod">Birthday</label>
<div class="col-sm-12">
<input type="date" class="form-control" id="birthday" name="birthday" value="" required="">
</div>
</div>

<div class="col-md-6 padding0"></div>

<div class="space"></div>                      
<div class="space"></div>                      

<div class="col-md-6 padding0">
<label for="name" class="col-sm-12 control-label textblod">Country</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="country" name="country" value="" required="">
</div>
</div>

<div class="col-md-6 padding0">
<label for="name" class="col-sm-12 control-label textblod">City</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="city" name="city" value="" required="">
</div>
</div>

<div class="space"></div>                      
<div class="space"></div>                      


<div class="col-md-6 padding0">
<label for="name" class="col-sm-12 control-label textblod">Address</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="address" name="address" value="" required="">
</div>
</div>

<div class="col-md-6 padding0">
<label for="name" class="col-sm-12 control-label textblod">Postcode</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="postcode" name="postcode" value="" required="">
</div>
</div>
<div class="space"></div>                      
<div class="space"></div>                      

<div class="col-md-6 padding0">
<label for="name" class="col-sm-12 control-label textblod">Tel</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="tel" name="tel" value="" required="">
</div>
</div>

<div class="col-md-6 padding0">
<label for="name" class="col-sm-12 control-label textblod">Mobile</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="mobile" name="mobile" value="" required="">
</div>
</div>

</div>
</div>


<div class="col-md-5">
<div class="cadre_add">
<div class="col-md-12 padding0">
<label class="page-title2 col-sm-12">Contact information</label>
</div>


<div class="col-md-6 padding0">
<label for="name" class="col-sm-12 control-label textblod">Role</label>
<div class="col-sm-12">
<div class="btn-group btn-group-toggle mb-3" data-toggle="buttons">
<label class="btn btn-white">
<input type="radio" name="role" id="option2" autocomplete="off" value="2"> Super admin </label>
<label class="btn btn-white active">
<input type="radio" name="role" id="option1" autocomplete="off" value="1" checked> Admin </label>

<label class="btn btn-white">
<input type="radio" name="role" id="option2" autocomplete="off" value="3"> Read-Only Admin </label>
</div>
</div>
</div>


<div class="col-md-12 padding0">
<label for="name" class="col-sm-12 control-label textblod">E-mail</label>
<div class="col-sm-12">
<input type="email" name="email" id="email" class="form-control input-lg" required=""/>
<span id="error_email"></span>
</div>
</div>
<div class="space"></div>                      
<div class="space"></div>

<div class="col-md-12 padding0">
<label for="name" class="col-sm-12 control-label textblod">Password</label>
<div class="col-sm-12">
<input type="password" class="form-control" id="password" name="password" value="" required="">
</div>
</div>
</div>
</div>

<div class="space"></div>                      
<div class="modal-footer col-md-12 cadre_add"style="text-align:right;">
<button type="submit" name="register" id="registercompany" class="model_btn_save">Save</button>
</div>

</div>

</form>
</div>
</div>
</div>









<script>
$(document).ready(function(){

	$('#email').blur(function(){
		var error_email = '';
		var email = $('#email').val();
		var _token = $('input[name="_token"]').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($.trim(email).length > 0)
		{
			if(!filter.test(email))
			{				
				$('#error_email').html('<label class="text-danger">Invalid Email</label>');
				$('#email').addClass('has-error');
				$('#register').attr('disabled', 'disabled');
			}
			else
			{
				$.ajax({
					url:"{{ route('admin.check') }}",
					method:"POST",
					data:{email:email, _token:_token},
					success:function(result)
					{
						if(result == 'unique')
						{
							$('#error_email').html('<label class="text-success">Email Available</label>');
							$('#email').removeClass('has-error');
							$('#register').attr('disabled', false);
						}
						else
						{
							$('#error_email').html('<label class="text-danger">Email not Available</label>');
							$('#email').addClass('has-error');
							$('#register').attr('disabled', 'disabled');
						}
					}
				})
			}
		}
		else
		{
			$('#error_email').html('<label class="text-danger">Email is required</label>');
			$('#email').addClass('has-error');
			$('#register').attr('disabled', 'disabled');
		}
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('#emailcompany').blur(function(){
		var error_email = '';
		var email = $('#emailcompany').val();
		var _token = $('input[name="_token"]').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($.trim(emailcompany).length > 0)
		{
			if(!filter.test(email))
			{				
				$('#error_emailcompany').html('<label class="text-danger">Invalid Email</label>');
				$('#emailcompany').addClass('has-error');
				$('#registercompany').attr('disabled', 'disabled');
			}
			else
			{
				$.ajax({
					url:"{{ route('admin.check') }}",
					method:"POST",
					data:{email:email, _token:_token},
					success:function(result)
					{
						if(result == 'unique')
						{
							$('#error_emailcompany').html('<label class="text-success">Email Available</label>');
							$('#emailcompany').removeClass('has-error');
							$('#registercompany').attr('disabled', false);
						}
						else
						{
							$('#error_emailcompany').html('<label class="text-danger">Email not Available</label>');
							$('#emailcompany').addClass('has-error');
							$('#registercompany').attr('disabled', 'disabled');
						}
					}
				})
			}
		}
		else
		{
			$('#error_email').html('<label class="text-danger">Email is required</label>');
			$('#email').addClass('has-error');
			$('#register').attr('disabled', 'disabled');
		}
	});
	
});
</script>
@endsection