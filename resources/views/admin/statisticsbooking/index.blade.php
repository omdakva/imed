@extends('layouts.admin')
@section('title', 'Manage Statistics')
@section('content')
<script src="{{ asset('public/ajax/libs/jquery.validate.js')}}"></script>




<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage Statistics</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('manage-statistics.index') }}">Manage Statistics /</a>
<a id="pagetitlesecondelink">Booking</a>
</div>
</div>



<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="card-header border-bottom">
<h6 class="m-0">Booking {{date("Y")}}</h6>
<div class="form-group col-lg-4 col-md-12 col-sm-12 mb-4" style="padding:0px;">
<label for="post-description">Filter by year</label>
<select class="form-control" name="year" onchange="location = this.value;">
<option value=""></option>
@foreach($yearslist as $year)
<option value="{{ $year }}">{{ $year }}</option>
@endforeach
</select>
</div>
</div>
				  
<div class="col-sm-12 col-md-12" style="padding:0px;">
<div class="card">
<div class="card-body" >
<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
</div>
<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
<div style="position:absolute;width:200%;height:100%;left:0; top:0"></div>
</div>
</div> 
<canvas id="chart-line" class="chartjs-render-monitor" style="display: block; width: 299px; height: 200px;"></canvas>
</div>
</div>
</div>				  
</div>



	
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>

</div>



<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js'></script>
<script>
    $(document).ready(function() {
        var ctx = $("#chart-line");
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
                datasets: [{
                   
                    data: [282, 350, 411, 502, 635, 809, 947, 1402, 3700, 5267, 5267, 5267],
                    label: "Male",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [168, 170, 178, 190, 203, 276, 408, 547, 675, 734, 734, 734],
                    label: "Female",
                    borderColor: "#3cba9f",
                    fill: false
                }]
            },
            options: {title: {
                    display: true,
                    text: 'Clients year {{date("Y")}}'
                }
            }
        });
    });
</script>

@endsection