@extends('layouts.admin')
@section('title', 'Notification')
@section('content')
<script src="{{ asset('public/ajax/libs/jquery.validate.js')}}"></script>




<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Notification</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Notification</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>



@if($notificationslist->count()=='0')
<div class="space"></div>
<div class="col-md-12" style="text-align:center;"><label class="textnormal">There are no new notifications<label></div>
@else
@foreach($notificationslist as $post)
<div class="row cadre_add" style="padding:0px 0px 5px 0px;">
<div class="col-md-12">
<label class="mt-4 textblod" >{{$post->firstname}} {{$post->lastname}}</label>
<label class="textnormal">{{$post->notifiable}}</label>
<label class="textnormal" style="width:100%;">{{$post->created_at}}</label>
</div>
</div>
<div class="space"></div>
<div class="space"></div>
@endforeach
@endif


</div>

@endsection