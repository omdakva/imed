@extends('layouts.admin')
@section('title', 'Manage blog')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage blog</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Manage blog</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row">
<div class="space"></div>
<div class="space"></div>

<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/envol.png')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Blog lists</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate blog lists
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{route('news.index')}}" class="btn_col2">View more</a>
</div>
</div>
</div>



<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/home.webp')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Blog category</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate blog category
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{route('blog.category')}}" class="btn_col2">View more</a>
</div>
</div>
</div>

</div>
</div>
@endsection