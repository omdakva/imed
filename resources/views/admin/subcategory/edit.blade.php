@extends('layouts.admin')
@section('title', 'Manage category')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage category</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Subcategory</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Edit subcategory</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('subcategory.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif

<div class="Individual cadre_add">
<form action = "{{ route('subcategory.update', $Category->id) }}" method = "POST">
@csrf
@method('PATCH') 

@if(empty($Category->name_de))
<input type="hidden" name="name_de" class="form-control" value="0" />
@else
<input type="hidden" name="name_de" class="form-control" value="{{$Category->name_de}}" />
@endif
<div class="row">
<h3 class="page-title2" >Subcategory details</h3>

<div class="form-group col-md-6">
<label for="name" class="control-label textblod">Category</label>
<select type="text" class="form-control" id="parent_id" name="parent_id" value="" required="">
<option value=""></option>
@foreach($Categorylist as $post_category)
<option value="{{$post_category->id}}"
 @if($post_category->id == $Category->parent_id)
SELECTED
@else
@endif>{{ $post_category->name  }}</option>
@endforeach

</select>
</div>

<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Name</label>
<input type="text" name="name" class="form-control" value="{{$Category->name}}" />
</div>


@if(empty($Category->name_de))
@else
<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Name ( DE )</label>
<input type="text" name="name_de" class="form-control" value="{{$Category->name_de}}" />
</div>
@endif


<div class="form-group col-md-6">
<label for="post-description" style="width:100%;" class="textblod">Activation</label>
<label for="r2" class="textnormal">
<input type="radio" name="active" id="r2"onClick="getResults()" value="1" 
@if($Category->active == 1)
checked  
@else
@endif > Active
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="active" id="r1" onClick="getResults()" value="0"
@if($Category->active == 0)
checked  
@else
@endif> Blocked
</label>		
</div>




</div>


<div class="modal-footer"style="text-align:right;">
<a href="{{ route('subcategory.index') }}" class="model_btn_close">Cancel</a>
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection