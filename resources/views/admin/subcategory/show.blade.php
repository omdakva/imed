@extends('layouts.admin')
@section('title', 'Manage category')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage category</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Subcategory</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Subcategory</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('subcategory.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="Individual cadre_add">
<div class="row">
<h3 class="page-title2" >Subcategory details</h3>



<div class="form-group col-md-12">
<label class="textblodfull">Name : <span class="textnormal">{{ $Category->name }}</span></label>
<label class="textblodfull">Translate : 
@if(empty($Category->name_de))
<span class="NA"> N / A</span> <a href="{{ route('subcategory.translate', $Category->id) }}" class="error_text">
Translate this subcategory <i class="fa fa-link" aria-hidden="true"></i>
</a>
@else
<span class="textnormal">{{ $Category->name_de }}</span>
@endif

</label>
<label class="textblodfull">Activation : <span class="textnormal">@if($Category->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></label>
<label class="textblodfull">Creation date : <span class="textnormal">{{ $Category->created_at }}</span></label>
<label class="textblodfull">Update date : <span class="textnormal">{{ $Category->updated_at }}</span></label>
</div>





</div>


</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection