@extends('layouts.admin')
@section('title', 'Manage category')
@section('content')
<script src="{{ asset('public/ajax/libs/jquery.validate.js')}}"></script>




<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage category</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Manage category</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>


<div class="row">
<div class="space"></div>
<div class="space"></div>
<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/1.png')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Category</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate Category
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{ route('category.index') }}" class="btn_col2">View more</a>
</div>
</div>
</div>


<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/file.png')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Subcategory</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate Subcategory
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{ route('subcategory.index') }}" class="btn_col2">View more</a>
</div>
</div>
</div>
</div>
</div>
@endsection