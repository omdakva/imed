@extends('layouts.admin')
@section('css')
<link href="{{URL::asset('assets/css/icons.css')}}" rel="stylesheet">


<link href="{{URL::asset('assets/css-rtl/style.css')}}" rel="stylesheet">
<!--- Dark-mode css -->
<link href="{{URL::asset('assets/css-rtl/style-dark.css')}}" rel="stylesheet">
<!---Skinmodes css-->
<link href="{{URL::asset('assets/css-rtl/skin-modes.css')}}" rel="stylesheet">

@endsection
@section('title', 'Subscribe newsletter')
@section('content')
<div class="container" ><br>
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Mail Box Trash</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Mail Box</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>
	<div class="row row-sm main-content-mail" >
					<div class="col-lg-4 col-xl-3 col-md-12">
						<div class="card mg-b-20 mg-md-b-0">
							<div class="card-body">
								<div class="main-content-left main-content-left-mail">
									<a class="btn btn-main-primary btn-compose" href="" id="btnCompose">Compose</a>
									<div class="main-mail-menu">
										<nav class="nav main-nav-column mg-b-20">
											<a class="nav-link" href="/admin/inbox"><i class="bx bxs-inbox"></i> Inbox <span>20</span></a> <a class="nav-link" href=""><i class="bx bx-star"></i> Starred <span>3</span></a> <a class="nav-link" href=""><i class="bx bx-bookmarks"></i> Important <span>10</span></a> <a class="nav-link " href="{{ route('Admin.send.mail')}}"><i class="bx bx-send"></i> Sent Mail  <span> ({{$nb_outbox}}) </span></a> <a class="nav-link" href=""><i class="bx bx-edit"></i> Drafts <span>15</span></a> <a class="nav-link" href=""><i class="bx bx-message-error"></i> Spam <span>128</span></a> <a class="nav-link active" href="/admin/trash"><i class="bx bx-trash"></i> Trash <span> ({{$nb_trash}})</span></a>
										</nav><label class="main-content-label main-content-label-sm">Label</label>
										<nav class="nav main-nav-column mg-b-20">
											<a class="nav-link" href="#"><i class="bx bx-folder-open"></i> Social <span>10</span></a> <a class="nav-link" href="#"><i class="bx bx-folder"></i> Promotions <span>22</span></a> <a class="nav-link" href="#"><i class="bx bx-folder-plus"></i> Updates <span>17</span></a>
										</nav><label class="main-content-label main-content-label-sm">Tags</label>
										<nav class="nav main-nav-column">
											<a class="nav-link" href="#"><i class="bx bxl-twitter"></i> Twitter <span>2</span></a> <a class="nav-link" href="#"><i class="bx bxl-github"></i> Github <span>32</span></a> <a class="nav-link" href="#"><i class="bx bxl-google-plus"></i> Google <span>7</span></a>
										</nav>
									</div><!-- main-mail-menu -->
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-8 col-xl-9 col-md-12">
						<div class="card">
							<div class="main-content-body main-content-body-mail card-body">
								<div class="main-mail-header">
									<div>
										<h4 class="main-content-title mg-b-5">Trash</h4>
										<p>You have {{$nb_trash}} messages</p>
									</div>
									<div>                                        

										<span style="margin:5px;">1-5 of {{$nb_trash}}</span>
										<div class="btn-group" role="group">
										{{$mails}} 
										</div>
									</div>
								</div><!-- main-mail-list-header -->
								<div class="main-mail-options">
									<label class="ckbox"><input id="checkAll" type="checkbox"> <span></span></label>
									<div class="btn-group">
										<button class="btn btn-light"><i class="bx bx-refresh"></i></button> <button class="btn btn-light disabled"><i class="bx bx-archive"></i></button> <button class="btn btn-light disabled"><i class="bx bx-info-circle"></i></button> <button class="btn btn-light disabled"><i class="bx bx-trash"></i></button> <button class="btn btn-light disabled"><i class="bx bx-folder-open"></i></button> <button class="btn btn-light disabled"><i class="bx bx-purchase-tag-alt"></i></button>
									</div><!-- btn-group -->
								</div><!-- main-mail-options -->
								<div class="main-mail-list">


                                                            @foreach ($mails as $mail)

                                <div class="main-mail-item unread">
										<div class="main-mail-checkbox">
											<label class="ckbox"><input type="checkbox"> <span></span></label>
										</div>
										<div class="main-mail-star active">
											<i class="typcn typcn-star"></i>
										</div>
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/2.jpg')}}"></div>
										<div class="main-mail-body">
											<div class="main-mail-from">
												{{$mail->tomail}}
											</div>
											<div class="main-mail-subject">
												<strong>{{$mail->sub}}</strong> <span><br>{{$mail->body}}</span><br><a href="sendbox/{{$mail->id}}">  more...</a>

											</div>
										</div>
										<div class="main-mail-date">
{{$mail->created_at}}										</div>
									</div>

                                            @endforeach							       




                      
                                    		       
									
									
								
								
									
									
									
									</div>
									
									
								</div>
								<div class="mg-lg-b-30"></div>
							</div>
						</div>
					</div>
					<div class="main-mail-compose"  >
						<div>
							<div class="container">
								<div class="main-mail-compose-box">
									<div class="main-mail-compose-header">
										<span>New Message</span>
										<nav class="nav">
											<a class="nav-link" href=""><i class="fas fa-minus"></i></a> <a class="nav-link" href=""><i class="fas fa-compress"></i></a> <a class="nav-link" href=""><i class="fas fa-times"></i></a>
										</nav>
									</div>
									<div class="main-mail-compose-body">
									   <form action="{{ route('Admin.mail')}}" method="post">
                                                                @csrf
										<div class="form-group">
											<label class="form-label">To</label>
											<div>
												<input class="form-control" placeholder="Enter recipient's email address" type="text" name="email">
											</div>
										</div>
										<div class="form-group">
											<label class="form-label">Subject</label>
											<div>
												<input class="form-control" type="text" name="subject">
											</div>
										</div>
										<div class="form-group">
											<textarea class="form-control" placeholder="Write your message here..." rows="8" name="message"></textarea>
										</div>
										<div class="form-group mg-b-0">
											<nav class="nav">
												<a class="nav-link" data-toggle="tooltip" href="" title="Add attachment"><i class="fas fa-paperclip"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Add photo"><i class="far fa-image"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Add link"><i class="fas fa-link"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Emoticons"><i class="far fa-smile"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Discard"><i class="far fa-trash-alt"></i></a>
											</nav><button type="submit" class="btn btn-primary">Send</button>
										</div>

										



                                                            </form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div><!-- container closed -->
		</div>





















</div>


@endsection
