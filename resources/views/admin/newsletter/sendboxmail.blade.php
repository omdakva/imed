@extends('layouts.admin')
@section('css')
<link href="{{URL::asset('assets/css/icons.css')}}" rel="stylesheet">


<link href="{{URL::asset('assets/css-rtl/style.css')}}" rel="stylesheet">
<!--- Dark-mode css -->
<link href="{{URL::asset('assets/css-rtl/style-dark.css')}}" rel="stylesheet">
<!---Skinmodes css-->
<link href="{{URL::asset('assets/css-rtl/skin-modes.css')}}" rel="stylesheet">

@endsection
@section('title', 'Subscribe newsletter')
@section('content')
<div class="container" ><br>


	<div class="row row-sm main-content-mail" >
					<div class="col-lg-4 col-xl-3 col-md-12">
						<div class="card mg-b-20 mg-md-b-0">
							<div class="card-body">
								<div class="main-content-left main-content-left-mail">
									<a class="btn btn-main-primary btn-compose" href="" id="btnCompose">Compose</a>
									<div class="main-mail-menu">
										<nav class="nav main-nav-column mg-b-20">
											<a class="nav-link" href="/admin/inbox"><i class="bx bxs-inbox"></i> Inbox <span>20</span></a> <a class="nav-link" href=""><i class="bx bx-star"></i> Starred <span>3</span></a> <a class="nav-link" href=""><i class="bx bx-bookmarks"></i> Important <span>10</span></a> <a class="nav-link " href="{{ route('Admin.send.mail')}}"><i class="bx bx-send"></i> Sent Mail  <span> ({{$count}}) </span></a> <a class="nav-link" href=""><i class="bx bx-edit"></i> Drafts <span>15</span></a> <a class="nav-link" href=""><i class="bx bx-message-error"></i> Spam <span>128</span></a> <a class="nav-link" href="/admin/trash"><i class="bx bx-trash"></i> Trash <span>6</span></a>
										</nav><label class="main-content-label main-content-label-sm">Label</label>
										<nav class="nav main-nav-column mg-b-20">
											<a class="nav-link" href="#"><i class="bx bx-folder-open"></i> Social <span>10</span></a> <a class="nav-link" href="#"><i class="bx bx-folder"></i> Promotions <span>22</span></a> <a class="nav-link" href="#"><i class="bx bx-folder-plus"></i> Updates <span>17</span></a>
										</nav><label class="main-content-label main-content-label-sm">Tags</label>
										<nav class="nav main-nav-column">
											<a class="nav-link" href="#"><i class="bx bxl-twitter"></i> Twitter <span>2</span></a> <a class="nav-link" href="#"><i class="bx bxl-github"></i> Github <span>32</span></a> <a class="nav-link" href="#"><i class="bx bxl-google-plus"></i> Google <span>7</span></a>
										</nav>
									</div><!-- main-mail-menu -->
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-9 col-lg-8 col-md-12 col-sm-12">

                            

						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Nom Prenom  <span class="badge badge-primary">mailbox</span></h4>
							</div>
							<div class="card-body">
								<div class="email-media">
									<div class="mt-0 d-sm-flex">
										<img class="ml-2 rounded-circle avatar-xl" src="{{URL::asset('assets/img/faces/6.jpg')}}" alt="avatar">
										<div class="media-body">
											<div class="float-left d-none d-md-flex fs-15">
												<span class="mr-3">{{$mails->created_at}}</span>
												<small class="mr-3"><i class="bx bx-star tx-18" data-toggle="tooltip" title="" data-original-title="Rated"></i></small>
												<small class="mr-3"><i class="bx bx-reply tx-18" data-toggle="tooltip" title="" data-original-title="Reply"></i></small>
												<div class="mr-3">
													<a href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fe fe-more-horizontal  tx-18" data-toggle="tooltip" title="" data-original-title="View more"></i></a>
													<div class="dropdown-menu">
														<a class="dropdown-item" href="#">Reply</a>
														<a class="dropdown-item" href="#">Report Spam</a>
														<a class="dropdown-item" href="#">Delete</a>
														<a class="dropdown-item" href="#">Show Original</a>
														<a class="dropdown-item" href="#">Print</a>
														<a class="dropdown-item" href="#">Filter</a>
													</div>
												</div>
											</div>
											<div class="media-title  font-weight-bold mt-3"> Dulucs shop <span class="text-muted">( {{$mails->frommail}} )</span></div>
											<p class="mb-0">to Nom Prenom ( {{$mails->tomail}}) </p>
											<small class="mr-2 d-md-none">Dec 13 , 2018 12:45 pm</small>
											<small class="mr-2 d-md-none"><i class="fe fe-star text-muted" data-toggle="tooltip" title="" data-original-title="Rated"></i></small>
											<small class="mr-2 d-md-none"><i class="fa fa-reply text-muted" data-toggle="tooltip" title="" data-original-title="Reply"></i></small>
										</div>
									</div>
								</div>
								<div class="eamil-body mt-5">
									<h6>Hi Sir/Madam</h6>
									<p> {{$mails->body}} </p>
									<p class="mb-0">Thanking you Sir/Madam</p>
									<hr>
									
								</div>
							</div>
							<div class="card-footer text-left">
								<a class="btn btn-primary mt-1 mb-1" href="#"><i class="fa fa-reply"></i> Reply</a>
								<a class="btn btn-info mt-1 mb-1" href="#"><i class="fa fa-share"></i> Forward</a>
							</div>
						</div>
                                                                   				       

					</div>
				</div>
					<div class="main-mail-compose"  >
						<div>
							<div class="container">
								<div class="main-mail-compose-box">
									<div class="main-mail-compose-header">
										<span>New Message</span>
										<nav class="nav">
											<a class="nav-link" href=""><i class="fas fa-minus"></i></a> <a class="nav-link" href=""><i class="fas fa-compress"></i></a> <a class="nav-link" href=""><i class="fas fa-times"></i></a>
										</nav>
									</div>
									<div class="main-mail-compose-body">
									   <form action="{{ route('Admin.mail')}}" method="post">
                                                                @csrf
										<div class="form-group">
											<label class="form-label">To</label>
											<div>
												<input class="form-control" placeholder="Enter recipient's email address" type="text" name="email">
											</div>
										</div>
										<div class="form-group">
											<label class="form-label">Subject</label>
											<div>
												<input class="form-control" type="text" name="subject">
											</div>
										</div>
										<div class="form-group">
											<textarea class="form-control" placeholder="Write your message here..." rows="8" name="message"></textarea>
										</div>
										<div class="form-group mg-b-0">
											<nav class="nav">
												<a class="nav-link" data-toggle="tooltip" href="" title="Add attachment"><i class="fas fa-paperclip"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Add photo"><i class="far fa-image"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Add link"><i class="fas fa-link"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Emoticons"><i class="far fa-smile"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Discard"><i class="far fa-trash-alt"></i></a>
											</nav><button type="submit" class="btn btn-primary">Send</button>
										</div>

										



                                                            </form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div><!-- container closed -->
		</div>





















</div>


@endsection
