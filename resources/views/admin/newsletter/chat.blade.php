@extends('layouts.admin')
@section('css')
<link href="{{URL::asset('assets/css/icons.css')}}" rel="stylesheet">


<link href="{{URL::asset('assets/css-rtl/style.css')}}" rel="stylesheet">
<!--- Dark-mode css -->
<link href="{{URL::asset('assets/css-rtl/style-dark.css')}}" rel="stylesheet">
<!---Skinmodes css-->
<link href="{{URL::asset('assets/css-rtl/skin-modes.css')}}" rel="stylesheet">

@endsection
@section('title', 'Subscribe newsletter')
@section('content')
<div class="container" ><br>
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Chat Room</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Chat Room</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>
<div class="space"></div>

<div class="space"></div>
<div class="space"></div>

<div class="row row-sm main-content-app mb-4">
					<div class="col-xl-4 col-lg-5">
						<div class="card">
							<div class="main-content-left main-content-left-chat">
								<nav class="nav main-nav-line main-nav-line-chat">
									<a class="nav-link active" data-toggle="tab" href="">Recent Chat</a> <a class="nav-link" data-toggle="tab" href="">Groups</a> <a class="nav-link" data-toggle="tab" href="">Calls</a>
								</nav>
								<div class="main-chat-contacts-wrapper">
									<label class="main-content-label main-content-label-sm">Active Contacts (5)</label>
									<div class="main-chat-contacts" id="chatActiveContacts">
										<div>
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/3.jpg')}}"></div><small>Adrian</small>
										</div>
										<div>
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/12.jpg')}}"></div><small>John</small>
										</div>
										<div>
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/4.jpg')}}"></div><small>Daniel</small>
										</div>
										<div>
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/13.jpg')}}"></div><small>Katherine</small>
										</div>
										<div>
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/5.jpg')}}"></div><small>Raymart</small>
										</div>
										<div>
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/14.jpg')}}"></div><small>Junrisk</small>
										</div>
										<div>
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/6.jpg')}}"></div><small>George</small>
										</div>
										<div>
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/15.jpg')}}"></div><small>Maryjane</small>
										</div>
										<div>
											<div class="main-chat-contacts-more">
												20+
											</div><small>More</small>
										</div>
									</div><!-- main-active-contacts -->
								</div><!-- main-chat-active-contacts -->
								<div class="main-chat-list" id="ChatList">
									<div class="media new">
										<div class="main-img-user online">
											<img alt="" src="{{URL::asset('assets/img/faces/5.jpg')}}"> <span>2</span>
										</div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Socrates Itumay</span> <span>2 hours</span>
											</div>
											<p>Découvrez comment retrouver les fichiers que vous avez échangé depuis une conversation sur le chat</p>
										</div>
									</div>
									<div class="media new">
										<div class="main-img-user">
											<img alt="" src="{{URL::asset('assets/img/faces/6.jpg')}}"> <span>1</span>
										</div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Dexter dela Cruz</span> <span>3 hours</span>
											</div>
											<p>les fichiers que vous avez échangé depuis une conversation sur le chat</p>
										</div>
									</div>
									<div class="media selected">
										<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/9.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Reynante Labares</span> <span>10 hours</span>
											</div>
											<p>Découvrez comment retrouver les fichiers que vous avez échangé depuis une conversation sur le chat</p>
										</div>
									</div>
									<div class="media">
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/11.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Joyce Chua</span> <span>2 days</span>
											</div>
											<p>les fichiers que vous avez échangé depuis une conversation sur le chat</p>
										</div>
									</div>
									<div class="media">
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/4.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Rolando Paloso</span> <span>2 days</span>
											</div>
											<p>La conversation n est pas supprimée de l historique des autres participants</p>
										</div>
									</div>
									<div class="media new">
										<div class="main-img-user">
											<img alt="" src="{{URL::asset('assets/img/faces/7.jpg')}}"> <span>1</span>
										</div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Ariana Monino</span> <span>3 days</span>
											</div>
											<p>La conversation n est pas supprimée de l historique des autres participants</p>
										</div>
									</div>
									<div class="media">
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/8.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Maricel Villalon</span> <span>4 days</span>
											</div>
											<p>La conversation n est pas supprimée de l historique des autres participants</p>
										</div>
									</div>
									<div class="media">
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/12.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Maryjane Cruiser</span> <span>5 days</span>
											</div>
											<p>La conversation n est pas supprimée de l historique des autres participants</p>
										</div>
									</div>
									<div class="media">
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/15.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Lovely Dela Cruz</span> <span>5 days</span>
											</div>
											<p>La conversation n est pas supprimée de l historique des autres participants</p>
										</div>
									</div>
									<div class="media">
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/10.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Daniel Padilla</span> <span>5 days</span>
											</div>
											<p>La conversation n est pas supprimée de l historique des autres participants</p>
										</div>
									</div>
									<div class="media">
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/3.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>John Pratts</span> <span>6 days</span>
											</div>
											<p>La conversation n est pas supprimée de l historique des autres participants</p>
										</div>
									</div>
									<div class="media">
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/7.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Raymart Santiago</span> <span>6 days</span>
											</div>
											<p>La conversation n est pas supprimée de l historique des autres participants</p>
										</div>
									</div>
									<div class="media border-bottom-0">
										<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/6.jpg')}}"></div>
										<div class="media-body">
											<div class="media-contact-name">
												<span>Samuel Lerin</span> <span>7 days</span>
											</div>
											<p>La conversation n est pas supprimée de l historique des autres participants</p>
										</div>
									</div>
								</div><!-- main-chat-list -->
							</div>
						</div>
					</div>
					<div class="col-xl-8 col-lg-7">
						<div class="card">
							<a class="main-header-arrow" href="" id="ChatBodyHide"><i class="icon ion-md-arrow-back"></i></a>
							<div class="main-content-body main-content-body-chat">
								<div class="main-chat-header">
									<div class="main-img-user"><img alt="" src="{{URL::asset('assets/img/faces/9.jpg')}}"></div>
									<div class="main-chat-msg-name">
										<h6>Imed Hamed</h6><small>Last seen: 2 minutes ago</small>
									</div>
									<nav class="nav">
										<a class="nav-link" href=""><i class="icon ion-md-more"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Call"><i class="icon ion-ios-call"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Archive"><i class="icon ion-ios-filing"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Trash"><i class="icon ion-md-trash"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="View Info"><i class="icon ion-md-information-circle"></i></a>
									</nav>
								</div><!-- main-chat-header -->
								<div class="main-chat-body" id="ChatBody">
									<div class="content-inner">
										<label class="main-chat-time"><span>3 days ago</span></label>
										<div class="media flex-row-reverse">
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/9.jpg')}}"></div>
											<div class="media-body">
												<div class="main-msg-wrapper right">
													Si vous souhaitez intégrer un autre membre de léquipe...
												</div>
												<div class="main-msg-wrapper right">
													 le nom du membre de votre équipe...
												</div>
												<div class="main-msg-wrapper pd-0"><img alt="" class="wd-100 ht-100" src="http://127.0.0.1:8000/photo/products_logo/Rectangle 37.png"></div>
												<div>
													<span>9:48 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a>
												</div>
											</div>
										</div>
										<div class="media">
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/6.jpg')}}"></div>
											<div class="media-body">
												<div class="main-msg-wrapper left">
													Lorsque vous êtes prêt à envoyer chaque réponse, cliquez sur Envoyer le Chat.
												</div>
												<div>
													<span>9:32 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a>
												</div>
											</div>
										</div>
										<div class="media flex-row-reverse">
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/9.jpg')}}"></div>
											<div class="media-body">
												<div class="main-msg-wrapper right">
													 partagez une vidéo via le chat.
												</div>
												<div>
													<span>11:22 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a>
												</div>
											</div>
										</div><label class="main-chat-time"><span>Yesterday</span></label>
										<div class="media">
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/6.jpg')}}"></div>
											<div class="media-body">
												<div class="main-msg-wrapper left">
													pour ajouter une pièce jointe à partir de votre appareil ou de votre gestionnaire de fichiers avec votre contact.
												</div>
												<div>
													<span>9:32 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a>
												</div>
											</div>
										</div>
										<div class="media flex-row-reverse">
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/9.jpg')}}"></div>
											<div class="media-body">
												<div class="main-msg-wrapper right">
                                                 pour ajouter une pièce jointe à partir de votre appareil ou de votre gestionnaire de fichiers avec votre contact												</div>
												<div class="main-msg-wrapper right">
													joindre un contenu à votre chat
												</div>
												<div>
													<span>9:48 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a>
												</div>
											</div>
										</div><label class="main-chat-time"><span>Today</span></label>
										<div class="media">
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/6.jpg')}}"></div>
											<div class="media-body">
												<div class="main-msg-wrapper left">
													joindre un contenu à votre chat
												</div>
												<div class="main-msg-wrapper left">
													pour ajouter une pièce jointe à partir de votre appareil ou de votre gestionnaire de fichiers avec votre contact.
												</div>
												<div>
													<span>10:12 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a>
												</div>
											</div>
										</div>
										<div class="media flex-row-reverse">
											<div class="main-img-user online"><img alt="" src="{{URL::asset('assets/img/faces/9.jpg')}}"></div>
											<div class="media-body">
												<div class="main-msg-wrapper right">
													 Gestionnaire de fichiers avec votre contact.
												</div>
												<div class="main-msg-wrapper right">
													pour ajouter une pièce jointe à partir de votre appareil ou de votre gestionnaire de fichiers avec votre contact.
												</div>
												<div>
													<span>09:40 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="main-chat-footer">
								<nav class="nav">
									<a class="nav-link" data-toggle="tooltip" href="" title="Add Photo"><i class="fas fa-camera"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Attach a File"><i class="fas fa-paperclip"></i></a> <a class="nav-link" data-toggle="tooltip" href="" title="Add Emoticons"><i class="far fa-smile"></i></a> <a class="nav-link" href=""><i class="fas fa-ellipsis-v"></i></a>
								</nav><input class="form-control" placeholder="Type your message here..." type="text"> <a class="main-msg-send" href=""><i class="far fa-paper-plane"></i></a>
							</div>
						</div>
					</div>
				</div>
			



</div>


@endsection

@section('js')

<!--Internal  Perfect-scrollbar js -->
<script src="{{URL::asset('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/perfect-scrollbar/p-scroll.js')}}"></script>
<script src="{{URL::asset('assets/plugins/lightslider/js/lightslider.min.js')}}"></script>
<script src="{{URL::asset('assets/js/chat.js')}}"></script>


@endsection