@extends('layouts.admin')
@section('title', 'Subscribe newsletter')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Subscribe newsletter</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Subscribe newsletter</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Subscribe newsletter ( {{count($Subscribe_newsletter)}}  )</span>
</div>
</div>
<div class="space"></div>
<div class="space"></div>





    <div class="row">
        <div class="col-12">
   @if(session()->get('success'))
    <div class="alert alert-success mt-3">
      {{ session()->get('success') }}  
    </div>
@endif 



<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Email</th>
<th>Date</th>
<th style="text-align:center;">Etat</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Subscribe_newsletter as $post)
<div class="modal fade" id="delete{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
		<a  href="javascript:void(0)" id="delete-post" data-id="{{ $post->id }}" class="model_btn_delete delete-post" data-dismiss="modal">Yes, Delete</a>
      </div>
    </div>
  </div>
</div>


<tr id="post_id_{{ $post->id }}">
<td>{{ $post->email  }}</td>
<td>{{ $post->created_at  }}</td>
<td style="text-align:center;">
@if($post->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif
</td>


<td class="td_btn" style="text-align:center;">
<a href="{{ route('newsletter.show', $post->id) }}" class="btn-delete"><i class="fa fa-eye"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>        

       </div> 
    </div>
</div>

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

 
@endsection