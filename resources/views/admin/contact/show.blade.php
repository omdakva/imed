@extends('layouts.admin')
@section('title', 'Manage contact')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage contact</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">contact</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Contact details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('contact.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif




<div class="cadre_add">
<div class="row">
<h3 class="page-title2" >Info</h3>
<div class="form-group col-md-12">
<label class="textblodfull">Name : <span class="textnormal">{{ $Contact->firstname }} {{ $Contact->lastname }}</span></label>
<label class="textblodfull">Email : <span class="textnormal">{{ $Contact->email }}</span></label>
<label class="textblodfull">Creation date : <span class="textnormal">{{ $Contact->created_at }}</span></label>
<label class="textblodfull">Update date : <span class="textnormal">{{ $Contact->updated_at }}</span></label>
</div>
</div>
</div>

    <div class="space"></div>
  <div class="space"></div>
 
  <div class="cadre_add">
<div class="row">
<div class="form-group col-md-12">
<label class="textblodfull">Message</label>
<label class="textblodfull"><span class="textnormal">{{ $Contact->message }}</span></label>
</div>
</div>
</div>





		

</div>
</div>
</div>
</br>


@endsection