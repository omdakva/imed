@extends('layouts.admin')
@section('title', 'Manage Statistics')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage Statistics</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Manage Statistics</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Clients</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('statistics-clients.index') }}" class="btn btn-success mb-2">View</a> 
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Booking</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('statistics-booking.index') }}" class="btn btn-success mb-2">View</a> 
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Sale</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('statistics-sale.index') }}" class="btn btn-success mb-2">View</a> 
</div>
</div>

</div>

@endsection