@extends('layouts.admin')
@section('title', 'Manage blog')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage blog</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('news.index') }}">Manage blog /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new blog</h3>
</div>
<div class="row">
<div class="col-lg-12">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
	
<form id="file-upload-form" class="uploader" action="{{ route('news.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="name_de" id="name_de" value="0">
<input type="hidden" name="description_de" id="description_de" value="0">
<input type="hidden" name="active" id="active" value="0">
<input type="hidden" name="photo" id="photo" value="background.jpg">

<div class="row cadre_filter">




<div class="col-lg-9">		
<div class="form-group">
<label for="name" class="col-sm-12 control-label textblod">Title</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="name" name="name" value="" required="">
</div>
</div>



<div class="form-group">
<label for="name" class="col-sm-12 control-label textblod">Description</label>
<div class="col-sm-12">
<textarea id="description" class="form-control"  name="description" required=""></textarea> 
</div>
</div>

</div>


<div class="col-lg-3">		
<p class="control-label textblod">Category</p>
@foreach($blog_category as $post)
<label class="textnormalfull">
<input type="checkbox" name="id_category[]" id="id_category" value="{{$post->id}}"> {{$post->name}}
</label>
@endforeach
</div>
</div>



<div class="space"></div>
<div class="space"></div>

<div class="row cadre_filter">
<div class="col-lg-12 text-right">		
<button type="submit" class="model_btn_save">Save</button>
</div>          
</div>          

</form>
</div>
</div>
</div>



<link href="{{ asset('public/text-editor/editor.css')}}" type="text/css" rel="stylesheet"/>
<script src="{{ asset('public/text-editor/editor.js')}}"></script>
<script>
$(document).ready(function() {
$("#txtEditor").Editor();
});
</script>




<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>  

<script>  
    CKEDITOR.replace('description');  
    CKEDITOR.replace('editor2');  
  
    function getData() {  
        //Get data written in first Editor   
        var editor_data = CKEDITOR.instances['description'].getData();  
        //Set data in Second Editor which is written in first Editor  
        CKEDITOR.instances['editor2'].setData(editor_data);  
    }  
</script> 

@endsection

