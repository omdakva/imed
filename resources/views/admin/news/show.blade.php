@extends('layouts.admin')
@section('title', 'Manage news')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage news</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('news.index') }}">Manage news /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Blog details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<td style="text-align:center;">
@if(Auth::user()->role=='3')
@else
@if($News->name_de =='0')
<a href="{{ route('news.translate', $News->id) }}" class="btn btn-success mb-2">Add translate</a>
@else
<a href="{{ route('news.show_translate', $News->id) }}" class="btn btn-success mb-2"><span>View translate</span></a>
@endif
@endif
<a href="{{ route('news.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif







<div class="row">
<div class="col-md-12">
<label class="mt-4 pagetitle textblod" >{{ $News->name }}</label>

<div class="cadre_add" style="height:420px;background: url({{ asset('photo/blog_image/'.$News->photo) }});background-repeat: no-repeat;background-size: 100% 100%;">
</div>
<div class="col-md-12 btn_slider_edit">
@if(Auth::user()->role=='3')
@else
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_photo" class="btn btn-success mb-2" id="create-new-post">Edit slider</a> 
@endif
<label class="textblodfull"><span class="textnormal">{{ date('d/m/Y', strtotime($News->created_at)) }}</span></label>
</div>





















<div class="modal fade" id="add_category" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Add category</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{route('news.store_category',$News->id)}}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<div class="modal-body">
<input type="hidden" name="products_id" id="products_id" value="{{ $News->id }}">
<input type="hidden" name="post_id" id="post_id">
<div class="col-lg-12 cadre_filter">
@foreach($Category as $category_list)

<label class="textblod" style="width:100%;margin-left:0px;">
<input type="checkbox" name="category_id[]" id="category" value="{{$category_list->id}}" @foreach($Category_blog as $category_view)
 @if($category_view->id==$category_list->id)
CHECKED disabled
@else
no
@endif
@endforeach > {{$category_list->name}}
</label>
@endforeach


</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>



<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >Category ( {{count($Category_blog)}} ) 
@if(Auth::user()->role=='3')
@else
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#add_category" class="icons-edit"> 
<i class="fa fa-plus"></i>
</a> 
@endif
</label>
<div class="space"></div>
<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Name</th>
<th>Type</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Category_blog as $category)
<div class="modal fade" id="delete{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
        <form action="{{ route('news.destroy_category', $category->id) }}" METHOD="post">
		@csrf
		<input type="hidden" value="{{ $category->id }}" name="id_category" />
		<button  type="submit" class="model_btn_delete delete-post">Yes, Delete</button>
		</form>
		</div>
    </div>
  </div>
</div>


<tr id="post_id_{{ $category->id }}">
<td>
<span class="textblod">{{ $category->name }}</span>
</td>

<td>
<span class="textblod">{{ date('d/m/Y', strtotime($category->created_at)) }}</span>
</td>

<td class="td_btn">
<a href="{{ route('blog.category.show', $category->id) }}" class="btn-delete"><i class="fa fa-eye"></i></a>
@if(Auth::user()->role=='3')
@else
<a href="javascript:void(0)" data-toggle="modal" data-target="#delete{{ $category->id }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
@endif
</td>
</tr>
@endforeach
</tbody>
</table>
</div>








<div class="space"></div>
<div class="space"></div>
<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >
@if(Auth::user()->role=='3')
@else
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#edit_description" class="icons-edit"> 
<i class="fa fa-edit"></i>
</a> 
@endif
</label>

<div class="col-md-12">
{!! ($News->description) !!}
</div>

</div>
</div>
</div>



  
 
  




<div class="modal fade" id="edit_photo" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit slider</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form  action = "{{ route('news.updateslider', $News->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<div class="row">
<div class="col-md-12" style="text-align:center;">
<img class="user-avatar mr-2" src="{{ asset('photo/blog_image/'.$News->photo) }}" style="width:250px;height:150px;border-radius:0%;">
</div>
</div>

<input type="hidden" name="products_id" id="products_id" value="{{ $News->id }}">

<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max" style="height:70px;">
<label for="title" class="textblod">Edit slider</label>
<input type="file" name="photo" id="photo" class="form-control" required />
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>



<div class="modal fade" id="edit_description" aria-hidden="true">
<div class="modal-dialog ">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit Description</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('news.update', $News->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<input type="hidden" name="name" value="{{ $News->name }}" />
<input type="hidden" name="active" value="{{ $News->active }}" />
<div class="">
<div class="form-group form-group-max">
<label for="title" class="textblod" style="padding:10px;">Short Description</label>
<textarea name="description" id="description" class="form-control" style="height:280px;">{{ $News->description }}</textarea>
</div>
</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>


		

</div>
</div>
</div>





<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>  

<script>  
    CKEDITOR.replace('description');  
    CKEDITOR.replace('editor2');  
  
    function getData() {  
        //Get data written in first Editor   
        var editor_data = CKEDITOR.instances['description'].getData();  
        //Set data in Second Editor which is written in first Editor  
        CKEDITOR.instances['editor2'].setData(editor_data);  
    }  
</script>

@endsection