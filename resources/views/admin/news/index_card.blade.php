@extends('layouts.admin')
@section('title', 'Manage news')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage news</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">news</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Blog ( {{ count($news) }} )</span>
</div>
<div class="col-md-5 text-right">
@if(Auth::user()->role=='3')
@else
<a href="javascript:void(0)" onclick="openNav()" class="btn btn-success mb-2">Add new</a>
@endif
<a href="{{ route('news.index') }}" class="btn-switch" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a> 
<a href="{{route('news.card')}}" class="btn-switch-active" title="List"><i class="fa fa-list" aria-hidden="true"></i></a> 
</div>
</div>
<div class="space"></div>
<div class="space"></div>





<div class="row">
<div class="col-12">
@if(session()->get('success'))
<div class="alert alert-success mt-3">
{{ session()->get('success') }}  
</div>
@endif 



<input type="text" id="input_search" placeholder="Search for names.." title="Type in a name">
<ul class="row" id="filter_list">
@foreach($news as $post)
<li class="list_search col-lg-4 col-md-6 col-sm-12 mb-4">
<div class="card card-small card-post card-post--1">
<div class="card-post__image" style="background-image: url('{{ asset('public/photo/blog_image/'.$post->photo) }}');">
<a href="{{ route('news.show', $post->id) }}" class="card-post__category badge badge-pill">@if($post->active =='1')
<span class="active_back">published</span>
@else
<span class="blocked_back">Not published</span>
@endif</a>
</div>
<div class="card-body">
<h5 class="card-title">
<a class="text-fiord-blue" href="{{ route('news.show', $post->id) }}">{{ $post->name  }}</a>
</h5>
<div class="space"></div>
<span class="text-muted">{{ date('d/m/Y', strtotime($post->created_at)) }}</span>
</div>
</div>
</li>
@endforeach
</ul>             
<div class="no-result text-center no-results" style="display: none;">No results</div>
</div> 
</div>
</div>
@endsection