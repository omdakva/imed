@extends('layouts.admin')
@section('title', 'Manage blog')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage blog</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('blog.index') }}">Manage blog /</a>
<a id="pagetitlesecondelink">category</a>
</div>
</div>

<div class="space"></div>
<div class="space"></div>




<div class="modal fade" id="add_new" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Add new category</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form action="{{ route('blog.category.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="active" id="active" value="0">
<div class="modal-body">
<div class="form-group col-md-12">
<label for="name" class="col-sm-12 control-label textblod">Title</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="name" name="name" value="" required="">
</div>
</div>

<div class="form-group col-md-12">
<label for="name" class="col-sm-12 control-label textblod">Title ( FR )</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="name_fr" name="name_fr" value="" required="">
</div>
</div>

<div class="form-group col-md-12">
<label for="name" class="col-sm-12 control-label textblod">Title ( DE )</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="name_de" name="name_de" value="" required="">
</div>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save">Save</button>
</div>
</form>

</div>
</div>
</div>












<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Category ( {{ $blog_categorycount }} )</span>
</div>
<div class="col-md-5" style="text-align:right;">
@if(Auth::user()->role=='3')
@else
<a href="javascript:void(0)" data-toggle="modal" data-target="#add_new" class="btn btn-success mb-2">Add new</a>
@endif 
<a href="{{ route('blog.category') }}" class="btn-switch-active" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a> 
<a href="{{route('blog.category.card')}}" class="btn-switch" title="List"><i class="fa fa-list" aria-hidden="true"></i></a> 
</div>
</div>
<div class="space"></div>
<div class="space"></div>





<div class="row">
<div class="col-12">
@if(session()->get('success'))
<div class="alert alert-success mt-3">
{{ session()->get('success') }}  
</div>
@endif 



<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Title</th>
<th>Date</th>
<th style="text-align:center;">Publish</th>
<th style="text-align:center;">Translate</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Blog_category as $post)
<div class="modal fade" id="delete{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
		<a  href="javascript:void(0)" id="delete-post" data-id="{{ $post->id }}" class="model_btn_delete delete-post" data-dismiss="modal">Yes, Delete</a>
      </div>
    </div>
  </div>
</div>


<tr id="post_id_{{ $post->id }}">
<td>{{ $post->name  }}</td>

<td>{{ $post->created_at  }}</td>
<td style="text-align:center;">
@if($post->active =='1')
<span class="active_back">published</span>
@else
<span class="blocked_back">Not published</span>
@endif
</td>

<td style="text-align:center;">
@if(Auth::user()->role=='3')
@if($post->name_de =='0')
<a href="javascript:void(0)" class="in_progress_back"><span>In progress</span></a>
@else
<a href="javascript:void(0)" class="active_back"><span>Translate</span></a>
@endif

@else
@if($post->name_de =='0')
<a href="{{ route('news.translate', $post->id) }}" class="in_progress_back"><span>In progress</span></a>
@else
<a href="{{ route('news.show_translate', $post->id) }}" class="active_back"><span>Translate</span></a>
@endif
@endif
</td>

<td class="td_btn">
<a href="{{ route('blog.category.show', $post->id) }}" class="btn-delete"><i class="fa fa-eye"></i></a>
@if(Auth::user()->role=='3')
@else
<a href="{{ route('blog.category.edit', $post->id) }}" class="btn-delete"><i class="fa fa-edit"></i></a>
@endif 
</td>
</tr>
@endforeach
</tbody>
</table>        

       </div> 
    </div>
</div>






<style>
.overlay {
  height: 0%;
  width: 100%;
  position: fixed;
  z-index: 99999999999999999;
  top: 0;
  left: 0;
  background-color:#ffffff;
  background-color:#ffffff;
  overflow-y: hidden;
  transition: 0.5s;
}

.overlay-content {
  position: relative;
  top: 5%;
  width: 100%;
  text-align: left;
  margin-top: 0px;
}

.overlay a {
  padding: 8px;
  text-decoration: none;
  font-size: 36px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.overlay a:hover, .overlay a:focus {
  color: #f1f1f1;
}

.overlay .closebtn {
  position: absolute;
  top: -20px;
  right: 45px;
  font-size: 60px;
}

@media screen and (max-height: 450px) {
  .overlay {overflow-y: auto;}
  .overlay a {font-size: 20px}
  .overlay .closebtn {
  font-size: 40px;
  top: 0px;
  right: 35px;
  }
}
</style>



<script>
function openNav() {
  document.getElementById("myNav").style.height = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.height = "0%";
}
</script>


<link href="{{ asset('public/text-editor/editor.css')}}" type="text/css" rel="stylesheet"/>
<script src="{{ asset('public/text-editor/editor.js')}}"></script>
<script>
$(document).ready(function() {
$("#txtEditor").Editor();
});
</script>




<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>  

<script>  
    CKEDITOR.replace('description');  
    CKEDITOR.replace('editor2');  
  
    function getData() {  
        //Get data written in first Editor   
        var editor_data = CKEDITOR.instances['description'].getData();  
        //Set data in Second Editor which is written in first Editor  
        CKEDITOR.instances['editor2'].setData(editor_data);  
    }  
</script>

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

 
@endsection