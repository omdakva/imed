@extends('layouts.admin')
@section('title', 'Manage blog')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage blog</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('blog.category') }}">Manage blog /</a>
<a id="pagetitlesecondelink">Category</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Edit category</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a onclick="goBack()" href="javascript:void(0)"  class="btn-switch" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a> 
<a href="{{route('blog.category')}}" class="btn-switch" title="List"><i class="fa fa-list" aria-hidden="true"></i></a> 
<a href="{{route('blog.category.edit', $Blog_category->id)}}" class="btn-switch-active" title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a> 
</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif




<div class="Individual cadre_add">
<form action = "{{ route('blog.category.update', $Blog_category->id) }}" method = "POST">
@csrf
@method('PATCH') @csrf
@method('PATCH') 

@if(empty($Category->name_de))
<input type="hidden" name="name_de" class="form-control" value="0" />
@else
@endif
<div class="row">
<h3 class="page-title2" >Category details</h3>



<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Name</label>
<input type="text" name="name" class="form-control" value="{{$Blog_category->name}}" />
</div>


@if(empty($Category->name_de))
@else
<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Name ( DE )</label>
<input type="text" name="name_de" class="form-control" value="{{$Blog_category->name_de}}" />
</div>
@endif

<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Name ( FR )</label>
<input type="text" name="name_fr" class="form-control" value="{{$Blog_category->name_fr}}" />
</div>

<div class="form-group col-md-6">
<label for="manage_client-description" class="textblod">Name ( DE )</label>
<input type="text" name="name_de" class="form-control" value="{{$Blog_category->name_de}}" />
</div>

<div class="form-group col-md-6">
<label for="post-description" style="width:100%;" class="textblod">Activation</label>
<label for="r2" class="textnormal">
<input type="radio" name="active" id="r2"onClick="getResults()" value="1" 
@if($Blog_category->active == 1)
checked  
@else
@endif > Active
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="active" id="r1" onClick="getResults()" value="0"
@if($Blog_category->active == 0)
checked  
@else
@endif> Blocked
</label>		
</div>
</div>
<div class="modal-footer"style="text-align:right;">
<a onclick="goBack()" href="javascript:void(0)" class="model_btn_close">Cancel</a>
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>

  
 
  





		

</div>
</div>
</div>
</br>


@endsection