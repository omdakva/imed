@extends('layouts.admin')
@section('title', 'Manage blog')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage blog</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('blog.category') }}">Manage blog /</a>
<a id="pagetitlesecondelink">Category</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Category</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a onclick="goBack()" href="javascript:void(0)"  class="btn-switch" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a> 
<a href="{{route('blog.category')}}" class="btn-switch" title="List"><i class="fa fa-list" aria-hidden="true"></i></a> 
@if(Auth::user()->role=='3')
@else
<a href="{{route('blog.category.edit', $Blog_category->id)}}" class="btn-switch" title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a> 
@endif

</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif




<div class="">
<div class="row">
<div class="col-md-8">
<div class="cadre_add">
<h3 class="page-title2" >Category details</h3>
<div class="form-group">

<div class="form-group row">

<div class="col-md-3">
<div class="textblodfull">Name</div>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">: {{ $Blog_category->name }}</span></div>
</div>
<div class="space"></div>


<div class="col-md-3">
<div class="textblodfull">Activation</div>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">: @if($Blog_category->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></div>
</div>
<div class="space"></div>



<div class="col-md-3">
<div class="textblodfull">Creation date</div>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">: {{ date('d/m/Y', strtotime($Blog_category->created_at)) }}</span></div>
</div>
<div class="space"></div>


<div class="col-md-3">
<div class="textblodfull">Update date</div>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">: {{ date('d/m/Y', strtotime($Blog_category->updated_at)) }}</span></div>
</div>
<div class="space"></div>
</div>

</div>
</div>
</div>








<div class="col-md-4">
<div class="cadre_add ">

<div class="col-md-12 padding0">
<label for="name" class="control-label textblod">Language</label>
<div class="col-sm-12 padding0">
<select  id="name" name="name" class="form-control" onchange="location=this.value">
<option value="{{route('category.show', $Blog_category->id)}}" SELECTED>English</option>
<option value="{{route('category_de.view', $Blog_category->id)}}">Deutsch</option>
<option value="{{route('category_fr.view', $Blog_category->id)}}">French</option>
</select>
</div>
</div>

<div class="card-header text-center">
<div class="mb-3 mx-auto">
</div>
<div class="space"></div>
</div>
</div>
</div>
</div>
@endsection