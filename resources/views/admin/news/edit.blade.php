@extends('layouts.admin')
@section('title', 'Manage news')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage news</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Manage news</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Edit blog</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('news.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif




<div class="Individual cadre_add">
<form action = "{{ route('news.update', $News->id) }}" method = "POST">
@csrf
@method('PATCH') 
<div class="row">


<div class="form-group col-md-12">
<label for="post-description" style="width:100%;" class="textblod">Published</label>
<label for="r2" class="textnormal">
<input type="radio" name="active" id="r2"onClick="getResults()" value="1" 
@if($News->active == 1)
checked  
@else
@endif > Active
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="active" id="r1" onClick="getResults()" value="0"
@if($News->active == 0)
checked  
@else
@endif> Blocked
</label>		
</div>


<div class="form-group col-md-12">
<label for="manage_client-description" class="textblod">Title</label>
<input type="text" name="name" class="form-control" value="{{$News->name}}" />
</div>


<div class="form-group form-group-max">
<label for="title" class="textblod" style="padding:10px;">Description</label>
<textarea name="description" id="description" class="form-control" style="height:280px;">{{$News->description}}</textarea>
</div>




</div>


<div class="modal-footer"style="text-align:right;">
<a href="{{ route('news.index') }}" class="model_btn_close">Cancel</a>
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>

  
 
  





		

</div>
</div>
</div>
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>  

<script>  
    CKEDITOR.replace('description');  
    CKEDITOR.replace('editor2');  
  
    function getData() {  
        //Get data written in first Editor   
        var editor_data = CKEDITOR.instances['description'].getData();  
        //Set data in Second Editor which is written in first Editor  
        CKEDITOR.instances['editor2'].setData(editor_data);  
    }  
</script>


@endsection