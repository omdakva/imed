@extends('layouts.admin')
@section('title', 'Manage user')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage user</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Manage user</a>
</div>
</div>

<div class="space"></div>
<div class="space"></div>



<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >User details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('user.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


<div class="space"></div>
<div class="space"></div>
  
<div class="row">
<div class="col-lg-12">
<div class="card card-small mb-4 pt-3 profile_bg">

<div class="row padding10">
<div class="text-center col-lg-4">
<div class="mb-3 mx-auto">
@if($User->role =='2')
<img class="rounded-circle" src="{{ asset('public/profile_pic/admin/'.$User->image) }}" alt="User Avatar" width="150" height="160"> </div>
@elseif($User->role =='1')
<img class="rounded-circle" src="{{ asset('public/profile_pic/admin/'.$User->image) }}" alt="User Avatar" width="150" height="160"> </div>
@else
<img class="rounded-circle" src="{{ asset('public/profile_pic/user/'.$User->image) }}" alt="User Avatar" width="150" height="160"> </div>
@endif
<h4 class="mb-0">{{ $User->firstname }} {{ $User->lastname }}</h4>
<span class="text-muted d-block mb-2">@if($User->role =='1')
<span class="">Admin</span>
@elseif($User->role =='2')
<span class="">Super admin</span>
@endif</span>
</div>

<div class="text-left col-lg-4" >
<ul class="list-group list-group-flush" style="margin-top:50px;">
<li class="list-group-item profile_bg">
<div class="progress-wrapper">
<strong class="text-muted d-block mb-2">{{ $User->email }}</strong>
<strong class="text-muted d-block mb-2">{{ $User->country }}</strong>
<strong class="text-muted d-block mb-2">{{ $User->tel }}</strong>
<strong class="text-muted d-block mb-2">@if($User->status =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</strong>
</div>
</li>
</ul>
</div>

<div class="text-right col-lg-4">
<ul class="list-group list-group-flush" style="margin-top:50px;">
@if($User->status =='1')
<li class="list-group-item px-4 profile_bg">
<div class="progress-wrapper">
<strong class="text-muted d-block mb-2">
@if(Auth::user()->role=='3')
<button href="javascript:void(0)"  style="float:right;margin-top:-5px;" type="button" class="btn_cercle_block" data-toggle="modal" data-target="#exampleModal">
Block
</button>
@else
<button href="javascript:void(0)" data-toggle="modal" data-target="#block" style="float:right;margin-top:-5px;" type="button" class="btn_cercle_block" data-toggle="modal" data-target="#exampleModal">
Block
</button>
@endif

</strong>
</div>
</li>
@else
<li class="list-group-item px-4 profile_bg">
<div class="progress-wrapper">
<strong class="text-muted d-block mb-2">
@if(Auth::user()->role=='3')
<button href="javascript:void(0)" style="float:right;margin-top:-5px;" type="button" class="btn_cercle_actived" data-toggle="modal" data-target="#exampleModal">
Block
</button>
@else
<button href="javascript:void(0)" data-toggle="modal" data-target="#actived" style="float:right;margin-top:-5px;" type="button" class="btn_cercle_actived" data-toggle="modal" data-target="#exampleModal">
Block
</button>
@endif
Actived
</button>
</strong>
</div>
</li>
@endif



</ul>
</div>

</div>
</div>
</div>
</div>





<div class="modal fade" id="block" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<form action = "{{ route('user.update', $User->id) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
@method('PATCH') 
<input type="hidden" value="0" name="status" />
<input type="hidden" name="image" id="image" value="{{$User->image}}">
<div class="modal-content">
<div class="modal-body" style="text-align:center;">
<h5 id="exampleModalLabel" class="textbloddelete">Block this user ?</h5>
<h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
</div>
<div class="modal-footer">
<button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
<button type="submit" class="model_btn_delete">Yes</a>
</div>
</div>
</form>
</div>
</div>










<div class="modal fade" id="actived" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<form action = "{{ route('user.update', $User->id) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
@method('PATCH') 
<input type="hidden" value="1" name="status" />
<input type="hidden" name="image" id="image" value="{{$User->image}}">
<div class="modal-content">
<div class="modal-body" style="text-align:center;">
<h5 id="exampleModalLabel" class="textbloddelete">Actived this user ?</h5>
<h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
</div>
<div class="modal-footer">
<button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
<button type="submit" class="model_btn_delete">Yes</a>
</div>
</div>
</form>
</div>
</div>














<div class="row">
<div class="col-lg-4">
<div class="card card-small mb-4 pt-3">
<ul class="list-group list-group-flush">
<li class="list-group-item px-4">
<div class="progress-wrapper">
<label class="textblodfull">E-mail <span class="textnormal">{{ $User->email }}</span></label>
<label class="textblodfull">Since <span class="textnormal">{{ $User->created_at }}</span></label>
<label class="textblodfull">Year <span class="textnormal">{{ $User->year }}</span></label>
</div>
</li>
</ul>
</div>
</div>


<div class="col-lg-8">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <span style="float:left;"class="m-0">Account Details  </span>
					  
					 
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                           

						   <div class="form-row">
                              <div class="form-group col-md-6">
                                <label class="textblodfull">Name</label>
                                <label class="textnormalfull">{{ $User->firstname }} {{ $User->lastname }}</label>
								</div>
								
								<div class="form-group col-md-6">
                                <label class="textblodfull">Email</label>
                                <label class="textnormalfull">{{ $User->email }}</label>
								</div>
                            </div>
							
							
							
							<div class="form-row">
                              <div class="form-group col-md-6">
                                <label class="textblodfull">Since</label>
                                <label class="textnormalfull">{{ $User->created_at }}</label>
								</div>
								
								<div class="form-group col-md-6">
                                <label class="textblodfull">Birthday</label>
                                <label class="textnormalfull">{{ $User->birthday }}</label>
								</div>
                            </div>
							
							
							<div class="form-row">
                              <div class="form-group col-md-6">
                                <label class="textblodfull">Tel</label>
                                <label class="textnormalfull">{{ $User->tel }}</label>
								</div>
								
								<div class="form-group col-md-6">
                                <label class="textblodfull">Mobile</label>
                                <label class="textnormalfull">{{ $User->mobile }}</label>
								</div>
                            </div>
							
							
							<div class="form-row">
                              <div class="form-group col-md-6">
                                <label class="textblodfull">Country</label>
                                <label class="textnormalfull">{{ $User->country }}</label>
								</div>
								
								<div class="form-group col-md-6">
                                <label class="textblodfull">City</label>
                                <label class="textnormalfull">{{ $User->city }}</label>
								</div>
                            </div>
							
							<div class="form-row">
                              <div class="form-group col-md-6">
                                <label class="textblodfull">Address</label>
                                <label class="textnormalfull">{{ $User->address }}</label>
								</div>
								
								<div class="form-group col-md-6">
                                <label class="textblodfull">Postcode</label>
                                <label class="textnormalfull">{{ $User->postcode }}</label>
								</div>
                            </div>

                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
</div>
</div>
</div>

@endsection