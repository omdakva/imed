@extends('layouts.admin')
@section('title', 'Account Setting')
@section('content')


<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Account Setting</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Account Setting</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>


<div class="row">

<div class="col-md-6 col-sm-12 mb-12" onclick="window.location.href='{{ route('admin.edit.profile') }}'">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/team_work.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3">Update my info</h6>
<span class="stats-small__label text-uppercase textnormal"></span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>



<div class="col-md-6 col-sm-12 mb-12" onclick="window.location.href='{{ route('change.password.adminview') }}'">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/safe.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3">Update my password</h6>
<span class="stats-small__label text-uppercase textnormal"></span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="col-md-6 col-sm-12 mb-12" onclick="window.location.href='{{ route('change.pic.adminview') }}'">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/target.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3">Update my photo</h6>
<span class="stats-small__label text-uppercase textnormal"></span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection