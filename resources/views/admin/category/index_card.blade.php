@extends('layouts.admin')
@section('title', 'Manage category')
@section('content')
<script src="{{ asset('public/ajax/libs/jquery.validate.js')}}"></script>




<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage category</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Category</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Category ( {{ $Categorycount }} )</span>
</div>
<div class="col-md-5 text-right">
@if(Auth::user()->role=='3')
@else
<a href="{{ route('category.create') }}" class="btn btn-success mb-2">Add new</a> 
@endif
<a href="{{ route('category.create') }}" class="btn-switch" title="Excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a> 
<a href="{{ route('category.create') }}" class="btn-switch" title="PDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> 
<a href="{{ route('category.index') }}" class="btn-switch" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a> 
<a href="{{route('category.card')}}" class="btn-switch-active" title="List"><i class="fa fa-list" aria-hidden="true"></i></a> 
</div>
</div>
<div class="space"></div>
<div class="space"></div>





<div class="row">
<div class="col-12">
@if(session()->get('success'))
<div class="alert alert-success mt-3">
{{ session()->get('success') }}  
</div>
@endif 






<input type="text" id="input_search" placeholder="Search for names.." title="Type in a name">
<ul class="row" id="filter_list">
@foreach($Category as $post)
<li class="list_search col-lg-3 col-md-6 col-sm-12 mb-4">
<div class="card card-small card-post card-post--1">
<div class="card-post__image" style="background-image: url('{{ asset('public/photo/category_logo/'.$post->photo) }}');">
<a href="{{ route('category.show', $post->id) }}" class="card-post__category badge badge-pill">@if($post->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</a>
</div>
<div class="card-body">
<h5 class="card-title">
<a class="text-fiord-blue" href="{{ route('category.show', $post->id) }}">{{ $post->name  }}</a>
</h5>
<p class="card-text d-inline-block mb-3">
Subcategory : @if(count($post->subcategory))
{{count($post->subcategory)}}
@else
0
@endif
</p>
<div class="space"></div>
<p class="card-text d-inline-block mb-3">
Product : @if(count($post->products))
 {{count($post->products)}}
 @else
 0
  @endif
</p>
<div class="space"></div>
<span class="text-muted">{{ date('d/m/Y', strtotime($post->created_at)) }}</span>
</div>
</div>
</li>
@endforeach
</ul>             
<div class="no-result text-center no-results" style="display: none;">No results</div>

       
</div> 
</div>
</div>




<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal"></h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form action="{{ route('category.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<div class="modal-body">

<input type="hidden" name="active" id="active" value="1">


<input type="hidden" name="id_admin" id="id_admin" value="{{ Auth::user()->id }}">
<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max" style="height:70px;">
<label for="title" class="textblod">Title</label>
<input name="name" id="name" class="form-control" style="height:40px;" required />
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>



<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#create-new-post').click(function () {
        $('#btn-save').val("create-post");
        $('#postForm').trigger("reset");
        $('#postCrudModal').html("Add New Category");
        $('#ajax-crud-modal').modal('show');
    });
 
    $('body').on('click', '#edit-post', function () {
      var post_id = $(this).data('id');
      $.get(+post_id+'/edit', function (data) {
         $('#postCrudModal').html("Edit Documents");
          $('#btn-save').val("edit-post");
          $('#ajax-crud-modal').modal('show');
          $('#post_id').val(data.id);
          $('#id_client').val(data.id_client);
          $('#crm_type').val(data.crm_type);
		  
		          
		  
          $('#title').val(data.title);
          $('#fichier').val(data.fichier);
          $('#crm_date').val(data.crm_date);  
          $('#crm_time').val(data.crm_time);  
          $('#note').val(data.note);  
          $('#id_admin').val(data.id_admin);  
          $('#id_day').val(data.id_day);  
          $('#id_month').val(data.id_month);  
          $('#month').val(data.month);  
          $('#id_year').val(data.id_year);  
      })
   });

    $('body').on('click', '.delete-post', function () {
        var post_id = $(this).data("id");		
		
		
		
		

 
        $.ajax({
            type: "DELETE",
            url: "{{ url('admin/category')}}"+'/'+post_id,
            success: function (data) {
                $("#post_id_" + post_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });   
  });
 
 if ($("#postForm").length > 0) {
      $("#postForm").validate({
 
     submitHandler: function(form) {

      var actionType = $('#btn-save').val();
      $('#btn-save').html('Sending..');


      $.ajax({
          data: $('#postForm').serialize(),
          url: "{{ route('category.store') }}",
        
          type: "POST",
          dataType: 'json',
          success: function (data) {
              var post = '<tr id="post_id_' + data.id + '"><td>' + data.crm_type + '</td><td>' + data.title + ' </td><td>' + data.crm_date + ' ' + data.crm_time + '</td><td>' + data.month + '</td>';
              post += '<td class="td_btn"><a href="{{ url("clients/documents") }}/' + data.id_client + '/' + data.id + '" class="btn-edite"><i class="fa fa-eye"></i></a> <a href="javascript:void(0)" id="edit-post" data-id="' + data.id + '" class="btn-edite"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)" id="delete-post" data-id="' + data.id + '" class="btn-delete delete-post"><i class="fa fa-trash"></i></a></td></tr>';
              post += '';
               
              
              if (actionType == "create-post") {
                  $('#posts-crud').prepend(post);
              } else {
                  $("#post_id_" + data.id).replaceWith(post);
              }
 
              $('#postForm').trigger("reset");
              $('#ajax-crud-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn-save').html('Save Changes');
          }
      });
    }
  })
}
</script>


<script>
$(document).ready(function () {
    $(".Call").hide();
    $("#r1").click(function () {
        $(".Call").show();
    });
	
    $("#r2").click(function () {
        $(".Call").hide();
    });
	
	$(".Visit").hide();
    $("#r1").click (function () {
        $(".Visit").hide();
    });
	
    $("#r2").click(function () {
        $(".Visit").show();
    });
});
</script>










 

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

 
@endsection