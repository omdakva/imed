@extends('layouts.admin')
@section('title', 'Manage category')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage category</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">Category</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Category</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a onclick="goBack()" href="javascript:void(0)"  class="btn-switch" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a> 
<a href="{{route('category.index')}}" class="btn-switch" title="List"><i class="fa fa-list" aria-hidden="true"></i></a> 
@if(Auth::user()->role=='3')
@else
<a href="{{route('category.edit', $Category->id)}}" class="btn-switch" title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a> 
@endif

</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif




<div class="">
<div class="row">
<div class="col-md-8">
<div class="cadre_add">
<h3 class="page-title2" >Category details</h3>
<div class="form-group">

<div class="form-group row">

<div class="col-md-3">
<div class="textblodfull">Name</div>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">: {{ $Category->name }}</span></div>
</div>
<div class="space"></div>


<div class="col-md-3">
<div class="textblodfull">Activation</div>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">: @if($Category->active =='1')
<span class="active_back">active</span>
@else
<span class="blocked_back">blocked</span>
@endif</span></div>
</div>
<div class="space"></div>



<div class="col-md-3">
<div class="textblodfull">Creation date</div>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">: {{ date('d/m/Y', strtotime($Category->created_at)) }}</span></div>
</div>
<div class="space"></div>


<div class="col-md-3">
<div class="textblodfull">Update date</div>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">: {{ date('d/m/Y', strtotime($Category->updated_at)) }}</span></div>
</div>
<div class="space"></div>
</div>

</div>
</div>
</div>








<div class="col-md-4">
<div class="cadre_add ">
<div class="col-md-12 padding0 text-center">
<a target="_blank" href="http://dluxxis.de/en/category/{{ $Category->name_url }}" class="btn btn-success mb-2">Preview</a>
</div>
<div class="col-md-12 padding0">
<label for="name" class="control-label textblod">Language</label>
<div class="col-sm-12 padding0">
<select  id="name" name="name" class="form-control" onchange="location=this.value">
<option value="{{route('category.show', $Category->id)}}" SELECTED>English</option>
<option value="{{route('category_de.view', $Category->id)}}">Deutsch</option>
<option value="{{route('category_fr.view', $Category->id)}}">French</option>
</select>
</div>
</div>

<div class="card-header text-center">
<div class="mb-3 mx-auto">
<img src="{{ asset('photo/category_logo/'.$Category->photo) }}" alt="Category logo" style="width:90%;height:160px;"> </div>
@if(Auth::user()->role=='3')
@else
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_photo" class="btn_add"> <i class="material-icons mr-1">person_add</i>Edit pic</a> 
@endif
</div>
<div class="space"></div>
</div>
</div>
</div>
</div>

  
 


 
 
 
 
 
 
 
 
 
 
 
 
 
 

<div class="modal fade" id="add_products" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Add product</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('category.store_product') }}"  method ="POST" accept-charset="utf-8">
@csrf
<div class="modal-body">
<input type="hidden" name="id_category" id="id_category" value="{{ $Category->id }}">
<div class="form-group form-group-max">
<label for="title" class="textblod">Product</label>
<select type="text" name="id_product" id="id_product" class="form-control" >
<option></option>
@foreach($product as $product_list)
<option value="{{ $product_list->id }}">{{ $product_list->product_name }} ( {{ $product_list->id }} )</option>
@endforeach
</select>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>


  
<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >Products ( {{count($products_count)}} )
@if(Auth::user()->role=='3')
@else
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#add_products" class="icons-edit"> 
<i class="fa fa-plus"></i>
</a> 
@endif

</label>
<div class="space"></div>

<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Products</th>
<th></th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($products_count as $post_product)
<div class="modal fade" id="products_count{{ $post_product->id_product }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
	          <h5 id="exampleModalLabel" class="textbloddelete">Delete ?</h5>
	          <h5 id="exampleModalLabel" class="textnormaldelete ">Please ensure and then confirm!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="model_btn_close" data-dismiss="modal">No, cancel</button>
		<form action="{{ route('category.destroy_product', $post_product->id_product) }}" METHOD="post">
		@csrf
		<input type="hidden" value="{{ $Category->id }}" name="category_id" />
		<button  type="submit" class="model_btn_delete delete-post">Yes, Delete</button>
		</form>
      </div>
    </div>
  </div>
</div>


<tr id="post_id_{{ $post_product->id_product }}">
<td>{{ $post_product->product_name }}</td>
<td class="td_btn">
<a href="javascript:void(0)" data-toggle="modal" data-target="#products_count{{ $post_product->id_product }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
















 
 
 
 
 
 
 

<div class="modal fade" id="edit_SEO" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit SEO settings</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>
<form  action = "{{ route('category.seo_en',$Category->id) }}"  method ="POST" accept-charset="utf-8">
@csrf
@method('PATCH')
<div class="modal-body">

<div class="form-group form-group-max">
<label for="title" class="textblod">SEO title</label>
<input type="text" name="seo_title_en" id="seo_title_en" class="form-control" value="{{ $Category->seo_title_en }}">
</div>

<div class="form-group form-group-max">
<label for="title" class="textblod">Focus keyword</label>
<input type="text" name="keyword_en" id="keyword_en" class="form-control" value="{{ $Category->keyword_en }}">
</div>


<div class="form-group form-group-max">
<label for="title" class="textblod">Meta description</label>
<textarea type="text" name="meta_description_en" id="textarea-height" class="form-control">{{ $Category->meta_description_en }}</textarea>
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>

</div>
</div>
</div>





<div class="space"></div>
<div class="space"></div>

<div class="cadre_add">
<label class="page-title2 col-md-12" style="padding:0px;" >SEO
@if(Auth::user()->role=='3')
@else
<a style="float:right;" href="javascript:void(0)" data-toggle="modal" data-target="#edit_SEO" class="icons-edit"> 
<i class="fa fa-edit"></i>
</a>  
@endif
</label>
<div class="space"></div>
<div class="form-group row">

<div class="col-md-3">
<div class="textblodfull">URL</div>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">: http://dluxxis.de/en/category/{{ $Category->name_url }}</span></div>
</div>
<div class="space"></div>

<div class="col-md-3">
<div class="textblodfull">SEO title</div>
</div>
<div class="col-md-9">
<div class="textblodfull">: @if(empty($Category->seo_title_en) OR $Category->seo_title_en=='0' )
<span class="NA">N / A</span>
@else
<span class="textnormal">{{ $Category->seo_title_en }}</span>
@endif
</div>
</div>
<div class="space"></div>
<div class="col-md-3">
<div class="textblodfull">Focus keyword</div>
</div>
<div class="col-md-9">
<div class="textblodfull">: @if(empty($Category->seo_title_en) OR $Category->seo_title_en=='0' )
<span class="NA">N / A</span>
@else
<span class="textnormal">{{ $Category->keyword_en }}</span>
@endif
</div>
</div>
<div class="space"></div>
<div class="col-md-3">
<div class="textblodfull">Meta description</div>
</div>
<div class="col-md-9">
<div class="textblodfull"> : @if(empty($Category->seo_title_en) OR $Category->seo_title_en=='0' )
<span class="NA">N / A</span>
@else
<span class="textnormal">{{ $Category->meta_description_en }}</span>
@endif</div>
</div>


</div>
</div>


<div class="space"></div>
<div class="space"></div>
<div class="space"></div>
<div class="space"></div>


























<div class="modal fade" id="edit_photo" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Edit image</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
	
<form  action = "{{ route('category.update_photo', $Category->id) }}" method = "POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">
<div class="modal-body">
<div class="row">
<div class="col-md-12" style="text-align:center;">
<img class="user-avatar mr-2" src="{{ asset('public/photo/category_logo/'.$Category->photo) }}" style="width:250px;height:150px;border-radius:0%;">
</div>
</div>

<input type="hidden" name="products_id" id="products_id" value="{{ $Category->id }}">

<input type="hidden" name="post_id" id="post_id">
<div class="form-group form-group-max" style="height:70px;">
<label for="title" class="textblod">Edit photo</label>
<input type="file" name="photo" id="photo" class="form-control" required />
</div>

</div>
<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save" id="btn-save" value="create">Save</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection