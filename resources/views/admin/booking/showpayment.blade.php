@extends('layouts.admin')
@section('title', 'Manage booking')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage booking</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('booking.index') }}">Booking /</a>
<a id="pagetitlesecondelink">Details</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Booking details</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('booking.index') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


  <div class="space"></div>
  <div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif

<div class="col-md-12 cadre_add" >
<a href="{{ route('booking.show', $orders_number) }}" class="btn_add2">Details</a> 
<a href="{{ route('booking.payment', $orders_number) }}" class="btn_add2active">Delivery and payment</a> 
</div>

<div class="row">
@foreach($Orders as $post)

<div class="space"></div>

<div class="col-lg-6 col-sm-12 mb-12">
<div class="card card-small h-100">
<div class="card-header border-bottom">
<h6 class="m-0">Information Delivery</h6>
</div>
<div class="space"></div>
<div class="card-body d-flex py-0">
<div class="form-group col-md-12">
<label class="textblodfull">Depart: 
<span class="textnormal"> Oui</span>
<span class="textnormal"> 12/12/21</span>

</label>
<label class="textblodfull">Arriver: 
<span class="textnormal">non</span>
<span class="textnormal"> </span>

</label>
<label class="textblodfull">Localisation etat : 
<span class="textnormal">
@if($post->localisation_etats =='1')
<span class="active_back">active</span>
@else
<span class="in_progress_back">In progress</span>
@endif
</span>
</label>
</div>
</div>
<div class="card-header border-top text-center">
<a href="javascript:void(0)" data-toggle="modal" data-target="#localisation" class="btn btn-success mb-2" id="create-new-post">Show Localisation</a> 
</div>
</div>
</div>
			  
	







<div class="modal fade" id="localisation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-body">
<h5 id="exampleModalLabel" class="textbloddelete">Localisation</h5>
<div class="mapouter">
<div class="gmap_canvas">
</div>
<style>.mapouter{position:relative;text-align:right;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}</style>
</div>



<div class="space"></div>
<form action = "{{ route('booking.update', $post->id) }}" method = "POST">
@csrf
@method('PATCH')
<input type="hidden" name="user_name" class="form-control" value=""/>
<input type="hidden" name="orders_number" value="">
<div class="row">
<div class="form-group col-md-12">

							
							
<label for="post-description" style="width:100%;" class="textblod">Activation</label>
<label for="r2" class="textnormal">
<input type="radio" name="localisation_etats" id="r2"onClick="getResults()" value="1" 
@if($post->localisation_etats == 1)
checked  
@else
@endif > Active
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="localisation_etats" id="r1" onClick="getResults()" value="0"
@if($post->localisation_etats == 0)
checked  
@else
@endif> Blocked
</label>		
</div>
</div>

<div class="modal-footer"style="text-align:right;">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>

<div class="modal-footer">
<button type="button" class="model_btn_close" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>




		

<div class="col-lg-6 col-sm-12 mb-12">
<div class="card card-small h-100">
<div class="card-header border-bottom">
<h6 class="m-0">Payement details</h6>
</div>
<div class="space"></div>
<div class="card-body d-flex py-0">
<div class="form-group col-md-12">
<label class="textblodfull">Type : <span class="textnormal">{{ $post->mode }}</span></label>
<label class="textblodfull">Transaction: <span class="textnormal">{{ $post->transaction }}</span></label>
<label class="textblodfull">Delivery  : <span class="textnormal"> En cours</span></label>
<label class="textblodfull">
Date : <span class="textnormal">{{ $post->created_at }}
</span>
</label>
</div>
</div>
<div class="card-header border-top text-center">
<label class="textblodfull">Total : {{ $post->total_amount }} {{ $post->currency }}</label>
</div>
</div>
</div>	

<div class="space"></div>
<div class="space"></div>

<div class="col-lg-12 col-sm-12 mb-12">
<div class="card card-small h-100">
<div class="card-header border-bottom">
<h6 class="m-0"> Details</h6>
</div>
<div class="space"></div>

<div class="card-body d-flex py-0">
<div class="row items_list"style="padding:10px;">

</div>
</div>

<div class="card-header border-top text-center">
<label class="textblodfull"></label>
</div>
</div>
</div>



@endforeach
			  





</div>



  
 
  





		

</div>
</div>
</div>
</br>


@endsection