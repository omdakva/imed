@extends('layouts.admin')
@section('title', 'Dluxxis SEO')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >D'luxxis SEO</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitlesecondelink">D'luxxis SEO</a>
</div>
</div>


<div class="row">
<div class="space"></div>
<div class="space"></div>

<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('photo/seo/searching.png')}}" class="">
</div>
<div class="col-md-12">
<h1 class="pagetitle">Welcome to d'luxxis SEO</h1>
</div>
<div class="col-md-12" style="text-align:center;">
<a href="{{route('seo.settings')}}" class="btn_col2">Start</a>
</div>
</div>


</div>
</div>
@endsection