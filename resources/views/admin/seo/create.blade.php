@extends('layouts.admin')
@section('title', 'Dluxxis location')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Dluxxis location</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('seo') }}">D'luxxis SEO /</a>
<a id="pagetitleprimerlink" href="{{ route('seo.settings') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('seo.settings.url') }}">Url /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>
<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new URL</h3>
</div>
<div class="row">
<div class="col-lg-12 cadre_filter">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
<form id="file-upload-form" class="uploader" action="{{ route('seo.settings.url.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="active" id="active" value="0">
<input type="hidden" name="date" id="date" value="{{date('Y-m-d')}}">
<div class="row">
<div class="form-group col-md-4">
<label for="name" class="col-sm-12 control-label textblod">D'luxxis</label>
<div class="col-sm-12">
<input type="text" class="form-control" value="http://dluxxis.de" disabled>
<input type="hidden" class="form-control" id="domain" name="domain" value="http://dluxxis.de" required="">
</div>
</div>

<div class="form-group col-md-4">

<label for="name" class="col-sm-12 control-label textblod">Language</label>
<div class="col-sm-12">
<select class="form-control" id="lang" name="lang" value="" required="">
<option value=""></option>

<option value="fr" @if(count($SEO_url_fr)==5)
disabled class="blocked_back"
@else
@endif>fr</option>

<option value="de" @if(count($SEO_url_de)==5)
disabled
@else
@endif>de</option>

<option value="en" @if(count($SEO_url_en)==5)
disabled
@else
@endif>en</option>
</select>
</div>
</div>


<div class="form-group col-md-4">
<label for="name" class="col-sm-12 control-label textblod">Pages</label>
<div class="col-sm-12">
<select class="form-control" id="page" name="page" required="">
<option value=""></option>
@foreach($Pages as $post)
<option value="{{ $post->name  }}">{{ $post->name  }}</option>
@endforeach
</select>
</div>
</div>


<div class="form-group col-md-6">
<label for="name" class="col-sm-12 control-label textblod">SEO title</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="seo_title" name="seo_title" required="">
</div>
</div>


<div class="form-group col-md-6">
<label for="name" class="col-sm-12 control-label textblod">Focus keyword</label>
<div class="col-sm-12">
<input type="text" class="form-control" id="keyword" name="keyword"required="">
</div>
</div>



<div class="form-group col-md-12">
<label for="name" class="col-sm-12 control-label textblod">Meta description</label>
<div class="col-sm-12">
<textarea type="text" class="form-control" id="meta_description" name="meta_description" style="height:120px;" required=""></textarea>
</div>
</div>



            
<div class="modal-footer"style="text-align:right;">
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>
</div>
</div>
</div>


@endsection