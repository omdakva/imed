@extends('layouts.admin')
@section('title', 'Dluxxis SEO')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >D'luxxis SEO</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('seo') }}">D'luxxis SEO /</a>
<a id="pagetitleprimerlink" href="{{ route('seo.settings') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('seo.settings.url') }}">Url /</a>
<a id="pagetitlesecondelink">Details</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Url</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('seo.settings.url') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif


<style>
.Snippet_Preview{color:#5c30ee;font-weight:blod;font-weight:700;}
.Snippet_Preview_url{color:#26a92d;font-weight:normal;font-weight:600;}
.Snippet_Preview_description{color:#676967;font-size:13px;}
</style>

<div class="">
<div class="row">
<div class="col-md-8">
<div class="cadre_add">

<div class="form-group row">
<div class="col-md-3">
<p class="textblodfull">Snippet Preview</p>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="Snippet_Preview">{{ $SEO_url->seo_title }}</span></div>
<div class="textblodfull"><span class="Snippet_Preview_url">{{ $SEO_url->url }}</span></div>
<div class="textblodfull"><span class="textnormal Snippet_Preview_description">{{ $SEO_url->meta_description }}</span></div>
</div>
</div>



<div class="form-group row">
<div class="col-md-3">
<p class="textblodfull">SEO title</p>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">{{ $SEO_url->seo_title }}</span></div>
</div>
</div>


<div class="form-group row">
<div class="col-md-3">
<p class="textblodfull">Focus keyword</p>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">{{ $SEO_url->keyword }}</span></div>
</div>
</div>


<div class="form-group row">
<div class="col-md-3">
<p class="textblodfull">Meta description</p>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">{{ $SEO_url->meta_description }}</span></div>
</div>
</div>



<div class="form-group row">
<div class="col-md-3">
<p class="textblodfull">URL</p>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">{{ $SEO_url->url }}</span></div>
</div>
</div>

</div>
</div>











<div class="col-md-4">


<div class="cadre_add ">
<div class="form-group row">



@if(Auth::user()->role=='3')
@else
<div class="col-md-6">
<div class="textblodfull">Published</div>
</div>
<div class="col-md-6">

@if($SEO_url->active =='1')
<div class="custom-control custom-toggle custom-toggle-sm mb-1">
<input id="customToggle2" name="id" value="{{ $SEO_url->id }}" onclick="window.location.href='{{route('seo.settings.url.update_pub_blocked', $SEO_url->id)}}'" type="checkbox" class="custom-control-input" CHECKED="CHECKED">
<label class="custom-control-label" for="customToggle2"></label>
</div>
@else
<div class="custom-control custom-toggle custom-toggle-sm mb-1">
<input id="customToggle2" name="id" value="{{ $SEO_url->id }}" onclick="window.location.href='{{route('seo.settings.url.update_pub_active', $SEO_url->id)}}'" type="checkbox" class="custom-control-input">
<label class="custom-control-label" for="customToggle2"></label>
</div>
@endif

@if(Auth::user()->role=='3')
@else
<a href="{{ route('seo.settings.url.edit', $post->id) }}" class="btn-delete"><i class="fa fa-edit"></i></a>
@endif
</div>
</div>
@endif





<div class="text-left">
<div class="form-group row">
<div class="col-md-6">
<div class="textblodfull">Page</div>
</div>
<div class="col-md-6">
<div class="textblodfull"><span class="textnormal">: {{ $SEO_url->page }}</span></div>
</div>
</div>


<div class="form-group row">
<div class="col-md-6">
<div class="textblodfull">Activation</div>
</div>
<div class="col-md-6">
<div class="textblodfull"><span class="textnormal">: @if($SEO_url->active =='1')
<span class="active_back">published</span>
@else
<span class="blocked_back">Not published</span>
@endif</span></div>
</div>
</div>


<div class="form-group row">
<div class="col-md-6">
<div class="textblodfull">Creation</div>
</div>
<div class="col-md-6">
<div class="textblodfull"><span class="textnormal">: {{ $SEO_url->date }}</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>


@endsection