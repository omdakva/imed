@extends('layouts.admin')
@section('title', 'Dluxxis SEO')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >D'luxxis SEO</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('seo') }}">D'luxxis SEO /</a>
<a id="pagetitleprimerlink" href="{{ route('seo.settings') }}">Settings /</a>
<a id="pagetitleprimerlink" href="{{ route('seo.settings.url') }}">Url /</a>
<a id="pagetitlesecondelink">Edit</a>
</div>
</div>


<div class="space"></div>
<div class="space"></div>




<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Edit Url</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a href="{{ route('seo.settings.url') }}" class="btn btn-success mb-2" id="create-new-post">Back to list</a> 
</div>
</div>


<div class="space"></div>
<div class="space"></div>

<div class="row">
<div class="col-lg-12 mx-auto"  style="padding:0px;">
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif


<style>
.Snippet_Preview{color:#5c30ee;font-weight:blod;font-weight:700;}
.Snippet_Preview_url{color:#26a92d;font-weight:normal;font-weight:600;}
.Snippet_Preview_description{color:#676967;font-size:13px;}
</style>

<div class="">
<div class="row">
<div class="col-md-12">
<form id="file-upload-form" class="uploader" action="{{ route('seo.settings.url.update' ,$SEO_url->id) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
@method('PATCH')
<div class="cadre_add">

<div class="form-group row">
<div class="col-md-3">
<p class="textblodfull">SEO title</p>
</div>
<div class="col-md-9">
<input type="text" class="form-control" id="seo_title" name="seo_title" value="{{ $SEO_url->seo_title }}" required="">
</div>
</div>


<div class="form-group row">
<div class="col-md-3">
<p class="textblodfull">Focus keyword</p>
</div>
<div class="col-md-9">
<input type="text" class="form-control" id="keyword" name="keyword" value="{{ $SEO_url->keyword }}" required="">
</div>
</div>


<div class="form-group row">
<div class="col-md-3">
<p class="textblodfull">Meta description</p>
</div>
<div class="col-md-9">
 <textarea type="text" class="form-control" id="meta_description" name="meta_description" required="" style="height:150px;">{{ $SEO_url->meta_description }}</textarea>
</div>
</div>



<div class="form-group row">
<div class="col-md-3">
<p class="textblodfull">URL</p>
</div>
<div class="col-md-9">
<div class="textblodfull"><span class="textnormal">{{ $SEO_url->url }}</span></div>
</div>
</div>

</div>
<div class="space"></div>
<div class="space"></div>
<div class="cadre_add text-right">
<button type="submit" class="model_btn_save">Save change</button>
</div>
</div>


</form>
</div>
</div>

</div>
</div>
</div>


@endsection