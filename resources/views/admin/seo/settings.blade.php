@extends('layouts.admin')
@section('title', 'Dluxxis SEO')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >D'luxxis SEO</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('seo') }}">D'luxxis SEO /</a>
<a id="pagetitlesecondelink">Settings</a>
</div>
</div>



<div class="row">
<div class="space"></div>
<div class="space"></div>

<div class="col-md-6 col-sm-6">
<div class="col-md-12 div_col4">
<div class="col-md-12 div_col_img4" style="text-align:center;">
<img src="{{ asset('public/image/icons/envol.png')}}" class="div_col_img_view4">
</div>
<div class="col-md-12">
<h1 class="pagetitle">D'luxxis URL & SEO title</h1>
</div>
<label class="text_font div_col_text4">
View, edit and generate D'luxxis URL
</label>
<div class="col-md-12" style="text-align:center;">
<a href="{{route('seo.settings.url')}}" class="btn_col2">View more</a>
</div>
</div>
</div>
</div>
</div>
@endsection