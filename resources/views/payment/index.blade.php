@extends('layout.store2')

@section('title', 'STORE-TEC - Cart')

@section('content')
</BR>
</BR>
</BR>
</BR>
</BR>
payment

    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Product</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
       
	   

	   
	   
<form method="POST" action="{{ url('orders') }}">
@csrf
 
 
@guest
@if (Route::has('register'))
@endif
@else
<input type="hidden" name="orders_number" class="form-control" value="<?php echo date('dmY')?><?php echo date('his')?>">

<div class="panel panel-header">
<div class="row">
<div class="col-md-6">
<div class="form-group">
<input type="hidden" name="customer_name" class="form-control" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}">
</div>
</div>

<div class="col-md-6">
<div class="form-group">
<input type="hidden" name="customer_id" class="form-control" value="{{ Auth::user()->id }}">
</div>
</div>

</div>
</div>
@endguest


<tbody>

        <?php $total = 0 ?>
         
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)

                <?php $total += $details['product_price'] * $details['quantity'] ?>

                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-3 hidden-xs"><img src="{{ asset('public/photo/products_logo/'.$details['photo']) }}" width="100" height="100" class="img-responsive"/></div>
                            <div class="col-sm-9">
                                <h4 class="nomargin">{{ $details['product_name'] }}</h4>
								<input type="hidden" value="{{ $id }}">
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">${{ $details['product_price'] }}</td>
                    <td data-th="Quantity">
                        <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity" />
                    </td>
                    <td data-th="Subtotal" class="text-center">${{ $details['product_price'] * $details['quantity'] }}</td>
                    <td class="actions" data-th="">
                        <a style="position:absolute;" class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-refresh"></i></a>
                        <a style="margin-left:70px;" class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
				
				
		 <tr>

         <td><input type="hidden" name="list_product[]" class="form-control" value="{{ $id }}"></td>
         <td><input type="hidden" name="product_name[]" class="form-control" value="{{ $id }}"></td>
@guest
@if (Route::has('register'))
@endif
@else
<td><input type="hidden" name="id_user[]" class="form-control" value="{{ Auth::user()->id_user }}"></td>  
@endguest
         <td><input type="hidden" name="quantity[]" class="form-control quantity" value="{{ $details['quantity'] }}"></td>
         <td><input type="hidden" name="budget[]" class="form-control budget" value="{{ $details['product_price'] }}"></td>
         <td><input type="hidden" name="amount[]" class="form-control amount" value="{{ $details['product_price'] * $details['quantity'] }}"></td>
         		<input type="hidden" name="brand[]" class="form-control amount" value="0">
</tr>
        @endforeach
        @endif
		

		<input type="hidden" name="total_amount" class="form-control amount" value="{{ $total }}">
		<input type="hidden" name="currency" class="form-control amount" value="TND">


		<input type="hidden" name="active" class="form-control amount" value="0">
		<input type="hidden" name="total" class="form-control amount" value="20">
		<input type="hidden" name="statut" class="form-control amount" value="2">

        </tbody>
        <tfoot>
       
	   
<tr>
<td><a href="{{route('index.store')}}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
<td colspan="2" class="hidden-xs"></td>
<td class="hidden-xs text-center" style="font-size:30px;"><strong>Total {{ $total }} {{ $details['product_currency'] }}</strong></td>
@guest
<td class="hidden-xs text-center">
<a style="width:100%;" class="btn btn-info" href="{{ route('login.store') }}">
 Login
 </a>
 </td>
@if (Route::has('register'))
<td class="hidden-xs text-center"><a style="width:100%;" class="btn btn-info" type="submit"> Login</a></td>
@endif
@else
<td class="hidden-xs text-center"><button style="width:100%;" class="btn btn-info" type="submit"> Continue</button></td>
@endguest
</tr>
		
</tfoot>
		
</form>
</table>
@section('scripts')






<div class="modal fade" id="login" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Login to your account</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
<form action="{{url('user/login')}}" method="POST" id="logForm">

                 {{ csrf_field() }}
<div class="modal-body">

   <div class="form-label-group">
                        <label for="inputEmail">Email address</label>
                         <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" >

                  @if ($errors->has('email'))
                  <span class="error">{{ $errors->first('email') }}</span>
                  @endif    
                </div> 
                </br>
                <div class="form-label-group">
                <label for="inputPassword">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
                  
                  @if ($errors->has('password'))
                  <span class="error">{{ $errors->first('password') }}</span>
                  @endif  
                </div>
</div>

<div class="modal-footer"style="text-align:right;">
<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-info" id="btn-save" value="create">Sign In</button>
</div>
</form>
</div>
</div>
</div>
    <script type="text/javascript">

        $(".update-cart").click(function (e) {
           e.preventDefault();

           var ele = $(this);

            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });

        $(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            if(confirm("Are you sure")) {
                $.ajax({
                    url: '{{ url('remove-from-cart') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

    </script>

@endsection
@endsection