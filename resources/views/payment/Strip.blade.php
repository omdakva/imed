@extends('layout.store2')
@section('css')
    <style>
        .StripeElement {
            box-sizing: border-box;
            height: 40px;
            padding: 10px 12px;
            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
            direction: initial;
        }
        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }
        .StripeElement--invalid {
            border-color: #fa755a;
        }
        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>
@endsection

@section('content')
 <!-- row opened -->

 <div class="row">
        <div class="col-xl-12 col-md-12">
                        <table class="table table-bordered table-hover mb-0 text-nowrap" style="margin-top:200px;">

                            <tbody>

                                <tr>
                                  

                                <td colspan="4" class="text-center">


                                    <form action="/charge" method="post" id="payment-form">
                                        @csrf
                                        <div class="">
                                            <input type="hidden" name="amount" value="{{ $amount}}">

                                            <div id="card-element">
                                                <!-- A Stripe Element will be inserted here. -->
                                            </div>

                                            <!-- Used to display form errors. -->
                                            <div id="card-errors" role="alert"></div>
                                        </div>

                                        <button class="btn btn-success mt-3">Submit Payment</button>
                                    </form></td>

                            </tr>

                            </tbody>
                        </table>
                    </div>


                </div>
      

    <!-- row closed -->
    </div>
    <!-- Container closed -->
    </div>
    <!-- main-content closed -->

@endsection
