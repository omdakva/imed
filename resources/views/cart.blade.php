@extends('layout.store2')
@section('title', 'STORE-TEC - Cart')
@section('content')


<div class="divcam">
<div><label class="text_title_home_blod">My cart</label></div>
@if(session('echec'))
<div class="alert alert-danger">
  {{ session('echec') }}
</div> 
@endif
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Product</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
       
	   

	   
	   
<form method="POST" action="{{ url('orders') }}">
@csrf
 
 
@guest
@if (Route::has('register'))
@endif
@else
<input type="hidden" name="orders_number" class="form-control" value="<?php echo date('dmY')?><?php echo date('his')?>">

<div class="panel panel-header">
<div class="row">
<div class="col-md-6">
<div class="form-group">
<input type="hidden" name="customer_name" class="form-control" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}">
</div>
</div>

<div class="col-md-6">
<div class="form-group">
<input type="hidden" name="customer_id" class="form-control" value="{{ Auth::user()->id }}">
</div>
</div>

</div>
</div>
@endguest

@if($cart)

<tbody>

@foreach( $cart->items as $product)


                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-3 hidden-xs"><img src="{{$product['image']}}" width="100" height="100" class="img-responsive"/></div>
                            <div class="col-sm-9">
                                <h4 class="nomargin">{{ $product['title'] }}</h4>
								<input type="hidden" value="">
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">{{ $product['price']}}</td>
                    <td data-th="Quantity">
                        <input type="number" value="{{ $product['qty']  }}" class="form-control quantity" />
                    </td>
                    <td data-th="Subtotal" class="text-center">{{ $product['price']*$product['qty'] }}</td>
                    <td class="actions" data-th="">
                        <a style="position:absolute;" class="btn btn-info btn-sm update-cart" data-id=""><i class="fa fa-refresh"></i></a>
                        <a style="margin-left:70px;" class="btn btn-danger btn-sm remove-from-cart" data-id=""><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
				
				
		 <tr>

         <td><input type="hidden" name="list_product[]" class="form-control" value=""></td>
         <td><input type="hidden" name="product_name[]" class="form-control" value=""></td>
@guest
@if (Route::has('register'))
@endif
@else
<td><input type="hidden" name="id_user[]" class="form-control" value="{{ Auth::user()->id }}"></td>  
         		<input type="hidden" name="brand[]" class="form-control amount" value="{{ Auth::user()->id }}">

@endguest
         <td><input type="hidden" name="quantity[]" class="form-control quantity" value="{{$product['qty'] }}"></td>
         <td><input type="hidden" name="budget[]" class="form-control budget" value="{{ $product['price'] }}"></td>
         <td><input type="hidden" name="amount[]" class="form-control amount" value="{{ $product['price']  * $product['qty']  }}"></td>
		<input type="hidden" name="currency" class="form-control amount" value="$">
</tr>
        @endforeach
		

		<input type="hidden" name="total_amount" class="form-control amount" value="{{$cart->totalPrice}}">

		<input type="hidden" name="delivery" class="form-control amount" value="0">

		<input type="hidden" name="active" class="form-control amount" value="0">
		<input type="hidden" name="total" class="form-control amount" value="20">
		<input type="hidden" name="statut" class="form-control amount" value="2">
		<input type="hidden" name="localisation_etats" class="form-control amount" value="0">

        </tbody>
        <tfoot>
       
	   
<tr>
<td><a href="{{route('index.store')}}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
<td colspan="2" class="hidden-xs"></td>
<td class="hidden-xs text-center" style="font-size:30px;"><strong>Total {{$cart->totalPrice}}</strong></td>

<td class="hidden-xs text-center"><a href="{{ url('orders') }}" class="btn btn-info"> checkout <a></td>
</tr>
		
</tfoot>
		
</form>
</table>
</div>
@endif

@section('scripts')






<div class="modal fade" id="login" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="postCrudModal">Login to your account</h4>
<button type="button" class="close model_fa_close" data-dismiss="modal">&times;</button>
</div>

	
<form action="{{url('user/login')}}" method="POST" id="logForm">

                 {{ csrf_field() }}
<div class="modal-body">

   <div class="form-label-group">
                        <label for="inputEmail">Email address</label>
                         <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" >

                  @if ($errors->has('email'))
                  <span class="error">{{ $errors->first('email') }}</span>
                  @endif    
                </div> 
                </br>
                <div class="form-label-group">
                <label for="inputPassword">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
                  
                  @if ($errors->has('password'))
                  <span class="error">{{ $errors->first('password') }}</span>
                  @endif  
                </div>
</div>

<div class="modal-footer"style="text-align:right;">
<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-info" id="btn-save" value="create">Sign In</button>
</div>
</form>
</div>
</div>
</div>
    <script type="text/javascript">

        $(".update-cart").click(function (e) {
           e.preventDefault();

           var ele = $(this);

            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });

        $(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            if(confirm("Are you sure")) {
                $.ajax({
                    url: '{{ url('remove-from-cart') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

    </script>

@endsection
@endsection