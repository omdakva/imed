@extends('layouts.app')

@section('title', $product->name)

@section('content')

    <div class="ui container masthead">

        <div class="ui breadcrumb">
            <a href="{{ route('home') }}" class="section">Home</a>
            <i class="right angle icon divider"></i>
            <a href="{{ route('shop.index') }}" class="section">Shop</a>
            <i class="right angle icon divider"></i>
            <div class="active section">{{ $product->name }}</div>
        </div>

    </div>

    <div class="ui divider"></div>

    <div class="ui vertical segment product">
        <div class="ui stackable grid container">
            <div class="row">
            
                <div class="six wide column">
                    <img src="/images/laptop.jpg" class="ui large bordered rounded image">
                </div>

                <div class="ten wide right floated column">
                    <h1 class="ui header">{{ $product->name }}</h1>
                    <p class="lead">{{ $product->details }}</p>

                    <h1 class="ui green header">{{ $product->present_price }}</h2>

                    <div class="ui product-content">
                        {{ $product->description }}
                    </div>
                    
                    <form action="{{ route('cart.store') }}" method="POST">
                        @csrf

                        <input type="hidden" name="id" value="{{ $product->id }}">
                        <input type="hidden" name="name" value="{{ $product->name }}">
                        <input type="hidden" name="price" value="{{ $product->price }}">

                        <button class="ui button" type="submit">
                            <i class="shopping cart icon"></i>
                            Add to Cart
                        </button>
                    </form>
					
					  {{-- Add Comment --}}
                <div class="add-comment mb-3">
                    @csrf
					<input class="comment" type="hidden" name="id" value="{{ $product->id }}">
                        <input type="hidden" name="name" value="{{ $product->name }}">
                        <input type="hidden" name="price" value="{{ $product->price }}">

                    <button data-post="{{ $product->id }}" class="btn btn-dark btn-sm mt-2 save-comment">Submit</button>
                </div>
                <hr/>
                    
                </div>
            </div>
        </div>
    </div>

@include('partials.might-like')


@endsection
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script type="text/javascript">
// Save Comment
$(".save-comment").on('click',function(){
    var _comment=$(".comment").val();
    var _post=$(this).data('post');
    var vm=$(this);
    // Run Ajax
    $.ajax({
        url:"{{ route('cart.store') }}",
        type:"post",
        dataType:'json',
        data:{
            comment:_comment,
            post:_post,
            _token:"{{ csrf_token() }}"
        },
        beforeSend:function(){
            vm.text('Saving...').addClass('disabled');
        },
        success:function(res){
            var _html='<blockquote class="blockquote animate__animated animate__bounce">\
            <small class="mb-0">'+_comment+'</small>\
            </blockquote><hr/>';
            if(res.bool==true){
                $(".comments").prepend(_html);
                $(".comment").val('');
                $(".comment-count").text($('blockquote').length);
                $(".no-comments").hide();
            }
            vm.text('Save').removeClass('disabled');
        }   
    });
});
</script>