-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2021 at 03:41 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_de` varchar(255) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`id`, `name`, `name_de`, `name_fr`, `active`, `updated_at`, `created_at`) VALUES
(1, 'blog_category 1', 'GGGGGGGGGG de 1', 'FFFFFFFFF RRRRRRRRR 1', 1, '2021-01-27 08:55:55', '2021-01-27 08:55:55'),
(2, 'blog_category 2', 'GGGGGGGGGG de 1', 'FFFFFFFFF RRRRRRRRR 1', 1, '2021-01-27 08:55:55', '2021-01-27 08:55:55'),
(3, 'blog_category 3', 'GGGGGGGGGG de 1', 'FFFFFFFFF RRRRRRRRR 1', 1, '2021-01-27 08:55:55', '2021-01-27 08:55:55'),
(4, 'blog_category 4', 'GGGGGGGGGG de 1', 'FFFFFFFFF RRRRRRRRR 1', 1, '2021-01-27 08:55:55', '2021-01-27 08:55:55');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category_list`
--

CREATE TABLE `blog_category_list` (
  `id` int(255) NOT NULL,
  `id_blog` varchar(255) NOT NULL,
  `id_category` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog_category_list`
--

INSERT INTO `blog_category_list` (`id`, `id_blog`, `id_category`, `updated_at`, `created_at`) VALUES
(5, '31', '3', '2021-01-27 09:52:34', '2021-01-27 09:52:34'),
(7, '32', '1', '2021-01-29 13:54:41', '2021-01-29 13:54:41');

-- --------------------------------------------------------

--
-- Table structure for table `catalogue`
--

CREATE TABLE `catalogue` (
  `id_catalogue` int(255) NOT NULL,
  `id_product` int(255) NOT NULL,
  `parent_id` int(100) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `coordinate_x` varchar(255) NOT NULL,
  `coordinate_y` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `catalogue`
--

INSERT INTO `catalogue` (`id_catalogue`, `id_product`, `parent_id`, `photo`, `coordinate_x`, `coordinate_y`, `active`, `updated_at`, `created_at`) VALUES
(1, 0, 0, 'Rectangle 48.png', '0', '0', 1, '2021-01-13', '2021-01-13'),
(6, 0, 0, 'Laptop_Browsing_Work_Twitter.png', '0', '0', 1, '2021-01-19', '2021-01-19'),
(28, 4, 6, '0', '167', '286', 1, '2021-01-19', '2021-01-19'),
(29, 6, 6, '0', '193', '74', 1, '2021-01-19', '2021-01-19'),
(34, 6, 1, '0', '1011', '370', 1, '2021-01-19', '2021-01-19'),
(35, 4, 1, '0', '609', '408', 1, '2021-02-01', '2021-02-01');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `seo_title_en` varchar(255) NOT NULL,
  `keyword_en` varchar(255) NOT NULL,
  `meta_description_en` text NOT NULL,
  `name_url` varchar(255) NOT NULL,
  `name_de` varchar(255) NOT NULL,
  `seo_title_de` varchar(255) NOT NULL,
  `keyword_de` varchar(255) NOT NULL,
  `meta_description_de` text NOT NULL,
  `name_de_url` varchar(255) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `seo_title_fr` varchar(255) NOT NULL,
  `keyword_fr` varchar(255) NOT NULL,
  `meta_description_fr` text NOT NULL,
  `name_fr_url` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `seo_title_en`, `keyword_en`, `meta_description_en`, `name_url`, `name_de`, `seo_title_de`, `keyword_de`, `meta_description_de`, `name_de_url`, `name_fr`, `seo_title_fr`, `keyword_fr`, `meta_description_fr`, `name_fr_url`, `parent_id`, `photo`, `active`, `updated_at`, `created_at`) VALUES
(1, 'Night Room', '', '', '', 'night-room', 'Schlafzimmer', '', '', '', 'schlafzimmer', 'Chambre de nuit', '', '', '', 'chambre-de-nuit', 0, 'chambre-de-nuit-turque.jpg', 1, '2021-01-30', '2020-12-18'),
(9, 'Canapés', '', '', '', '', 'rrr DE', '', '', '', '', '', '', '', '', '', 16, '', 1, '2021-01-04', '2020-12-19'),
(10, 'Buffet', '', '', '', '', 'GGGGGGGGGG de tttttttttttttt', '', '', '', '', '', '', '', '', '', 16, '', 1, '2021-01-04', '2020-12-19'),
(11, 'Table', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 16, '', 1, '2020-12-21', '2020-12-19'),
(12, 'Enfants', '', '', '', '', 'Kinderzimmer', '', '', '', '', '', '', '', '', '', 0, 'image-not-found-99.png', 1, '2021-01-04', '2020-12-21'),
(13, 'Salle à manger', '', '', '', '', 'Esszimmer', '', '', '', '', '', '', '', '', '', 0, 'image-not-found-99.png', 1, '2021-01-04', '2020-12-21'),
(14, 'Office', '', '', '', 'office', 'Büro', '', '', '', 'büro', 'Bureau', '', '', '', 'bureau', 0, 'Rectangle 48.png', 1, '2021-01-30', '2020-12-21'),
(15, 'Extérieur', '', '', '', '', 'Outdoor', '', '', '', '', '', '', '', '', '', 0, 'image-not-found-99.png', 1, '2021-01-04', '2020-12-21'),
(16, 'Salon', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit,', 'salon', 'Wohnzimmer', '', '', '', 'wohnzimmer', 'Salon', 'fr fffffffff', 'fr ggggggggggggggg', 'fr ghhhhnb bnbnb', 'salon', 0, 'image-not-found-99.png', 1, '2021-01-25', '2020-12-21'),
(17, 'Fauteuil', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 16, '', 1, '2020-12-21', '2020-12-21'),
(18, 'Lits', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', 1, '2020-12-21', '2020-12-21'),
(19, 'Sommiers à lattes', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', 1, '2020-12-21', '2020-12-21'),
(20, 'Commodes', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', 1, '2020-12-21', '2020-12-21'),
(21, 'Garde de robe', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', 1, '2020-12-21', '2020-12-21'),
(22, 'Tables de nuits', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', 1, '2020-12-21', '2020-12-21'),
(23, 'Chambre de bébé', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 12, '', 1, '2020-12-21', '2020-12-21'),
(24, 'Chambre pour enfant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 12, '', 1, '2020-12-21', '2020-12-21'),
(25, 'Chambre pour adolescent', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 12, '', 1, '2020-12-21', '2020-12-21'),
(26, 'Table à manger', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 13, '', 1, '2020-12-21', '2020-12-21'),
(27, 'Chaise de salle à manger', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 13, '', 1, '2020-12-21', '2020-12-21'),
(28, 'Siège', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 13, '', 1, '2020-12-21', '2020-12-21'),
(29, 'Table de bureau', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 14, '', 1, '2020-12-21', '2020-12-21'),
(30, 'Chaise', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 14, '', 1, '2020-12-21', '2020-12-21'),
(31, 'Placards', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 14, '', 1, '2020-12-21', '2020-12-21'),
(32, 'Étagère', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 14, '', 1, '2020-12-21', '2020-12-21'),
(33, 'Bureau de secrétaire', '', '', '', '', 'rrr fffffff', '', '', '', '', '', '', '', '', '', 14, '', 1, '2021-01-04', '2020-12-21'),
(34, 'Meubles lounge', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 15, '', 1, '2020-12-21', '2020-12-21'),
(35, 'Tables', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 15, '', 1, '2020-12-21', '2020-12-21'),
(36, 'Canapés et fauteuils d\'extérieur', '', '', '', '', 'GGGGGGGGGG de', '', '', '', '', '', '', '', '', '', 15, '', 1, '2021-01-04', '2020-12-21'),
(37, 'Chaises longues', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 15, '', 1, '2020-12-21', '2020-12-21'),
(38, 'Hamacs', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 15, '', 1, '2020-12-21', '2020-12-21');

-- --------------------------------------------------------

--
-- Table structure for table `category_products`
--

CREATE TABLE `category_products` (
  `id` int(255) NOT NULL,
  `id_category` int(255) NOT NULL,
  `id_product` int(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_products`
--

INSERT INTO `category_products` (`id`, `id_category`, `id_product`, `updated_at`, `created_at`) VALUES
(73, 12, 3, '2021-01-19 15:50:18', '2021-01-19 15:50:18'),
(69, 13, 6, '2021-01-17 14:48:47', '2021-01-17 14:48:47'),
(68, 12, 6, '2021-01-17 14:43:26', '2021-01-17 14:43:26'),
(15, 16, 7, '2021-01-05 07:30:24', '2021-01-05 07:30:24'),
(16, 17, 7, '2021-01-05 07:30:24', '2021-01-05 07:30:24'),
(17, 16, 8, '2021-01-05 07:35:02', '2021-01-05 07:35:02'),
(18, 17, 8, '2021-01-05 07:35:02', '2021-01-05 07:35:02'),
(19, 16, 9, '2021-01-05 07:35:22', '2021-01-05 07:35:22'),
(20, 17, 9, '2021-01-05 07:35:22', '2021-01-05 07:35:22'),
(21, 16, 10, '2021-01-05 07:50:53', '2021-01-05 07:50:53'),
(22, 17, 10, '2021-01-05 07:50:53', '2021-01-05 07:50:53'),
(23, 16, 11, '2021-01-05 07:52:12', '2021-01-05 07:52:12'),
(24, 17, 11, '2021-01-05 07:52:12', '2021-01-05 07:52:12'),
(25, 16, 12, '2021-01-05 07:54:32', '2021-01-05 07:54:32'),
(26, 17, 12, '2021-01-05 07:54:32', '2021-01-05 07:54:32'),
(27, 16, 13, '2021-01-05 07:55:04', '2021-01-05 07:55:04'),
(28, 17, 13, '2021-01-05 07:55:04', '2021-01-05 07:55:04'),
(29, 16, 14, '2021-01-05 07:56:49', '2021-01-05 07:56:49'),
(30, 17, 14, '2021-01-05 07:56:49', '2021-01-05 07:56:49'),
(31, 16, 15, '2021-01-05 07:58:58', '2021-01-05 07:58:58'),
(32, 17, 15, '2021-01-05 07:58:58', '2021-01-05 07:58:58'),
(33, 16, 16, '2021-01-05 08:06:25', '2021-01-05 08:06:25'),
(34, 17, 16, '2021-01-05 08:06:25', '2021-01-05 08:06:25'),
(35, 16, 17, '2021-01-05 09:23:35', '2021-01-05 09:23:35'),
(36, 17, 17, '2021-01-05 09:23:35', '2021-01-05 09:23:35'),
(37, 16, 18, '2021-01-05 09:31:36', '2021-01-05 09:31:36'),
(38, 17, 18, '2021-01-05 09:31:36', '2021-01-05 09:31:36'),
(39, 16, 19, '2021-01-05 09:37:27', '2021-01-05 09:37:27'),
(40, 17, 19, '2021-01-05 09:37:27', '2021-01-05 09:37:27'),
(41, 16, 20, '2021-01-05 09:39:24', '2021-01-05 09:39:24'),
(42, 17, 20, '2021-01-05 09:39:24', '2021-01-05 09:39:24'),
(43, 16, 21, '2021-01-05 09:58:47', '2021-01-05 09:58:47'),
(44, 17, 21, '2021-01-05 09:58:47', '2021-01-05 09:58:47'),
(45, 16, 22, '2021-01-05 10:04:07', '2021-01-05 10:04:07'),
(46, 17, 22, '2021-01-05 10:04:07', '2021-01-05 10:04:07'),
(47, 16, 23, '2021-01-05 11:01:45', '2021-01-05 11:01:45'),
(48, 17, 23, '2021-01-05 11:01:45', '2021-01-05 11:01:45'),
(49, 16, 24, '2021-01-05 11:07:25', '2021-01-05 11:07:25'),
(50, 17, 24, '2021-01-05 11:07:25', '2021-01-05 11:07:25'),
(51, 16, 25, '2021-01-06 10:14:37', '2021-01-06 10:14:37'),
(52, 17, 25, '2021-01-06 10:14:37', '2021-01-06 10:14:37'),
(53, 16, 26, '2021-01-06 10:15:31', '2021-01-06 10:15:31'),
(54, 17, 26, '2021-01-06 10:15:31', '2021-01-06 10:15:31'),
(55, 16, 27, '2021-01-06 10:17:12', '2021-01-06 10:17:12'),
(56, 17, 27, '2021-01-06 10:17:12', '2021-01-06 10:17:12'),
(72, 1, 3, '2021-01-19 15:50:18', '2021-01-19 15:50:18'),
(82, 13, 3, '2021-01-29 14:59:23', '2021-01-29 14:59:23'),
(81, 1, 35, '2021-01-25 13:31:13', '2021-01-25 13:31:13'),
(83, 18, 3, '2021-01-29 14:59:31', '2021-01-29 14:59:31');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `symbole` varchar(255) NOT NULL,
  `code` varchar(100) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `parent_id` int(50) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country`, `symbole`, `code`, `photo`, `parent_id`, `active`, `updated_at`, `created_at`) VALUES
(4, 'France', 'FRA', '+33', 'téléchargement.png', 0, 1, '2021-01-16', '2021-01-16'),
(5, 'Italie', 'ITA', '+39', '2157-ZWADdL._AC_SX425_.jpg', 0, 1, '2021-01-16', '2021-01-16'),
(6, 'Tunisia', 'TUN', '+216', 'Tunisia-flag.jpg', 0, 1, '2021-01-16', '2021-01-16'),
(7, 'Ariana', 'TUN', '', '', 6, 1, '2021-01-16', '2021-01-16'),
(8, 'ccccccccccc', 'FRA', '1', '', 4, 1, '2021-01-16', '2021-01-16'),
(9, 'Italie', 'FRA', '1', '', 4, 1, '2021-01-16', '2021-01-16'),
(12, 'Germany', 'GER', '+49', 'drapeau-allemand-de-l-allemagne-126809851.jpg', 0, 1, '2021-01-18', '2021-01-18'),
(13, 'Berlin', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(14, 'Hamburg', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(15, 'Munich', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(16, 'Cologne', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(17, 'Frankfurt am Main', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(18, 'Stuttgart', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(19, 'Düsseldorf', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(20, 'Dortmund', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(21, 'Essen', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(22, 'Leipzig', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(23, 'Bremen', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(24, 'Dresden', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(25, 'Hanover', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(26, 'Nuremberg', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(27, 'Duisburg', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(28, 'Bochum', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(29, 'Wuppertal', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(30, 'Bielefeld', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(31, 'Bonn', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18'),
(32, 'Münster', 'GER', '+49', '0', 12, 1, '2021-01-18', '2021-01-18');

-- --------------------------------------------------------

--
-- Table structure for table `crm_about`
--

CREATE TABLE `crm_about` (
  `id` int(255) NOT NULL,
  `about` text NOT NULL,
  `about_de` text NOT NULL,
  `photo` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `crm_about`
--

INSERT INTO `crm_about` (`id`, `about`, `about_de`, `photo`, `updated_at`, `created_at`) VALUES
(1, 'TEST But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure', '0', 'Laptop_Browsing_Work_Twitter.png', '2021-01-12 12:53:08', '2021-01-12 12:53:08');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `symbole` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `symbole`, `active`, `updated_at`, `created_at`) VALUES
(2, 'EURO', '€', 1, '2020-12-30', '2020-12-21'),
(4, 'Dolar', '$', 0, '2020-12-30', '2020-12-24');

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `frommail` varchar(50) NOT NULL,
  `tomail` varchar(50) NOT NULL,
  `sub` varchar(50) NOT NULL,
  `body` varchar(250) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `frommail`, `tomail`, `sub`, `body`, `active`, `updated_at`, `created_at`) VALUES
(1, 'Dulucsshop@gmail.com', 'your@email.com', 'Demande', 'je cherche', 0, '2021-02-19 14:06:22', '2021-02-19 14:06:22'),
(2, 'Dulucsshop@gmail.com', 'my1@email.com', 'Demande', 'je cherche de demande', 1, '2021-02-17 14:52:47', '2021-02-17 14:52:47'),
(3, 'Dulucsshop@gmail.com', 'your@email.com', 'Objet', 'mon compte', 1, '2021-02-17 14:52:50', '2021-02-17 14:52:50'),
(4, 'Dulucsshop@gmail.com', 'my1@email.com', 'Demande', 'je cherche', 1, '2021-02-17 14:52:54', '2021-02-17 14:52:54'),
(5, 'Dulucsshop@gmail.com', 'my1@email.com', 'Demande', 'niveau de spécificité inférieur à !important et à !veryimportant. Concrètement la gestion du poids des priorités', 1, '2021-02-17 13:47:36', '2021-02-17 13:47:36'),
(6, 'Dulucsshop@gmail.com', 'imdealingtn@gmail.com', 'Demande', 'niveau de spécificité inférieur à !important et à !veryimportant. Concrètement la gestion du poids des priorités', 1, '2021-02-19 14:06:46', '2021-02-19 14:06:46'),
(7, 'Dulucsshop@gmail.com', 'my@mail.com', 'Demande', 'niveau de spécificité inférieur à priorités', 1, '2021-02-17 14:53:01', '2021-02-17 14:53:01'),
(8, 'Dulucsshop@gmail.com', 'my@email.com', 'Object', 'faire passer un texte rédigé dans une langue', 0, '2021-02-18 14:50:07', '2021-02-18 14:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `filter_test`
--

CREATE TABLE `filter_test` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_month` int(255) NOT NULL,
  `year` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `filter_test`
--

INSERT INTO `filter_test` (`id`, `name`, `id_month`, `year`) VALUES
(1, 'test', 202101, 2021);

-- --------------------------------------------------------

--
-- Table structure for table `image_favicon`
--

CREATE TABLE `image_favicon` (
  `id` int(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `active` int(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `image_favicon`
--

INSERT INTO `image_favicon` (`id`, `photo`, `active`, `updated_at`, `created_at`) VALUES
(12, 'User-Avatar-in-Suit-PNG.png', 0, '2021-02-01 07:53:59', '2021-02-01 08:53:59'),
(14, 'avatar-380-456332.png', 0, '2021-02-01 07:58:53', '2021-02-01 07:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(100) NOT NULL,
  `orders_id` int(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `budget` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `orders_id`, `product_name`, `brand`, `quantity`, `budget`, `amount`, `created_at`, `updated_at`) VALUES
(31, 16, '10', '361', '1', '1500', '1500', '2020-12-24 12:42:35', '2020-12-24 12:42:35'),
(30, 16, '9', '361', '1', '2000', '2000', '2020-12-24 12:42:35', '2020-12-24 12:42:35'),
(32, 20, '5', '1', '2', '250', '500', '2021-02-04 15:38:34', '2021-02-04 15:38:34'),
(33, 20, '6', '1', '3', '999', '2997', '2021-02-04 15:38:34', '2021-02-04 15:38:34'),
(34, 20, '3', '1', '2', '1200', '2400', '2021-02-04 15:38:34', '2021-02-04 15:38:34'),
(35, 21, '5', '1', '3', '250', '750', '2021-02-04 15:44:59', '2021-02-04 15:44:59'),
(36, 21, '6', '1', '4', '999', '3996', '2021-02-04 15:44:59', '2021-02-04 15:44:59'),
(37, 21, '3', '1', '2', '1200', '2400', '2021-02-04 15:44:59', '2021-02-04 15:44:59'),
(38, 22, '5', '1', '1', '250', '250', '2021-02-05 08:37:40', '2021-02-05 08:37:40'),
(39, 22, '6', '1', '1', '999', '999', '2021-02-05 08:37:40', '2021-02-05 08:37:40');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `symbole` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `symbole`, `active`, `updated_at`, `created_at`) VALUES
(4, 'English', 'En', 1, '2021-01-06', '2020-12-21'),
(5, 'French', 'Fr', 1, '2021-01-06', '2021-01-06'),
(6, 'Deutsch', 'De', 1, '2021-01-06', '2021-01-06');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `location_type` int(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `location_type`, `country`, `city`, `address`, `postcode`, `email`, `tel`, `mobile`, `active`, `updated_at`, `created_at`) VALUES
(3, 1, 'Tunisie', 'ARIANA', '49 rue Ibn zakour soukra', '2087', 'aymenaboudi4@gmail.com', '20174595', '20174595', 1, '2021-01-11', '2021-01-11');

-- --------------------------------------------------------

--
-- Table structure for table `method-delivery`
--

CREATE TABLE `method-delivery` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `shipment_details` text NOT NULL,
  `description` text NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `method-delivery`
--

INSERT INTO `method-delivery` (`id`, `name`, `plugin`, `price`, `currency`, `shipment_details`, `description`, `active`, `updated_at`, `created_at`) VALUES
(2, 'methods of Delivery 1', 'Plugin methods of Delivery', '10', '€', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2020-12-24 14:43:15', '2020-12-24 14:43:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_de` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_de` text NOT NULL,
  `active` tinyint(4) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `name_de`, `description`, `description_de`, `active`, `photo`, `updated_at`, `created_at`) VALUES
(7, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '0', '\"test Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', '0', 0, 'Laptop_Browsing_Work_Twitter.png', '2021-01-07', '2021-01-07'),
(16, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'gggggggg', '<blockquote>\r\n<h1><strong>Blog test Lorem ipsum dolor sit amet</strong>,</h1>\r\n</blockquote>\r\n\r\n<p>consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>\r\n\r\n<ul>\r\n	<li>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>\r\n	<li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</li>\r\n</ul>\r\n\r\n<blockquote>\r\n<p>. <strong>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</strong>.&quot;&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n</blockquote>', '<p>bbbbbbbbbbbbbbbbbbbb</p>', 0, 'Laptop_Browsing_Work_Twitter.png', '2021-01-13', '2021-01-13'),
(17, 'AYMEN 8888 ABOUDI', '0', '<p>bbbbbbbbbbbbbbbbbbbbb ggggg</p>', '0', 1, 'machine.png', '2021-01-17', '2021-01-17'),
(30, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '0', '<p>vc c cv vb vvbdfgdfvc</p>', '0', 0, 'background.jpg', '2021-01-27', '2021-01-27'),
(31, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '0', '<p>vcf cvdfv cc</p>', '0', 0, 'background.jpg', '2021-01-27', '2021-01-27'),
(32, 'hhhhhhhhhhhhhhhhh', '0', '<p>bv vbv vbvc cvcvc c</p>', '0', 0, 'background.jpg', '2021-01-29', '2021-01-29');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL,
  `date_creation` date DEFAULT NULL,
  `notice` text DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_update` date NOT NULL,
  `active` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(255) NOT NULL,
  `id_user` int(225) NOT NULL,
  `notifiable_type` varchar(255) NOT NULL,
  `notifiable` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `read_at` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `id_user`, `notifiable_type`, `notifiable`, `status`, `read_at`, `created_at`, `updated_at`) VALUES
(3, 1, 'Edit stock', 'Edit stock number of Product 4 to 0', '1', '2020-12-28 09:22:10', '2020-12-28 09:22:10', '2020-12-28 09:22:10'),
(4, 1, 'Edit localisation', 'Confirmed localisation of Test User', '1', '2020-12-28 09:22:52', '2020-12-28 09:22:52', '2020-12-28 09:22:52'),
(5, 1, 'Edit localisation', 'Confirmed localisation of Test User', '1', '2020-12-28 09:22:52', '2020-12-28 09:22:52', '2020-12-28 09:22:52'),
(6, 375, 'Register', 'Create new account', '1', '2020-12-29 09:57:33', '2020-12-29 09:57:33', '2020-12-29 09:57:33'),
(7, 376, 'Register', 'Create new account', '1', '2020-12-29 09:57:33', '2020-12-29 09:57:33', '2020-12-29 09:57:33'),
(8, 377, 'Register', 'Create new account', '1', '2020-12-29 10:13:27', '2020-12-29 10:13:27', '2020-12-29 10:13:27'),
(9, 378, 'Register', 'Create new account', '1', '2020-12-29 10:13:27', '2020-12-29 10:13:27', '2020-12-29 10:13:27'),
(10, 379, 'Register', 'Create new admin', '1', '2020-12-29 10:33:07', '2020-12-29 10:33:07', '2020-12-29 10:33:07'),
(11, 1, 'Edit stock', 'Edit stock number of Product 2 to 2', '1', '2020-12-29 03:56:29', '2020-12-29 15:56:29', '2020-12-29 15:56:29'),
(12, 1, 'Edit stock', 'Edit stock number of Product 2 to 0', '1', '2021-01-06 10:50:44', '2021-01-06 10:50:44', '2021-01-06 10:50:44'),
(13, 1, 'Edit stock', 'Edit stock number of Product 2 to 1', '1', '2021-01-13 07:29:02', '2021-01-13 07:29:02', '2021-01-13 07:29:02'),
(14, 380, 'Register', 'Create new admin', '1', '2021-01-16 10:51:43', '2021-01-16 10:51:43', '2021-01-16 10:51:43'),
(15, 381, 'Register', 'Create new admin', '1', '2021-01-16 10:51:43', '2021-01-16 10:51:43', '2021-01-16 10:51:43'),
(16, 382, 'Register', 'Create new admin', '1', '2021-01-16 12:10:29', '2021-01-16 12:10:29', '2021-01-16 12:10:29'),
(17, 383, 'Register', 'Create new account', '1', '2021-01-16 12:10:29', '2021-01-16 12:10:29', '2021-01-16 12:10:29'),
(18, 1, 'Edit stock', 'Edit stock number of Product 3 to 1', '1', '2021-01-30 08:13:17', '2021-01-30 08:13:17', '2021-01-30 08:13:17'),
(19, 1, 'Edit stock', 'Edit stock number of Product 3 to 0', '1', '2021-01-30 08:47:50', '2021-01-30 08:47:50', '2021-01-30 08:47:50'),
(20, 1, 'Edit stock', 'Edit stock number of Product 3 to 1', '1', '2021-02-01 02:59:42', '2021-02-01 14:59:42', '2021-02-01 14:59:42');

-- --------------------------------------------------------

--
-- Table structure for table `ordres`
--

CREATE TABLE `ordres` (
  `id` int(100) NOT NULL,
  `orders_number` varchar(255) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `totalQty` varchar(10) NOT NULL,
  `total_amount` varchar(100) NOT NULL,
  `currency` varchar(100) NOT NULL,
  `items` text NOT NULL,
  `cost` varchar(25) NOT NULL,
  `localisation_etats` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `statut` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordres`
--

INSERT INTO `ordres` (`id`, `orders_number`, `customer_id`, `totalQty`, `total_amount`, `currency`, `items`, `cost`, `localisation_etats`, `active`, `statut`, `created_at`, `updated_at`) VALUES
(116, 'C-1613723334', '389', '1', '999', '$', '{\"4\":{\"id\":4,\"title\":\"Product 6\",\"price\":\"999\",\"qty\":1,\"image\":\"http:\\/\\/127.0.0.1:8000\\/photo\\/products_logo\\/Groupe 114.png\"}}', '10', 1, 1, 1, '2021-02-19 13:46:41', '2021-02-19 13:46:41'),
(117, 'C-1613724334', '389', '2', '1899', '$', '{\"4\":{\"id\":4,\"title\":\"Product 6\",\"price\":\"999\",\"qty\":1,\"image\":\"http:\\/\\/127.0.0.1:8000\\/photo\\/products_logo\\/Groupe 114.png\"},\"2\":{\"id\":2,\"title\":\"Product 4\",\"price\":\"900\",\"qty\":1,\"image\":\"http:\\/\\/127.0.0.1:8000\\/photo\\/products_logo\\/Rectangle 37.png\"}}', '10', 1, 1, 1, '2021-02-19 13:46:50', '2021-02-19 13:46:50'),
(118, 'C-1613728479', '389', '2', '1749', '$', '{\"4\":{\"id\":4,\"title\":\"Product 6\",\"price\":\"999\",\"qty\":1,\"image\":\"http:\\/\\/127.0.0.1:8000\\/photo\\/products_logo\\/Groupe 114.png\"},\"3\":{\"id\":3,\"title\":\"Product 5\",\"price\":\"250\",\"qty\":3,\"image\":\"http:\\/\\/127.0.0.1:8000\\/photo\\/products_logo\\/Groupe 115.png\"}}', '10', 1, 1, 1, '2021-02-19 13:53:40', '2021-02-19 13:53:40'),
(119, 'C-1613730348', '1', '2', '200', '$', '{\"5\":{\"id\":5,\"title\":\"Salon4\",\"price\":\"200\",\"qty\":1,\"image\":\"http:\\/\\/127.0.0.1:8000\\/photo\\/products_logo\\/Groupe 112.png\"}}', '10', 1, 1, 1, '2021-02-19 13:47:02', '2021-02-19 13:47:02'),
(120, 'C-1613731275', '1', '1', '200', '$', '{\"5\":{\"id\":5,\"title\":\"Salon4\",\"price\":\"200\",\"qty\":1,\"image\":\"http:\\/\\/127.0.0.1:8000\\/photo\\/products_logo\\/Groupe 112.png\"}}', '10', 1, 1, 1, '2021-02-19 13:47:05', '2021-02-19 13:47:05'),
(121, 'C-1613742921', '389', '2', '1899', '$', '{\"2\":{\"id\":2,\"title\":\"Product 4\",\"price\":\"900\",\"qty\":1,\"image\":\"http:\\/\\/127.0.0.1:8000\\/photo\\/products_logo\\/Rectangle 37.png\"},\"4\":{\"id\":4,\"title\":\"Product 6\",\"price\":\"999\",\"qty\":1,\"image\":\"http:\\/\\/127.0.0.1:8000\\/photo\\/products_logo\\/Groupe 114.png\"}}', '10', 1, 1, 1, '2021-02-19 13:55:36', '2021-02-19 12:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `our_article`
--

CREATE TABLE `our_article` (
  `id_article` int(255) NOT NULL,
  `description` text NOT NULL,
  `id_product_p` int(255) NOT NULL,
  `id_product_s1` int(255) NOT NULL,
  `id_product_s2` int(255) NOT NULL,
  `id_product_s3` int(255) NOT NULL,
  `id_product_s4` int(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `our_article`
--

INSERT INTO `our_article` (`id_article`, `description`, `id_product_p`, `id_product_s1`, `id_product_s2`, `id_product_s3`, `id_product_s4`, `active`, `updated_at`, `created_at`) VALUES
(1, 'TESTGGGGGGG But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find faultccc', 6, 4, 35, 5, 6, 1, '2021-01-13', '2021-01-13');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `name_de` varchar(255) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `front_page` int(10) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `name_de`, `name_fr`, `front_page`, `active`, `updated_at`, `created_at`) VALUES
(2, 'HOME', 'ACCUEIL', 'ACCUEIL', 1, 1, '2021-01-20', '2021-01-20'),
(3, 'COLLECTION', 'SAMMLUNG', 'COLLECTION', 0, 1, '2021-01-20', '2021-01-20'),
(4, 'CATALOG', 'KATALOG', 'CATALOGUE', 0, 1, '2021-01-20', '2021-01-20'),
(5, 'BLOG', 'BLOG', 'BLOG', 0, 1, '2021-01-20', '2021-01-20'),
(6, 'CONTACT', 'KONTAKT', 'CONTACT', 0, 1, '2021-01-20', '2021-01-20');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(100) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_name_de` varchar(255) NOT NULL,
  `product_name_fr` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `short_description_de` text NOT NULL,
  `short_description_fr` text NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `product_currency` varchar(100) NOT NULL,
  `long_description` text NOT NULL,
  `long_description_de` text NOT NULL,
  `long_description_fr` text NOT NULL,
  `statut` tinyint(5) NOT NULL,
  `remise` varchar(255) NOT NULL,
  `photo` varchar(800) NOT NULL,
  `stock` varchar(255) NOT NULL,
  `offer` tinyint(4) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_name_de`, `product_name_fr`, `short_description`, `short_description_de`, `short_description_fr`, `product_price`, `active`, `product_currency`, `long_description`, `long_description_de`, `long_description_fr`, `statut`, `remise`, `photo`, `stock`, `offer`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Product 3', '1', '', 'At vero eos et accusamus et iusto odio dignissimos', '', '', '1200', 1, '€', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum', '', '', 1, '10', 'Rectangle 49.png', '1', 0, 1, '2021-02-11 14:51:08', '2021-02-11 14:51:08'),
(2, 'Product 4', '1', '', 'At vero eos et accusamus et iusto odio dignissimos', '', '', '900', 1, '€', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum', '', '', 1, '10', 'Rectangle 37.png', '0', 0, 1, '2021-02-11 14:51:13', '2021-02-11 14:51:13'),
(3, 'Product 5', '1', '', 'At vero eos et accusamus et iusto odio ffff ddd gggg', '', '', '250', 1, '€', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum', '', '', 1, '0', 'Groupe 115.png', '5', 0, 1, '2021-02-11 14:51:17', '2021-02-11 14:51:17'),
(4, 'Product 6', '1', '', 'At vero eos et accusamus et iusto odio ffff ddd gggg', '', '', '999', 1, '€', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum', '', '', 1, '0', 'Groupe 114.png', '3', 0, 1, '2021-02-11 14:51:20', '2021-02-11 14:51:20'),
(5, 'Salon4', '1', '1', '', '1', '1', '200', 1, '€', 'ffffffffff', '1', '1', 1, '0', 'Groupe 112.png', '2', 0, 1, '2021-02-11 14:51:25', '2021-02-11 14:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `products_photo`
--

CREATE TABLE `products_photo` (
  `id` int(255) NOT NULL,
  `products_id` int(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `active` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products_photo`
--

INSERT INTO `products_photo` (`id`, `products_id`, `photo`, `active`, `created_at`, `updated_at`) VALUES
(9, 2, 'images.jfif', 1, '2020-12-28 09:19:51', '2020-12-28 09:19:51'),
(10, 2, 'téléchargement (2).jfif', 1, '2020-12-28 09:19:58', '2020-12-28 09:19:58'),
(11, 2, 'téléchargement (1).jfif', 1, '2020-12-28 09:20:05', '2020-12-28 09:20:05'),
(15, 2, 'shutterstock_1465769324.jpg', 1, '2020-12-28 14:57:37', '2020-12-28 14:57:37'),
(16, 3, 'capture1.PNG', 1, '2021-02-02 14:14:08', '2021-02-02 14:14:08');

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `id_user` varchar(45) NOT NULL,
  `statut` tinyint(4) NOT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `list_product` varchar(500) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `date_comfirmation` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `regulations`
--

CREATE TABLE `regulations` (
  `id` int(11) NOT NULL,
  `mode` varchar(100) NOT NULL,
  `active` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `transaction` varchar(40) NOT NULL,
  `id_ordre` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regulations`
--

INSERT INTO `regulations` (`id`, `mode`, `active`, `status`, `transaction`, `id_ordre`, `updated_at`, `created_at`) VALUES
(13, 'Adomicile', '1', '0', 'In Progress', 'C-1613723334', '2021-02-19 09:52:06', '2021-02-19 09:52:06'),
(14, 'Transfer', '1', '0', 'In Progress', 'C-1613724334', '2021-02-19 09:52:10', '2021-02-19 09:52:10'),
(19, 'Paypal', '1', '1', '1IMWHRGIrZbbGYPC0p8b92eA', 'C-1613728479', '2021-02-19 10:37:19', '2021-02-19 10:37:19'),
(20, 'stripe', '1', '1', 'ch_1IMWHRGIrZbbGYPC0p8b92eA', 'C-1613730348', '2021-02-19 09:32:14', '2021-02-19 10:32:14'),
(22, 'Paypal', '1', '0', 'In Progress', 'C-1613731275', '2021-02-19 09:44:58', '2021-02-19 09:44:58'),
(23, 'Paypal', '1', '0', 'In Progress', 'C-1613742921', '2021-02-19 12:55:36', '2021-02-19 12:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `seo_url`
--

CREATE TABLE `seo_url` (
  `id` int(11) NOT NULL,
  `page` varchar(255) NOT NULL,
  `lang` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `active` tinyint(4) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seo_url`
--

INSERT INTO `seo_url` (`id`, `page`, `lang`, `url`, `seo_title`, `keyword`, `meta_description`, `active`, `date`) VALUES
(1, 'HOME', 'fr', 'http://dluxxis.de/fr', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-21'),
(2, 'COLLECTION', 'fr', 'http://dluxxis.de/fr/collection', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-21'),
(3, 'CATALOG', 'fr', 'http://dluxxis.de/fr/catalog', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-21'),
(4, 'BLOG', 'fr', 'http://dluxxis.de/fr/blog', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-21'),
(5, 'CONTACT', 'fr', 'http://dluxxis.de/fr/contact', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-21'),
(11, 'HOME', 'de', 'http://dluxxis.de/de', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22'),
(12, 'COLLECTION', 'de', 'http://dluxxis.de/de/sammlung', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22'),
(13, 'CATALOG', 'de', 'http://dluxxis.de/de/katalog', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22'),
(14, 'BLOG', 'de', 'http://dluxxis.de/de/blog', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22'),
(15, 'CONTACT', 'de', 'http://dluxxis.de/de/kontakt', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22'),
(16, 'HOME', 'en', 'http://dluxxis.de/en', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22'),
(17, 'COLLECTION', 'en', 'http://dluxxis.de/en/collection', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22'),
(18, 'CATALOG', 'en', 'http://dluxxis.de/en/catalog', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22'),
(19, 'BLOG', 'en', 'http://dluxxis.de/en/blog', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22'),
(20, 'CONTACT', 'en', 'http://dluxxis.de/en/contact', 'The standard Lorem Ipsum passage, used since the 1500s', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 1, '2021-01-22');

-- --------------------------------------------------------

--
-- Table structure for table `slider_image`
--

CREATE TABLE `slider_image` (
  `id_slider` int(255) NOT NULL,
  `id_product` int(255) NOT NULL,
  `coordinate_x` int(255) NOT NULL,
  `coordinate_y` int(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `slider_image`
--

INSERT INTO `slider_image` (`id_slider`, `id_product`, `coordinate_x`, `coordinate_y`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 535, 541, 1, '2021-01-19', '2021-01-19'),
(2, 6, 691, 378, 1, '2021-01-19', '2021-01-19'),
(4, 5, 882, 167, 1, '2021-01-19', '2021-01-19'),
(5, 5, 376, 647, 1, '2021-01-19', '2021-01-19'),
(6, 6, 1065, 666, 1, '2021-01-19', '2021-01-19'),
(7, 6, 1341, 727, 1, '2021-01-19', '2021-01-19'),
(80, 6, 1654, 585, 1, '2021-01-19', '2021-01-19'),
(81, 4, 1435, 364, 1, '2021-01-19', '2021-01-19'),
(82, 4, 1421, 65, 1, '2021-01-19', '2021-01-19'),
(83, 3, 1015, 532, 1, '2021-01-19', '2021-01-19'),
(84, 3, 1511, 104, 1, '2021-01-26', '2021-01-26'),
(85, 6, 306, 155, 1, '2021-02-01', '2021-02-01');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_newsletter`
--

CREATE TABLE `subscribe_newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscribe_newsletter`
--

INSERT INTO `subscribe_newsletter` (`id`, `email`, `active`, `updated_at`, `created_at`) VALUES
(1, 'email1@gmail.com', 1, '2021-01-16', '2021-01-16');

-- --------------------------------------------------------

--
-- Table structure for table `transport_costs`
--

CREATE TABLE `transport_costs` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `kilometre` varchar(255) NOT NULL,
  `quantity` varchar(225) NOT NULL,
  `price` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `updated_at` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transport_costs`
--

INSERT INTO `transport_costs` (`id`, `type`, `kilometre`, `quantity`, `price`, `currency`, `active`, `updated_at`, `created_at`) VALUES
(11, '1', '1', '0', '10', '€', 0, '2021-01-12', '2021-01-12'),
(12, '2', '0', '10', '15', '€', 0, '2021-01-12', '2021-01-12'),
(13, '3', '0', '0', '50', '€', 0, '2021-01-12', '2021-01-12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime NOT NULL DEFAULT current_timestamp(),
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `block_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `month_number` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `status`, `role`, `birthday`, `gender`, `firstname`, `lastname`, `image`, `country`, `city`, `tel`, `mobile`, `address`, `postcode`, `email`, `email_verified_at`, `password`, `remember_token`, `block_description`, `year`, `month_number`, `created_at`, `updated_at`) VALUES
(389, '1', '1', '2021-02-08', '1', 'hamed', 'imed', '1613655767.jpg', 'tunisie', 'tunis', '1213456', '12456789', 'tunis', 120345, 'my1@email.com', '2021-02-08 09:38:35', '$2y$10$GJ6YcbLsE0rVDBAhd6Aqm.nWce5kSpaaHrxsXlcMtmf8cm2prpxZC', NULL, 'Description', '2021', '022021', '2021-02-18 13:42:47', '2021-02-18 13:42:47'),
(1, '1', '2', '2021-02-04', '1', 'Hamed', 'Imed', '1613642235.jpg', 'Tunisie', 'Tunisie', '1213456', '12456789', 'Tunis', 120345, 'your@email.com', '2021-02-02 16:13:07', '$2y$10$aWMTZXn4cSdCjUJapTl7Tu9h1NNtUMMyLcj3YEEXzIS80nuafBP9K', NULL, 'Description', '2021', '022021', '2021-02-18 09:57:15', '2021-02-18 09:57:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_category_list`
--
ALTER TABLE `blog_category_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catalogue`
--
ALTER TABLE `catalogue`
  ADD PRIMARY KEY (`id_catalogue`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_products`
--
ALTER TABLE `category_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crm_about`
--
ALTER TABLE `crm_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filter_test`
--
ALTER TABLE `filter_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_favicon`
--
ALTER TABLE `image_favicon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `method-delivery`
--
ALTER TABLE `method-delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_notice_product1_idx` (`product_id`),
  ADD KEY `fk_notice_user1_idx` (`user_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordres`
--
ALTER TABLE `ordres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_article`
--
ALTER TABLE `our_article`
  ADD PRIMARY KEY (`id_article`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_photo`
--
ALTER TABLE `products_photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regulations`
--
ALTER TABLE `regulations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_url`
--
ALTER TABLE `seo_url`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_image`
--
ALTER TABLE `slider_image`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `subscribe_newsletter`
--
ALTER TABLE `subscribe_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transport_costs`
--
ALTER TABLE `transport_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `blog_category_list`
--
ALTER TABLE `blog_category_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `catalogue`
--
ALTER TABLE `catalogue`
  MODIFY `id_catalogue` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `category_products`
--
ALTER TABLE `category_products`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `crm_about`
--
ALTER TABLE `crm_about`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `filter_test`
--
ALTER TABLE `filter_test`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `image_favicon`
--
ALTER TABLE `image_favicon`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `method-delivery`
--
ALTER TABLE `method-delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ordres`
--
ALTER TABLE `ordres`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `our_article`
--
ALTER TABLE `our_article`
  MODIFY `id_article` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `products_photo`
--
ALTER TABLE `products_photo`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `regulations`
--
ALTER TABLE `regulations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `seo_url`
--
ALTER TABLE `seo_url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `slider_image`
--
ALTER TABLE `slider_image`
  MODIFY `id_slider` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `subscribe_newsletter`
--
ALTER TABLE `subscribe_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transport_costs`
--
ALTER TABLE `transport_costs`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=390;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
